---
title: {{ .Name }}
date: {{ .Date }}
draft: true
merk:
model:
leverancier:
attachments:
- title: 
  src: 
- title: 
  url: 
---

<!-- Voeg een overzichtsafbeelding toe -->
<!-- Voeg een algemene omschrijving van het component toe -->

## Eigenschappen

* Vermogen: 
* Afmetingen:

  * Hoogte: 
  * Breedte: 
  * Diepte: 

* Gewicht: 

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
