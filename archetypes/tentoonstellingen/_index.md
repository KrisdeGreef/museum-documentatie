---
title: {{ .Name }}
date: {{ .Date }}
draft: true
status:
attachments:
- title: 
  src: 
- title: 
  url: 
---

<!-- Voeg een overzichtsafbeelding toe -->

## Functionele omschrijving

<!-- Voeg hier een functionele omschrijving van de tentoonstelling toe -->

## Technische omschrijving

<!-- Voeg hier een blokschema van de hele tentoonstelling toe -->

<!-- Voeg hier een technische omschrijving toe -->

## Onderdelen

Een functionele en technische beschrijving van de specifieke onderdelen vind je
op de onderstaande pagina's.

### Show

Meer informatie over de show in deze tentoonstelling vind je hier:

{{< pagelist shows >}}

### Interactives

In de tentoonstelling staan de volgende interactives:

{{< pagelist interactives >}}

### Exhibits

In de tentoonstelling staan de volgende exhibits:

{{< pagelist exhibits >}}

### Decors

De volgende decors maken onderdeel uit van de tentoonstelling:

{{< pagelist decors >}}

## Handleidingen en procedures

<!-- Voeg hier verwijzingen naar gebruikers- en beheerdershandleidingen en
procedures toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues met betrekking tot de gehele
tentoonstelling bekend.

## Afspraken en verantwoordelijkheden

<!-- Voeg hier een overzicht van verantwoordelijken en afspraken omtrent de
exhibit toe -->

### Contact

<!-- Interne contactpersonen -->

### SLA

<!-- SLAs (met link naar Topdesk) -->

### Bouwer

<!-- Naam bouwer interactives (met link naar Topdesk)-->

### Ontwerper

<!-- Naam ontwerper interactives (met link naar Topdesk)-->

## Bijlagen

{{< pagelist bijlagen >}}
