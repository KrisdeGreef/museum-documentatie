#!/bin/bash
function updatedocs {
    git checkout --quiet $1
    git submodule update --init --recursive --quiet
    if [[ $1 == 'master' ]]; then
	version=latest
    else
        version=$1
    fi
    echo 'Updating docs.museum.naturalis.nl/'$version
    sed -e "s/^baseURL:.*/baseURL:\ \/$version\//g" config.yaml > config-$version.yaml
    hugo --quiet -D --enableGitInfo --config config-$version.yaml -s ./
    mkdir -p /var/www/docs.museum.naturalis.nl/$version
    rsync -ah ./public/ /var/www/docs.museum.naturalis.nl/$version
}

echo 'Start updating docs.museum.naturalis.nl'
buildjob=hugo-build-$(cat /dev/urandom | tr -dc 'a-z0-9' | fold -w 6 | head -n 1)
# Clone the museum documentation
git clone --recursive --quiet https://gitlab.com/naturalis/mii/museum-documentatie /tmp/$buildjob
cd /tmp/$buildjob
git checkout --quiet master
# Track all branches
for branch in `git branch -a | grep remotes | grep -v HEAD | grep -v master `;
do
    git branch --quiet --track ${branch#remotes/origin/} $branch
done
# Loop over  branches
for c in `git branch --list | sed -e 's/* //g' -e 's/  //g'`;
do
    updatedocs $c
done
# Loop over tags
for c in `git tag`;
do
    updatedocs $c
done
echo rm -rf /tmp/$buildjob
echo 'Documentation has been built!'
