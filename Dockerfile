#--- BUILDER -----------------------------------------------------
FROM ubuntu:18.04 as builder
ENV HUGO_VERSION='0.94.2'
COPY build-for-docker.sh /var/tmp/
RUN apt-get update && \
    apt-get upgrade --yes && \
    apt-get install --yes --no-install-recommends \
        git \
        wget \
        rsync \
        ca-certificates && \
    wget --quiet https://github.com/gohugoio/hugo/releases/download/v${HUGO_VERSION}/hugo_${HUGO_VERSION}_Linux-64bit.deb && \
    dpkg -i hugo_${HUGO_VERSION}_Linux-64bit.deb && \
    bash /var/tmp/build-for-docker.sh

#--- DOCUMENTATION SERVER -------------------------
FROM nginx:latest  
WORKDIR /root/
COPY --from=builder /var/www/docs.museum.naturalis.nl/ /usr/share/nginx/html/
