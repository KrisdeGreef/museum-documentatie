---
title: "Smart Event Manager (SEM)"
date: 2020-03-02T13:52:19+01:00
draft: true
status:
attachments:
- title: Beheer informatie SEM
  src: https://
---

## Functionele omschrijving
Reserveringssysteem voor afdeling Sales t.b.v.  evenementen, waaruit alle betrokken afdelingen (support, facilitair, PB/EB, AV ondersteuning) informatie uit kunnen halen en vanuit waar financiële gegevens naar AFAS overgedragen worden voor facturatie.

## Dienstverlening
Kantoortijden (MA t/m VR): Eindgebruikers kunnen zelf de storingen melden bij Support via 071-7519333 of de [Self-Service Portal](https://naturalis.topdesk.net/tas/public/ssp/)
Weekend (ZA & ZO): Eindgebruikers kunnen kunnen zelf de storing melden bij Support via de [Self-Service Portal](https://naturalis.topdesk.net/tas/public/ssp/). Het betreft hier een kantoor applicatie, er is in het weekend geen dienstverlening. De melding zal maandag ochtend worden opgepakt.

## Bijlagen

{{< pagelist bijlagen >}}
