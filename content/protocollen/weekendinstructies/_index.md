---
title: "Weekend Instructies"
date: 2020-03-02T13:51:08+01:00
draft: true
status:
attachments:
---

## Draaiboek weekenddienst Technisch Team
Zo doen wij dat

## Opfrissen
Als het al tijdje geleden is dat je een TT dienst hebt gedraaid is het belangrijk om jezelf weer bekend te maken met de huidige staat van de tentoonstellingen. Neem daarom contact op met één van de teamleden en loop samen een rondje door het museum. Bij voorkeur tijdens een ochtendronde.

## Ronde
Begin je dag met een tentoonstellingsronde, kijk hierbij of alle interactives en shows goed draaien. Mocht dit niet het geval zijn probeer dit dan waar mogelijk direct te verhelpen. Meeste zaken zijn met AWX snel verholpen.

## Porto
Na je ronde pak je een porto met display bij de centrale meldkamer. Zorg dat je op het BHV kanaal zit.
De dagcoördinatoren weet dat je op dit kanaal zit.

Het is belangrijk om tijdens je dienst goed bereikbaar te zijn, in het weekend zijn de dagcoördinatoren in hun eentje aanwezig en operationeel verantwoordelijk.

## Briefing (9:30)
Het is belangrijk om ‘s ochtend de briefing van PB & EB in LiveScience bij te wonen.

Tijdens de meeting vraagt de dagcoördinator of er nog bijzonderheden zijn, mochten er zaken in storing staan die je ‘s ochtends nog niet hebt kunnen oplossen, geef dit dan aan.

## Logboek

We houden een logboek bij, hiermee kan het TT lezen wat er dat weekend gebeurd is. Schrijf dus in het kort op wat je werkzaamheden waren, zodat je collega die maandag start weet wat de bijzonderheden van het weekend waren.

Het TT logboek vind je [hier](https://docs.google.com/document/d/1ItgFgDkc8gyK5-YqRzF5o50rYL0zH7jF45c0vfIYQGU/edit).

## Vertrek
Als je weg gaat, lunchen of naar huis, meld je je altijd even af bij de dagcoördinator, zodat ze weten dat je niet bereikbaar bent op de porto. Dit kan door even langs te lopen of via de porto. 

Nu de Rexperience geopend is, is de mogelijkheid om eerder naar huis te gaan vervallen.

N.B. Voor goede dienstverlening in het weekend is goed bereikbaar zijn en overleg met de Dagco van groot belang. Zoals gezegd heeft de Dagco een grote verantwoordelijkheid die nog wel een gepaard kan gaan met de nodige werkdruk en stress. Het is daarom voor hun fijn om te weten dat ze altijd kunnen rekenen op het TT in geval van storingen. Om ze te verhelpen of aan te geven dat het tot maandag zal moeten wachten. Als dienstdoende TTer heb je de verantwoordelijkheid en vrijheid om die afweging te maken.   

## Handige links

- [Logboek](https://docs.google.com/document/d/1ItgFgDkc8gyK5-YqRzF5o50rYL0zH7jF45c0vfIYQGU/edit)
- [Piket](https://sites.google.com/a/naturalis.nl/social-intranet/home/museum/piket/piket-rooster)
- [Technisch Museum beheer (wat verouderd)](https://docs.museum.naturalis.nl/latest/handleidingen/museum-beheer/)
- [AWX/Ansible](https://awx.museum.naturalis.nl/#/login)
- Helpdesk Gantner (kassa's, handscanners, ticketzuilen): +31(0)10-8106020
- [Meer informatie Gantner](https://docs.museum.naturalis.nl/latest/faciliteiten/servicebaliekassa1/)
