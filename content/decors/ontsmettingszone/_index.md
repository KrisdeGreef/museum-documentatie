---
title: "ontsmettingszone"
date: 2019-07-24T13:49:45Z
draft: false
status:
componenten:
 - iiyama-tf3238mscb1ag
 - microsonic-mic130dtc
 - intel-nuc8i3beh
tentoonstellingen:
experiences:
- rexperience
unid: fcd501f3-7600-4c1c-96cf-efd7c3e0f6cf
attachments:
- title: rexperience_e_installatie_netwerk_dmx_audio_ontsmettingszone_aansluitschema (pdf)
  url: https://files.museum.naturalis.nl/s/qnN8QJZa4ScJri3
- title: rexperience_ontsmettingszone_kozijn3_bouwtekening (pdf)
  url: https://files.museum.naturalis.nl/s/bBiHzyysNskNTXb
- title: rexperience_ontsmettingszone_monitorbevestiging_bouwtekening (pdf)
  url: https://files.museum.naturalis.nl/s/CCn6g5CwJxGcTAw
- title: rexperience_ontsmettingszone_overzicht_bouwtekening (pdf)
  url: https://files.museum.naturalis.nl/s/Hp5CrogKbNJR3AY
- title: rexperience_ontsmettingszone_plaatwerkscanners_bouwtekening (pdf)
  url: https://files.museum.naturalis.nl/s/pqyNe5q82QWBPpq
- title: rexperience_ontsmettingszone_plafondplaten_bouwtekening (pdf)
  url: https://files.museum.naturalis.nl/s/qCmNqCwyBag8TW2
- title: rexperience_ontsmettingszone_vloer_bouwtekening (pdf)
  url: https://files.museum.naturalis.nl/s/g6QAAnJ8PNTmFct
- title: rexperience__deurkozijn_ser_bouwtekening (pdf)
  url: https://files.museum.naturalis.nl/s/rbCLWfW97dZoeiQ
---

![ontsmettingszone](./ontsmettingszone.png)

## Functionele omschrijving

<!-- Voeg hier een functionele omschrijving van de decor toe -->

## Technische omschrijving

<!-- Voeg hier een blokschema toe -->

<!-- Voeg hier een technische omschrijving toe -->

Dit decor bestaat uit de volgende componenten:

{{< pagelist componenten >}}

## Handleidingen en procedures

<!-- Voeg hier verwijzingen naar gebruikers- en beheerdershandleidingen en
procedures toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Afspraken en verantwoordelijkheden

<!-- Voeg hier een overzicht van verantwoordelijken en afspraken omtrent de
decor toe -->

### Contact

<!-- Interne contactpersonen -->

### SLA

<!-- SLAs (met link naar Topdesk) -->

### Bouwer

<!-- Naam bouwer interactives (met link naar Topdesk)-->

### Ontwerper

<!-- Naam ontwerper interactives (met link naar Topdesk)-->

## Bijlagen

{{< pagelist bijlagen >}}
