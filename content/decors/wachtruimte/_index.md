---
title: "wachtruimte"
date: 2019-07-24T13:49:45Z
draft: false
status:
componenten:
- iiyama-tf3238mscb1ag
tentoonstellingen:
experiences:
- rexperience
attachments:
- title: rexperience_wachtruimte_afmetingenhaakwanden_bouwtekening (pdf)
  url: https://files.museum.naturalis.nl/s/Bt9XPCgfabdJ3JE
- title: rexperience_wachtruimte_bouwtekening (pdf)
  url: https://files.museum.naturalis.nl/s/kDJ2xZfeGD7gSpY
- title: rexperience_wachtruimte_controlekast_bouwtekening (pdf)
  url: https://files.museum.naturalis.nl/s/RXG9YefAZBSkxiE
- title: rexperience_wachtruimte_deurpostcapsule_bouwtekening (pdf)
  url: https://files.museum.naturalis.nl/s/LdCRGqszLwzRSDR
- title: rexperience_wachtruimte_harmonica_bouwtekening (pdf)
  url: https://files.museum.naturalis.nl/s/NAS8zFWHMJEdqCR
- title: rexperience_wachtruimte_plafond_bouwtekening (pdf)
  url: https://files.museum.naturalis.nl/s/ks9gWQksAKTtZQb
- title: rexperience_wachtruimte_plafondplaten_bouwtekening (pdf)
  url: https://files.museum.naturalis.nl/s/MafX67fdBcRdra5
- title: rexperience_wachtruimte_verlichtingenspeakerbox_bouwtekening (pdf)
  url: https://files.museum.naturalis.nl/s/EbenKg5LznPnAmj
---

![wachtruimte](./wachtruimte.png)

## Functionele omschrijving

<!-- Voeg hier een functionele omschrijving van de decor toe -->

## Technische omschrijving

<!-- Voeg hier een blokschema toe -->

<!-- Voeg hier een technische omschrijving toe -->

Dit decor bestaat uit de volgende componenten:

{{< pagelist componenten >}}

## Handleidingen en procedures

<!-- Voeg hier verwijzingen naar gebruikers- en beheerdershandleidingen en
procedures toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Afspraken en verantwoordelijkheden

<!-- Voeg hier een overzicht van verantwoordelijken en afspraken omtrent de
decor toe -->

### Contact

<!-- Interne contactpersonen -->

### SLA

<!-- SLAs (met link naar Topdesk) -->

### Bouwer

<!-- Naam bouwer interactives (met link naar Topdesk)-->

### Ontwerper

<!-- Naam ontwerper interactives (met link naar Topdesk)-->

## Bijlagen

{{< pagelist bijlagen >}}
