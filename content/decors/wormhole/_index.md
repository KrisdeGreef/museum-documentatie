---
title: "wormhole"
date: 2019-07-24T16:03:38Z
draft: false
status:
unid: ca26de4e-7421-41cf-a62d-6250ed08a690
componenten:
- soraa-sle30
- eldoled-lineaedrive180d
- proled-diversen
- soliddrive-sd1g
tentoonstellingen:
experiences:
- rexperience
attachments:
- title: rexperience_wormhole_bouwtekening (pdf)
  src:  https://files.museum.naturalis.nl/s/qW2fPgf9mNtA9oq
- title: rexperience_wormhole_glasenspiegels_bouwtekening  (pdf)
  url: https://files.museum.naturalis.nl/s/DRXNEbyqzAaJAZ3
- title: rexperience_wormhole_maatvoeringbovendetail_bouwtekening (pdf)
  src: https://files.museum.naturalis.nl/s/d8dxYcZ5dAfGtCm
- title: rexperience_wormhole_onderprofiel_bouwtekening (pdf)
  url: https://files.museum.naturalis.nl/s/JioXcSGoCwnXjDr
- title: rexperience_wormhole_overzichtonderregels_bouwtekening (pdf)
  src: https://files.museum.naturalis.nl/s/FYpa4r6JGFtd4Ly
- title: rexperience_wormhole_rvsonderdelen_bouwtekening (pdf)
  url: https://files.museum.naturalis.nl/s/ss7bYHGgGnABm6C
- title: rexperience_wormhole_vloeren_bouwtekening (pdf)
  src: https://files.museum.naturalis.nl/s/BTn4JpRnAJ3n4eM
- title: rexperience_e_installatie_netwerk_dmx_audio_pixelledwormhole (pdf)
  src: https://files.museum.naturalis.nl/s/mEQ4BResxRXgLzY
---

![wormhole](./wormhole.png)

## Functionele omschrijving

<!-- Voeg hier een functionele omschrijving van de interactive toe -->

<!-- Voeg indien beschikbaar een tijd-volgorde-toestands diagram toe -->

## Technische omschrijving

<!-- Voeg hier een blokschema toe -->

<!-- Voeg hier een technische omschrijving toe -->

Deze interactive bestaat uit de volgende componenten:

{{< pagelist componenten >}}

## Configuratie

<!-- Voeg hier een overzicht van handmatig ingestelde parameters toe -->

<!-- Voeg hier een verwijzing naar de configuratie in Ansible toe -->

## Handleidingen en procedures

<!-- Voeg hier verwijzingen naar gebruikers- en beheerdershandleidingen en
procedures toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Afspraken en verantwoordelijkheden

<!-- Voeg hier een overzicht van verantwoordelijken en afspraken omtrent de
interactive toe -->

### Contact

<!-- Interne contactpersonen -->

### SLA

<!-- SLAs (met link naar Topdesk) -->

### Bouwer

<!-- Naam bouwer interactives (met link naar Topdesk)-->

### Ontwerper

<!-- Naam ontwerper interactives (met link naar Topdesk)-->

## Bijlagen

{{< pagelist bijlagen >}}
