---
title: "panorama"
date: 2019-07-24T13:49:46Z
draft: true
status:
componenten:
tentoonstellingen:
- deaarde
attachments:

- title: afbeelding definitiefontwerp (png)
  url: https://files.museum.naturalis.nl/s/2fCssFEoXTeHzDM

- title: bouwkundig defintiefontwerp (png)
  url: https://files.museum.naturalis.nl/s/zZbNdJKGgtBeBBL

- title: details definitiefontwerp (png)
  url: https://files.museum.naturalis.nl/s/qpkYPK78XMFRCmn

- title: deuren werktekening (pdf)
  url: https://files.museum.naturalis.nl/s/GTFzrnDz4YS3iPp

- title: ID sheets
  src: https://files.museum.naturalis.nl/s/cLPf2jCEzm7KBPG
---

![panorama](https://files.museum.naturalis.nl/s/8e8zeyxPFSe9GL7/preview)

## Functionele omschrijving

<!-- Voeg hier een functionele omschrijving van de decor toe -->

## Technische omschrijving

<!-- Voeg hier een blokschema toe -->

<!-- Voeg hier een technische omschrijving toe -->

Dit decor bestaat uit de volgende componenten:

{{< pagelist componenten >}}

## Handleidingen en procedures

<!-- Voeg hier verwijzingen naar gebruikers- en beheerdershandleidingen en
procedures toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Afspraken en verantwoordelijkheden

<!-- Voeg hier een overzicht van verantwoordelijken en afspraken omtrent de
decor toe -->

### Contact

<!-- Interne contactpersonen -->

### SLA

<!-- SLAs (met link naar Topdesk) -->

### Bouwer

<!-- Naam bouwer interactives (met link naar Topdesk)-->

### Ontwerper

<!-- Naam ontwerper interactives (met link naar Topdesk)-->

## Bijlagen

{{< pagelist bijlagen >}}
