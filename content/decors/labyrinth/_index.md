---
title: "labyrinth"
draft: false
status:
componenten:
tentoonstellingen:
- dedood
attachments:
- title: labyrinth (directory)
  src: https://files.museum.naturalis.nl/s/n6JFfnmkSpGMWBG
---

![labyrinth-voorbeeld](https://files.museum.naturalis.nl/s/GdeMAPwtLNcpPxF/preview)

## Functionele omschrijving

Het labyrinth van de Dood.

## Technische omschrijving

<!-- Voeg hier een blokschema toe -->

<!-- Voeg hier een technische omschrijving toe -->

Dit decor bestaat uit de volgende componenten:

{{< pagelist componenten >}}

## Handleidingen en procedures

<!-- Voeg hier verwijzingen naar gebruikers- en beheerdershandleidingen en
procedures toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Afspraken en verantwoordelijkheden

<!-- Voeg hier een overzicht van verantwoordelijken en afspraken omtrent de
decor toe -->

### Contact

<!-- Interne contactpersonen -->

### SLA

<!-- SLAs (met link naar Topdesk) -->

### Bouwer

 * Decor: [Kloosterboer](http://www.kloosterboer-decor.nl/)

### Ontwerper

* Decor: [Kloosterboer](http://www.kloosterboer-decor.nl/)
* Inhoud:

## Bijlagen

{{< pagelist bijlagen >}}
