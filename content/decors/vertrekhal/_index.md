---
title: "vertrekhal"
date: 2019-07-24T13:49:46Z
draft: true
status:
componenten:
- philips-bdl4330ql00
- hikvision-ds2cd2125fwdis
- martinaudio-cdd5
- meyersound-mm4xp
- apartaudio-ovo5
- jbl-control24c
- soraa-sle30
- iiyama-tf2738mscb1
- kramer-tp580r
- kramer-vm3h2
- ecler-mpage4
- pharos-tpsbb
tentoonstellingen:
experiences:
- rexperience
attachments:
- title: rexperience_vertrekhal_afmetingenkozijn2_bouwtekening (pdf)
  src: https://files.museum.naturalis.nl/s/7FXqnHNkELY6sc9
- title: rexperience_vertrekhal_balie_en_gate_bouwtekening (pdf)
  url: https://files.museum.naturalis.nl/s/arZMwWGHycnQAPH
- title: rexperience_vertrekhal_balielinks_bouwtekening (pdf)
  src: https://files.museum.naturalis.nl/s/RAAKcf4CriSbAME
- title: rexperience_vertrekhal_baliemetgate_bouwtekening (pdf)
  url: https://files.museum.naturalis.nl/s/HbMHLKMy6WXNRLW
- title: rexperience_vertrekhal_balieonderdelen_overzicht_bouwtekening (pdf)
  src: https://files.museum.naturalis.nl/s/RKw8Q9GwdC89jHJ
- title: rexperience_vertrekhal_brillenbak_bouwtekening (pdf)
  url: https://files.museum.naturalis.nl/s/TG5ffZic6kF55pD
- title: rexperience_vertrekhal_hekjebalie_bouwtekening (pdf)
  src: https://files.museum.naturalis.nl/s/LRADLSxZErMa7Ny
- title: rexperience_vertrekhal_hekwerkhellingbanen_bouwtekening (pdf)
  url: https://files.museum.naturalis.nl/s/jQ5jKEDLMb87ryn
- title: rexperience_vertrekhal_plafond_bouwtekening (pdf)
  src: https://files.museum.naturalis.nl/s/8X3WcDYKy4TxPEF
- title: rexperience_vertrekhal_plafondoverzicht_bouwtekening (pdf)
  url: https://files.museum.naturalis.nl/s/Yz9NBWzjQz9nQwE
- title: rexperience_vertrekhal_ramp_bouwtekening (pdf)
  src: https://files.museum.naturalis.nl/s/8J3459rWfkL34z7
- title: rexperience_e_installatie_netwerk_dmx_audio_vertrekhal_aansluitschema (pdf)
  url: https://files.museum.naturalis.nl/s/P7A2M3g3e8imTNW
---

![vertrekhal](./vertrekhal.png)

## Functionele omschrijving

<!-- Voeg hier een functionele omschrijving van de decor toe -->

## Technische omschrijving

<!-- Voeg hier een blokschema toe -->

<!-- Voeg hier een technische omschrijving toe -->

Dit decor bestaat uit de volgende componenten:

{{< pagelist componenten >}}

## Handleidingen en procedures

<!-- Voeg hier verwijzingen naar gebruikers- en beheerdershandleidingen en
procedures toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Afspraken en verantwoordelijkheden

<!-- Voeg hier een overzicht van verantwoordelijken en afspraken omtrent de
decor toe -->

### Contact

<!-- Interne contactpersonen -->

### SLA

<!-- SLAs (met link naar Topdesk) -->

### Bouwer

<!-- Naam bouwer interactives (met link naar Topdesk)-->

### Ontwerper

<!-- Naam ontwerper interactives (met link naar Topdesk)-->

## Bijlagen

{{< pagelist bijlagen >}}
