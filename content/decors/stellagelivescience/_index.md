---
title: "stellagelivescience"
draft: false
status:
componenten:
tentoonstellingen:
- livescience
attachments:
- title: Stellingkasten (pdf)
  url: https://files.museum.naturalis.nl/s/ioEZCQSg4pYbckd
- title: Memo vloerlast computervloer (pdf)
  url: https://files.museum.naturalis.nl/s/95or5Ct3DiypadC
- title: Berekening belasting stellingkast (excel)
  url: https://files.museum.naturalis.nl/s/Y2ASMk94kWEfDL8
- title: Afbeelding draadmandrek 1
  url: https://files.museum.naturalis.nl/s/bRM9SmM3KNyrtQj
- title: Afbeelding draadmandrek 2
  url: https://files.museum.naturalis.nl/s/LE9n6Wen3i4bKoo
- title: livescience_decors_stellagelivescience_natuurbalie_bouwtekening (pdf)
  url: https://files.museum.naturalis.nl/s/bRmqB4zNRyxYKBc
---

![Entree](entree.png)

## Functionele omschrijving

Hoge stellingkast in de zaal Live Science die de ruimte scheidt en
wordt gebruikt om via diverse vitrines belangrijke onderdelen
uit de collectie te tonen.

## Known issues

Tijdens de bouw is gekeken naar de belasting van de vloer onder de stellingen.
Hierover is een
[memo geschreven door PT Structural Design & analysis BV](https://files.museum.naturalis.nl/s/95or5Ct3DiypadC).


### Contact

* [Pieter Aartsen](mailto:pieter.aartsen@naturalis.nl)

### SLA

### Bouwer

* Technisch: [PT Structural Design & Analysis bv](https://www.pt-structural.com)
* Decor: [Bruns](https://www.bruns.nl)

### Ontwerper

* Inhoud: [Pieter Aartsen](mailto:pieter.aartsen@naturalis.nl)

## Bijlagen

{{< pagelist bijlagen >}}
