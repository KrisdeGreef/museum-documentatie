---
title: "decorjapanstheater"
date: 2019-07-24T13:49:45Z
draft: true
status:
componenten:
tentoonstellingen:
experiences:
- japanstheater
attachments:
- title: decorjapanstheater (directory)
  src: https://files.museum.naturalis.nl/s/oypBM5p26pG4Q65
---

![decorjapanstheater](https://files.museum.naturalis.nl/s/XTWL6inYNWEAHLR/preview)

## Functionele omschrijving

Decor van het Japans theater.

## Technische omschrijving

<!-- Voeg hier een blokschema toe -->

<!-- Voeg hier een technische omschrijving toe -->

Dit decor bestaat uit de volgende componenten:

{{< pagelist componenten >}}

## Handleidingen en procedures

<!-- Voeg hier verwijzingen naar gebruikers- en beheerdershandleidingen en
procedures toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Afspraken en verantwoordelijkheden

<!-- Voeg hier een overzicht van verantwoordelijken en afspraken omtrent de
decor toe -->

### Contact

<!-- Interne contactpersonen -->

### SLA

<!-- SLAs (met link naar Topdesk) -->

### Bouwer

 * Decor: [Bruns](https://bruns.nl)

### Ontwerper

* Decor: [Bruns](https://bruns.nl)
* Inhoud:

## Bijlagen

{{< pagelist bijlagen >}}
