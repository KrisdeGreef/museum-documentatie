---
title: "boomlivescience"
date: 2019-07-24T13:49:44Z
draft: false
status:
componenten:
tentoonstellingen:
- livescience
attachments:
- title: Ontwerp (png)
  url: https://files.museum.naturalis.nl/s/p5Ptqoe2mgqgZGZ
---

![ontwerp](https://files.museum.naturalis.nl/s/p5Ptqoe2mgqgZGZ/preview)

## Functionele omschrijving

Boom en bankje als Entree voor Live science.

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Afspraken en verantwoordelijkheden

<!-- Voeg hier een overzicht van verantwoordelijken en afspraken omtrent de
decor toe -->

### Contact

 * [Pieter Aartsen](mailto:pieter.aartsen@naturalis.nl)

### Bouwer

 * Decor: [Bruns](https://bruns.nl)

### Ontwerper

 * Decor: [Bruns](https://bruns.nl)
 * Inhoud: [Pieter Aartsen](mailto:pieter.aartsen@naturalis.nl)

## Bijlagen

{{< pagelist bijlagen >}}
