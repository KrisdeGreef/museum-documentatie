---
title: "mpv"
date: 2018-10-09T11:31:30+02:00
draft: false
tentoonstellingen:
 - deaarde
 - leven
 - deverleiding
 - zaal10
 - dedood
 - dinotijd
 - devroegemens
---

Voor standaard mediaplayer functionaliteit maakt Naturalis gebruik van
[mpv](https://mpv.io/). mpv is een open source mediaplayer die voortkomt uit het
Mplayer project. Voor het verwerken van video en audio bestanden maakt mpv
gebruik van [ffmpeg](https://ffmpeg.org/).

mpv is bedoeld voor alle usecases waarbij video en/of audio content moet worden
afgespeeld en waarbij het framesync afspelen van video's vanaf meerdere players
niet is vereist.

## Documentatie

mpv heeft een uitgebreide set aan configuratieopties en mogelijkheden. Deze zijn
door middel van command line parameters, configuratiebestanden, Lua scripting en
een JSON IPC mechanisme in te stellen. Alle configuratiemogelijkheden staan
[uitgebreid gedocumenteerd op de site van mpv](https://mpv.io/manual/stable/).

## Aansturing

Voor de aansturing van mpv vanuit externe programma's biedt mpv
[JSON IPC-mechanisme](https://mpv.io/manual/stable/#json-ipc). Door mpv
met de optie `--input-ipc-server` te starten kan mpv via een Unix-socket worden
aangestuurd. De communicatie gebeurt op basis van JSON-berichten.

Om mpv ook remote over het netwerk aan te kunnen sturen - bijvoorbeeld vanuit
een show controller - draaien op de mediaplayers drie services waarmee de
functionaliteit van dit IPC-mechanisme wordt ontsloten:

* UDP unicast
* UDP multicast
* HTTP

De werking van deze services staat hieronder toegelicht.

Naast het aansturen van mpv over het netwerk is het in bepaalde usecases ook
nodig om lokaal, op basis van user input, de player aan te sturen. Een simpel
voorbeeld is dat een video start met afspelen wanneer een bezoeker een knop
indrukt. De wijze waarop dit soort toepassingen kunnen worden uitgevoerd met mpv
staat hieronder eveneens toegelicht.

### UDP unicast

Op basis van deze optie kan een individuele player via UDP aangestuurd worden.
Hiertoe dient een UDP bericht te worden verstuurd naar het IP-adres van de
betreffende player (bijvoorbeeld `10.129.1.123`).

Als *payload* van het UDP bericht kunnen de [reguliere
commando's](https://mpv.io/manual/stable/#protocol) worden verstuurd. Met
onderstaand commando wordt de player bijvoorbeeld gepauzeerd:

```json
{ "command": ["set_property", "pause", true] }
```

Behalve vanaf andere Linux systemen is het versturen van deze commando's ook
succesvol getest vanaf een Pharos licht controller.

### UDP multicast

Naast het aansturen van individuele players is biedt de UDP multicast de optie
om een groep players door middel van het versturen van één UDP bericht aan te
sturen.

Om hiervan gebruik te maken dient er een
UDP bericht te worden verstuurd naar het [multicast
adres](https://en.wikipedia.org/wiki/IP_multicast)
van de betreffende groep players (een adres in de range `224.0.1.0` tot en met
`239.255.255.255`, bijvoorbeeld `224.0.1.1`).

Het maakt daarbij niet uit of de commando's vanaf de showcontroller worden
verstuurd of vanaf een master player.

Net als bij unicast heb je in het geval van multicast de beschikking over de
volledige set aan mpv commando's.

### HTTP interface

Een derde optie voor het aansturen en uitvragen van gegevens over mpv is een
HTTP interface. Een belangrijk verschil met de bovengenoemde UDP opties is
dat de set aan toegestane commando's op basis van whitelisting is beperkt.

### Input devices

Om mpv lokaal aan te sturen vanuit input devices is gekozen om gebruik te maken
van keypresses. Al dan niet in combinatie met [Lua
scripting](https://mpv.io/manual/stable/#lua-scripting) zijn alle mpv commando's
te triggeren op basis van het indrukken, ingedrukt houden en/of loslaten van
toetsenbord toetsen.

Het voordeel van het gebruik van keypresses is dat het eenvoudig is om de
configuratie met een simpel toetsenbord te testen. Daarnaast blijven de eisen
aan de input devices relatief beperkt.

Voor input devices is gekozen om te werken met Arduino microcontrollers op basis
van een ATmega32U4 chip. Zoals [hier
uitgelegd](https://www.arduino.cc/en/Tutorial/KeyboardMessage) kunnen deze
eenvoudig worden geconfigureerd om keyboard messages uit te sturen.

## Monitoring

Voor het monitoren van mpv wordt - net als in de rest van het museum - gebruik
gemaakt van [Prometheus](https://prometheus.io/). Om de metrics van mpv te
publiceren zodat deze opgevraagd kunnen worden door de Prometheus server is een
zogeheten [exporter geschreven](https://github.com/MakeExpose/mpv_exporter).

Deze exporter communiceert eveneens over een Unix-socket met het JSON
IPC-mechanisme van mpv. Op basis daarvan is het mogelijk om elke gewenste en
door mpv ondersteunde statistiek bij te houden.

De gegevens die door middel van Prometheus worden verzameld worden, over de tijd
uitgezet, visueel inzichtelijk gemaakt door middel van
[Grafana](https://grafana.com/). Tevens kunnen de statistieken worden gebruikt
voor het triggeren van alerts.
