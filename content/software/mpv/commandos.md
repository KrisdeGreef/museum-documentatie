---
title: "Commandos"
date: 2018-11-21T11:43:11+01:00
draft: true
---

mpv is via het netwerk (of lokaal) aan te sturen met commando's. De set aan
commando's die je kunt gebruiken beslaat de volle set aan
configuratieinstellingen en input commando's van mpv. Voor een uitputtend
overzicht van de opties wordt verwezen naar de [mpv
documentatie](https://mpv.io/manual/stable/#json-ipc).

Hieronder vind je een overzicht van de meest gebruikte commando's:

| Actie                    | Commando                                           |
|--------------------------|----------------------------------------------------|
| Pauzeer                  | `{ "command": ["set_property", "pause", true] }`   |
| Play                     | `{ "command": ["set_property", "pause", false] }`  |
| Mute                     | `{ "command": ["set_property", "mute", true] }`    |
| Unmute                   | `{ "command": ["set_property", "mute", false] }`   |
| Stel volume in           | `{ "command": ["set_property", "volume", 75] }`    |
| Ga naar start            | `{ "command": ["seek", "0", "absolute"] }`         |
| Volgende in playlist     | `{ "command": ["playlist-next", "weak"] }`         |
| Laad specifiek bestand   | `{ "command": ["loadfile", "show/1.mp4"] }`        |
| Laad playlist show       | `{ "command": ["loadlist", "show"] }`              |
| Laad playlist test       | `{ "command": ["loadlist", "test"] }`              |
| Laad playlist emergency  | `{ "command": ["loadlist", "emergency"] }`         |

Commando's richting mpv verstuur je als volgt vanaf de commandline:

`echo '{ "command": ["seek", "0", "absolute"] }' | socat STDIO UDP4-DATAGRAM:172.16.70.226:12347`

Bij het versturen van commando's vanuit Pharos (en elke andere tool) is het
belangrijk dat er een newline karakter (`\n`) aan het eind van het bericht wordt
meegestuurd:

`{ "command": ["seek", "0", "absolute"] }\n`

