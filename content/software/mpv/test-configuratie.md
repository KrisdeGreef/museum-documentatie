---
title: "Testconfiguratie"
date: 2019-01-28T11:48:33+01:00
draft: true
---

De NUC's die in het museum worden ingezet zijn door Naturalis voorzien van
testcontent en een testconfiguratie. Op deze pagina staat deze test configuratie
beschreven.

## Software

De NUC's zijn geïnstalleerd met de laatste BIOS versie
(BECFL357.86A.0056.2018.1128.1717) en zijn voorzien van de laatste versie van
het Ubuntu 18.04 LTS Server besturingssysteem. Voor het afspelen van
multimedia-bestanden is mpv 0.29.1 geïnstalleerd.

## BIOS instellingen

De BIOS instellingen zijn op basis van een standaard profiel op alle NUC's
gelijk getrokken. Voor de test zijn dit de meest relevante instellingen:

* PXE boot: de NUC's zullen als eerste proberen door middel van PXE te booten.
  De reden hiervoor is dat hierdoor het management en de herinstallatie van de
  NUC's door middel van MAAS zonder verdere handmatig actie kan worden
  uitgevoerd. Mocht het booten via PXE onverhoopt problemen opleveren dan is dat
  te verhelpen door bij het opstarten op `F10` te drukken en te kiezen voor
  `ubuntu`.

## Content

Shosho heeft testcontent aangeleverd waarvan een deel wordt gebruikt voor de testconfiguratie.
De gebruikte testcontent is beschikbaar voor download in
deze map op [Google
Drive](https://drive.google.com/drive/folders/1T2nvwR1_suHyShtOZNwAdZKtXwStDSHp).

## Sneltoetsen

De players zijn dusdanig geconfigureerd dat bij het opstarten de eerste video
(`testchart_1920x1080_25fps_CBR_20Mbps.mp4`) gelijk start met spelen. Zonder
verdere interactie blijft deze video in een loop afspelen.

Op basis van deze toets(combinatie's) kan worden geswitchd naar andere
bestanden:

| Sneltoets  | Actie                                                          |
| ---------- | -------------------------------------------------------------- |
| `Ctrl+1`   | Speel `testchart_1920x1080_25fps_CBR_20Mbps.mp4`               |
| `Ctrl+2`   | Speel `testchart_1920x1200_25fps_CBR_20Mbps.mp4`               |
| `Ctrl+3`   | Speel `testchart_1920x1080_25fps_CBR_20Mbps_mute.mp4`          |
| `Ctrl+4`   | Speel `audio_seperate_channels_1920x1080_25fps_CBR_20Mbps.mp4` |
| `Ctrl+5`   | Speel `testchart_1920x1080.png`                                |
| `Ctrl+6`   | Speel `testchart_1920x1200.png`                                |
| `Ctrl+7`   | Speel `grid_1920x1080.png`                                     |
| `Ctrl+8`   | Speel `grid_1920x1200.png`                                     |
| `z`        | Speel `1_black.png` (toont '1' voor testen Arduino)            |
| `x`        | Speel `2_black.png` (toont '2' voor testen Arduino)            |
| `c`        | Speel `3_black.png` (toont '3' voor testen Arduino)            |
| `v`        | Speel `4_black.png` (toont '4' voor testen Arduino)            |
| `i`        | Toon statistieken                                              |
| `I`        | Toon statistieken blijvend                                     |
| `Shift+O`  | Toon voortgang video                                           |
| `m`        | Mute audio                                                     |
| `Spatie`   | Toggle pauze                                                   |
| `<` en `>` | Ga vooruit en achteruit in de playlist                         |
| `9` en `0` | Ga vooruit en achteruit in de playlist                         |

Voor overige sneltoetsen wordt verwezen naar de
[mpv-documentatie](https://mpv.io/manual/stable/#interactive-control)

## Aansturing

De players zijn in de testconfiguratie ingericht om te luisten naar commando's
die over UDP worden verstuurd. In de testconfiguratie maakt het niet uit vanaf
welk IP adres de UDP berichten worden verstuurd. De NUC's luisteren op de
volgende adressen en poorten:

* Het (via DHCP verkregen) IP adres van de player in combinatie met poort `12347`
* Het multicast IP adres 224.1.0.1 in combinatie met poort `12345`

Het versturen van commando's staat [hier gedocumenteerd]({{< relref
"commandos.md" >}}).

## Audio

De players zijn geconfigureerd om audio over de audio-poort aan de voorzijde van
de NUC te sturen. Het volume van de NUC's staat op ongeveer 80% ingesteld, het
volume van mpv zelf staat standaard op 100%. Dit is interactief aan te passen
met de toetsten `9` en `0`.
