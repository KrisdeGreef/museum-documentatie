---
title: "Demo setup"
date: 2018-07-04T13:36:48+02:00
draft: false
---

Tijdens de sprint hebben we toegewerkt naar een eerste demo
versie van een mpv setup. Deze werkende versie is te vinden
in de [google drive map van het TIM project](https://drive.google.com/open?id=1dzS_qgMJmN7TNixsC-MMlSBD5Q13KprW).

Om deze installatie werkende te krijgen moet je een recente versie van mpv
installeren en de directory downloaden.

In de directory zelf is de presentatie te starten met het commando:

```bash
mpv --config-dir=config --scripts=scripts/back-to-loop.lua
```

Na start verschijnt een fullscreen scherm met het Naturalis logo. Door te
drukken op de letters a, b, c of d worden diverse scriptjes gestart. Na afspelen
keert de speler terug naar het standaard filmpje.

In `config/input.conf` staan de keystrokes gedefinieerd:

```ini
a loadlist "./playlist-a.txt"; no-osd set loop-playlist no
b loadlist "./playlist-b.txt"; no-osd set loop-playlist no
c loadlist "./playlist-c.txt"; no-osd set loop-playlist no
d loadlist "./playlist-d.txt"; no-osd set loop-playlist no
```

In een playlist staat bijvoorbeeld:

```ini
demo/krab.mp4
```

De configuratie van de mpv speler staat in mpv.conf

```ini
idle=yes
force-window=yes
fullscreen=yes
osd-level=0
input-default-bindings=no
input-conf=config/input.conf
```

Tot slot wordt er een lua script gebruikt die bepaald welke playlist er gespeeld
wordt als er niets gespeeld wordt. Deze staat in `scripts/back-to-loop.lua`.

```lua
local msg = require('mp.msg')
local utils = require('mp.utils')

function restart_playlist()
    msg.info('restart playlist')
    cwd = utils.getcwd()
    mp.commandv('loadlist', cwd .. '/playlist.txt')
    mp.set_property('loop-playlist', 'inf')
end

mp.register_event("idle", restart_playlist)
```


