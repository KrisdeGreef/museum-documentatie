---
title: "MPV web wrapper"
date: 2018-07-04T13:36:48+02:00
draft: false
---

De communicatie met de mpv speler verloopt via een socket file. Deze kan worden
gezet met de optie `--input-ipc-server=/pad/naar/mpv.socket`

De mpv web applicatie,
[github.com/MakeExpose/mpv_web](https://github.com/MakeExpose/mpv_web), is een
flask applicatie als http wrapper om deze socket communicatie heen.

De applicatie is te installeren via pip:

```bash
pip install -e git+https://github.com/MakeExpose/mpv_web#egg=mpv_web
```

En te starten met:

```bash
export MPV_SOCKET=/pad/naar/mpv.socket
export FLASK_APP=mpv_web
flask run
```

Hierna kan je de speler bedienen door commando's te versturen via de webserver.

```bash
curl 'http://127.0.0.1:5000/get_property/playback-time'
```

Of

```bash
curl 'http://127.0.0.1:5000/keypress/a'
curl 'http://127.0.0.1:5000/keypress/b'
curl 'http://127.0.0.1:5000/keypress/c'
curl 'http://127.0.0.1:5000/keypress/d'
```

Niet alle commandos worden geaccepteerd door deze webinterface, alleen die
gedefinieerd staan in de whitelist, die bestaat staandaard uit:

```python
           'get_version',
           'get_property',
           'set_property',
           'property_list',
           'observe_property',
           'request_log_messages',
           'get_version',
           'seek',
           'keypress'
           'revert-seek',
           'frame-step',
           'frame-back-step',
           'screenshot',
           'screenshot-to-file',
           'playlist-clear',
           'playlist-next',
           'playlist-prev',
           'loadlist',
           'loadfile',
           'show-text'
```

Deze whitelist is ook in configuratie te overriden door een instance directory
aan te maken met een
[config.py](https://github.com/MakeExpose/mpv_web/blob/master/instance_config.py).

