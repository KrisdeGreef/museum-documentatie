---
title: "sensu"
date: 2019-11-04T16:22:47+01:00
draft: false
status:
infrastructuren:
---

Om de onderdelen van het museum en de rest van de organisatie te monitoren
maken we gebruik van [Sensu](https://sensu.io/). De status van ieder onderdeel
is terug te vinden via het Uchiwa dashboard op
[sensu.naturalis.nl](https://sensu.naturalis.nl).

## Sensu Exporter

De statusgegevens die worden bijgehouden in Sensu worden ook doorgegeven aan
Prometheus die deze informatie vasthoudt in de tijd. Dit gebeurt via de
[sensu exporter](https://gitlab.com/naturalis/mii/sensu_exporter).

Deze exporter draait als service op sensu.naturalis.nl. De configuratie is
terug te vinden in `/opt/sensu_exporter/` bestaat uit een golang programma die
gegevens ophaalt uit de Sensu server en beschikbaar stelt aan Prometheus.

```
[Unit]
Description=Sensu exporter

[Service]
Type=simple
PIDFile=/var/run/sensu_exporter.pid
EnvironmentFile=/opt/sensu_exporter/.env
ExecStart=/opt/sensu_exporter/sensu_exporter
ExecStop=/bin/kill -s QUIT $MAINPID
Restart=always

[Install]
WantedBy=multi-user.target
Alias=sensu_exporter.service
```

