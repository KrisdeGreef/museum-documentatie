---
title: "Content Server"
date: 2019-02-21T09:15:10+02:00
draft: false
---

De contentserver is eind februari 2019 in productie genomen op
[files.museum.naturalis.nl](https://files.museum.naturalis.nl).

De configuratie draait op een Openstack omgeving en is afgetrapt via Foreman en
Puppet. Hierbij is gebruik gemaakt van de puppet role
[puppet-role_nextcloud](https://github.com/naturalis/puppet-role_nextcloud).

## Opbouw

De installatie bestaat uit een docker-compose configuratie met de volgende Docker
instances:

* `mariadb`
* `redis`
* `nextcloud`
* `traefik`

Het certificaat is een single domain certificaat aangevraagd met _let's
encrypt_. De setup draait op een Ubuntu 18 Linux setup en wordt met behulp van
Foreman/Puppet in de lucht gehouden. Dagelijks wordt er een backup gedraait met
Restic.

## Directories

Belangrijke directories die worden gebackupt zijn:

* `/opt/nextcloud`

Hier staat de docker-compose configuratie, inclusief _.env_ en de traefik
configuratie.

* `/data/files`

Bevat de files die worden geupload naar nextcloud. Dit is een OpenStack storage
van 250 gigabyte.

* `/data/nextcloud`

Bevat de nextcloud scripts.

## Users

Er is een standaard admin account, die alleen toegankelijk is voor het technisch
team. Verder worden er volgens de [structuur]({{< relref
"systemen/nextcloud/structuur/_index.md" >}}) meerdere gebruikers en groepen aangemaakt
met verschillende rechten en toegankelijkheid.
