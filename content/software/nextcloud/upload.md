---
title: "content upload"
date: 2019-02-18T13:15:00+02:00
draft: false
---

Voor het uploaden van content (media- en softwarebestanden) en documentatie
bijlagen maakt Naturalis gebruik van een [content
server](https://files.museum.naturalis.nl). In deze handleiding
lees je hoe je in enkele stappen content, media en documentatie kunt uploaden en
kunt inzien. 

De multimedia-bestanden moeten voldoen aan de daarvoor vastgestelde [standaarden](https://docs.museum.naturalis.nl/latest/standaarden/technisch/multimedia-bestanden/).

## Inloggen

Om te kunnen inloggen heb je een gebruikersnaam en wachtwoord nodig. Je ontvangt
deze van het Technisch Team van Naturalis.

![Login screenshot](nextcloud-login.png)

Als je het wachtwoord bent vergeten of het niet langer werkt, vraag je een
nieuwe aan door op _Forgot password?_ te klikken en je gebruikersnaam of
mailadres in te vullen:

![Forget screenshot](nextcloud-forget.png)

## Mappen

Zodra je bent ingelogd zie je een overzicht van mappen. Afhankelijk van je rol
bij de bouw van het museum zie je een overzicht van mappen.

Als contentleverancier zie je uitsluitend de interactives waarvoor jij content
maakt. Als AV installateur of medewerker van Naturalis zie je de volledige
mappenstructuur die [hier staat toegelicht](https://docs.museum.naturalis.nl/latest/software/nextcloud/structuur).

![Exhibits screenshot](nextcloud-exhibits.png)

Binnen de interactive map bevinden zich één of meerdere mappen met de naam van
de computer waarop media of applicatie draait (bijvoorbeeld
`levennadedood-cmp-1`).

### Mediaplayers

Indien op de computer media wordt afgespeeld staan in de map één of meerdere
submappen met volgnummers. De reden daarvoor is dat een deel van de computers
waarop media wordt afgespeeld worden voorzien van meerdere
onafhankelijk van elkaar draaiende mediaplayers ([mpv]({{< relref "componenten/mpv/_index.md" >}})). De content
voor de eerste mediaplayer op de computer kan in map `1` worden gezet, de
content voor de tweede player in map `2` etc.

Mediabestanden die je in deze mappen plaatst dienen eveneens een volgnummer als
bestandsnaam te hebben (bijvoorbeeld `1.mp4`, `2.mp4` etc.). Als het een
playlist van bestanden betreft wordt `1.mp4` als eerste afgespeeld etc.

### Unity games

Wanneer op de computer een Unity game komt te draaien dient in de map een ZIP
bestand te worden geplaatst met daarin alle voor het draaien van de game
benodigde bestanden. Het ZIP-bestand dient de volgende structuur te hebben:

```bash
naamvandegame
├── naamvandegame_Data
├── naamvandegame.x86_64
├── README.md
└── settings.json
```

In het README.md bestand kunnen de configuratie parameters in het settings.json
bestand worden toegelicht.

Na het uploaden staat het bestand klaar voor publicatie in de desbetreffende map.

![Upload screenshot](nextcloud-contentupload.png)

## Overschrijven

Nieuwe versies van media of andere bestanden moeten op dezelfde plaats en met
dezelfde bestandsnaam worden geupload. Voor het uploaden van mediabestanden en
software moet voor nieuwe versies dezelfde bestandsnaam worden gebruikt.

Bij het overschrijven van bestanden met dezelfde naam verschijnt de volgende
dialoog:

![Overwrite screenshot](nextcloud-overwrite.png)

In het geval van overschrijven klik je alleen de linker vinkjes aan. Hierdoor
wordt een nieuwe versie van het bestand neergezet. De oude wordt gearchiveerd en
kan via versie beheer worden teruggehaald.

## Herstellen

Een foutje is snel gemaakt. Mocht bepaalde media met de verkeerde naam zijn geupload
waardoor een filmpje of applicatie is overschreven, klik dan op de drie puntjes achter
het bestand en op de _Details_ link. Er verschijnt nu een tab met aan de meest
rechterkant het _Versions_ gedeelte. Als je daarop klikt kan je precies zien welke
eerdere versies er zijn geupload.

![Versions screenshot](nextcloud-versions.png)

Door te klikken op het ronde pijltje icoontje kan je andere versie van het bestand
terughalen.

## Activiteiten

Om te zien welke activiteiten er zijn geweest met jouw bestanden kan je klikken
op het bliksem icoontje bovenin de navigatie balk. Hierdoor zie je alle recente
acties die te maken hebben met jouw account en de files die je hebt geupload.

## Publiceren

Het technisch team van Naturalis is verantwoordelijk voor de publicatie van de
content. Dit gebeurt zoveel mogelijk geautomatiseerd, zodat bij aanpassingen
geen tussenkomst nodig is van technische medewerkers. Na opening van het museum
wordt de publicatie van nieuwe content alleen op verzoek vernieuwd of aangepast.
