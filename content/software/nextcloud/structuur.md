---
title: "Mappen- en rechtenstructuur"
date: 2018-08-08T09:15:10+02:00
draft: false
---

Om het aanleveren en downloaden van content vanaf de Nextcloud server op een
eenduidige en efficiënte wijze uit te voeren hanteert Naturalis een mappen- en
bijbehorende rechtenstructuur. De gehanteerde structuur staat op deze pagina
toegelicht.

## Mappenstructuur

De opbouw van de volledige mappenstructuur is als volgt:

```bash
├── content
│   └── groep
│       └── functioneleeenheid
│           └── component
│               └── volgnummer # in het geval van mediaplayers
│                   └── volgnummer.ext
└── documentatie
    ├── componenten
    │   └── merk-model
    ├── interactives
    │   └── naaminteractive
    ├── systemen
    │   └── naamsysteem
    └── tentoonstellingen
        └── naamtentoonstelling
```

Het aanleveren van content gebeurt in de map van het component waarop de
content geplaatst moet worden.

Voor het aanleveren van content voor de mediaplayers worden submappen met
volgnummers gehanteerd. Een deel van de computers waarop media wordt afgespeeld
worden namelijk voorzien van meerdere onafhankelijk van elkaar draaiende
mediaplayers ([mpv]({{< relref "componenten/mpv/_index.md" >}})). De content
voor de eerste mediaplayer op de computer kan in map `1` worden gezet, de
content voor de tweede player in map `2` etc.

Daarnaast zullen bij een deel van de mediaplayers ook meerdere bestanden worden
gebruikt. Om de configuratie en aansturing zo eenvoudig en eenduidig mogelijk
te maken hanteren we voor de namen van deze bestanden eveneens een volgnummer.

De structuur van de documentatiemap volgt één op één de structuur van de
documentatiesite, die [hier verder staat toegelicht]({{< relref
"software/hugo/documentatie/_index.md" >}}).

Dit leidt in de praktijk bijvoorbeeld tot de volgende indeling:

```bash
├── content
│   └── dedood
│       └── levennadedood
│           ├── levennadedood-cmp-1
│           │   ├── 1
│           │   │   └── 1.mp4
│           │   └── 2
│           │       ├── 1.flac
│           │       └── 2.flac
│           └── levennadedood-cmp-2
│               ├── 1_data
│               └── 1.x86_64
└── documentatie
    ├── componenten
    │   └── intel-nuc8i7beh
    │       └── techspecs.pdf
    ├── interactives
    │   └── stamboom
    ├── systemen
    │   └── nextcloud
    └── tentoonstellingen
        └── dedood
```

## Rechtenstructuur

Bij het aanleveren, beheren en downloaden van alle content en
documentatiebestanden zijn veel partijen betrokken. Om het aanleveren van data
zo makkelijk mogelijk te maken en tegelijkertijd de continuïteit zoveel
mogelijk te bevorderen wordt de volgende rechtenstructuur gehanteerd:

* Admin account: account dat eigenaar is van de gedeelde mappen
  `content` en `documentatie` en daarmee van alle onderliggende mappen en
  bestanden. Dit account wordt uitsluitend gebruikt voor het instellen van
  rechten en andere instellingen.
* Download account: account waarmee vanaf de computers relevante content wordt
  gedownload. Dit account heeft leesrechten op alle component mappen in de map
  content.
* Groep `technischteam`: deze groep gebruikers heeft schrijfrechten op de
  gehele mappenstructuur.
* Groep `atatech`: deze groep gebruikers heeft gedurende de opbouw
  leesrechten op de map `content` en schrijfrechten op de map `documentatie`.
* Groep `top`: deze groep gebruikers heeft gedurende de opbouw
  leesrechten op de gehele mappenstructuur. Opmerkingen maken moet wel kunnen.
* Groep `<leverancier>`: deze groep gebruikers krijgen schrijfrechten op het
  niveau van de relevante functionele eenheden in de content map en leesrechten
  op de documentatiemap.

Praktisch gezien hanteren we de volgende uitgangspunten bij het instellen van
rechten en delen van mappen:

* Gebruikers zijn altijd lid van een groep.
* We geven alleen groepen rechten op mappen.

## Uitgangspunten

Bij het bepalen van de structuur en de werkwijze zijn de volgende uitgangspunten
gehanteerd:

* Bij het voorzien van content aan de players gaat voorspelbaarheid en controle
  boven uploadgemak en snelheid. Om die reden wordt in de Ansible
  configuratiecode verwezen naar specifieke bestanden, niet naar de inhoud van
  een map op Nextcloud.
* Bij het bepalen van de namen van contentbestanden staat de voorspelbaarheid
  en uniformiteit bij de configuratie en aansturing boven upload- en
  aanlevergemak.
* Om de voorspelbaarheid en herkenbaarheid te vergroten dient de gekozen
  structuur aan te sluiten bij de in de rest van het museum gehanteerde
  [naamgevingsstructuur]({{< relref "naamgeving.md" >}}) en conventie.
* De structuur kan worden gebruikt voor het aanleveren van zowel content als
  bijlagen voor de documentatie in [Hugo]({{< relref
  "software/hugo/documentatie" >}}).
* Voor het bijhouden van verschillende versies maken we gebruik van het
  versiebeheer dat in Nextcloud wordt ondersteund.
* Een map bevat in beginsel altijd bestanden óf submappen, nooit een combinatie
  van de twee. Uitzondering daarop zijn technische afhankelijkheden zoals bij
  de bestanden voor Unity games die verwijzen naar data in een submap.

Daarnaast is de structuur gebaseerd op de volgende technische kenmerken van Nextcloud:

* Mappen die gedeeld worden met andere gebruikers of groepen komen voor die
  gebruikers in hun interface beschikbaar met die rechten. Als je een submap met
  andere rechten wilt delen dan komt die als een aparte map beschikbaar.

* Het is mogelijk om mappen met submappen in één keer te uploaden, maar mappen
  zonder bestanden worden niet aangemaakt.

* Bestanden met een bestandsnaam beginnend met een '.' worden als verborgen
  bestanden behandeld door Nextcloud. Het is daarom mogelijk om bijvoorbeeld met
  `.placeholder` bestanden een volledige mappenstructuur klaar te zetten.

* Bestanden die in een gedeelde map worden geplaatst worden eigendom van de
  gebruiker die de share heeft aangemaakt.

* Er zijn wel opties om door middel van apps als File Access Control of Group
  Folders op een andere manier om te gaan met permissies in Nextcloud. Geen van
  die opties biedt de mogelijkheid om alle groepen gebruikers leesrechten te geven
  op de hele mappenstructuur, en specifieke groepen op specifieke subfolders ook
  schrijfrechten.
