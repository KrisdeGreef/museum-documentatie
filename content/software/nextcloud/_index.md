---
title: "nextcloud"
date: 2018-08-08T09:15:10+02:00
draft: false
---

Om leveranciers zelfstandig content, media en applicaties te laten publiceren hebben
we een zogeheten content management server in het leven geroepen. Dit is een
webapplicatie die via het internet toegankelijk is voor zowel de leveranciers als
de Naturalis medewerkers, maar die ook gebruikt kan worden om automatisch
interactives van content te voorzien. De applicatie moest voldoen aan de volgende
eisen:

 * webinterface
 * toegangsbeheer
 * grote schrijfruimte
 * logische indeling naar mediaspeler
 * alleen video/audio en unity executables
 * bijhouden meerdere versies
 * toegankelijk voor secury copy/ansible voor deployment
 * open source
 * geautomatiseerde toegang via api
 * gebruik maken van gedeelde storage zoals s3

Een van de weinige applicaties die aan al deze eisen voldoet
is [Nextcloud](https://nextcloud.com/), een opensource fork van het OwnCloud
project. Dit pakket zal worden gebruikt om deze dienst mogelijk te maken.


## API

De api van nextcloud is exact hetzelfde als die van OwnCloud en daarmee kunnen we
gelijk gebruik maken van de python library
[pyoclient](https://github.com/owncloud/pyocclient) en de commandline tool
[pocli](https://pypi.org/project/pocli/).


## Script

Gebaseerd op de pyoclient library hebben we al gelijk een script gebouwd waarmee
we het automatisch _syncen_ van content mogelijk kunnen maken. Dit script heet
[nextcloud update](https://github.com/MakeExpose/nextcloud-update). Na installatie
met pip:

```
pip install -e git+https://github.com/MakeExpose/nextcloud-update#egg=nextcloud-update
```

En het verkrijgen van een login in de nextcloud installatie komt de aanroep neer op
het commando:

```
nextcloud_update nexcloud/path/to/file ./destination/path
```

De credentials moeten staan opgeslagen in een config.json bestand.

```
{
    "server" : "http://yournextcloudserver.com",
    "user"   : "admin",
    "password" : "youradmin"
}
```

Of kunnen worden meegegeven met de commandline aanroep:

```
paths                Remote path on the nextcloud instance

optional arguments:
    -h, --help           show this help message and exit
    --server SERVER      Cloud server url
    --user USER          Cloud server user
    --password PASSWORD  Cloud server password
    --config CONFIG      Cloud server config (json) file
```

*[Content Upload](https://docs.museum.naturalis.nl/latest/software/nextcloud/upload)
*[Content Server](https://docs.museum.naturalis.nl/latest/software/nextcloud/contentserver)
*[Mappen- en rechtenstructuur](https://docs.museum.naturalis.nl/latest/software/nextcloud/structuur)
