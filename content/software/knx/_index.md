---
title: "knx"
date:
draft: true
attachments:
- title: KNX-Basics_en (pdf)
  url: https://files.museum.naturalis.nl/s/iFeQ5nJb6J55ZrQ
- title: KNX Uitdraai van ETS5
  url: https://files.museum.naturalis.nl/s/tA2j4KxAJ8qwF3g
---

## Inleiding

KNX is een techniek om apparaten zoals lampen, verwarming en deuren te bedienen. Het werkt met een eigen protocol over:

* KNX TP - 'twisted pair' bus (eigen bekabeling met 24V)
* KNX PL - powerline (via de bestaande 230V) ook wel PL110 genoemd
* KNX RF - radio frequentie
* KNX IP - Over Ethernet/IP

KNX componenten kan je onder 3 catogorien verdelen:

* System devices (power suply, programming interface)
* Sensors (detecteert iets: deur staat open, persoon in de ruimte)
* Actuators (ontvanger zoals een lamp)

Wanneer een sensor iets detecteert stuurt hij een bericht over de bus met deze informatie naar een actuator. Dit heet een telegram.

![](./sensor_actuator_principe.png)
![](./telegram.png)

Per protocol ziet de telegram er wel anders uit, bijvoorbeeld bij KNX IP zit er nog de IP body omheen.

![](./knx_ip_telegram.png)

## KNX IP

KNX IP maakt gebruik van twee communicatie methodes, beiden over UDP:

* Tunneling (KNXnet/IP tunneling)
* Routering (KNXnet/IP routing)

![](./knx_iso.png)
Dit is het ISO model die gebruikt wordt.


Tunneling wordt gebruikt om de (KNX) bus te benaderen vanaf het LAN of internet om de KNX installatie te programmeren. Bijvoorbeeld met de KNX programeer software ETS.

Routering is voor het uitwisselen van telegrams tussen de diverse KNX devices. Bijvoorbeeld vanaf een PC naar de KNXnet router (IP coupler) die het vervolgens doorstuurt naar de KNX device die daar achterhangt via KNX TP.

![](./KNX_IP_couplers.png)

## Topology

De basis unit van een KNX TP installatie is een lijn (line). Dit is de bus waar maximaal 64 devices met elkaar kunnen communiceren (telegrams versturen). Een line heeft een eigen power supply (24V) (en eventueel choke) nodig. Een choke kan je gebuiken voor redundante powersupplies.

Met een line repeater of een line coupler kan je meerdere lijnen aan elkaar koppelen, dan werkt de line coupler als router (hij zorgt er dus voor dat een KNX device een telegram kan sturen naar een ander KNX devices op een andere line d.m.v. adresering). Meerdere lines bij elkaar heeft een area.
Meerdere area's kunnen ook weer aan elkaar gekoppeld worden.

Dit zie je terug in de KNX adresering van de Devices:

x.y.z (bijvoorbeeld 1.1.1)

* x = area nummer
* y = line nummer
* z = device nummer

Waarbij de lijn of de coupler als device nummer 0 krijgt.
Jammer genoeg werdt deze logica bij de bouw van Naturalis *niet* gebruikt.

Meer informatie over topology is te vinden in de bijlage [KNX-Basics_en.pdf](https://files.museum.naturalis.nl/s/iFeQ5nJb6J55ZrQ) pagina 11.

## Afstanden

* Max afstand tussen PSU en KNX Device: 350 meter
* Max afstand tussen twee KNX Devices: 700 meter


## Bijlagen

{{< pagelist bijlagen >}}
