---
title: "Beheer DNS/DHCP"
date: 2018-06-15T14:33:29+02:00
draft: false
weight: 5
---

Een belangrijk onderdeel van de infrastructuur zijn DHCP, DNS en PXE. Dit kan
gebruikt worden om fysieke machines te installeren, bereikbaar te maken en van
namen en IP adressen te voorzien. [MAAS](https://www.ubuntu.com/server/maas)
ondersteunt al deze functies.

## Algemene terminologie

* `DNS`: Deze servers vertalen domeinnamen (bv google.nl) naar IP adressen.
* `DHCP`: DHCP servers zorgen ervoor dat apparaten een IP adres krijgen. De
  meeste DHCP servers ondersteunen ook functionaliteit waarbij op basis van een MAC-adres,
  IP adressen voor specifieke machines worden gereserveerd.
* `PXE`: Een protocol dat er voor zorgt dat fysieke computers van het netwerk
  kunnen opstarten in plaats van een lokale schijf. PXE maakt het mogelijk om
  computers op afstand te installeren.

## MAAS specifieke terminologie

MAAS onderscheidt, zoals ook zichtbaar is in de webinterface, de volgende
onderdelen:

* `Nodes`: Hier staan de machines, devices en controllers. De controllers
  spreken voor zich. Devices zijn apparaten die voorzien moeten worden van een
  IP adres en een DNS record (bijv. projector) en de machines zijn apparaten die
  daarnaast ook geïnstalleerd moeten/kunnen worden.
* `Pods`: Behalve fysieke machines kan MAAS bijv. ook libvirt aansturen om
  virtuele machines aan te maken. Deze staan bekend als Pods.
* `Images`: Hier staan de images van de OS'es.
* `Zones`: Netwerken en machines zijn in MAAS onder te verdelen in zones. Een
  optie is om per tentoonstelling een  aparte zone te maken.
* `Subnets`: Hier vind je alles rondom netwerk configuratie.
* `Settings`: Hier zijn alle andere settings (bijv. keys, DHCP snippets etc.) te
  vinden.

## DNS

De MAAS servers zullen als nameservers dienen voor alle devices waarvoor MAAS
ook DHCP verzorgd. In beginsel zijn dat alle VLAN's op de Darwinweg met
uitzondering van het kantoor VLAN. Het kantoor VLAN zal vanwege de nauwe
integratie van de Windows machines met Active Directory (AD), de domain
controllers in het datacenter als nameservers gebruiken.

Voor de devices die op basis van een DHCP reservering door MAAS van een IP adres
worden voorzien hanteren we één subzone, te weten `dw2.naturalis.nl`. De
voorwaarde is dat alle devices een unieke naam moeten hebben. Omdat al die
devices al in één Ansible inventory worden beheerd, was dit toch al een
voorwaarde. Anderzijds blijft de DNS configuratie op deze manier relatief
simpel; door middel van één zone-verwijzing vanaf de domain controllers zijn
alle devices ook vanuit het kantoor VLAN via hun domeinnaam te benaderen.

Op basis van deze opzet zien de full qualified domain names van deze devices er
zo uit:

```bash
stamboom-cmp-1.dw2.naturalis.nl
netdw2-leaf-a1a.dw2.naturalis.nl
```

## Toevoegen van een DHCP reservering met DNS

Voordat een device van een DHCP reservering hostname kan worden voorzien is er
een subnet nodig. In MAAS taal is een Fabric een netwerk interface op je MAAS
server. Aan dat fabric moet je een (tagged of untagged) VLAN toekennen en op dat
VLAN kun je een subnet definiëren. Je kan de subnetten eventueel groeperen in
een `space`. DHCP kun je aanzetten nadat het subnet is aangemaakt.

Met de onderstaande commando's kan een DHCP reservering worden toegevoegd voor
een device:

```shell
system_id=$(maas admin devices create hostname=beamert-001 domain=vdl.maas mac_addresses=dc:fe:07:e0:ec:f1|jq -r '.system_id')
int_id=$(maas admin interfaces read $system_id | jq -r '.[] | select(.mac_address == "dc:fe:07:e0:ec:f1") | .id')
subnet_id=$(maas admin subnets read | jq -r '.[] | select(.cidr == "172.16.44.0/24") | .id')
maas admin interface link-subnet $system_id $int_id mode=STATIC subnet=$subnet_id ip_address=172.16.44.33
```

## Deployen van een machine

De stappen voor het deployen van een machine op basis van PXE staan op [deze
pagina](relref "deploy-machine.md") gedocumenteerd.
