---
title: "maas"
date: 2018-08-08T09:15:10+02:00
draft: true
---

Een belangrijk onderdeel van de infrastructuur zijn DHCP, DNS en PXE. Dit kan
gebruikt worden om fysieke machines te installeren, bereikbaar te maken en van
namen en IP adressen te voorzien.

[MAAS](https://www.ubuntu.com/server/maas)
is een open source product van Canonical dat al deze functies ondersteunt.

MAAS bestaat uit twee delen, een *region contoller* en een *rack controller*. Een
rack controller voert opdrachten uit op een specifieke locatie. De region
controller beheert de rack controller(s). Vanuit de region controller kun je
opdrachten initiëren die dus door de rackcontroller worden uitgevoerd.

## API

Naast een CLI en webinterface is MAAS ook aan te sturen middels een Restful API.
De API staat [hier gedocumenteerd](https://docs.maas.io/2.4/en/api). Voor het
gebruik van de MAAS API is er eveneens een officieel ondersteunde [Python client
library](https://github.com/maas/python-libmaas). De documentatie van deze
library vind je [hier](http://maas.github.io/python-libmaas/).

* [Beheer DNS/DHCP](https://docs.museum.naturalis.nl/latest/software/maas/beheer-dns-dhcp/)
* [Deployment van MAAS](https://docs.museum.naturalis.nl/latest/software/maas/deploy-maas/)
* [Deployment machine](https://docs.museum.naturalis.nl/latest/software/maas/deploy-machine/)
* [Server architectuur](https://docs.museum.naturalis.nl/latest/software/maas/server-architectuur/)
