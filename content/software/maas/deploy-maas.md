---
title: "Deployment van MAAS"
date: 2018-08-08T16:05:31+02:00
draft: true
weight: 2
---

MAAS kan door middel van Ansible op worden geïnstalleerd op
één of twee servers. Voor het museum maken we gebruik van twee servers: een
master en slave server.

De deployment en configuratie van MAAS is in twee stappen uit te voeren door
middel van de volgende Ansible roles:

* [ansible-maas-deploy](https://github.com/naturalis/ansible-maas-deploy)
* [ansible-maas-control](https://github.com/naturalis/ansible-maas-control)

Het gebruik van deze roles staat gedocumenteerd in de repositories van die
roles.

## Netwerk

De fysieke MAAS servers hebben elke drie koper en twee glaspoorten. Deze zijn
als volgt aangesloten:

* Koper poort voor IPMI (de enkele poort aan de achterzijde van de switch) is
  aangesloten op poort op leafswitch met untagged het management VLAN
* Koper poort voor OOB op poort op oob-switch met untagged het OOB netwerk
* Koper poort voor database sync op koper poort van andere MAAS server
* Glaspoort op poort op spineswitch met untagged het Servers VLAN en tagged alle
  andere VLAN's die door MAAS van DNS, DHCP en PXE worden voorzien.
