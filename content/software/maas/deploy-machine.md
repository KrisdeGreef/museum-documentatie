---
title: "Deployment machine"
date: 2018-08-08T16:02:24+02:00
draft: true
---

Voordat er een machine gedeployed kan worden is er een image nodig. Daarnaast is
er ook een subnet nodig. In MAAS taal is een Fabric een netwerk interface op je
MAAS server. Aan dat fabric moet je een (tagged of untagged) VLAN toekennen en
op dat VLAN kun je een subnet definiëren. Je kan de subnetten eventueel
groeperen in een `space`. Voor PXE heb je ook DHCP in een
subnet nodig. Die kun je aanzetten nadat het subnet is aangemaakt.

Als je dit allemaal hebt ben je klaar om een machine te deployen. Dit kan met de
onderstaande commando reeks.

{{% notice note %}}
Let op dat het woord `admin` in onderstaande commando een referentie is naar het
profiel met de API credentials.
{{% /notice %}}

```shell
maas admin machines create hostname=nuc-003 domain=vdl.maas mac_addresses=dc:fe:07:e0:ec:97 architecture=amd64 power_type=wakeonlan power_parameters_power_mac=dc:fe:07:e0:ec:97 power_parameters_power_address=unknown power_parameters_switch_ip=172.16.43.8 power_parameters_port=25
```

Dit proces duurt even. MAAS kijkt eerst welke hardware er in de betreffende
machine zit. Vervolgens voert MAAS een aantal hardware tests uitgevoerd. Als dit
klaar is zoek dan het system id op:

```shell
maas admin machines read mac_address=dc:fe:07:e0:ec:97 | jq '.[0].system_id'
```

Zoek vervolgens naar de (netwerk) interfaces:

```shell
maas admin interfaces read dy8dxe | jq '.[] | select(.mac_address == "dc:fe:07:e0:ec:97") | .id'
```

Vind het id van de standaard link:

```shell
maas admin interfaces read dy8dxe | jq '.[] | select(.mac_address == "dc:fe:07:e0:ec:97") | .links[0].id'
```

Verwijder de standaard link:

```shell
maas admin interface unlink-subnet dy8dxe 76 id=231
```

Vind het id van het subnet dat je wilt gebruiken:

```shell
maas admin subnets read | jq '.[] | select(.cidr == "172.16.44.0/24") | .id'
```

Link de machine tot dat subnet en geeft het IP adres dat je wilt configureren:

```shell
maas admin interface link-subnet  dy8dxe 76 mode=STATIC subnet=1 ip_address=172.16.44.32
```

Alloceer de machine:

```shell
maas admin machines allocate system_id=dy8dxe
```

Deploy de machine:

```shell
maas admin machine deploy  dy8dxe distro_series=bionic
```

Done!
