---
title: "gitlab"
date: 2019-11-04T16:22:47+01:00
draft: false
status:
infrastructuren:
---

Voor de ontwikkeling, het testen en het beheer van de voor de technische
infrastructuur van het museum benodigde software en config management code
maken we gebruik van [GitLab](https://gitlab.com).

## Organisatie

Alle projecten van Naturalis zijn ondergebracht in de Naturalis organisatie op
GitLab en zijn onderverdeeld in zogeheten subgroepen, die samenvallen met de
binnen onze sector gehanteerde infrastructuren. Alle projecten en daarmee
samenhangende code, issues etc. vallen onder de [subgroep
MII](https://gitlab.com/naturalis/MII).

## User stories en sprints

Voor het bijhouden van technische en functionele verbeterpunten en problemen
maken we gebruik van GitLab issues.

Tijdens de sprints maken we eveneens gebruik van GitLab. Als leidraad voor het
gebruik van GitLab voor sprints wordt deze uitleg gehanteerd: user stories
worden in issues bijgehouden, waarbij issues zoveel mogelijk worden aangemaakt
bij het project waar deze aan zijn gerelateerd. Zo is het issue [HWE
kernel](https://gitlab.com/naturalis/mii/ansible-interactive/issues/5)
aangemaakt als onderdeel van het project
[ansible-interactive](https://gitlab.com/naturalis/mii/ansible-interactive/).
Wanneer een issue niet overduidelijk bij één project hoort kan deze worden
toegevoegd aan het project
[ansible-darwinweg-placeholder](https://gitlab.com/naturalis/mii/ansible-darwinweg-placeholder).

Voor een specifieke sprint maken we in de eerste plaats [een milestone op het
niveau van subgroep MII
aan](https://gitlab.com/groups/naturalis/mii/-/milestones) met daaraan de
juiste begin- en einddatum gekoppeld. Alle issues / user stories die horen bij
de sprint worden aan deze milestone gekoppeld. Daarnaast maken we voor [de
sprint een kanbanbord aan (eveneens op het MII
niveau)](https://gitlab.com/groups/naturalis/mii/-/boards) waarin we de
status van de afhandeling van de user stories bij kunnen houden. De voortgang
van de sprint kan worden gevolgd in [de burndown
chart](https://gitlab.com/groups/naturalis/mii/-/milestones/1)
