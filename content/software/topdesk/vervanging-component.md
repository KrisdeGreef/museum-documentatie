---
title: "Vervanging Component"
date: 2018-07-13T15:09:23+02:00
draft: true
---

Hieronder een overzicht van de handelingen die nodig zijn bij het vervangen van
een component in het museum. Deze werkwijze gaat op voor alle componenten die in
TOPdesk zijn geadministreerd.

* Je zoekt het te vervangen component op in TOPdesk aan de hand van de naam,
  voorbeeld: `feestvandedood-cmp-1`

* Je verwijdert hier de beoogde naam, in dit geval: `feestvandedood-cmp-1`

* Je zoekt een geschikte asset op die op voorraad is, bijvoorbeeld: `nn-cmp-923`

We gaan hier in het gemak vanuit dat de asset die op voorraad is al is geïmporteerd.

* Je vult bij deze asset de beoogde_naam in, in dit geval `feestvandedood-cmp-1`

Vanaf hier zijn er twee opties:

1. DIY: Je maakt gebruik van de [Museum Houskeeping
   tooling](https://gitlab.com/naturalis/mii/housekeeping).

1. Je maakt een call aan met het verzoek om de aangepaste asset informatie te
   verwerken in Ansible.
