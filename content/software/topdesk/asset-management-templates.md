---
title: "Asset Management Templates"
date: 2018-09-03T11:33:14+02:00
draft: true
---

Topdesk assetmanagement maakt gebruik van templates. templates bevatten in ons
geval een fieldset widget. Hier onder een overzicht van alle fields, fieldsets
en templates zoals die in Topdesk assetmanagement moeten worden aangemaakt.

#### Defaults

* allow multiple fields: no

#### Default template

Label:

Type:

Omschrijving:

Bron sheet:

Bron topdesk:

## Fields

### Algemene informatie

#### Label: naam

Type: text

Omschrijving: de naam die de asset heeft

Bron sheet: topdesk

Bron topdesk: topdesk of awx

Opmerking: Wordt door topdesk aangemaakt tijdens import, en door awx_to_topdesk
overschreven na configuratie en deployment. awx beseerd de waarde op de gegevens
uit het veld beoogde_naam uit de sheet.

#### Label: merk

Type: text

Omschrijving:

Bron sheet: gebruiker

Bron topdesk: sheet

#### Label: model

Type: text

Omschrijving:

Bron sheet: gebruiker

Bron topdesk: sheet

#### Label: serienummer

Type: text

Omschrijving:

Bron sheet: gebruiker

Bron topdesk: sheet

#### Label: mac_adres

Type: text

Omschrijving:

Bron sheet: gebruiker

Bron topdesk: sheet

#### Label: leverancier

Type: drop-down

Selectable options: supplier

Omschrijving: overeenkomende met een in TOPdesk aangemaakte leverancier

Bron sheet: gebruiker (?)

Bron topdesk: sheet

#### Label: documentatie

Type: text

Omschrijving: een url naar de documentatie pagina. In het geval van een
component is dit de url naar de documentatie pagina van het type component. In
het geval van een functionele eenheid de url naar de documentatie pagina van de
functionele eenheid.

Bron sheet: topdesk

Bron topdesk: script (moet nog een script voor gemaakt worden)

#### Label: chatops

Type: text

Omschrijving: Een url naar het mattermost chatkanaal van de desbetreffende functionele eenheid. 

Bron sheet: topdesk

Bron topdesk: script (moet nog een script voor gemaakt worden)

### Configuratie

### Label: configuratie_status

Type: dropdown

Selectable options: defect, in onderhoud, in productie, in voorbereiding, inzetbaar

Omschrijving: [zie
asset-import](https://gitlab.com/naturalis/mii/museum-documentatie/blob/master/content/systemen/topdesk/asset-import.md)

Bron sheet: topdesk

Bron topdesk: gebruiker of awx_to_topdesk

#### Label: beoogde_naam

Type: text

Max. lenght: 30

Omschrijving: rol die de asset moet krijgen, leeg tot bekend

Bron sheet: gebruiker

Bron topdesk: sheet of gebruiker

#### Label: ip_adres

Type: text

Max. lenght: 16

Omschrijving:

Bron sheet: topdesk

Bron topdesk: ???

Opmerking: Hoe gaan we dit aanpakken?

#### Label: os

Type: text

Omschrijving: Operating System Device + versie

Bron sheet: topdesk

Bron topdesk: ???

Opmerking: Hoe gaan we dit aanpakken?

#### Label: bios

Type: text

Omschrijving: BIOS versie

Bron sheet: topdesk

Bron topdesk: ???

Opmerking: Hoe gaan we dit aanpakken?

### Stroomvoorziening

#### Label: aansluitpunt

Type: text

Omschrijving:

Bron sheet: gebruiker

Bron topdesk: sheet of gebruiker

#### Label: schakelpunt

Type: text

Omschrijving:

Bron sheet: topdesk

Bron topdesk: sheets_to_topdesk of gebruiker

Opmerking: sheets\_to\_topdesk kan waarde bepalen op basis van waarde _aansluitpunt_.

#### Label: elektragroep

Type: text

Omschrijving:

Bron sheet: topdesk

Bron topdesk: sheets_to_topdesk of gebruiker

Opmerking: sheets\_to\_topdesk kan waarde bepalen op basis van waarde _aansluitpunt_.

### Netwerk

#### Label: outlet

Type: text

Omschrijving: [zie naamgeving outlet](http://docs.museum.naturalis.nl/latest/standaarden/documentatie/naamgeving/#outlet)

Bron sheet: gebruiker

Bron topdesk: sheet of gebruiker

#### Label: patchpaneel

Type: text

Omschrijving:

Bron sheet: topdesk

Bron topdesk: sheets_to_topdesk of gebruiker

Opmerking: sheets\_to\_topdesk kan waarde bepalen op basis van waarde _outlet_.

#### Label: patchkast

Type: text

Omschrijving:

Bron sheet: topdesk

Bron topdesk: sheets_to_topdesk of gebruiker

Opmerking: sheets\_to\_topdesk kan waarde bepalen op basis van waarde _outlet_.

#### Label: technische_ruimte

Type: text

Omschrijving:

Bron sheet: topdesk

Bron topdesk: sheets_to_topdesk of gebruiker

Opmerking: sheets\_to\_topdesk kan waarde bepalen op basis van waarde _outlet_.

## Fieldsets

* Algemene informatie
* Configuratie
* Stroomvoorziening
* Netwerk

## Templates

### Assets

#### Computer

-- Algemene informatie

* naam
* merk
* model
* serienummer
* mac_adres
* leverancier
* documentatie

-- Configuratie

* configuratie_status
* beoogde_naam
* ip_adres
* os
* bios

-- Stroomvoorziening

* aansluitpunt
* schakelpunt
* elektragroep

-- Netwerk

* outlet
* patchpaneel
* patchkast
* technische_ruimte

#### Projector

-- Algemene informatie

* naam
* merk
* model
* serienummer
* mac_adres
* leverancier
* documentatie

-- Configuratie

* configuratie_status
* beoogde_naam
* ip_adres

-- Stroomvoorziening

* aansluitpunt
* schakelpunt
* elektragroep

-- Netwerk

* outlet
* patchpaneel
* patchkast
* technische_ruimte

#### Controller

-- Algemene informatie

* naam
* merk
* model
* serienummer
* mac_adres
* leverancier
* documentatie

-- Configuratie

* configuratie_status
* beoogde_naam
* ip_adres

-- Stroomvoorziening

* aansluitpunt
* schakelpunt
* elektragroep

-- Netwerk

* outlet
* patchpaneel
* patchkast
* technische_ruimte

#### Beeldscherm

-- Algemene informatie

* naam
* merk
* model
* serienummer
* leverancier
* documentatie

-- Configuratie

* configuratie_status
* beoogde_naam

-- Stroomvoorziening

* aansluitpunt
* schakelpunt
* elektragroep

#### Versterker

-- Algemene informatie

* naam
* merk
* model
* serienummer
* leverancier
* documentatie

-- Configuratie

* configuratie_status
* beoogde_naam

-- Stroomvoorziening

* aansluitpunt
* schakelpunt
* elektragroep

#### Speaker

-- Algemene informatie

* naam
* merk
* model
* serienummer
* leverancier
* documentatie

-- Configuratie

* configuratie_status
* beoogde_naam

-- Stroomvoorziening

* aansluitpunt
* schakelpunt
* elektragroep

#### Power Distribution Unit

-- Algemene informatie

* naam
* merk
* model
* serienummer
* mac_adres
* leverancier
* documentatie

-- Configuratie

* configuratie_status
* beoogde_naam
* ip_adres
* os
* bios

-- Stroomvoorziening

* aansluitpunt
* schakelpunt
* elektragroep

-- Netwerk

* outlet
* patchpaneel
* patchkast
* technische_ruimte

#### Component

-- Algemene informatie

* naam
* merk
* model
* leverancier
* documentatie

-- Configuratie

* configuratie_status
* beoogde_naam

#### Decor

-- Algemene informatie

* naam
* documentatie

#### Experience 

-- Algemene informatie

* naam
* documentatie
* chatops

#### Interactive

-- Algemene informatie

* naam
* documentatie
* chatops

#### Show

* naam
* documentatie
* chatops

#### Museum

-- Algemene informatie

* naam
* documentatie
* chatops

#### Tentoonstelling

-- Algemene informatie

* naam
* documentatie
* chatops



