---
title: "Call rapportage Assets"
date: 2018-07-11T10:35:15+02:00
draft: true
---

## Overzicht meldingen per functionele eenheid

Als lid van het technische team wil je wanneer je onderhoud gaat doen aan een
functionele eenheid kunnen zien welke meldingen er open staan op die functionele
eenheid zodat je deze wanneer mogelijk allemaal kunt oplossen zodat de
functionele eenheid weer volledig in de staat verkeerd zoals vastgesteld in de
documentatie en hij volledig operationeel is.

Dit kan via de asset pagina van de functionele eenheid, filters kunnen alleen
nog niet worden vastgezet, wens is bekend gemaakt bij TOPdesk en dit zal naar
verwachting komend jaar worden gerealieerd. Hou hiervoor de [topdesk asset
manament
roadmap](https://trello.com/b/qdH51G8x/topdesk-product-roadmap?menu=filter&filter=label:Asset%20Management)
in de gaten.

Dit kan via een selectie. Volgens het format Select all Calls where Object ID
contains _functionele eenheid_.

## Nice to haves

Naast het bovenstaande overzicht zijn er nog andere overzichten die kunnen
helpen bij het inzichtelijk maken van het werk dat er gedaan moet worden.
Hieronder enkele voorbeelden:

* Overzicht meldingen per tentoonstelling

  TOPdesk ondersteunt momenteel nog niet de mogelijkheid om selecties te maken
  op basis relaties tussen assets. Alle tentoonstellingen hebben echter ook een
  locatie. Een andere mogelijkheid zou zijn om een selectie te maken van
  meldingen op basis van de locatie van de gekoppelde asset (functionele
  eenheid).

* Aantal meldingen per functionele eenheid

  Het is de wens om snel te zien welke functionele eenheden de meeste meldingen
  genereren. Dit kan helpen bij het prioriteren van werk. TOPdesk voorziet hier
  momenteel niet in.

* Aantal meldingen per tentoonstelling

  Het is de wens om snel te zien welke tentoonstelling(en) de meeste meldingen
  genereren. Dit kan helpen bij het prioriteren van werk. TOPdesk voorziet hier
  momenteel niet in.

* Overzicht van meldingen per type functionele eenheid

  Het kunnen filteren van meldingen op basis van het type functionele eenheid,
  denk hierbij aan interactive, exhibit, signage of faciliteit is een wens.

* Overzicht van meldingen per type component

  Dit kan via een selectie. Volgens het format Select all Calls where Object ID
  contains _afkorting component type_.

{{% notice note %}}
Er zal gekeken worden naar welke extra inzichtelijkheid er geboden kan worden
door dashboards te maken in Grafana of Kibana met behulp van de TOPdesk api.
{{% /notice %}}

## Management rapportages

Vanuit verschillende management gremia zijn er ook nog verschillende rapportages
gewenst als het gaat om calls in relatie tot assets.

### Service Manager

Jira: FEM-102

### Leidinggevende (Coordinator / Afdelingshoofd / Projectleider)

Jira: FEM-102
