---
title: "Asset Import"
date: 2018-07-11T10:01:12+02:00
draft: true
---

Als basis voor het invoeren van lijsten van assets die we willen managen in
TOPdesk gebruiken we Google Sheets.

Er zijn twee type lijsten die we moeten importeren.

1. Een lijst met alle interactives en de groep (tentoonstelling) waar van ze
   onderdeel vanuit maken.
1. Een lijst met alle assets en bij behorende naam (rol).

## Interactives

We maken een googlesheet met daarin de naam van alle functionele eenheden van
het type interactive en de naam van de tentoonstelling waar ze onderdeel vanuit
maken.

Deze lijst wordt in TOPdesk geïmporteerd door middel van het
[`sheet_to_topdesk.py`
housekeepingscript](https://gitlab.com/naturalis/mii/housekeeping). Dit script kan
worden gestart door middel van de Housekeeping job in AWX. Deze job is gebaseerd
op de [ansible-housekeeping
role](https://gitlab.com/naturalis/mii/ansible-housekeeping) waarmee op een server
in OpenStack, in een Docker container de Python scripts worden gedraaid.

Op basis van deze lijst zal in TOPdesk de link type _onderdeel van_ gezet worden
tussen de tentoonstelling en de interactive. Op basis van de naam van de
functionele eenheid en het eerste deel van de naam van de asset (voor het -) zal
er ook de link type _onderdeel van_ gezet worden tussen functionele eenheid en
asset.

Zie script: [MakeExpose/housekeeping](https://gitlab.com/naturalis/mii/housekeeping)

## Assets

### Overzicht

De AV-leverancier krijgt een googlesheet aangeleverd voor de administratie van
assets.

Voor elk soort component is er een tabblad met de o.a. velden zoals beschreven
in de [documentatie standaard asset
informatie](http://docs.museum.naturalis.nl/latest/standaarden/documentatie/asset-informatie/)

Computer:

* naam (de naam die de asset heeft in TOPdesk)
* configuratie_status (geeft de status aan van de desbetreffende computer; inzetbaar, in voorbereiding, in productie, in onderhoud of defect)
* documentatie (een url naar de documentatie pagina van dit type component)
* leverancier (overeenkomende met een in TOPdesk aangemaakte leverancier)
* merk
* model
* serienummer
* locatie (overeenkomende met een in TOPdesk aangemaakte locatie)
* beoogde_naam (rol die de asset moet krijgen, leeg tot bekend)
* aansluitpunt (overeenkomende met de tekst op het label bij de contactstop
  waarmee het apparaat aangesloten is conform de [standaard documentatie
  labels](http://docs.museum.naturalis.nl/latest/standaarden/documentatie/labels/))
* mac_adres
* ip_ades
* outlet

Beeldscherm:

* naam 
* documentatie 
* leverancier 
* merk
* model
* serienummer
* locatie 
* beoogde_naam 
* aansluitpunt 

Projector:

* naam 
* configuratie_status 
* documentatie 
* leverancier 
* merk
* model
* serienummer
* locatie
* beoogde_naam
* aansluitpunt 
* mac_adres
* ip_ades
* outlet

Versterker:

* naam 
* documentatie 
* leverancier 
* merk
* model
* serienummer
* locatie
* beoogde_naam
* aansluitpunt 

Speaker:

* naam 
* documentatie 
* leverancier 
* merk
* model
* serienummer
* locatie
* beoogde_naam
* aansluitpunt 

Controller (Show en lichtcontrollers):

* naam 
* configuratie_status 
* documentatie 
* leverancier 
* merk
* model
* serienummer
* locatie
* beoogde_naam
* aansluitpunt 
* mac_adres
* ip_ades
* outlet

Armatuur

* naam 
* documentatie 
* leverancier 
* merk
* model
* serienummer
* locatie
* beoogde_naam
* aansluitpunt 
* dmx_adres

Power Distribution Unit (over netwerk schakelbare tafelcontactdoos)

* naam 
* configuratie_status 
* documentatie 
* leverancier 
* merk
* model
* serienummer
* locatie
* beoogde_naam
* aansluitpunt 
* mac_adres
* ip_ades
* outlet

Component (componenten die niet onder een van de bovenstaande categorien vallen
maar waarvoor het wel zinnig is ze als asset te managen):

* naam 
* configuratie_status 
* documentatie 
* leverancier 
* merk
* model
* serienummer
* locatie
* beoogde_naam
* aansluitpunt 
* mac_adres
* ip_ades
* outlet

De AV installateur draagt er zorg voor dat alle velden correct worden ingevuld
met uitzondering van de colom naam.

### Locatie registratie

Aangezien er tijdens de bouw op verschillende locaties gewerkt gaat worden is
het voor zowel de AV installateur als het Technisch Team handig om te weten wat
de locatie is van een component. Deshalve zullen ook voor alle werkplaatsen in
den lande locaties aanmaakt worden in TOPdesk. De locaties binnen het museum
zullen worden aangeleverd door het verhuis project en ook geimporteerd worden in
TOPdesk. Deze import is geen onderdeel van het Museum Infra Structuur project.

Het import script zal voorzien in het linken van een asset aan de juiste
locatie.

Zie user story: Jira FEM-98

Zie script: [MakeExpose/housekeeping](https://gitlab.com/naturalis/mii/housekeeping)

TOPdesk werkt aan de overerving van de locatie op basis van de link tussen
assets. Dit zal in ons geval waarschijnlijk gaan betekenen dat componenten hun
locatie erven van de functionele eenheid waar ze onderdeel van uit maken.

### Component documentatie pagina's

Voor elk type component zal een documentatie pagina aangemaakt moeten worden op
basis van een template (archetype) voor de component soort waar het type
component onder valt.

Het import script zal dus op basis van het merk en het model moeten cheken of
deze pagina al bestaat in het documentatie systeem (gohugo.io). En wanneer niet
deze aanmaken. Op basis van het tablad (soort component) weet het script welke
archetype template er gebruikt moet worden.

Het script zal de documentatie pagina vervolgens op zoeken op basis van merk &
model en de url in het field documentatie zetten in TOPdesk.

Het script zal als laatste de url naar de documentatie pagina opvragen uit
TOPdesk van het specifieke asset en deze url in de google sheet vermelden onder
documentatie.

De AV installateur of Technisch team medewerker kan zo simpel zien of er al een
documentatie pagina voor is aangemaakt en snel naar de juiste pagina gaan en
controleren of de informatie volledig is en aanvullen waar nodig.

Zie script: [MakeExpose/housekeeping](https://gitlab.com/naturalis/mii/housekeeping)

### Status

Het is voor de AV installateur van belang om te kunnen zien in hoeverre de
aangeleverde asset informatie is verwerkt in de verschillende systemen en dus
klaar is om ingezet te worden.

Dit kan de AV installateur aflezen in het veld naam.

Dit veld kan verschillende type waardes hebben.

1. Leeg, in dit geval is de sheet nog niet geimporteerd in TOPdesk.

1. Een naam in het format nn-_cmp_-_01_, in dit geval is er wel een import
   geweest in TOPdesk maar heeft de asset nog geen rol toegewezen gekregen in
   Ansible.

1. Een naam die niet gelijk is aan de beoogde naam, in dit geval is de beoogde
   naam aangepast maar heeft ansible de wijziging nog niet toegepast.

1. Een naam die gelijk is aan de beoogde naam, in dit geval is voor ansible
   duidelijk wat de rol van de asset moet zijn en is of wordt hij geconfigureerd
   zodra hij aan staat.

Zie script: [MakeExpose/housekeeping](https://gitlab.com/naturalis/mii/housekeeping)

Het is echter de wens dit explicieter inzichtelijk te maken. Daarnaast is het
ook voor het Technisch Team nuttig om op eenvoudige wijze de configuratie status
van een component na te kunnen kijken. De overzichts pagina van asset informatie
in TOPdesk is hiervoor een voor de hand liggende keuze.

Het script awx\_to\_topdesk.py zal de waarde in het veld _configuratie status_
in TOPdesk importeren in het veld _configuratie status_ in de google sheet.

Het veld _configuratie status_  kan vijf verschillende waardes aannemen. Deze
waardes kunnen zowel handmatig als door een script worden ingesteld.

* inzetbaar

De asset heeft een naam in het format _nn-cmp-1_

Deze naam wordt door TOPdesk aangemaakt door het importeren van assets zonder
naam met behulp van het script asset\_import.py

Vanuit TOPdesk kan deze waarde bij door awx geconfigureerde assets via het
script worden aangepast door de _beoogde\_naam_ weg te halen. De asset krijgt in
dit geval weer een naam in het format _nn-cmp-1_

Wanneer het een asset betreft waarvan de configuratie handmatig is, dient de
status ook handmatig te worden gewijzigd.

* in voorbereiding

De asset heeft een beoogde naam, deze is echter niet gelijk aan de beoogde naam.

Het script asset\_import.py zet vanuit de googlesheet de waarde van het veld
_beoogde\_naam_ in TOPdesk.

Vanuit TOPdesk kan deze waarde bij door awx geconfigureerde assets via het
script worden aangepast door de _beoogde\_naam_ in te vullen.

Wanneer het een asset betreft waarvan de configuratie handmatig is, dient de
status ook handmatig te worden gewijzigd.

* in productie

De asset heeft een naam die gelijk is aan de beoogde naam.

Het script awx\_to\_topdesk.py veranderd na het sussecvol uitrollen van de
configuratie de naam van de asset naar de beoogde naam.

Wanneer het een asset betreft waarvan de configuratie handmatig is, dient de
status ook handmatig te worden gewijzigd.

* in onderhoud

Hiermee geeft een operator aan dat de asset in onderhoud is. Naam en beoogde
naam dienen te worden verwijderd.

Wanneer het een asset betreft waarvan de configuratie handmatig is, dient de
status ook handmatig te worden gewijzigd.

* defect

Hiermee geeft een operator aan dat de asset defect is. Naam en beoogde naam
dienen te worden verwijderd.

Wanneer het een asset betreft waarvan de configuratie handmatig is, dient de
status ook handmatig te worden gewijzigd.

Zie user story: FEM-105

Zie script: [MakeExpose/housekeeping](https://gitlab.com/naturalis/mii/housekeeping)

### Stroomvoorziening

Op de asset informatie pagina in TOPdesk kunnen operators bij assets waarop dat
van toepassing is met betrekking tot de stroomvoorziening aflezen wat het
aansluitpunt, schakelpunt (als hier sprake van is) en de electragroep is waarop
het component op is aangesloten. Hiertoe is de fieldset widget Stroomvoorziening
toegevoegd.

Het _aansluitpunt_ komt overeen met de tekst op het label bij de contactstop
waarmee het apparaat aangesloten is conform de [standaard documentatie
labels](http://docs.museum.naturalis.nl/latest/standaarden/documentatie/labels/).
Het kan hier een wandcontactdoos (WCD), power distibution unit (PDU) of een
tafelcontactdoos (TCD) betreffen.

In het geval dat het component is aangesloten op een PDU is er spraken van een
_schakelpunt_. Dit is het specifieke stopcontact op de PDU.

De waarde _electragroep_ geeft aan van welke zaalverdeler (groepenkast) en
electragroep het component wat stroomvoorziening afhankelijk van is.

In de googlesheet geeft de AV-installateur of TTer alleen het aansluitpunt op
dat hij kan aflezen op het desbetreffende label.

Aan de waarde in het veld aansluitpunt kunnen de waardes van de velden
schakelpunt en electragroep herleiden.

Zie user story: FEM-106

Zie script: [MakeExpose/housekeeping](https://gitlab.com/naturalis/mii/housekeeping)

### Import

Naturalis draagt er zorg voor dat de gegevens worden verwerkt (in TOPdesk &
Ansible) en de status wordt terug gekoppeld.

Dit met behulp van [scripts](https://gitlab.com/naturalis/mii/housekeeping).
