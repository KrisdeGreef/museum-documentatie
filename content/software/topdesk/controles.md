---
title: "Controles"
date: 2018-07-31T13:10:00+02:00
draft: true
---

Voor verschillende zaken willen we scripts maken die controleren of zaken
correct of compleet zijn. Hieronder een overzicht.

## Meldingen

Meldingen maken we op het asset type functionele eenheid. Maar hoe voorkom je
dat mensen meldingen maken op een ander asset type zoals component of op een
groep zoals tentoonstelling?

Wat betreft meldingen gemaakt door eindgebruikers in de self-service desk hebben
we hiervoor een oplossing. In de asset templates in TOPdesk heb je de optie om
bepaalde typen Assets niet te tonen in de Self-service portal. Hiermee stimuleer
je dat meldingen worden gekoppeld aan uitsluitend functionele eenheden.

Voor wat betreft meldingen gemaakt door behandelaars is dit nog niet mogelijk in
TOPdesk. Wel gaat TOPdesk bezig met Permissions per asset type dat op de backlog
staat. Al is het nog afwachten of dit de gewenste functionaliteit gaat
opleveren. Op dit te bevoorderen gaan we onze user case aan TOPdesk bekend
maken.

Een oplossing die we zelf zouden kunnen implementeren is een script dat
meldingen die staan op assets die onderdeel uitmaken van een functionele eenheid
verplaatst naar de functionele eenheid waar ze onderdeel van uitmaken.

Script: Jira 95

## Documentatie component type

Userstory: Als TT wil ik inzicht hebben in de volledigheid van de documentatie
van alle typen componenten zodat ik erop kan vertouwen dat deze ook
daadwerkelijk aanwezig is als ik deze nodig heb.

Voor elk component type maken we automatisch een documenatie pagina aan. Elk
component type maakt onder deel uit van een bepaalde component soort. Per
component soort hebben we een aantal standaard velden zoals vermeld in de
[standaard documentie
component](http://docs.museum.naturalis.nl/latest/standaarden/documentatie/component/).

Naast handmatige controle willen we op een overzichtelijke en fisuele manier
inzichtelijk maken waar de documenatie wat betreft de standaard velden nog niet
compleet is.

Als basis hiervoor moet een script lijsten genereren voor elke soort component.
Waarbij de eerste kolom een lijst is van alle component type gevolgd door een
kolom voor elke standaard specificatie. De waarde in deze kolom is een 1 wanneer
een iets staat ingevuld en een 0 wanneer er niets staat. Op basis hier van
kunnen we een visualisatie maken wat betreft de voortgang van het invullen. Het
gaat hier om een kwantitatieve meting geen kwalitatieve.

Script: Jira 91
