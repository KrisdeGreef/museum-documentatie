---
title: "topdesk"
date: 2018-07-04T13:36:48+02:00
draft: true
---

De centrale administratie van alle assets in het museum zal worden bijgehouden
in TOPdesk. Deze administratie wordt zowel handmatig, als geautomatiseerd
up-to-date gehouden. De automatisering zal zoveel mogelijk via de TOPdesk API
verlopen.

*[TOPdesk API](https://docs.museum.naturalis.nl/latest/software/topdesk/api)
*[Asset Designer](https://docs.museum.naturalis.nl/latest/software/topdesk/asset-designer)
*[Asset Import](https://docs.museum.naturalis.nl/latest/software/topdesk/asset-import)
*[Asset Management Templates](https://docs.museum.naturalis.nl/latest/software/topdesk/asset-management-templates)
*[Call rapportage Assets](https://docs.museum.naturalis.nl/latest/software/topdesk/call-raportage-assets)
*[Controles](https://docs.museum.naturalis.nl/latest/software/topdesk/controles)
*[Vervanging Component](https://docs.museum.naturalis.nl/latest/software/topdesk/vervanging-component)
