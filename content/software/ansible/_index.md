---
title: "ansible awx"
date: 2018-07-04T13:36:48+02:00
draft: true
---

Ansible heeft zich bewezen als een passende config management software voor het
inrichten van fysieke hardware. Ansible AWX is een API en webinterface die gebruikt
kan worden om Ansible scripts mee te beheren en uit te voeren. Daarnaast biedt
AWX ook 'role based access control' (RBAC) zodat bepaalde taken uitsluitend
kunnen worden uitgevoerd door geautoriseerde (groepen van) gebruikers.

* [deployment](https://docs.museum.naturalis.nl/latest/software/ansible/deployment/)
* [scripts](https://docs.museum.naturalis.nl/latest/software/ansible/scripts/)
* [structuur](https://docs.museum.naturalis.nl/latest/software/ansible/structuur/)
