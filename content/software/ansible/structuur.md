---
title: "Structuur"
date: 2018-10-16T09:18:27+02:00
draft: true
weight: 1
---

Voor het configureren en aansturen van apparatuur wordt in het museum gebruik
gemaakt van Ansible en AWX. In dit document staat omschreven waar en hoe alle
variabelen, playbooks en roles worden bewerkt en beheerd en op welke wijze
wijzigingen worden doorgevoerd.

## Algemene indeling

Ansible onderscheidt ruwweg de volgende onderdelen:

* Inventories
* Playbooks
* Roles

In AWX hebben we te maken met:

* Projects (verwijzingen naar repositories)
* Inventories (gelijk aan reguliere Ansible inventories, bron kan een Project of
  repository zijn)
* Job templates (equivalent van een `ansible-playbook` commando met specifieke
  parameters) op basis waarvan Jobs (elke individuele run van een Job template /
  `ansible-playbook`)
* Workflow templates (het conditioneel en achter elkaar uitvoeren van playbooks)

Naturalis hanteert twee typen repositories:

* Een niet-publiek toegankelijke Git repository met daarin alle inventories en
  playbooks.
* Individuele, publiek toegankelijke Git repositories voor roles.

## Centrale repository

Het beheer van de niet-publiek toegankelijke repo met inventories en playbooks
is gebaseerd op het Git flow model. Dit betekent dat in `master` uitsluitend
productie-proof code staat en dat wijzigingen (idealiter op basis van feature
branches) eerst in de `develop` branch worden doorgevoerd.

Door te werken met één in plaats van meerdere repositories voor inventories en
playbooks, kunnen wijzigingen in inventories en/of playbooks gecontroleerd
worden doorgevoerd naar productie, en zijn deze wijzigingen in de Git repo zelf
te volgen. Bovendien is het op basis van één repo makkelijker om handmatig
playbooks af te trappen in het geval van bijv. een netwerkstoring.

In de repository staan inventories en playbooks die betrekking op systemen en
apparaten op de locatie Darwinweg. De [Git
repo](https://gitlab.com/naturalis/darwinweg/ansible-darwinweg) heet om die
reden `ansible-darwinweg`.

In AWX werken we met twee projecten op basis van deze repository:

* Het project `ansible-darwinweg-master`, dat verwijst naar de `master`
  branch.
* Het project `ansible-darwinweg-develop`, dat verwijst naar de `develop`
  branch.

De basis indeling van de repository zelf is als volgt:

```bash
├── inventories
│   ├── production
│   │   ├── group_vars
│   │   └── host_vars
│   └── staging
│       ├── group_vars
│       └── host_vars
├── playbooks
│   ├── museum
│   └── network
└── roles
   └── requirements.yml
```

Hieronder wordt nader ingegaan op de verschillende onderdelen.

## Inventories

Hoewel AWX mogelijkheden biedt voor het dynamisch laden van inventories kiezen
we er (dus) voor om inventories in Git te beheren. Het voordeel van deze
werkwijze is dat alle variabelen in de inventory onder versiebeheer vallen en er
tests uitgevoerd kunnen worden op de instellingen nog voordat ze daadwerkelijk
worden toegepast.

In de repository staan twee inventories; `production` en `staging`. Beide worden
ook in AWX als Inventory aangemaakt:

* `darwinweg-production` is gebaseerd op het `ansible-darwinweg-master` project
  en verwijst naar `inventories/production/hosts`.
* `darwinweg-staging` is gebaseerd op het `ansible-darwinweg-develop` project en
  verwijst naar `inventories/staging/hosts`.

{{% notice note %}}
Om een inventory in AWX te importeren voeg je een Project toe dat verwijst naar
de betreffende Git repo. Vervolgens maak je een Inventory op basis van de
`Sourced from a Project` optie. Door te kiezen voor de optie `Update on Project
Change` wordt de inventory automatisch geupdated wanneer het Project al dan niet
op basis van een schedule wordt geupdated.
{{% /notice %}}

In `darwinweg-production` staan alle systemen en apparaten die een productie
status hebben. In `darwinweg-staging` staan alle systemen en apparaten die ofwel
een min of meer vaste status als staging machine hebben (bijv. voor het
testen van een wijziging in een playbook) of apparaten die ter
voorbereiding op productiestatus van een configuratie moeten worden voorzien.

Wijzigingen in de naamgeving of toepassing van variabelen worden op basis
hiervan als volgt doorgevoerd:

1. Wijzig variabelen in `inventories/staging` in de `develop` branch.
1. Voer evt. automatische tests uit.
1. Pas de wijzigingen toe op de staging hosts.
1. Indien alles naar behoren werkt: pas variableen aan in
   `inventories/production` in de `develop` branch.
1. Voer evt. automatische tests uit.
1. Merge de `develop` branch met `master`
1. Pas de wijzigingen toe op de production hosts.

### Vault

Voor het opslaan van secrets maken we gebruik van [Ansible
Vault](https://docs.ansible.com/ansible/latest/user_guide/vault.html). Bij
secrets die van toepassing zijn op groepen (en evt. hosts) in de inventory
wijken we af van de [best
practice](https://docs.ansible.com/ansible/latest/user_guide/playbooks_best_practices.html#best-practices-for-variables-and-vaults)
omdat Ansible AWX bij het importeren van inventories niet goed omgaat met
volledig versleutelde bestanden. In plaats daarvan maken we gebruik van
[encrypted
strings](https://docs.ansible.com/ansible/latest/user_guide/vault.html#use-encrypt-string-to-create-encrypted-variables-to-embed-in-yaml).

Voor de gehele inventory maken we gebruik van één secret die als Credential
wordt toegevoegd in AWX en die is opgeslagen in Bitwarden.

## Playbooks

Playbooks (en de AWX equivalent Job templates) hebben altijd betrekking op hosts
en/of groups in een inventory en zijn vaak / zoveel mogelijk gebaseerd op
generieke Ansible code in zogeheten *roles*. De playbooks staan in de directory
`playbooks`. In deze directory kan een verdere onderverdeling worden gemaakt,
bijvoorbeeld tussen `museum` en `network`.

Een belangrijk uitgangspunt is dat playbooks steeds per tentoonstelling (of per
functionele eenheid - zoals een interactive) uitgevoerd kunnen worden. Ansible
ondersteunt twee manieren om de scope en werking van playbooks te limiteren:

1. De set aan hosts waar het playbook mee aan de slag gaat kan worden ingeperkt
   met de `--limit` optie.
1. De set aan taken die wordt uitgevoerd door het playbook kan worden ingeperkt
   met de `--tag` optie.

Om er voor te zorgen dat een playbook per tentoonstelling (of functionele
eenheid) wordt uitgevoerd maken we gebruik van de eerste optie. Om dit voor een
tentoonstelling voor elkaar te krijgen zorgen we er voor dat hosts in een [groep](https://docs.ansible.com/ansible/latest/user_guide/intro_inventory.html#hosts-and-groups)
met de naam van de tentoonstelling worden geplaatst. Playbooks kunnen dan als
volgt worden uitgevoerd:

`ansible-playbook --limit 'oerparade' playbooks/museum/start.yml`

Omdat de `--limit` optie ook werkt met
[patterns](https://docs.ansible.com/ansible/latest/user_guide/intro_patterns.html)
kan een playbook zonder gebruik te maken van een groep ook worden toegepast op
een functionele eenheid:

`ansible-playbook --limit 'stamboom-*' playbooks/museum/start.yml`

Op basis van patterns is het daarnaast mogelijk om een playbook toe te passen op
een intersectie van twee groepen. In dit voorbeeld wordt het playbook toegepast
op de mediaplayers in de Oerparade:

`ansible-playbook --limit 'oerparade:&mediaplayers' playbooks/museum/start.yml`

En op alle mediaplayers binnen een functionele eenheid:

`ansible-playbook --limit 'stamboom-*:&mediaplayers' playbooks/museum/start.yml`

Op basis van behoefte zullen [tags](https://docs.ansible.com/ansible/latest/user_guide/playbooks_tags.html)
worden toegevoegd waarmee de plays en tasks binnen een playbook gelimiteerd 
kunnen worden. Tags worden in beginsel niet gebruikt om hosts te selecteren:

`ansible-playbook --limit 'stamboom-*:&mediaplayers' --tag restart playbooks/museum/configure.yml`

## Roles

Voor het hergebruik van playbook code worden Ansible
[roles](https://docs.ansible.com/ansible/latest/user_guide/playbooks_reuse_roles.html)
gebruikt. Zoals hierboven gesteld ontwikkelen en beheren we elke rol in een
aparte publiek toegankelijke Git repository. De naam van de repository is als
volgt opgebouwd: `ansible-<role_name>`. De locatie van de repository hangt af
van de toepassing:

* Repo's van roles die betrekking hebben op het museum worden in de [MakeExpose
  subgroep](https://gitlab.com/naturalis/makeexpose) gezet.
* Repo's van roles die betrekking hebben op het netwerk of andere infrastructuur
  worden in de [Infra subgroep](https://gitlab.com/naturalis/infra) gezet.

Zoals
[hier](https://galaxy.ansible.com/docs/contributing/creating_role.html#role-names)
staat uitgelegd hanteert Galaxy sinds versie 3.0 standaard de naam van de
repository als basis voor de naam van de rol. Voorvoegsels als `ansible-role` en
`ansible` worden niet langer automatisch vervangen. Daarom hanteren we de
standaard dat de naam van de rol in `meta/main.yml` wordt ingesteld met het
`role_name` attribuut.

In de inventory worden afhankelijkheden bijgehouden in het `requirements.yml`
bestand in de roles directory. We hanteren als standaard dat in
`requirements.yml` altijd wordt verwezen naar een specifieke versie / tag van de
rol.

Wanneer er playbooks zijn die gebruik maken van verschillende versies van een
rol hanteren we voor de afwijkende versie een aparte naam die in
`requirements.yml` is toe te voegen, bijvoorbeeld:

```yaml
- src: https://github.com/naturalis/ansible-cumulus-common
  version: nextgen
  name: cumulus-common-nextgen
```

Omdat playbooks in de directory `playbooks` en subdirectories daaronder staan,
en AWX roles uitsluitend installeert in de `roles` directory, zal - totdat daar
een betere oplossing voor is - door middel van relatieve paden worden verwezen
naar roles:

```yaml
- hosts: switches
  roles:
    - role: '../../roles/cumulus-common-nextgen'
```

## Rechten

Een kernfunctie van AWX is het bieden van zogeheten Role Based Access Control
(RBAC) voor het uitvoeren van Ansible playbooks en roles. AWX kent dan ook een
[uitgebreide
structuur](https://docs.ansible.com/ansible-tower/3.4.3/html/userguide/security.html#rbac-ug)
voor het uitdelen van rechten.

Op verschillende objecten en niveau's kunnen individuele gebruikers of teams van
gebruikers rollen worden toegekend. Deze rollen variëren per object. Voorbeelden
zijn: 'Admin', 'Read' en 'Use'.

Bij het uitdelen van rechten hanteren we de volgende uitgangspunten:

* Accounts met system administrator rechten hebben op alle onderdelen de rol
  'System Administrator' en hoeven dus nooit ergens aan te worden toegevoegd.
* Objectpermissies voor de overige accounts worden altijd uitgedeeld door een
  het relevante team waar zij lid van zijn de gewenste rol te geven. Als een
  account in meerdere teams zit, krijgt het de combinatie van aan al die teams
  toegekende rollen.
* Waar mogelijk en relevant worden permissies op een zo hoog mogelijk niveau
  uitgedeeld. Zo is het mogelijk om de 'Use' rol uit de delen op projectniveau
  waardoor het betreffende team ook de 'Use' rol heeft op de 'Job Templates' die
  onderdeel uitmaken van dit 'Project'.
