---
title: "Deployment Ansible AWX"
date: 2018-06-15T14:33:29+02:00
draft: true
weight: 5
---

De deployment van Ansible AWX doen we op OpenStack met behulp van [Docker
Compose](https://docs.docker.com/compose/overview/) en Puppet/Foreman.

De setup bestaat uit:

* `postgres`
* `rabbitmq`
* `memcached`
* `awx_web`
* `awx_task`
* `traefik`

De code voor de deployment vind je
[hier](https://github.com/naturalis/puppet-docker_compose).

## Inrichting

### Credentials (authenticatie voor scripts)

AWX moet toegang krijgen tot de machines die gemanaged gaan worden. Dit kan
onder het kopje `credentials`. In het algemeen zal er een `ssh keypair` gebruikt
worden.

### Ansible playbooks

Onder het kopje projects kunnen Ansible scripts worden toegevoegd. Je kan hier
verwijzen naar een Git repo.

### Inventory

Je kan zelf met de hand een inventory maken. Aan een inventory kun je hosts
koppelen. Om je inventory te hosten op Git, zou je inventory scripts kunnen
gebruiken. Deze moeten dan JSON terugkoppelen. Meer info vind je
[hier](https://docs.ansible.com/ansible-tower/latest/html/administration/custom_inventory_script.html)

### Templates

Een template is een combinatie van:

* credentials
* project
* inventory

Hier mee kun je dus een set van Ansible scripts uitvoeren op een specifieke set
van hosts.

### Gebruikersauthenticatie

Voor het authenticeren van gebruikers is een LDAP koppeling met Active Directory
geconfigureerd. De configuratie zorgt er voor dat:

* Uitsluitend leden van de AD-groep 'Ansible AWX - Login' kunnen inloggen. De
  onderstaande groepen zijn lid van deze groep.
* Leden van de AD-groep 'Ansible AWX - System Administrators' hebben als System
  Administrator volledige rechten binnen AWX.
* Leden van overige AD-groepen 'Ansible AWX - `<teamnaam>`' worden lid van het
  team in AWX met `<teamnaam>`. De rechtenstructuur staat [hier]({{< relref
  "systemen/ansible/structuur/_index.md#rechten" >}}) uitgelegd.

Dit zijn de gebruikte configuratieparameters:

* LDAP Server URI: `ldap://172.16.51.11:389`
* LDAP Bind DN: `cn=Service - LDAP,ou=Service,ou=Gebruikers,dc=nnm,dc=local`
* LDAP Bind Password: See password in Bitwarden
* LDAP Group Type: NestedActiveDirectoryGroupType
* LDAP Require Group: `cn=Ansible AWX - Login,ou=AWX,ou=Resources,ou=Groepen,dc=nnm,dc=local`


* LDAP User Search:

  ```
  [
   [
    "ou=NNM Users,dc=nnm,dc=local",
    "SCOPE_SUBTREE",
    "(mail=%(user)s)"
   ],
   [
    "ou=Administrators,ou=Gebruikers,dc=nnm,dc=local",
    "SCOPE_SUBTREE",
    "(userPrincipalName=%(user)s)"
   ]
  ]
  ```

* LDAP Group Search:

  ```
  [
   "ou=Groepen,dc=nnm,dc=local",
   "SCOPE_SUBTREE",
   "(objectClass=group)"
  ]
  ```

* LDAP User Atrribute Map:

  ```
  {
   "first_name": "givenName",
   "last_name": "sn",
   "email": "mail"
  }
  ```

* LDAP Group Type Parameters:

  ```
  {}
  ```

* LDAP User Flags by Group:

  ```
  {
   "is_superuser": [
    "cn=Ansible AWX - System Administrators,ou=AWX,ou=Resources,ou=Groepen,dc=nnm,dc=local"
   ]
  }
  ```

* LDAP Organization Map:

  ```
  {
   "Naturalis": {
    "admins": false,
    "users": [
     "cn=Ansible AWX - Viewers,ou=AWX,ou=Resources,ou=Groepen,dc=nnm,dc=local",
     "cn=Ansible AWX - Infra,ou=AWX,ou=Resources,ou=Groepen,dc=nnm,dc=local",
     "cn=Ansible AWX - Technisch team,ou=AWX,ou=Resources,ou=Groepen,dc=nnm,dc=local"
    ],
    "remove_users": true
   }
  }
  ```

* LDAP Team Map:

  ```
  {
  "Viewers": {
    "organization": "Naturalis",
    "users": "cn=Ansible AWX - Viewers,ou=AWX,ou=Resources,ou=Groepen,dc=nnm,dc=local",
    "remove": true
  },
  "Infra": {
    "organization": "Naturalis",
    "users": "cn=Ansible AWX - Infra,ou=AWX,ou=Resources,ou=Groepen,dc=nnm,dc=local",
    "remove": true
  },
  "Technisch team": {
    "organization": "Naturalis",
    "users": "cn=Ansible AWX - Technisch team,ou=AWX,ou=Resources,ou=Groepen,dc=nnm,dc=local",
    "remove": true
  }
  }
  ```

## Draaien van playbooks

### Playbook via jumphost

Omdat AWX in het datacenter draait en het opdracten moet uitvoeren op machines
in het museum is een zo genaamde `jumphost` of `bastion host` nodig. Je kan het
volgende toevoegen aan je inventory variablen:

```yaml
---
ansible_ssh_common_args: '-o ProxyCommand="ssh -W %h:%p -q user@jumphost_ip"'
```

## Backup

Dit gebeurd via onze standaard werkwijze bij deployments op OpenStack naar een
bucket op AWS.
