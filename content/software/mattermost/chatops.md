---
title: "Chatops"
date: 2018-07-09T13:58:25+02:00
draft: true
---

Er is de wens binnen het Technisch Team om op een centrale plek te kunnen
communiceren over een functionele eenheid. Hiervoor gebruiken we Mattermost.

Elke functionele eenheid van het type interactive krijgt hiertoe een eigen chat
kanaal (channel) in Mattermost.

Om dit te realiseren hebben we het volgende nodig:

Stap 1:

Lijst van interactives uit Topdesk

Stap 2:

Kanalen aanmaken in Mattermost

## Topdesk

Met behulp van de Topdesk API kunnen we een lijst assets opvragen: [Get
Assets](https://developers.topdesk.com/explorer/?page=assets#/Assets/getAssets)

Aangezien we alleen geinteresseerd zijn in de interactives filteren de
resultaten op basis van de template naam *interactives*

```http
GET https://naturalis-test.topdesk.net/tas/api/assetmgmt/assets?templateName=interactive
```

We krijgen vervolgens een in JSON een lijst met interactives terug.

Voor elke naam achter "text": moeten we een kanaal aan maken.

## Mattermost

In Mattermost is een team aangemaakt voor het museum. Alle kanalen(channels)
zullen hieronder vallen.

API documentatie Mattermost [channels](https://api.mattermost.com/#tag/channels)

Benodige informatie voor het aanmaken van een kanaal:

> team_id
>
> string Required
>
> The team ID of the team to create the channel on

team_id van het technisch team tentoonstellingen

> name
>
> string Required
>
> The unique handle for the channel, will be present in the channel URL

Naam van de interactive

> display_name
>
> string Required
>
> The non-unique UI name for the channel

Naam van de interactive

> purpose
>
> string
>
> A short description of the purpose of the channel

Chat kanaal over *naam_interactive*

> header
>
> string
>
> Markdown-formatted text to display in the header of the channel.

Link naar documentatie pagina:
`http://docs.museum.naturalis.nl/latest/documentatie/interactives/*naam_interactive*`

> type
>
> string Required
>
> 'O' for a public channel, 'P' for a private channel

Zie tooling: [Housekeeping](https://github.com/MakeExpose/housekeeping)
