---
title: "Installatie"
date: 2019-01-25T11:40:00+02:00
draft: false
---

De installatie van [chat.naturalis.io](https://chat.naturalis.io) is een docker-compose
configuratie, bestaande uit de componenten:

 - mattermost (de applicatie)
 - postgres (database)
 - traefik

De docker-compose setup is terug te vinden op [github.com/naturalis/docker-mattermost](https://github.com/naturalis/docker-mattermost).

De installatie wordt met puppet en foreman beheert door infra.

## Beheer

Gebruikers met admin rechten kunnen in mattermost veel instellen en veranderen aan de configuratie.
De echte poweruser kan het beste gebruik maken van de commandline opties van mattermost waarmee
nog meer krachtige dingen kunnen.

 1. Login via ssh op de server
 2. `cd /opt/docker-mattermost`
 3. `docker-compose exec app sh`
 4. `bin/mattermost` gevolgd door een commando

## Handige mattermost commandos

Maak een nieuwe gebruiker aan:

```
bin/mattermost user create --email pietje.puk@naturalis.nl --firstname Pietje --lastname Puk --nick pp --password hunter2 --system_admin --username pietpuk
```

Beheer webhooks

```
bin/mattermost webhook
```

Beheer teams

```
bin/mattermost team
```

Beheer channels

```
bin/mattermost channel list museum
```

Maak een kanaal aan

```
bin/mattermost channel create --team ict-infra --name testje --display_name 'test create'
```




