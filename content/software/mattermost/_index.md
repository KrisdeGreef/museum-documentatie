---
title: "mattermost"
date: 2019-01-25T11:40:00+02:00
draft: false
---

De chat service van het museum draait is installatie van het open source pakket
[Mattermost](https://www.mattermost.org/). Dit pakket is een alternatief voor het populaire Slack,
wat je zelf kan hosten. Voor het museum draaien we dit systeem op het domein
[chat.naturalis.io](https://chat.naturalis.io).

Het doel van deze mattermost installatie is om de medewerkers van infra en support toegang te
geven tot alle discussies en opmerkingen aangaande onderdelen in het nieuwe museum. Als er iets
moet gebeuren of er zijn issues met bepaalde tentoonstellingen of onderdelen daarin kan iedere
medewerker daar teruglezen wat er aan vooraf is gegaan.

Waarschijnlijk willen deze mattermost installatie ook gaan gebruiken voor alle andere chat
communicatie binnen Naturalis, zodat we helemaal van Slack weg kunnen.

*[mattermost](https://docs.museum.naturalis.nl/latest/software/mattermost/installatie/)
*[chatops](https://docs.museum.naturalis.nl/latest/software/mattermost/chatops/)
