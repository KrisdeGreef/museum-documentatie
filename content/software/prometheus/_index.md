---
title: "prometheus"
date: 2019-11-04T16:22:47+01:00
draft: false
status:
---

[Prometheus](https://prometheus.io/docs/introduction/overview/) is een metrics
en alerting systeem dat gebaseerd is op een time series database. Deze database
kan worden doorzocht met
[PromSQL](https://prometheus.io/docs/prometheus/latest/querying/basics/)
 en [gevisualiseerd](https://prometheus.io/docs/visualization/grafana/) met
 tools zoals [Grafana](https://grafana.com/get).

## Exporters

Om gegevens in Prometheus te krijgen maken we gebruik van
[node_exporter](https://github.com/prometheus/node_exporter)
die draait op alle computers in het museum en diverse andere computers binnen
het Naturalis netwerk. Voor het museum maken we daarnaast gebruik van:

* sensu exporter (voor koppeling met Sensu)
* helmpjes exporter (voor het spel schattenjacht)

## Dashboard

Voor Naturalis hebben we veel van de dashboards samengebracht in een Grafana
installatie die draait op
[https://dashboard.analytics.naturalis.io/](https://dashboard.analytics.naturalis.io/). Houd er rekening mee dat dit dashboard alleen te bereiken is via de Edu VPN. Belangrijke dashboards voor het monitoren van het museum zijn:

* Sensu summary
* Museum
* Schattenjacht
* Sensu checks
* Cumulus
