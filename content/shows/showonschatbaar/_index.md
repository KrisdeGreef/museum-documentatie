---
title: "showonschatbaar"
date:
draft: true
status:
unid:
componenten:
- pharos-lpc1
tentoonstellingen:
- zaal10
attachments:
- title: NAT 10 Onschatbaar v275 20210112.upload (pd2)
  src: https://files.museum.naturalis.nl/s/9jPeeomQoojRkP7
---

<!-- Voeg een overzichtsafbeelding of blokschema toe -->

## Functionele omschrijving

<!-- Voeg hier een functionele omschrijving van de show toe -->

<!-- Voeg indien beschikbaar een tijd-volgorde-toestands diagram toe -->

## Technische omschrijving

<!-- Voeg hier een blokschema toe -->

<!-- Voeg hier een technische omschrijving toe -->

Deze show bestaat uit de volgende componenten:

{{< pagelist componenten >}}

## Configuratie

<!-- Voeg hier een overzicht van handmatig ingestelde parameters toe -->

<!-- Voeg hier een verwijzing naar de configuratie in Ansible toe -->

## Handleidingen en procedures

<!-- Voeg hier verwijzingen naar gebruikers- en beheerdershandleidingen en
procedures toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Afspraken en verantwoordelijkheden

<!-- Voeg hier een overzicht van verantwoordelijken en afspraken omtrent de
show toe -->

### Contact

<!-- Interne contactpersonen -->

### SLA

<!-- SLAs (met link naar Topdesk) -->

### Bouwer

<!-- Naam bouwer shows (met link naar Topdesk)-->

### Ontwerper

<!-- Naam ontwerper shows (met link naar Topdesk)-->

## Bijlagen

{{< pagelist bijlagen >}}
