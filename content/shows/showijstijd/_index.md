---
title: "showijstijd"
date: 2019-07-24T13:49:18Z
status:
unid: d4ae9c0b-e1d2-4ef5-8702-f29e5db2ef82
componenten:
- mpv
- crestron-audext100
- dapaudio-pra82
- esi-gigaporthdplus
- hager-svn311
- intel-nuc8i3beh
- meanwell-hdr1512
- srs-dsr10n
- visualproductions-cuecore2
tentoonstellingen:
 - deijstijd
attachments:
- title: 20190829_specifiek_kabelschema_4_showijstijd_v02 (pdf)
  src: https://files.museum.naturalis.nl/s/CfpB2oSmt5xYRN8
- title: 20190829_specifiek_kabelschema_4_showijstijd_v02 (vwx)
  url: https://files.museum.naturalis.nl/s/qqkatcAFCR3kDyM
- title: Cuecore2 configuratiebestand showijstijd-ctl-1
  src: https://files.museum.naturalis.nl/s/FtME4GotmbNmkGG
---

<!-- Voeg een overzichtsafbeelding of blokschema toe -->

## Functionele omschrijving

<!-- Voeg hier een functionele omschrijving van de show toe -->

<!-- Voeg indien beschikbaar een tijd-volgorde-toestands diagram toe -->

## Technische omschrijving

<!-- Voeg hier een blokschema toe -->

<!-- Voeg hier een technische omschrijving toe -->

Deze show bestaat uit de volgende componenten:

{{< pagelist componenten >}}

## Configuratie

<!-- Voeg hier een overzicht van handmatig ingestelde parameters toe -->

<!-- Voeg hier een verwijzing naar de configuratie in Ansible toe -->

## Handleidingen en procedures

<!-- Voeg hier verwijzingen naar gebruikers- en beheerdershandleidingen en
procedures toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Afspraken en verantwoordelijkheden

<!-- Voeg hier een overzicht van verantwoordelijken en afspraken omtrent de
show toe -->

### Contact

<!-- Interne contactpersonen -->

### SLA

<!-- SLAs (met link naar Topdesk) -->

### Bouwer

<!-- Naam bouwer shows (met link naar Topdesk)-->

### Ontwerper

<!-- Naam ontwerper shows (met link naar Topdesk)-->

## Bijlagen

{{< pagelist bijlagen >}}
