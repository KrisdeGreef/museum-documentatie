---
title: "showjapanstheater"
date: 2019-07-24T13:49:18Z
draft: true
status:
unid: b9a80c32-352b-41a8-8d67-3811e7150a0a
componenten:
- ecler-nxa4200
- hager-svn311
- apple-macmini
- meanwell-hdr1512
- motu-24ao
- pharos-lpc1
- qsc-acs4tbk
- qsc-adc4tbk
- qsc-kw181
- srs-dsr10n
tentoonstellingen:
experiences:
- japanstheater
attachments:
- title: 20190829_specifiek_kabelschema_6_showjapanstheater_v04 (pdf)
  src: https://files.museum.naturalis.nl/s/RSwW6E4gZ29yWmB
- title: 20190829_specifiek_kabelschema_6_showjapanstheater_v04 (vwx)
  url: https://files.museum.naturalis.nl/s/oAmqi9XLJwf5cHA
- title: Pharos configuratiebestand showjapanstheater-ctl-1
  src: https://files.museum.naturalis.nl/s/czQ2BwJYdxAMQpz
- title: Japans_Theater_QLab_productie-20200520 (qlab4)
  src: https://files.museum.naturalis.nl/s/xGqnkf9ymQwi4Qp
---

<!-- Voeg een overzichtsafbeelding of blokschema toe -->

## Functionele omschrijving

<!-- Voeg hier een functionele omschrijving van de show toe -->

<!-- Voeg indien beschikbaar een tijd-volgorde-toestands diagram toe -->

## Technische omschrijving

<!-- Voeg hier een blokschema toe -->

<!-- Voeg hier een technische omschrijving toe -->

De audio van de show draait op basis van QLab op een Apple Mac Mini. De Mac
Mini is via VNC te benaderen (zie Bitwarden voor credentials). Na het opstarten
van de computer wordt het juiste qlab4 showbestand geladen via de instelling
`Apple-logo > Systeemvoorkeuren > Gebruikers en groepen > showjapanstheater >
Inloggen`
waarin een verwijzing staat naar het bestand
`Japans_Theater_QLab_productie.qlab4`.

Deze show bestaat uit de volgende componenten:

{{< pagelist componenten >}}

## Configuratie


### MacMini (Qlab)
De audio in het theater wordt volledig door de Macmini met Qlab aangestuurd. Deze Macmini wordt
niet beheerd door Ansible en zal dus dag en nacht aan blijven staan. Daarom herstaren we de Mac elke nacht.

Deze scripts en instellingen zijn hiervoor gebruikt:

1. Een Automator applicatie die alle applicaties netjes afsluit zonder wijzigingen
op te slaan en vervolgens de Mac herstart

  * Naam: `auto_restart.app`
  * Locatie: `/Users/showjapanstheater/Documents/`
  * Automator code:

![auto_restart](./auto_restart.png)


2. Een LaunchDaemon om deze Automator applicatie op te starten (Launchd is te vergelijken met Systemd)

  * Naam: `naturalis.nl.auto_restart.plist`
  * Locatie: `/Library/LaunchDaemons/`
  * XML code:

```XML
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<dict>
        <key>Label</key>
        <string>naturalis.nl.auto_restart</string>
        <key>ProgramArguments</key>
	<array>
		<string>open</string>
		<string>-a</string>
		<string>/Users/showjapanstheater/Documents/auto_restart.app</string>
	</array><key>StartCalendarInterval</key>
        <dict>
                <key>Hour</key>
                <integer>3</integer>
                <key>Minute</key>
                <integer>00</integer>
        </dict>
</dict>
</plist>
```

LaunchDaemons kan je op deze manier uit en aanzetten:

  * LaunchDaemon aanzetten voor deze keer (overleeft een herstart niet)

    `sudo launchctl load /Library/LaunchDaemons/nl.naturalis.auto_restart.plist`

  * LaunchDaemon aanzetten, actief ook na herstart:

    `sudo launchctl load -w /Library/LaunchDaemons/nl.naturalis.auto_restart.plist`

  * LaunchDaemon uitzetten voor deze keer (overleeft een herstart niet)

    `sudo launchctl unload /Library/LaunchDaemons/nl.naturalis.auto_restart.plist`

  * LaunchDaemon uitzetten, actief ook na herstart:

    `sudo launchctl unload -w /Library/LaunchDaemons/nl.naturalis.auto_restart.plist`

3. Login Items ingesteld voor de User showjapanstheater

  * Ingesteld dat de user showjapanstheater automatisch inlogt bij het opstarten (Appletje > Systempreferences > Users & Groups)

![loginoptions](./loginoptions.png)

  * Ingesteld dat wanneer de user showjapanstheater inlogt het document automatisch opstart (Appletje > Systempreferences > Users & Groups)

![loginitems](./loginitems.png)

## Handleidingen en procedures

<!-- Voeg hier verwijzingen naar gebruikers- en beheerdershandleidingen en
procedures toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Afspraken en verantwoordelijkheden

<!-- Voeg hier een overzicht van verantwoordelijken en afspraken omtrent de
show toe -->

### Contact

<!-- Interne contactpersonen -->

### SLA

<!-- SLAs (met link naar Topdesk) -->

### Bouwer

<!-- Naam bouwer shows (met link naar Topdesk)-->

### Ontwerper

<!-- Naam ontwerper shows (met link naar Topdesk)-->

## Bijlagen

{{< pagelist bijlagen >}}
