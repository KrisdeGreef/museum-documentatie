---
title: "showlivescience"
date: 2019-07-24T13:49:19Z
draft: false
status:
unid: b388a921-743d-48b5-a441-94da854077fb
componenten:
- hager-svn311
- meanwell-hdr1512
- pharos-lpc1
tentoonstellingen:
- livescience
attachments:
- title: Pharos configuratiebestand showlivescience-ctl-1
  src: https://files.museum.naturalis.nl/s/jPYKjioqWf8KXDT
- title: Pharos configuratiebestand showlivescience-ctl-2 (studiolab)
  src: https://files.museum.naturalis.nl/s/8gXBnk2rrEFKJWq
---

<!-- Voeg een overzichtsafbeelding of blokschema toe -->

## Functionele omschrijving

<!-- Voeg hier een functionele omschrijving van de show toe -->

<!-- Voeg indien beschikbaar een tijd-volgorde-toestands diagram toe -->

## Technische omschrijving

<!-- Voeg hier een blokschema toe -->

<!-- Voeg hier een technische omschrijving toe -->

Deze show bestaat uit de volgende componenten:

{{< pagelist componenten >}}

## Configuratie

<!-- Voeg hier een overzicht van handmatig ingestelde parameters toe -->

<!-- Voeg hier een verwijzing naar de configuratie in Ansible toe -->

## Handleidingen en procedures

<!-- Voeg hier verwijzingen naar gebruikers- en beheerdershandleidingen en
procedures toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Afspraken en verantwoordelijkheden

<!-- Voeg hier een overzicht van verantwoordelijken en afspraken omtrent de
show toe -->

### Contact

<!-- Interne contactpersonen -->

### SLA

<!-- SLAs (met link naar Topdesk) -->

### Bouwer

<!-- Naam bouwer shows (met link naar Topdesk)-->

### Ontwerper

<!-- Naam ontwerper shows (met link naar Topdesk)-->

## Bijlagen

{{< pagelist bijlagen >}}
