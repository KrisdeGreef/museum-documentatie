---
title: "showrexperience"
date: 2019-07-24T13:49:19Z
draft: true
status:
unid: 62fdfce6-297b-49aa-afd8-b03a5731f77e
componenten:
- powersoft-o1204dsp
- pharos-lpc1
- pharos-lpc2
- soliddrive-sd250
- soliddrive-sd1g
- meyersound-mps488hp
- powersoft-m28q
- powersoft-ottocanali1204dsp
- powersoft-quattrocanali4804
- elcer-ehsa4150
- ecler-mpage4
- ecler-dam614
- iiyama-tf2738mscb1
- prodvx-sd15
- apple-macmini2018
- kramer-vm3h2
- kramer-tp590txr
- kramer-tp590rxr
- kramer-tp580t
- kramer-tp580r
- sonnet-xmacmsat3e
- motu-avbswitch
- motu-24ao
- hikvision-ds7608nii2p8
- hikvision-ds7604nik14p
- startech-sv431dhd4ku
- startech-fcreadmicro3
- startech-usb2001ext2
- startech-st4300usb3
- startech-spdifcoaxtos
- startech-hb30c3a1cst
- cherry-mc1000
- qsc-acs4tbk
- voiceacoustic-alea4
- martinaudio-cdd5
- srs-dsr10n
- apc-smt1500rmi2unc
- apc-smx2200hvnc
- intel-nuc8i3beh
- focusrite-6i6
- apartaudio-ovo5
- microsonic-mic130dtc
- jbl-ac2826
- jbl-acseries
- jbl-control14ct
- jbl-control19cs
- jbl-control24c
- kissbox-io3cc
- qsc-ks112
- qsc-acs4tbk


tentoonstellingen:
experiences:
- rexperience
attachments:
- title: REXperience MAIN 20200821 (pd2)
  src: https://files.museum.naturalis.nl/s/a4jbTen5Dy9DCPz
- title: REXperience SHOW 20200821 (pd2)
  url: https://files.museum.naturalis.nl/s/9LQiEdpzpHe4Bcb
- title: Rexperiece Qlab V04-01 (qlab4)
  url: https://files.museum.naturalis.nl/s/bjy5D4FcrFGYAL6
---

<!-- Voeg een overzichtsafbeelding of blokschema toe -->

## Functionele omschrijving

<!-- Voeg hier een functionele omschrijving van de show toe -->

<!-- Voeg indien beschikbaar een tijd-volgorde-toestands diagram toe -->

## Technische omschrijving

Het VLAN van de rexperience heeft het subnet 10.138.0.0/16. Hij heeft 2 showcontrollers (Pharos). De Main controller (10.138.1.55) is voor de algemene besturing: alles initieel goed zetten, reageren op de Pharos touchscreens zoals deuren open, etc.
De Main controller heeft een zwarte achtergrond in de web-interface om verwarring te voorkomen.

De Show controller (10.138.1.56) draait de show. Deze verdeling is gemaakt omdat het allemaal teveel werd voor één Pharos.
De Show controller heeft, zoals gewoonlijk, een witte achtergrond in de web-interface.

Deze show bestaat uit de volgende componenten:

{{< pagelist componenten >}}

## Configuratie

### MacMini (Qlab)
De audio in de Rexperience wordt volledig door de Macmini met Qlab aangestuurd. Deze Macmini wordt
niet beheerd door Ansible en zal dus dag en nacht aan blijven staan. Dit heeft al
gezorgd voor een hangende Qlab in de testfase. Daarom herstaren we de Mac elke nacht.

Deze scripts en instellingen zijn hiervoor gebruikt:

1. Een Automator applicatie die alle applicaties netjes afsluit zonder wijzigingen
op te slaan en vervolgens de Mac herstart

  * Naam: `auto_restart.app`
  * Locatie: `/Users/trex/Documents/`
  * Automator code:

![auto_restart](./auto_restart.png)


2. Een LaunchDaemon om deze Automator applicatie op te starten (Launchd is te vergelijken met Systemd)

  * Naam: `naturalis.nl.auto_restart.plist`
  * Locatie: `/Library/LaunchDaemons/`
  * XML code:

```XML
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<dict>
        <key>Label</key>
        <string>naturalis.nl.auto_restart</string>
        <key>ProgramArguments</key>
	<array>
		<string>open</string>
		<string>-a</string>
		<string>/Users/trex/Documents/auto_restart.app</string>
	</array><key>StartCalendarInterval</key>
        <dict>
                <key>Hour</key>
                <integer>3</integer>
                <key>Minute</key>
                <integer>15</integer>
        </dict>
</dict>
</plist>
```

LaunchDaemons kan je op deze manier uit en aanzetten:

  * LaunchDaemon aanzetten voor deze keer (overleeft een herstart niet)

    `sudo launchctl load /Library/LaunchDaemons/nl.naturalis.auto_restart.plist`

  * LaunchDaemon aanzetten, actief ook na herstart:

    `sudo launchctl load -w /Library/LaunchDaemons/nl.naturalis.auto_restart.plist`

  * LaunchDaemon uitzetten voor deze keer (overleeft een herstart niet)

    `sudo launchctl unload /Library/LaunchDaemons/nl.naturalis.auto_restart.plist`

  * LaunchDaemon uitzetten, actief ook na herstart:

    `sudo launchctl unload -w /Library/LaunchDaemons/nl.naturalis.auto_restart.plist`

3. Login Items ingesteld voor de User Trex

  * Ingesteld dat de user Trex automatisch inlogt bij het opstarten (Appletje > Systempreferences > Users & Groups)

![loginoptions](./loginoptions.png)

  * Ingesteld dat wanneer de user Trex inlogt het document automatisch opstart (Appletje > Systempreferences > Users & Groups)

![loginitems](./loginitems.png)

### MOTU

In Qlab heb je rechts onder een tandwiel, hierin kan je instellingen wijzigen.
Om te controleren of de MOTU's goed zijn ingesteld moet je hier wezen.

De juiste instellingen:

![motu](./motu.png)

## Handleidingen en procedures

<!-- Voeg hier verwijzingen naar gebruikers- en beheerdershandleidingen en
procedures toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Afspraken en verantwoordelijkheden

<!-- Voeg hier een overzicht van verantwoordelijken en afspraken omtrent de
show toe -->

### Contact

<!-- Interne contactpersonen -->

### SLA

<!-- SLAs (met link naar Topdesk) -->

### Bouwer

<!-- Naam bouwer shows (met link naar Topdesk)-->

### Ontwerper

<!-- Naam ontwerper shows (met link naar Topdesk)-->

## Bijlagen

{{< pagelist bijlagen >}}
