---
title: "lightworks-lira2red"
date:
draft: false
merk: Lightworks
model: Lira2 Red
leverancier: Lightworks
attachments:
- title: Lightworks Lira2 Red (pdf)
  src: https://files.museum.naturalis.nl/s/HtfnQyZ6n8rWSyH

---

![lira2red](./lira2red.png)
Red LED IP65 wall mounted guide light

## Eigenschappen


## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
