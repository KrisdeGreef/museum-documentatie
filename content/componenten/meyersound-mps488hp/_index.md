---
title: "meyersound-mps488hp"
date:
draft: false
merk: meyer sound
model: mps488hp
leverancier:
shows:
- showrexperience
attachments:
- title: OPERATING INSTRUCTIONS
  src: https://files.museum.naturalis.nl/s/5iowoYXZzNeGmdy
- title: Technische specificaties
  src: https://files.museum.naturalis.nl/s/ozL33cpQxg6DDGc
---

![mps488hp](./mps488hp.png)
The MPS‑488HP IntelligentDC power supply delivers power and balanced audio to up to eight Meyer Sound loudspeakers that require an external DC power supply, such as [MM-4XP](https://docs.museum.naturalis.nl/latest/componenten/meyersound-mm4xp)

## Eigenschappen

* Afmetingen:

  * Hoogte: 44 mm
  * Breedte: 483 mm (19")
  * Diepte: 364 mm

* Gewicht: 6,6 kg

## Technische specificaties

* Output Voltage: 48 V DC per channel (with intelligent circuit protection against surges and shorts)

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Shows

Dit scherm wordt gebruik in de shows:

{{< pagelist shows >}}


## Bijlagen

{{< pagelist bijlagen >}}
