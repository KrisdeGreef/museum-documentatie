---
title: "eldoled-powerdrive562A"
date:
draft: false
merk: eldoLED
model: POWERdrive 562A
leverancier:
attachments:
- title: EldoLED POWERdrive 562A 3ch (pdf)
  src: https://files.museum.naturalis.nl/s/mNtLCTAweiYLxpC

---

![powerdrive562A](./powerdrive562A.png)
50W DMX/RDM Full-Colour (RGB) Dimmable LED Driver

## Eigenschappen

* Afmetingen:
  * Diepte: 152,5 mm
  * Hoogte: 30,1 mm
  * Breedte: 76 mm
* Gewicht: 372 gr


## Technische specificaties

POWERdrive 562/A delivers 50W and is DMX compatible. The end cap functions as a wire restraint and makes the driver perfectly suited for independent installations. POWERdrive 562 has three separately controllable LED outputs, which can be used for wide range of full-colour (RGB), dynamic LED lighting applications.

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
