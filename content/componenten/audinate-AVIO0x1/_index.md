---
title: "audinate-AVIO0x1"
date:
draft: false
merk: audinate
model: AVIO 0x1
leverancier:
attachments:
- title: Dante-AVIO-Adapters-Datasheet_v1.2-en.pdf
  src: https://files.museum.naturalis.nl/s/nLQ3eNMDcDF5wRL
---

![dao_au_0x1](./dao_au_0x1.jpg)
RJ45 - Analog Output 1 Ch adapter

## Eigenschappen

* Input:
  - RJ45 / Dante over IP
* Output:
  - XLR analog out


## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
