---
title: "datavideo-dac70"
date: 2019-12-03T14:50:03+01:00
draft: false
merk: Datavideo
model: DAC-70
leverancier:
interactives:
- studiolab
attachments:
- title: datavideo-dac70_handleiding.pdf
  src: https://files.museum.naturalis.nl/s/7gEwH2rKbFZHJ3R
---
![DAC-70](./DAC-70.png "DAC-70")
![dac-70-2](./dac-70-2.png "dac-70-2")
Up / Down / Cross Video Converter

## Eigenschappen

* Vermogen: onder 6 W
* Afmetingen:

  * Hoogte: 45.2mm
  * Breedte: 85mm
  * Diepte: 144.8mm

* Gewicht:

## Technische specificaties

* Input:
  * VGA x 1
  * HDMI x 1
  * HD/SD-SDI x 1
  * Analog Audio L/R

* Output:
  * SDI x 2
  * HDMI x 1
  * SDI Loop thru BNC(front panel)

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.


## Interactives

  Dit component wordt gebruik in de interactives:

  {{< pagelist interactives >}}


## Bijlagen

{{< pagelist bijlagen >}}
