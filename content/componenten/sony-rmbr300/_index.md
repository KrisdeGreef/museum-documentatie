---
title: "sony-rmbr300"
date: 2019-12-03T14:50:09+01:00
draft: false
merk: Sony
model: rmbr300
leverancier:
interactives:
- studiolab
attachments:
- title: sony-rmbr300_handleiding.pdf
  src: https://files.museum.naturalis.nl/s/YrPXKDXmPjcoGy2
---

![rmbr300](./rmbr300.jpg)
Sony RM-BR300 controller t.b.v. de Sony PTZ camera’s. Een gemakkelijk besturingssysteem voor de bijhorende PTZ camera’s.

## Eigenschappen

* Vermogen: 2,4 W
* Afmetingen:

  * Hoogte: 165 mm
  * Breedte: 391,3 mm
  * Diepte: 145,9 mm

* Gewicht: 950 g

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Interactives

{{< pagelist interactives >}}


## Bijlagen

{{< pagelist bijlagen >}}
