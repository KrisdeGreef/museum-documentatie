---
title: "wago-221613"
date: 2019-12-03T14:50:10+01:00
draft: false
merk: Wago
model: 221613
leverancier:
attachments:
- title: wago-221613_gegevensblad.pdf
  src: https://files.museum.naturalis.nl/s/XPYzN7YfyHRBtL9
---

![221613](./221613.png)
COMPACT-verbindingsklem; voor alle soorten geleiders

## Eigenschappen

* Vermogen:
* Afmetingen:

  * Hoogte:10,1 mm
  * Breedte: 22,9 mm
  * Diepte: 21,1 mm

* Gewicht: 4,014 g

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
