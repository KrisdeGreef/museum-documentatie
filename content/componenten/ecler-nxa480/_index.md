---
title: "ecler-nxa480"
date:
draft: false
merk: ecler
model: nxa480
leverancier:
shows:
- showjapanstheater
attachments:
- title: User Manual
  src: https://files.museum.naturalis.nl/s/tjSjyo2o8QdSM5R
- title: Data Sheet
  src: https://files.museum.naturalis.nl/s/8RXKr8MpwED3Wrd
---

![nxa480](./nxa480.png)
![nxa480-rear](./nxa480-rear.png)

NXA4-80  is  a  4  channel  self-powered  digital  manager  that  stays  halfway  between  a  digital  matrix  and  a multichannel  amplifier.  An  "all-in-one"  digital  audio  device  that  becomes  a  complete  audio  solution,  including several remote control options and a long list of smart features.

## Eigenschappen

* Versterkerklasse:
* Aantal speaker kanalen: 4 powered audio outputs
* RMS vermogen:
  * 1 Channel @ 4 Ohm (RMS)	85W
  * 1 Channel @ 8 Ohm (RMS)	51W
  * All Channels @ 4 Ohm (RMS)	66W
  * All Channels @ 8 Ohm (RMS)	43W
  * 1 Bridge channel @ 8Ohm (RMS) 160W
* Broningangen: 4 analogue audio inputs
* Afmetingen: 482,6 x 88 x 373 mm
* Gewicht: 9,2 kg

## Technische specificaties

* 4 analogue audio inputs x 4 powered audio outputs
* Class D amplifiers (eco friendly)
* Auto stand-by function (eco friendly)
* 100% silent (fanless convection cooling system)
* Health self-test mode function, with FAULT RELAY (for an external redundancy system)
* Integrated anti-clip system
* Integrated DSP processor. Main features:
  * Inputs mixer independent per channel (all inputs available)
  * VOLUME, MUTE, SOLO, PHASE INVERSION, MAX. VOL limit and MIN.VOL limit, LP and HP Crossover filters, parametric EQ filters bank, Ducker, Delay, Compressor and more settings configurable per channel.
 * Ethernet interface, compatible with EclerNet Manager platform and UCP remote control system
 * TP-NET third-party remote control (compatible with CRESTRON®, AMX®, RTI®, VITY®, etc.)


## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Show

Dit component wordt in de volgende shows gebruikt:
{{< pagelist shows >}}

## Bijlagen

{{< pagelist bijlagen >}}
