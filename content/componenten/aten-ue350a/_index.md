---
title: "aten-ue350a"
date: 2019-12-03T14:50:02+01:00
draft: false
merk: Aten
model: ue350a
leverancier:
attachments:
- title: aten-ue350a_gegevensblad.pdf
  src: https://files.museum.naturalis.nl/s/EXBDRxSwcpNnSDQ
---

![USB verleng kabel](./15110.jpg "USB verleng kabel")
<!-- Voeg een algemene omschrijving van het component toe -->

## Eigenschappen

* Type: USB 3.0 verleng kabel
* Connector: USB 3 Type A
* Lengte: up to 5m
* Kabeldikte:

* Gewicht:

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
