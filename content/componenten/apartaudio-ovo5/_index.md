---
title: "apartaudio-ovo5"
date:
draft: false
merk:
model:
leverancier:
shows:
  - showrexperience
attachments:
- title: rexperience_hardware_avh_speakers_handleiding_ovo5 (pdf)
  src: rexperience_hardware_avh_speakers_handleiding_ovo5
---

![ovo5](./ovo5.png)


## Eigenschappen

* Afmetingen:

  * Hoogte: 260 mm
  * Breedte: 170 mm
  * Diepte: 180 mm

* Gewicht: 5,7 kg (paar)
* Power RMS: 80 W/8 Ohm

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Show

Dit component wordt in de volgende shows gebruikt:
{{< pagelist shows >}}

## Bijlagen

{{< pagelist bijlagen >}}
