---
title: "roblights-framingspot"
date:
draft: false
merk: Roblight
model: Framing Spot
leverancier: Q-Cat
attachments:
- title: Roblight Framing Spot (pdf)
  src: https://files.museum.naturalis.nl/s/LjztRoH5Toy8tDq
- title: Roblight Framing Spot manual (pdf)
  src: https://files.museum.naturalis.nl/s/PfzwZbdW4JTY8jb
---

![framingspot](./framingspot.png)

## Eigenschappen

* Afmetingen:
  * Diepte: 130 mm
  * Hoogte: 106 mm
  * Breedte: 68 mm


## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
