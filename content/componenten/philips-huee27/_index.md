---
title: "philips-huee27"
date:
draft: false
merk: Philips
model: Hue E27
leverancier: De Cirkel
attachments:
- title: Philips Hue E27 (pdf)
  src:

---

![huee27](./huee27.png)

## Eigenschappen

* Afmetingen:
  * Diameter: 62 mm
  * Hoogte: 79/107 mm


## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
