---
title: "iiyama-tf2234mcb5x"
date: 2019-12-03T14:50:04+01:00
draft: false
merk: iiyama
model: ProLite tf2234mcb5x
leverancier:
interactives:
  - bewakingjapanstheater
  - datingquiz
  - digibieb
  - expeditietent
  - waternesten
attachments:
- title: iiyama-tf2234mcb5x_gegevensblad.pdf
  src: https://files.museum.naturalis.nl/s/5C7LTwaLFSWBRbm
- title: iiyama-tf2234mcb5x_handleiding.pdf
  src: https://files.museum.naturalis.nl/s/3xHKMD4zREmCoBj
---

![tf2234mcb5x](./tf2234mcb5x.jpg "tf2234mcb5x")
Open Frame PCAP 10 point touch screen voorzien van een schuimrubberen afdichting vooreen naadloze (kiosk) integratie

## Eigenschappen

* Video-in:
  * 1 x Displayport
  * 1 x HDMI
  * 1 x VGA
* Bediening: touch
* Schermdiagonaal: 21.5", 55cm
* Resolutie: 1920 x 1080 (2.1 megapixel Full HD)
* Beeldverhouding: 16:9
* Vermogen: 18 W
* Kijkhoek: horizontal/vertical: 178°/178°, right/left: 89°/89°, up/down: 89°/89°
* Afmetingen: 517,5 x 313,5 x 46 mm
* Gewicht: 4,4kg

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Interactives

Dit component wordt gebruik in de interactives:

{{< pagelist interactives >}}

## Bijlagen

{{< pagelist bijlagen >}}
