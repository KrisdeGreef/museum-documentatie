---
title: "starview-sv231usbgb"
date: 2019-12-03T14:50:10+01:00
draft: false
merk: Starview
model: SV231USB
leverancier:
interactives:
- studiolab
attachments:
- title: starview-sv231usbgb_handleiding.pdf
  src: https://files.museum.naturalis.nl/s/5GHb2bwSbjoyZeP
---

![SV231USB](./SV231USB.png)
![SV231USB_rear](./SV231USB_rear.png)
KVM switch met VGA en USB.

## Eigenschappen

* Vermogen:
* Afmetingen:

  * Hoogte: 40 mm
  * Breedte: 124 mm
  * Diepte: 70 mm

* Gewicht: 420 g

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Interactives

{{< pagelist interactives >}}

## Bijlagen

{{< pagelist bijlagen >}}
