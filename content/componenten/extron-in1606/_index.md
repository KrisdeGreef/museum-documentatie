---
title: "extron-in1606"
date: 2019-12-03T14:50:04+01:00
draft: false
merk:
model:
leverancier:
attachments:
- title: Specifications_IN1606.pdf
  src: https://files.museum.naturalis.nl/s/HPNH36DEQsCmtEj
- title: extron-in1606_handleiding.pdf
  src: https://files.museum.naturalis.nl/s/JYd4oCWbsAtARg9
- title: 68-2290-01_I_IN1606-1608_UG.pdf
  src: https://files.museum.naturalis.nl/s/CMxWo5XFAzeKatx

---

![in1606](./in1606.jpg "in1606")
The Extron IN1606 Scaling Presentation Switcher is a six input, HDCP-compliant video scaler that accepts a wide variety of audio and video formats.

## Eigenschappen

* Vermogen: 26 W
* Afmetingen:

  * Hoogte: 44 mm
  * Breedte: 444 mm
  * Diepte: 241 mm

* Gewicht: 2,2 KG

## Technische specificaties

Zie [specificaties](https://files.museum.naturalis.nl/s/HPNH36DEQsCmtEj).

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
