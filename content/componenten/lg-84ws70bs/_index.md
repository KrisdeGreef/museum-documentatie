---
title: "lg-84ws70bs"
date: 2019-12-03T14:50:06+01:00
draft: false
merk: LG
model: 84ws70bs
leverancier:
interactives:
- studiolab
attachments:
- title: lg-84ws70bs_handleiding.pdf
  src: https://files.museum.naturalis.nl/s/2sYwwL8i6LLbpry
---

![84ws70bs](./84ws70bs.png "84ws70bs")
84'' class (84.04'' diagonal) LED Widescreen Ultra HD Display

## Eigenschappen

* Video-in:
  * 1 x Displayport
  * 2 x HDMI
  * 1 x DVI-D
  * RGB (analog)
  * Composite (analog)
* Bediening:
  * RS-232c x1 (Get/Set Command)
  * RJ45 (LAN) x1
  * IR 1x
* Schermdiagonaal: 84" (84.04" measured diagonally)
* Resolutie: 3,840 x 2,160 (UHD)
* Beeldverhouding: 16:9
* Vermogen: 350W
* Kijkhoek:
* Afmetingen: 1922 x 1109 x 101 mm
* Gewicht: 87,1 KG

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Interactives

Dit component wordt gebruik in de interactives:

{{< pagelist interactives >}}



## Bijlagen

{{< pagelist bijlagen >}}
