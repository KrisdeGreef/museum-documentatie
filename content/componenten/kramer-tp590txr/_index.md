---
title: "kramer-tp590txr"
date: 2019-12-03T14:50:05+01:00
draft: false
merk: kramer
model: tp590txr
leverancier:
interactives:
- camarasaurus
- cirkelprojectie
- edmontosaurus
- plateosaurus
- projectiebeweging
- projectiediepzee
- projectiegetijden
- projectiegiraf
- projectiekleinedieren
- projectieluchtdieren
- projectieolifant
- projectiewaterplaats
- stegosaurus
- trex
- triceratops
- waterwand
shows:
- showrexperience
attachments:
- title: kramer-tp590txr_handleiding.pdf
  src: https://files.museum.naturalis.nl/s/367oYS2sr6iqSTo
- title: TP-590TXR Datasheet.pdf
  src: https://files.museum.naturalis.nl/s/MDyb7XoBZH56XHe
- title: rexperience_hardware_extender_handleidingquickstart_kramer_tp-590tx (pdf)
  src: https://files.museum.naturalis.nl/s/jTPxtQM6qMcEdCj
---

![tp-590txr](./tp-590txr.png "tp-590txr")
HDBaseT 2.0 transmitter for 4K60Hz (4:2:0) HDMI, USB, Ethernet, RS−232, IR and stereo audio signalsover twisted pair. The TP−590TXR converts all inputsignals into the transmitted HDBaseT 2.0 signal. It extends video signals to up 100m (330ft) over CATcopper cables at up to 4K@60Hz (4:2:0) 24bpp videoresolution and provides even further reach for lowerHD video resolutions

## Eigenschappen

* Inputs:
  * 1 HDMI connector
  * 1 Stereo analog unbalanced audio on a 3.5mm mini jack
* Outputs:
    * 1 HDBT on an RJ−45 female connector
* Poorten:
  * 1 IR on a 3.5mm mini jack for IR link extension
  * 1 USB 2.0 on a USB−B connector
  * 1 RS−232 on a 3−pin terminal block for serial link extension
  * 1 RS−232 on a 3−pin terminal block for device control
  * 1 10Base−T/100BaseTx Ethernet on an RJ−45 female connector for device controland network traffic extension
* Voeding: 48V DC, 1.36A
* Power: 48V DC, 800mA
* Afmetingen: 18.75cm x 11.50cm x 2.54cm
* Gewicht: 0,5 kg

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Interactives

Dit component wordt gebruik in de interactives:

{{< pagelist interactives >}}

## Shows

Dit scherm wordt gebruik in de shows:

{{< pagelist shows >}}

## Bijlagen

{{< pagelist bijlagen >}}
