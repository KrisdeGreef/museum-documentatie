---
title: "wahlberg-winch25"
date:
draft: false
merk: Wahlberg
model: Winch 25
leverancier:
interactives:
- aardbevingplafond
attachments:
- title: Wahlberg Winch 25 User Manual
  src: https://files.museum.naturalis.nl/s/bb5qxZETRPYy4BA
---

![winch25](./winch25.png)


## Eigenschappen

* Afmetingen Winch 25 220:

  * Hoogte: 229 mm
  * Breedte: 287 mm
  * Diepte: 355 mm

* Gewicht: 21 kg

* Afmetingen Winch 25 221:

  * Hoogte: 229 mm
  * Breedte: 687 mm
  * Diepte: 355 mm

* Gewicht: 25 kg

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## interactives

{{< pagelist interactives >}}

## Bijlagen

{{< pagelist bijlagen >}}
