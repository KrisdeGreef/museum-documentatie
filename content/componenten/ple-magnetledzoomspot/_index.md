---
title: "ple-magnetledzoomspot"
date:
draft: false
merk: PLE
model: Magnet Led Zoom Spot
leverancier: ATA Tech
attachments:
- title: PLE Magnet Led Zoom Spot (pdf)
  src: https://files.museum.naturalis.nl/s/9cQHwDeSaHpjpW2

---

![magnetledzoomspot](./magnetledzoomspot.png)

## Eigenschappen


## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
