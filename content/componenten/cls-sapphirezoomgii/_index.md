---
title: cls-sapphirezoomgii
date:
draft: false
merk: CLS
model: Sapphire Zoom GII
leverancier: CLS
attachments:
- title: CLS Sapphire Zoom GII manual (pdf)
  src: https://files.museum.naturalis.nl/s/tjKwRDirPw3Y9w8
- title: CLS Sapphire Zoom GII (pdf)
  src: https://files.museum.naturalis.nl/s/3pJeMj4iync6R9Q
- title: CLS Sapphire DMX Manual (pdfß)
  src: https://files.museum.naturalis.nl/s/DAQxRoe7AKTMnwB
---

![sapphire-zoom](./sapphire-zoom.jpg)

## Eigenschappen

* Dimensions (LxWxH):
* Weight:

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
