---
title: chauvet-ovationf145ww
date:
draft: false
merk: Chauvet
model: Ovation F-145WW
leverancier: Lichtpunt
attachments:
- title: Chauvet Ovation F-145WW DMX (pdf)
  src: https://files.museum.naturalis.nl/s/Big9gw7DZMswB9J
- title: Chauvet Ovation F-145WW manual (pdf)
  src: https://files.museum.naturalis.nl/s/DWbePMcaZMteB92
- title: Chauvet Ovation F-145WW Quick (pdf)
  src: https://files.museum.naturalis.nl/s/dCtQQNxGf63oeW4


---

![f145ww](./f145ww.png)
The Ovation F-145WW is a warm white Fresnel-style LED lighting fixture with 16-bit dimming resolution for smooth fades. It delivers a beautiful, flat, even field of light. Its motorized zoom, with a range between 25° and 65°, can be controlled either manually or via DMX. Simple and complex DMX channel profiles offer programming versatility. Virtually quiet operation makes it ideal for use in any situation.

## Eigenschappen

* Dimensions (LxWxH): 404.4 mm x 282.9 mm x 241.1 mm
* Weight: 5.1 kg

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
