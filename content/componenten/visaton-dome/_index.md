---
title: "visaton-dome"
date: 2019-12-03T14:50:10+01:00
draft: true
merk:
model:
leverancier:
attachments:
- title: visaton-dome_techischetekening.jpg
  src: https://files.museum.naturalis.nl/s/HTZdTDx7i7YF8dC
---

<!-- Voeg een overzichtsafbeelding toe -->
<!-- Voeg een algemene omschrijving van het component toe -->

## Eigenschappen

* Vermogen:
* Afmetingen:

  * Hoogte:
  * Breedte:
  * Diepte:

* Gewicht:

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
