---
title: "dapaudio-pra82"
date: 2019-12-03T14:50:03+01:00
draft: false
merk: DAP audio
model: PRA-82
leverancier:
interactives:
- cirkelprojectie
- camarasaurus
- dinovoetstappen
- edmontorasurus
- filmdubois
- plateosaurus
- stegosaurus
- trex
shows:
- showleven
- showijstijd
attachments:
- title: dapaudio-pra82_gegevensblad.pdf
  src: https://files.museum.naturalis.nl/s/oSPL6Dqdxa3adp6
---

![PRA-82](./PR-82.jpg "PRA-82")
![PRA-82-rear](./PRA-82-rear.jpg "PRA-82-rear")
Actieve PA luidspreker
## Eigenschappen


* Actief of passief: Actief
* RMS vermogen: 2 x 30 W
* Type connector: 1 pair RCA female
* Impedantie: 8 Ohm
* Afmetingen: (LxBxH) 270x255x365 mm

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Interactives

Dit component wordt gebruik in de interactives:

{{< pagelist interactives >}}

## Show

Dit component wordt in de volgende shows gebruikt:
{{< pagelist shows >}}


## Bijlagen

{{< pagelist bijlagen >}}
