---
title: "showtec-cityflash"
date:
draft: false
merk: Showtec
model: City Flash
leverancier: Flashlight
attachments:
- title: Showtec City Flash (pdf)
  src: https://files.museum.naturalis.nl/s/PBE6YjQQaYAzbyb
---

![cityflash](./cityflash.png)


## Eigenschappen

* Afmetingen:

  * Socket: E27

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
