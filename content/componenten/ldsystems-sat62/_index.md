---
title: "ldsystems-sat62"
date: 2019-12-03T14:50:06+01:00
draft: false
merk: LD Systems
model: Sat 62 G2
leverancier:
interactives:
- studiolab
attachments:
- title: ldsystems-sat62_handleiding.pdf
  src: https://files.museum.naturalis.nl/s/QGRY8AgDGw6BQ7f
---

![LDSAT62G2](./LDSAT62G2.png "LDSAT62G2")
Wall-mounted Loudspeaker

## Eigenschappen

* RMS Vermogen: 80 W
* Afmetingen:

  * Hoogte: 320 mm
  * Breedte: 200 mm
  * Diepte: 200 mm

* Speaker input connections: Clamp connector
* Impedance: 16 Ohm
* Gewicht: 8,2 Kg

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Interactives

Dit component wordt gebruik in de interactives:

{{< pagelist interactives >}}

## Bijlagen

{{< pagelist bijlagen >}}
