---
title: "visaton-amp22ln"
date: 2019-12-03T14:50:10+01:00
draft: false
merk: Visaton
model: amp22ln
leverancier:
attachments:
- title: visaton-amp22ln_gegevensblad.pdf
  src: https://files.museum.naturalis.nl/s/272WJsnpbyGcSCr
---

![amp22ln](./amp22ln.jpg)
Stereo amplifier with level controls, e.g. for multimedia applications

## Eigenschappen

* Vermogen: 12V DC / (min) 46mA (max) 1.1A
* Afmetingen:

  * Hoogte: 28 mm
  * Breedte: 65 mm
  * Diepte: 17 mm +14 mm (knop)

* Gewicht: 0,178 kg

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
