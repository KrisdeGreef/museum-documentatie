---
title: "intel-nuc8i3beh"
draft: false
interactives:
- expeditietent
- greenporn
- waternesten
- showleven
- showijstijd
- projectiegiraf
- sexystories
- projectiekleinedieren
- projectiebeweging
- projectieluchtdieren
- projectieolifant
- projectiegetijden
- projectiediepzee
- projectiedans
- camperhawaii
- geilewand
- hartbank
- vogelsvoeren
- cabinijsland

attachments:
- title: intel-nuc8i3beh_gegevensblad.pdf
  src: https://files.museum.naturalis.nl/s/pJ623R2Y5NPJxLz
- title: intel-nuc8_specs (pdf)
  src: https://files.museum.naturalis.nl/s/WRDdxRQF469Gqoe
---

![nuc](./nuc.png "nuc")

## Algemeen

Voor mediaplayers, kiosksystemen en andere relatief lichte taken in het museum
worden Intel NUC8i3BEH computers ingezet.

## Eigenschappen

* Vermogen: 90W voeding
* Afmetingen:

  * Hoogte: 51 mm
  * Breedte: 115 mm
  * Diepte: 111 mm

* Gewicht: +/- 500 gram

Op de
[pagina van de fabrikant](https://www.intel.com/content/www/us/en/products/boards-kits/nuc/kits/nuc8i3beh.html)
vind je een overzicht van de eigenschappen van deze NUC. Uitgebreide
documentatie vind je in de [technische product
specificatie](https://www.intel.com/content/dam/support/us/en/documents/mini-pcs/NUC8i3BE_NUC8i5BE_NUC8i7BE_TechProdSpec.pdf).

## Hardware

De NUC's zijn voorzien van:

* Intel(R) Core(TM) i3-8109U CPU
* Samsung SSD 860 250GB 2.5" SATA-600
* of M2 EVO 860 250GB
* 1 x Kingston ValueRAM DDR4 8GB 2400MHz
* 1 x Liteon PA-1900-32 voeding

## Interactives

Dit component wordt gebruik bij:

{{< pagelist interactives >}}

## Bijlagen

{{< pagelist bijlagen >}}
