---
title: "monacor-hdcam440"
date: 2019-12-03T14:50:06+01:00
draft: false
merk: Monacor
model: hdcam440
leverancier:
interactives:
- studiolab
attachments:
- title: monacor-hdcam440_handleiding.pdf
  src: https://files.museum.naturalis.nl/s/8NnsLJMiianGeZx
---

![hdcam440](./hdcam440.jpg "hdcam440")
HD-SDI Surveillance Camera

## Eigenschappen

* Vermogen:
* Afmetingen: 47 mm × 48 mm × 71 mm

* Gewicht: 175g

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Interactives

Dit component wordt gebruik in de interactives:

{{< pagelist interactives >}}


## Bijlagen

{{< pagelist bijlagen >}}
