---
title: "eiki-ek820u"
date:
draft: false
merk: Eiki
model: EK-820U
leverancier:
interactives:
- projectiejungle
attachments:
- title: rexperience_hardware_avh_beamer_handleiding_eiki_ek_820u (pdf)
  src: https://files.museum.naturalis.nl/s/i4Dm25N76yeB4K3
- title: rexperience_hardware_avh_beamer_handleiding_eiki_ek_820u_color_data_sheet (pdf)
  src: https://files.museum.naturalis.nl/s/BgXzNDmQqBD2Kkp
- title: rexperience_hardware_avh_beamer_specsheet_eikiprojector (pdf)
  src: https://files.museum.naturalis.nl/s/eFXSTkfCkRDQKMi
---

![ek815_m](./ek815_m.jpg)
Multimedia Projector

## Eigenschappen


* Levensduur lichtbron: 20.000 hrs
* Type lamp: 1-chip DLP® technology
* Projectie techniek: Laser (Blue & Red) + Phospher
* ANSI lumen: 10.000
* Resolutie: 1920 x 1200
* Beeldverhouding: 16:10
* Statische contrastverhouding: 100.000:1
* Video-in:
  * Dsub15
  * HDMI
  * DVI-D
  * 3G-SDI
  * HDBaseT
  * 3D sync in (mini Din 3)
* Bediening:
  * RS232 In
  * Ethernet (10/100) RJ45
  * IR remote
  * Wired remote control (3.5mm stereo)
* Verbinding (Ethernet): 10/100MB/s RJ45
* Afmetingen (excl legs): (LxWxH): 18.0 x 48.1 x 50.8cm
* Vermogen: 1050W
* Gewicht: 26,1kg w/o lens


## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.


## Interactives

Dit component wordt gebruik in de interactives:

{{< pagelist interactives >}}

## Bijlagen

{{< pagelist bijlagen >}}
