---
title: "philips-bdl4330ql00"
date: 2019-12-03T14:50:08+01:00
draft: false
merk: Philips
model: bdl4330ql00
leverancier:
interactives:
- flipperkast
- monitorenontvangsthal
attachments:
- title: philips-bdl4330ql00_gegevensblad.pdf
  src: https://files.museum.naturalis.nl/s/JwcMeAACLMwbSPC
---

![bdl4330ql00](./bdl4330ql00.png)
Q-line scherm 43 inch Direct LED-achtergrondverlichting, Full HD

## Eigenschappen

* Video-in:
  * DVI-D
  * HDMI
  * Component (RCA)
  * Composite (RCA)
  * VGA (analoog D-Sub)
* Bediening:
  * RS232
  * RJ-45
  * IR
* Schermdiagonaal: 42,5 inch /  108 cm
* Resolutie: 1920 x 1080p
* Beeldverhouding: 16:9
* Vermogen: 87 Watt
* Kijkhoek: (h /  v): 178 /  178 graad
* Afmetingen: 968,2x 559,4 x  59,9 mm
* Gewicht: 8,7 kg

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Interactives

Dit component wordt gebruik in de interactives:

{{< pagelist interactives >}}

## Bijlagen

{{< pagelist bijlagen >}}
