---
title: "palmer-ppb10"
date: 2019-12-03T14:50:07+01:00
draft: false
merk: Palmer
model: ppb10
leverancier:
attachments:
- title: palmer-ppb10_handleiding.pdf
  src: https://files.museum.naturalis.nl/s/DnQX9eTBk9WzAKW
---

![ppb10](ppb10.png)
![ppb10_rear](ppb10_rear.png)
The PPB10 is a special kind of audio splitter in that it splits one incoming signal up to twenty outputs (as opposed to the usual three).

## Eigenschappen

* Vermogen: 10W max
* Afmetingen: 19”, 1U Steel housing 205mm deep

* Gewicht: 4Kg

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
