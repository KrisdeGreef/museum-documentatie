---
title: "hikvision-ds7608nii2p8"
date:
draft: false
merk: hikvision
model: DS-7608NI-I2/8P
leverancier:
shows:
- showrexperience
attachments:
- title: DatasheetofDS-7600NI-I2_P_NVR_V4.40.01020200727 (pdf)
  src: https://files.museum.naturalis.nl/s/6AMSJWejejobFyf
- title: UD18726N_Neutral_User-Manual-of-I-Series-Network-Video-Recorder_V4.40.010_20200508 (pdf)
  src: https://files.museum.naturalis.nl/s/xgsx84sMjYmFHg4
---

![DS7608](./DS7608.png)

8 channel Network Video Recorder

## Eigenschappen

* Afmetingen: 385 × 315× 52 mm

* Gewicht: ≤ 3 kg

* IP Video Input: 8-ch Up to 12 MP resolution
* Remote Connections: 32
* Network Protocols: TCP/IP, DHCP, Hik-Connect, DNS, DDNS, NTP, SADP, SMTP, NFS, iSCSI, UPnP™, HTTPS

## Known issues

Het zou natuurlijk vet cool zijn om de beelden ook op je eigen computer te bekijken.
Dit kan, alleen gebruikt Hikvision een oude techniek voor de video in je browser: NPAPI

NPAPI is eruit gesloopt bij alle moderne browsers dus de enige mogenlijkheid is
om het via Internet Explorer te proberen.

Ik heb ook een work-around gevonden om het via de Chrome Extensie "IE Tab"  te doen
maar bij mijn test (via VPN thuis) ging dat al mis.

De wachtwoorden staan in Bitwaren.

![Hivision in de browser](https://files.museum.naturalis.nl/s/SAsrLHgF9jMGqyG/preview)

## Show

Dit component wordt in de volgende shows gebruikt:
{{< pagelist shows >}}


## Bijlagen

{{< pagelist bijlagen >}}
