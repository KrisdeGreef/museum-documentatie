---
title: "brightsign-xd1034"
date:
draft: false
merk: BrightSign
model: XD-1034
leverancier: VHS
shows:
- showrexperience
attachments:
- title: XD datasheet (pdf)
  src: https://files.museum.naturalis.nl/s/CA7nt2oZF9iDGNC
---

![xd4](./xd4.png)

TRUE 4K DOLBY VISION™ AND HDR10 PLAYBACK. ADVANCED PERFORMANCE.

## Eigenschappen

* Afmetingen:

  * Hoogte:
  * Breedte:
  * Diepte:

* Gewicht:

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Show

Dit component wordt in de volgende shows gebruikt:
{{< pagelist shows >}}

## Bijlagen

{{< pagelist bijlagen >}}
