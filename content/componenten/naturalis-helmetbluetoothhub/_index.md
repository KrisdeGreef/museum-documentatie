---
title: "naturalis-helmetbluetoothhub"
date: 2019-07-25T13:06:06Z
merk: Olimex 
model: ESP32
leverancier: Frank Verweij/Naturalis
interactives:
- schattenjacht
exhibits:
- schattenjacht
tentoonstelling:
- deaarde
attachments:
- title: Helmet Bluetooth hub Gitlab repo
  url: https://gitlab.com/naturalis/mii/helmet-bluetooth-hub
- title: Olimex ESP32-POE-ISO
  url: https://www.olimex.com/Products/IoT/ESP32/ESP32-POE-ISO/open-source-hardware
- title: formatting BLE packets
  src: "ble-advertisement-packets.pdf"
- title: naturalis-helmetbluetoothhub_afbeelding.jpg
  src: https://files.museum.naturalis.nl/s/pXyrpBJkb9L8iWC
---

De Bluetooth hub communiceert gegevens van de bluetooth helmpjes naar de
monitoring software op het netwerk.

De BLE-Hub wordt op twee plaatsen in de aarde-zaal toegepast.

In de schatkamer. De unit is geplaatst achter het luik in het midden van de
lange kant van de vitrine. De afstand tussen de helmen en de Ble-hub is op deze
manier nooit langer dan een meter of 7.  

De Ble-hub is naast de IR-transmitter geplaatst.

![de hub](./image3.png "de hub")

*   Bij het laadstation van de helpjes. Dit is te vinden in de ruimte die toegang geeft tot de SER van de Aarde-zaal.

![de helmpjes](image1.png "de helmpjes")

## Printplaat:


![printplaat](image2.jpg "printplaat")


De Olimex ESP32_poe_Iso microprocessor is op een docking board gemonteerd.

Op dit board is een blauwe led geplaatst. Tijdens normaal bedrijf dient deze
led 1 x per seconde te knipperen.

De twee BLE_Hub units zijn op redelijke afstand van elkaar geplaatst. Toch
blijkt het zo te zijn dat de hub bij het laadstation de signalen van de helmen
in de schatkamer ontvangt. Dit is op zich geen probleem als het gaat om het
doorgeven van door helmen gevonden objecten. De hub stuurt dit soort
boodschappen door naar de Pharos lichtcomputer. Het is niet erg als deze
dubbele triggers krijgt. Overigens is het mogelijk deze functionaliteit uit te
zetten met dipswitch 1.

De hub houdt echter ook bij welke objecten door welke helmen gevonden zijn.
Deze data wordt naar de housekeeping server verstuurd. Als beide hub’s dit
doorgeven ontstaat een meetfout.

Door dipswitch 2 op ON te zetten wordt deze data doorgeven aan de monitoring
software.

### Dipswitches

* Dipswitch 1: ON = Stuur "Found-object" messages naar PharosHost
* Dipswitch 2: ON = Stuur "Found-object" records naar TIM


![bluetooth hub](bluetoothhub.jpg)

De hub is een kastje wat eens in de zoveel tijd (ieder half uur) via bluetooth een 
json record ontvangt. Meestal in de vorm:

```json
{
    "MsgType":"Status",
    "HelmetNR":15,
    "BatteryVoltage":3.52,
    "BatteryPerc":14.54,
    "ChargerTemp":25.1
}
```

Dit record wordt doorgezet naar de 
[centrale exporter](https://gitlab.com/naturalis/mii/helmet_exporter) die het
weer aanbiedt voor [Prometheus](https://prometheus.io/).

## Hardware

### BLE-Hub

De BLE-Hub (Bluetooth Low Energy) is een apparaat dat in de schatkamer de
ble-signalen van de daar gebruikte schattenjacht helmpjes opvangt en via
ethernet, verpakt in JSon records, doorstuurt naar T.I.M. 

Het belangrijkste deel van de hardware is het Olimex ESP32-POE-ISO
processorbord. Er is een
[printplaat](https://drive.google.com/file/d/1U0Nvt5rWri9xpMeGLYatJSrBB7Jo08uj/view?usp=sharing)
gemaakt waar dit processorbord op gemonteerd kan worden. 


![ESP32](image2.jpg "ESP32")

Olimex ESP32-POE-ISO gemonteerd op printplaat

Het schema van de print is
[hier](https://drive.google.com/file/d/1447V6wf-67RFHaECsiYqMcwy6bsiN0YL/view?usp=sharing)
te vinden.

In het schema zijn de volgende twee deelschakelingen te zien:

### Processor

![Processor](image3.png "Processor")


Op de print zijn headers geplaatst waar het processorbord ingestoken kan
worden. De led is in de schakeling opgenomen als statusindicator. Als de led
knippert wordt de ble scan uitgevoerd. Met een van de dipswitches kan de manier
van data-output worden ingesteld. 

Een belangrijke feature van het ESP-POE-ISO bord is de power over ethernet functie.

Er is geen externe voeding nodig. Een DC-DC converter op het bordje zorgt voor
een galvanische scheiding tussen de POE en de voedingsspanning van de
processor.

In de schakeling is een viervoudige dipswitch opgenomen. De functionaliteit hiervan wordt 
hieronder besproken.

### Extra I/O

![extra IO](image1.png "extra IO")


Als extra i/o is de I2C bus is naar buiten gevoerd. Hiermee kunnen I2C devices aangestuurd worden.

## Software

De BLE-Hub (Bluetooth Low Energy) is een apparaat dat in de schatkamer de
ble-signalen van de daar gebruikte schattenjacht helmpjes opvangt en via
ethernet doorstuurt naar de Pharos lichtcomputer en verpakt in JSon records
naar T.I.M.  De communicatie tussen de helmen en de hub verloopt via een BLE protocol.

[Informatie over BLE](https://www.oreilly.com/library/view/getting-started-with/9781491900550/ch01.html) is ruimschoots te vinden op internet.

In BLE terminologie krijgen BLE apparaten rollen toebedeeld. In het geval van
het helmen systeem gebruiken we de rollen “Broadcaster” en “Observer”.

> A device in the **Broadcaster** role does nothing more than **transmitting** data to its surroundings. It does so by constantly advertising, and usually has useful data in the advertising packet, data that is meant for everyone to see. Such a device does not require a receiver, as its only role is to broadcast to others, so it never accepts connections. The **Observer** is the opposite of the Broadcaster: it passively **listens** to BLE devices in its area and processes the data from the advertising packets it receives. It does not need a transmitter, as it sends nothing and is never meant to enter a connection.

De helmen zijn “Broadcasters”. Ze zenden advertisement packets uit en ze zijn niet connecteerbaar.

De ontvanger is een “Observer”. Deze scant  de BLE radiofrequenties af voor advertisement packets.

Over de formatering en syntax van de advertisement packets voor deze toepassing [hier](https://docs.google.com/document/d/1L0iY1KwRpBaLZ0ajCHbuVczqWuKo2LDx3xcPTv1cbyE/edit?usp=sharing) meer.

De keuze voor deze manier van communiceren is gemaakt vanwege de
energiezuinigheid. De helmen werken op een accu dus hoe minder stroomverbruik
hoe beter.

De software loopt op een Olimex ESP32-POE-ISO systeem.

De Ble-Hub software voert elke seconde een ble scan uit. In deze scan worden
alle advertisement packets die op dat moment uitgezonden worden verzameld. De
software selecteert de packets die afkomstig zijn van schattenjachthelmen en
slaat, per helm, de relevante data op.

Om de 30 minuten wordt de data via ethernet als JSon records naar T.I.M.
verstuurd. 

### JSON format

De door de BLE-hub ontvangen gegevens van de schattenjachthelmpjes worden periodiek naar het T.I.M. systeem verstuurd.

De JSon records bevatten de volgende items:

- *MsgType* Heeft altijd waarde "Status". In een eerder stadium was er ook “Notification”
- *HelmetNr* Een integer met het nummer van de helm. Een getal tussen 1 en 16.
- *BatteryVoltage* Een float met 2 decimalen. Varieert tussen 3,20 en 4,25
- *BatteryPerc* Een integer. Geeft de acculading in procenten weer.
- *ChargerTemp* Een float met 2 decimalen. Geeft de temperatuur van de acculader-chip weer. Als de accu geladen wordt kan de temperatuur oplopen tot 50 graden
- *Objects* Een array van 14 integers. Elk getal staat voor hoe vaak een object in de vitrine door de helm is gevonden. Als er geen objecten gespot zijn wordt het Objects-array weggelaten.

 Voorbeelden van JSon record:

```json
{
  "MsgType": "Status",
  "HelmetNr": 9,
  "BatteryVoltage": 3.91,
  "BatteryPerc": 68.51,
  "ChargerTemp": 21.58,
  "Objects": [ 0, 2, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2 ]
}
```

 Als er geen objecten gevonden zijn:  
 
```json
{
  "MsgType": "Status",
  "HelmetNr": 3,
  "BatteryVoltage": 3.9,
  "BatteryPerc": 68.04,
  "ChargerTemp": 19.82
}
```

De commando’s die de software naar de Pharos lichtcomputer stuurt is een record
dat bestaat uit tekens “OBJ” + twee digits. Voorbeeld: “OBJ09” als de spot bij
object 9 moet gaan branden.

### Algemeen

Er is een printplaat gemaakt waarop de ESP32-POE-ISO microcontoller gemonteerd wordt.

Op deze printplaat is een led geplaatst die elke seconde flitst. Hieraan kan
men zien dat de software loopt. 

De ESP32 heeft een aantal timers. Timer0 is zo geconfigureerd dat deze om de
100 milliseconde een interrupt geeft. Deze timer vormt de tijdbasis voor de
scheduler.

Timer1 is geconfigureerd om een interrupt te genereren na 5 seconden. Deze
timer wordt gebruikt als Watchdog. In de loop() functie wordt timer1 steeds
gereset. Als de software ergens blijft hangen zal er na 5 seconde een reset
plaatsvinden.

Op de printplaat is een viervoudige dipswitch geplaatst. De functie hiervan is:


* Dipswitch 1: ON = Stuur "Found-object" messages naar PharosHost

Hiermee kan het versturen van commando’s naar de Pharos lichtcomputer aan of uitgezet worden.

* Dipswitch 2: ON = Stuur "Found-object" records naar TIM

Hiermee kan het versturen van scores van helmpjes naar TIM aan of uitgezet
worden. Er zijn twee BLE-Hub’s en slechts een daarvan mag de scores doorgeven.
Anders wordt er dubbel geteld.


Op de printplaat zijn nog een aantal andere features ondergebracht zoals een
dipswitch en een RS422 interface. Deze worden vooralsnog  niet gebruikt.

Het IP adres en poortnummer van de T.I.M. host is vastgelegd in de sourcefile


```
#define ExporterHost "145.136.241.35"
#define ExporterPort 9111
```

Het IP adres en poortnummer van de Pharos lichtcomputer is ook in de sourcefile vastgelegd.


```
#define PharosHost "10.133.1.1"
#define PharosPort 8000
```


## Eigenschappen

* Vermogen: 2W
* Afmetingen:

  * Hoogte: 55mm
  * Breedte: 79.6mm
  * Diepte: 157mm

* Gewicht: 50g

## Technische specificaties

 - esp32-poe-ISO
 - esp32-poe-ISO docking board

Te programmeren met behulp van [platformio.org](https://platformio.org/).

## Known issues

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
