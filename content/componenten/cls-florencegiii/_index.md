---
title: cls-florencegiii
date:
draft: false
merk: CLS
model: Florence GIII
leverancier: CLS
attachments:
- title: CLS Florence GIII manual (pdf)
  src: https://files.museum.naturalis.nl/s/ZLrHry5Cg535pD8
- title: CLS Florence GIII (pdf)
  src: https://files.museum.naturalis.nl/s/69LxLey54mY5odm

---

![florencegiii](./florencegiii.png)
CLS Florence GIII IP40 Down lighter, zwart gecoat

## Eigenschappen

* Dimensions:
  * Depth: 25 mm
  * Diameter: 60 mm
* Weight: 75 gr

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
