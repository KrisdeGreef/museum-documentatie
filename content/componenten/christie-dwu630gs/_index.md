---
title: "christie-dwu630gs"
date: 2019-12-03T14:50:02+01:00
draft: false
merk: Christie
model: DWU630-GS
leverancier:
interactives:
- waterwand
attachments:
- title: christie-dwu630gs_handleiding.pdf
  src: https://files.museum.naturalis.nl/s/r8YGz5jnLiPKS9P
- title: christie-630-gs-specsheet.pdf
  src: https://files.museum.naturalis.nl/s/rkCqMagcaiCLpi8
---

![dwu630-gs-hero1](./dwu630-gs-hero1.png "dwu630-gs-hero1")
<!-- Voeg een algemene omschrijving van het component toe -->

## Eigenschappen

* Levensduur lichtbron: 20.000 hrs
* Type lamp: 1 x 0.67" DMD S600HB
* Projectie techniek: High Efficiency Blue Laser Phosphor
* ANSI lumen: 6000
* Resolutie: 1920 x 1080
* Beeldverhouding: 16:9
* Statische contrastverhouding:
  * Full On/Off (Christie® RealBlack™ enabled): 4,000,000:1 • Dynamic contrast: 6000:1
  * Full On/Off (Christie RealBlack disabled): 1200:1 • ANSI 250:1
* Video-in:
  * HDBaseT (RJ45 x 1)
  * HDMI x 2
  * DVI-D x 1 (digital only)
  * HD15 (VGA) x 1
* Bediening:
  * RS232 In
  * Ethernet (10/100) RJ45
  * Built-in backlit keypad
  * IR remote
  * Wired remote control (3.5mm stereo)
* Verbinding (Ethernet): 10/100MB/s RJ45
* Afmetingen: (LxWxH): 05 x 456 x 190mm w/o feet or lens
* Vermogen: 650W

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.


## Interactives

Dit component wordt gebruik in de interactives:

{{< pagelist interactives >}}


## Bijlagen

{{< pagelist bijlagen >}}
