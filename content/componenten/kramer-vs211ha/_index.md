---
title: "kramer-vs211ha"
date: 2019-12-03T14:50:05+01:00
draft: false
merk: kramer
model: vs211ha
leverancier:
interactives:
- studiolab
attachments:
- title: kramer-vs211ha_handleiding.pdf
  src: https://files.museum.naturalis.nl/s/ajr4dEnRwYcXi2b
- title: VS-211HA Datasheet.pdf
  src: https://files.museum.naturalis.nl/s/nSNTCxrnexAdcmQ
---

![vs-211ha](./vs-211ha.png "vs-211ha")
Automatic HDMI Standby Switcher

## Eigenschappen

* Inputs:
  * 2 HDMI connectors
  * 2 unbalanced stereo audio on 3.5mm mini jacks
* Outputs:
  * 1 HDMI connector
  * 1 unbalanced stereo audio on a 3.5mm mini jack
* 5V DC, 120mA (without load), 350mA (fully loaded)
* Afmetingen: 12.00cm x 7.15cm x 2.44cm
* Gewicht: 0,2 kg

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Interactives

Dit component wordt gebruik in de interactives:

{{< pagelist interactives >}}

## Bijlagen

{{< pagelist bijlagen >}}
