---
title: "meanwell-gsm60b12p1j"
date: 2019-12-03T14:50:06+01:00
draft: false
merk: Meanwell
model: gsm60b
leverancier:
attachments:
- title: meanwell-gsm60b12p1j_gegevensblad.pdf
  src: https://files.museum.naturalis.nl/s/2TD2KRrFdbyJ9zc
---

![gsm60b](./gsm60b.png "gsm60b")
Medical desktop adaptor 12V/0.1-5A IEC 320-C8

## Eigenschappen

* Vermogen: 60W
* Output: 12V / 5A
* Afmetingen:

  * Hoogte: 31.5mm
  * Breedte: 50mm
  * Diepte: 125mm

* Gewicht: 320g

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
