---
title: "sennheiser-ew300g4"
date: 2019-12-03T14:50:08+01:00
draft: false
merk: sennheiser
model: ew300g4
leverancier:
attachments:
- title: handleiding
  src: https://files.museum.naturalis.nl/s/xrekokdm6P6Wr4Q
- title: specificaties
  scr: https://files.museum.naturalis.nl/s/HcNpcbynLbEF948
---

![ew300g4](./ew300g4.jpg)
Sennheiser Evolution Wireless 300 series Generation 4

## Eigenschappen


## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
