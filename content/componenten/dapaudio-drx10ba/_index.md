---
title: "dapaudio-drx10ba"
date: 2019-12-03T14:50:02+01:00
draft: false
merk: DAP Audio
model: DRX-10BA
leverancier:
interactives:
- fimdubouis
attachments:
- title: dapaudio-drx10ba_handleiding.pdf
  src: https://files.museum.naturalis.nl/s/2TwG8T8KgKPzZLA
---

![drx10ba](./drx10ba.jpg "drx10ba")
Actieve subwoofer 10 inch

## Eigenschappen

* Actief of passief: Actief
* Versterkerklasse: Bi-Amp
* RMS vermogen: 600W
* Broningangen: 3-pin XLR combinatie female links (line) & 3-pin XLR female rechts, 2x male XLR
* Uitgang: 2x 3-pin XLR male
* Afmetingen: 325 x 465 x 421mm (HxBxD)
* Gewicht: 15,46 kg

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.


## Interactives

Dit component wordt gebruik in de interactives:

{{< pagelist interactives >}}

## Bijlagen

{{< pagelist bijlagen >}}
