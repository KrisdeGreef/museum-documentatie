---
title: "xilica-neutrinoa1608"
date:
draft: false
merk: xilica
model: neutrinoa1608
leverancier:
attachments:
- title: specifications
  src: https://files.museum.naturalis.nl/s/fWRoJMG5Rge4rrT
- title: user manual
  src: https://files.museum.naturalis.nl/s/5Dg6y23pnbdA7Ho
---

![A1608](./A1608.png)
digital  signal audio processors

## Eigenschappen

* Afmetingen:

  * Hoogte: 44 mm (1U)
  * Breedte: 483 mm
  * Diepte: 229 mm

* Gewicht: 5 kg

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
