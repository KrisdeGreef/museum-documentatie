---
title: "microsonic-ultrasonic"
date:
draft: false
merk: Microsonic
model: Ultrasonic Sensor
leverancier: sensor partners
attachments:
- title: rexperience_hardware_gate_specsheet_ultrasonicsensor (pdf)
  src: https://files.museum.naturalis.nl/s/58r5HLAqgRQYSHw
---

![sensor](./sensor.png)
crm+ Ultrasonic Sensors with one switched output

## Eigenschappen


## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
