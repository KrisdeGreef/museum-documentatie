---
title: "tdelighttech-izilink"
date:
draft: false
merk: TDE Lighttech
model: IZI-link DMX interface
leverancier: TDE Lighttech
attachments:
- title: TDE IZI-Link DMX interface (pdf)
  src: https://files.museum.naturalis.nl/s/ycrNQjHZiXixs5N
---

![izilink](./izilink.png)
DMX interface

## Eigenschappen

* Afmetingen:

  * Hoogte: 58 mm
  * Breedte: 71 mm
  * Diepte: 90 mm


## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
