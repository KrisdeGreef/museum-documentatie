---
title: cls-focuscompactgii
date:
draft: false
merk: CLS
model: CLS Focus Compact GII
leverancier: CLS
attachments:
- title: CLS Focus Compact GII manual (pdf)
  src: https://files.museum.naturalis.nl/s/CBJHckiP7m8XgfD
- title: CLS Focus Compact GII (pdf)
  src: https://files.museum.naturalis.nl/s/gin74gswSdqp8Q2

---

![focuscompactgii](./focuscompactgii.png)
4 Watt, ultra compact zoom fixture.

## Eigenschappen

* Dimensions:
  * Hoogte: 85 mm
  * Diameter: 36 mm
* Weight: 135 gr

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
