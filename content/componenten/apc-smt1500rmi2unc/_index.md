---
title: "apc-smt1500rmi2unc"
date:
draft: false
merk: APC
model: Smart-UPS 1500VA LCD RM 2U 230V with Network Card
leverancier:
shows:
  - showrexperience
attachments:
- title: Technical Specifications (pdf)
  src: https://files.museum.naturalis.nl/s/3SXjBr9ePtqSTgo
- title: Installation Guide Smart-UPS (pdf)
  src: https://files.museum.naturalis.nl/s/FSisQyJYS7nHGH8
- title: Brochure Smart-UPS (pdf)
  src: https://files.museum.naturalis.nl/s/gJJTwbLMG5kZNft
---

![smartups-front](./smartups-front.png)
![smartups-rear](./smartups-rear.png)
Smart-UPS 1500VA LCD RM 2U 230V with Network Card

## Eigenschappen

* Afmetingen:

  * Hoogte: 89 mm
  * Breedte: 432 mm
  * Diepte: 457 mm

* Gewicht: 28,58 kg

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Show

Dit component wordt in de volgende shows gebruikt:
{{< pagelist shows >}}

## Bijlagen

{{< pagelist bijlagen >}}
