---
title: chauvet-ovationf55
date:
draft: false
merk: Chauvet
model: Ovation F55
leverancier: Lichtpunt
attachments:
- title: Chauvet Ovation F-55 DMX (pdf)
  src: https://files.museum.naturalis.nl/s/mXfzSMKdMjzMqQK
- title: Chauvet Ovation F-55 manual (pdf)
  src: https://files.museum.naturalis.nl/s/CyzAkoJsWLsBJjr
- title: Chauvet Ovation F-55 Quick (pdf)
  src: https://files.museum.naturalis.nl/s/4XZPMAr4nJaxx3K

---

![f55](./f55.png)
The Ovation F-55FC is a full-color RGBAL inkie Fresnel-style fixture. The Virtual Color Wheel and Color Temperature presets are shared from our other full-color Ovation fixtures. A manual zoom with a 32° to 87° field angle allows the product to have a soft field of light when needed.

## Eigenschappen

* Dimensions (LxWxH): 245 mm x 118 mm x 166 mm
* Weight: 2.4 kg

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
