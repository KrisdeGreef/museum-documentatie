---
title: dewijs-1804d55x
date: 2019-12-10T14:29:55+01:00
draft: false
merk: dewijs
model: 18-04-D55-x
leverancier:
interactives:
- kijkersbrabant
- kijkersdrenthe
- kijkersfriesland
- kijkersgelderland
- kijkersholland
attachments:
- title: dewijs-180d55x_handleiding.pdf
  src: https://files.museum.naturalis.nl/s/5DAyAF7pKCJ9z4i
---

![18-04-d55](./18-04-d55.jpg "18-04-d55")
<!-- Voeg een algemene omschrijving van het component toe -->

## Eigenschappen

* Vermogen:
* Afmetingen:

  * Hoogte: 120 mm
  * Breedte: 200 mm
  * Diepte: 150 mm

* Gewicht:

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.


## Interactives

Dit component wordt gebruik in de interactives:

{{< pagelist interactives >}}

## Bijlagen

{{< pagelist bijlagen >}}
