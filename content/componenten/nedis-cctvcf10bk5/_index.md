---
title: "nedis-cctvcf10bk5"
date: 2019-12-03T14:50:06+01:00
draft: false
merk: Nedis
model: cctvcf10bk5
leverancier:
attachments:
- title: nedis-cctvcf10bk5_gegevensblad.pdf
  src: https://files.museum.naturalis.nl/s/F3QaE2rjke88b3j
---

![cctvcf10bk5](./cctvcf10bk5.png "cctvcf10bk5")
Connector voor CCTV-beveiliging | 2-aderig naar female DC

## Eigenschappen

* Vermogen:
* Afmetingen:

  * Hoogte:
  * Breedte:
  * Diepte:

* Gewicht:

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
