---
title: "birddog-flex4kin"
date:
draft: false
merk: BirdDog
model: FLEX 4K IN
leverancier: The Future Store
interactives:
- studiolab
attachments:
- title: FLEX 4K IN manual (pdf)
  src: https://files.museum.naturalis.nl/s/agkxNwFBtXkQzD9
---

![](./flex4kin.png)

HDMI naar NDI converter

## Eigenschappen

* Afmetingen: 107×63×31mm
* Gewicht: 140 gr

* SUPPORTED VIDEO FORMAT
  * UHD 2160p: 25.00, 29.97, 30.00
  * HD 1080p: 25.00, 29.97, 30.00, 50.00, 59.94, 60.00
  * HD 1080i: 50.00, 59.94
  * HD 720p: 50.00, 59.94, 60.00

* VIDEO I/O CONNECTIVITY: HDMI 1.4b Input
* AUDIO INPUT: 2Ch, 48kHz
* VIDEO CODEC SUPPORT: NDI (Full bandwidth i-frame compression)
* TALLY: Integrated Tricolour Halo Tally
* PTZ CONTROL: 2.5mm 4-pole RS232 (VISCA)

* NETWORK CONNECTIVITY
  * Ethernet RJ45 1000baseT w/integrated PoE (Power over Ethernet)
  * Embedded Web configuration Panel

* POWER
  * PoE (Power over Ethernet): PoE+ 802.3at
  * DC Input: DC 12v (30w max)
  * DC Output: DC 12v (15w max)


## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Interactives

Dit onderdeel wordt gebruik in de interactives:

{{< pagelist interactives >}}


## Bijlagen

{{< pagelist bijlagen >}}
