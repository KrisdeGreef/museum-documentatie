---
title: "eldoled-lineaedrive180d"
date:
draft: false
merk: Eldoled
model: LineaeDrive 180D
leverancier: De Circel
attachments:
- title: Datasheet LIN180D3 (pdf)
  src: https://files.museum.naturalis.nl/s/bcxFw5zg2Scg8YN
- title: WD LINEARdrive 180D v1.0 (pdf)
  src: https://files.museum.naturalis.nl/s/ZzEWanmXRcDKipK
---

![lineardrive180d](./lineardrive180d.png)

6A DMX/RDM Full-Colour Dimmable LED Driver


## Eigenschappen


## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
