---
title: "mikestoanelighting-popeye"
date:
draft: false
merk: Mike Stoane Lighting
model: Mike Stoane Popeye
leverancier: Mike Stoane Lighting
attachments:
- title: Mike Stoane Popeye (pdf)
  src: https://files.museum.naturalis.nl/s/Qd5D578ysKy7B2d

---

![popeye](./popeye.png)

## Eigenschappen




## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
