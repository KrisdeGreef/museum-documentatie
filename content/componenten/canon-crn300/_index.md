---
title: "canon-crn300"
date:
draft: false
merk: Canon
model: CR-N300
leverancier: ALV
interactives:
- studiolab
attachments:
- title: Canon CR-N300 Setting Guide
  src: https://files.museum.naturalis.nl/s/B8tz8EyEiLtk6oc
- title: Canon CR-N300 User Manuel
  src: https://files.museum.naturalis.nl/s/e7c6e8PY2wQdkw2
---

![cr-n300_front](./cr-n300_front.webp)
![cr-n300_rear](./cr-n300_rear.webp)

4K PTZ Camera

## Eigenschappen

### Camera

* Beeldsensor: Type 1/2,3 (1/2,3 inch) single-plate CMOS-sensor
* Effectieve pixels: ca. 8,29 Megapixel (3840 x 2160)
* Lens: f=3.67 – 73.4 mm, F/1.8 – 2.8, 20x optische zoom, cirkelvormig diafragma met 8 lamellen
* Optische zoom: 20x optische zoom
* Digitale zoom: 20x digitale zoom
* Brandpuntsafstand: f=3,67 – 73,4 mm, f/1,8 – 2,8,
    * Brandpuntsafstand equivalent aan 35 mm:
      - [4K UHD] ca. 29,3 (W) – 601 mm (T)
      - [Full HD] ca. 30,5 (W) – 627 mm (T)
      - Digitaal
* Beeldhoek:
    * [4K UHD]
      - Horizontaal: 65,6 (W) – 3,6° (T)
      - Verticaal: 39,8° (W) – 2,0° (T)
    * [Full HD]
      - Horizontaal: 63,5 (W) – 3,4° (T)
        - Verticaal: 38,4° (W) – 1,9° (T)
* Minimale belichting van onderwerpen: Ca. 1,5 lux (sluitertijd 1/30 sec., framerate 59,947P (Programma AE) opnamemodus, automatische lange sluitertijd aan)
* Sluitertijd: 1/6 – 1/2000 sec. (specifieke waarden zijn afhankelijk van de framefrequentie en framerate)
* Iris: Handmatig / automatisch diafragma
* Gain: 0,0 dB – 36 dB
* ND-filter: Geïntegreerd (1/8 bij maximum, ND-gradatie), motorbediening
* Witbalans: AUTO (AWB), Set A, Set B, voorkeuze-instellingen (daglicht: 5600 K\*, kunstlicht: 3200 K\*), instellingen kleurtemperatuur (2000 K – 15.000 K), Handmatig
* Focus: Scherpstelmodus: Handmatig, Continue AF, Face AF, Tracking AF-type: Hybride AF, Contrast AF
* Gamm: Normal1 (Standaard), Normal3 (BT.709)
* Pannen en tilten
    * Pan-bereik: horizontaal ±170°
    * Pan-snelheid: 0,2° – 300°/sec.
    * Tilt-bereik: verticaal -30° – +100°
    * Tilt-lsnelheid: 0,2° – 170°/sec.

### Pannen/tilten
* Protocol Protocol: XC (origineel van Canon), RTSP/RTP, NDI|HX, RTMP/RTMPS, standaardcommunicatie (serieel), standaardcommunicatie (IP)

### Server
* Uitvoerindeling video: SDI
    * 1920 x 1080: 59,94P / 59,94i, 50,00P / 50,00i / 25,00P, 29,97P / 23,98P (4:2:2 10-bits)
    * 1280 x 720: 59,94P, 50,00P (4:2:2 10 bits)
    – Dezelfde video-indeling is vereist voor SDI en HDMI (er kunnen geen verschillende indelingen worden geselecteerd voor SDI en HDMI)
    – Wanneer 3840 x 2160 is geselecteerd voor HDMI, wordt de video niet als SDI gegenereerd.
* Uitvoerindeling video: HDMI
    * HDMI 3840 x 2160: 29,97P, 25,00P, 23,98P (4:2:2 10 bits)
    * 1920 x 1080: 59,94P / 59,94i, 50,00P / 50,00i / 25,00P, 29,97P / 23,98P (4:2:2 10-bits)
    * 1280 x 720: 59,94P, 50,00P (4:2:2 10 bits)
    – Dezelfde video-indeling is vereist voor SDI en HDMI (er kunnen geen verschillende indelingen worden geselecteerd voor SDI en HDMI)
    – Wanneer 3840 x 2160 is geselecteerd voor HDMI, wordt de video niet als SDI gegenereerd.
* Uitvoerindeling video: IP
    * 3840 x 2160: 29,97 fps, 14,99 fps, 5,00 fps (4:2:0 8 bits)
    * 1920 x 1080: 59,94 fps, 29,97 fps, 14,99 fps, 5,00 fps (4:2:0 8-bits)
    * 1280 x 720: 59,94 fps, 29,97 fps, 14,99 fps, 5,00 fps (4:2:0 8-bits)
    * 640 x 360: 59,94 fps, 29,97 fps, 14,99 fps, 5,00 fps (4:2:0 8-bits)
    – Als 59,94/50,00 Hz is geselecteerd als framefrequentie, kan de indeling 3840 x 2160 niet worden geselecteerd.
    – Er kan geen framerate worden geselecteerd die de framefrequentie overschrijdt.
    – JPEG heeft één vast patroon, afhankelijk van de framefrequentie (indeling staat vast en kan niet worden geselecteerd)
    - Resolutie: 1280 x 720
    - Wanneer de framefrequentie 59,94/50,00 Hz: 14,99 fps bedraagt, wanneer de framefrequentie 23,98 Hz: 11,99 fps bedraagt, wanneer de framefrequentie 29,97/25,00 Hz: 12,50 fps bedraagt
* Uitvoerindeling video: USB
    * Motion JPEG:
    * 1920 x 1080
      * 59,94 Hz: 12,00 fps, 5,00 fps
      * 50,00 Hz: 12,50 fps, 5,00 fps
    * 1280 x 720
      * 59,94 Hz: 12,00 fps, 5,00 fps
      * 50,00 Hz: 12,50 fps, 5,00 fps
    * 640 x 360
      * 59,94 Hz: 12,00 fps, 5,00 fps
      * 50,00 Hz: 12,50 fps, 5,00 fps
* Vooraf ingesteld: Aantal voorkeuze-instellingen: max. 100 (startpositie inbegrepen)

### Interface
* Communicatiebediening: LAN, Wi-Fi, Serieel, IR
* Netwerkaansluiting: LAN x 1, RJ45, 1000Base-T
* 3G-SDI OUT-aansluitpunt:
  * BNC-aansluiting (alleen uitvoer) x 1, 0.8 Vp-p/75 Ω, ongebalanceerd
  * Compatibel met SMPTE 424, SMPTE 425, SMPTE ST 299-2
  * Ingesloten geluid en tijdcode (VITC/LTC)
* GEN-LOCK-aansluiting: -
* HDMI OUT-aansluitpunt: HDMI-aansluiting x 1, alleen uitvoer
* RS-422-aansluiting: RJ45-connector x 1
* INPUT 1/INPUT 2-aansluitingen: -
* MIC-aansluiting:
  * ϕ3,5 mm-stereominiplug (ongebalanceerd, plug-invoeding ondersteund)
  * Gevoeligheid (MIC): -72 dBV (handmatig volumecentrum, volledige schaal-18 dB)/1 kΩ of meer / Att.: 20 dB
  * Gevoeligheid (LINE):- 10 dBV (handmatig volumecentrum, volledige schaal -18 dB) / 1 kΩ of meer
  * Voedingsspanning: 2,4 V DC (weerstand: 2,2 kΩ)
* USB-aansluiting:
  * Type-A (USB 2.0) x 1 (toekomstige uitbreiding)
  * Type-C (USB 3.0) x 1

### Fysieke kenmerken
* Gebruiksomgeving
    * Temperatuur
    * 0°C – +40°C
    * Luchtvochtigheid
    * 10% – 90% (zonder condensvorming)
* Voeding
  * PoE: PoE+-voeding via LAN-aansluiting (compatibel met IEEE802.3at)
  * Externe voeding: 24 V DC (bij gebruik van meegeleverde AC-adapter)
* Stroomverbruik
  * PoE+-invoer: ca. 16,2 W* max. (alleen camera)
  * DC-invoer: ca. 15,0 W max. (alleen camera)
    * Klasse 4 (25,5 W vereist) voor stroomvoorziening
* Afmetingen (b x h x d): Ca. 154 x 178 x 164 mm (zonder uitstekende delen)
* Gewicht: Ca. 2,2 kg (alleen behuizing)

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.


## Interactives

Dit onderdeel wordt gebruik in de interactives:

{{< pagelist interactives >}}

## Bijlagen

{{< pagelist bijlagen >}}
