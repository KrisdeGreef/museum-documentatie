---
title: "christie-dwu1075gs"
date: 2019-12-03T14:50:02+01:00
draft: false
merk: Christie
model: DWU1075-GS
leverancier:
attachments:
- title: christie-dwu1075gs_handleiding.pdf
  src: https://files.museum.naturalis.nl/s/sjzy7Eg2j98YLsp
- title: christie-1075-gs-specsheet.pdf
  src: https://files.museum.naturalis.nl/s/33w64DsKrDjcG58
---

![dwu1075gs](./dwu1075gs.jpg)
![dwu1075gs_rear](./dwu1075gs_rear.jpg)
<!-- Voeg een algemene omschrijving van het component toe -->

## Eigenschappen

* Levensduur lichtbron: 20.000 hrs
* Type lamp: 1 x 0.67" DMD
* Projectie techniek: Laser Phosphor with Christie BoldColor Technology
* ANSI lumen: 9500
* Resolutie: 1920 x 1200
* Beeldverhouding: 16:10
* Statische contrastverhouding:
  * Full On/Off (Christie® RealBlack™ enabled): 2,000,000:1 • Dynamic contrast: 6000:1
  * Full On/Off (Christie RealBlack disabled): 1200:1 • ANSI 250:1
* Video-in:
  * Standaard
    * HDBaseT (RJ45 x 1)
    * HDMI x 2
    * DVI-D x 1 (digital only)
    * HD15 (VGA) x 1
    * 3GSDI x 1 (BNC)
    * USB x 1 (type B mini) – display over USB
    * USB x 1 (type A) – image viewer or WiFi® dongle (optional)
    * RJ45 x 1 (ChristiePresenter over wired network connection)
  * 3D
    * HDMI 1.4, HDBaseT – frame packed, top/bottom, side by side • Up to 120Hz output
    * 3D Sync in (BNC) – for genlocking projectors
* Bediening:
  * RS232 In
  * Ethernet (10/100) RJ45
  * Built-in backlit keypad
  * IR remote
  * Wired remote control (3.5mm stereo)
* Verbinding (Ethernet): 10/100MB/s RJ45
* Afmetingen: (LxWxH): 480 x 555 x 190mm w/o feet or lens
* Vermogen: 1070W
* Gewicht: 24,9kg w/o lens

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
