---
title: "pharos-lpc2"
date: 2019-12-03T14:50:07+01:00
draft: false
merk: Pharos
model: LPC2
leverancier: Ata Tech
shows:
- showaarde
- showrexperience
attachments:
- title: pharos-lpc2_gegevensblad.pdf
  src: https://files.museum.naturalis.nl/s/z7Qkzi2YCwdgEtD
- title: Pharos Installation Guide (pdf)
  src: https://files.museum.naturalis.nl/s/PSqzQW5QbrLdTQT
---

![pharos-lpc](pharos_lpc.png)
The Pharos LPC (Lighting Playback Controller) is an award-winning, all-in-one control solution for the med entertainment and LED lighting installations.

## Eigenschappen

* Vermogen: 4W
* Afmetingen:

  * Hoogte: 58 mm
  * Breedte: 143,6 mm
  * Diepte: 90 mm

* Gewicht: 0,5 kg

## Technische specificaties

Lighting Playback Controller 1: 1024 channels DMX/eDMX

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Shows

Dit scherm wordt gebruik in de shows:

{{< pagelist shows >}}

## Bijlagen

{{< pagelist bijlagen >}}
