---
title: "flir-brickstream3dgen2"
date:
draft: false
merk: Flir
model: Brickstream 3D Gen2
leverancier:
attachments:
- title: Brickstream3D-Gen2-Datasheet-US (pdf)
  src: https://files.museum.naturalis.nl/s/MWKdJXDasKLEg9D
- title: Quick-Guide-Configuration-English
  src: https://files.museum.naturalis.nl/s/ABsZysaLjCiJEfD
- title: Quick-Guide-Calibration-English
  src: https://files.museum.naturalis.nl/s/H8NM9g6LrATndct
- title: Quick-Guide-Setup-English
  src: https://files.museum.naturalis.nl/s/Lg8L4XXE9tP9TfW
- title: Brickstream-3D-Gen2-Release-Notes-5.2.5246.2386
  src: https://files.museum.naturalis.nl/s/NnCtbNKrg75JsLm
- title: Brickstream-3D-Gen2-Programmer-Guide-5.2
  src: https://files.museum.naturalis.nl/s/aigobsYHRbHqfEb
- title: Brickstream-3D-Gen2-Configuration-Guide-5.2
  src: https://files.museum.naturalis.nl/s/LancbcmWtoEpjQz
- title: Brickstream-3D-Gen2-Install-Guide-5.2
  src: https://files.museum.naturalis.nl/s/EaaZ5mZbj7H3RNk
- title: Brickstream-3D-Gen2-Advanced-Configuration-Guide-5.2
  src: https://files.museum.naturalis.nl/s/mMZFFJkSENxc9tn
- title: Tech-Note-Employee-Filtering
  src: https://files.museum.naturalis.nl/s/gCyFAF8ZGforBEF
- title: firmware
  src: https://files.museum.naturalis.nl/s/EcXctaW3JDnBiFo
---

![brickstream3dgen2](./brickstream3dgen2.png)

<!-- Voeg een algemene omschrijving van het component toe -->

## Eigenschappen

* Afmetingen: 59 x 162 x 40.5 mm
* Gewicht: 0.25 kg
* Power: Maximum 6W, Power-over-Ethernet (Class 2)


## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
