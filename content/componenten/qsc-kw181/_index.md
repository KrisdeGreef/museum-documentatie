---
title: "qsc-kw181"
date: 2019-12-03T14:50:08+01:00
draft: false
merk: QSC
model: kw181
leverancier:
interactives:
- cirkelprojectie
shows:
- showjapanstheater
- showleven
- showrexperience
attachments:
- title: qsc-kw181_gegevensblad.pdf
  src: https://files.museum.naturalis.nl/s/NBojwYW4QizzLJE
- title: rexperience_hardware_avh_speakers_qsc_kwseries (pdf)
  src: https://files.museum.naturalis.nl/s/iSXnLTnPMeEEKYY
---

![kw181](./kw181.png)
![kw181](./kw181_rear.png)
QSC KW181 actieve subwoofer


## Eigenschappen

* Vermogen: 1000W (2 x 500W)
* Afmetingen: 510 mm x 595 mm x 761 mm with casters
* Inputs:
  * XLR
  * ¼": 38k balanced
  * 19k unbalanced
* Gewicht: 37,5kg

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Interactives

{{< pagelist interactives >}}

## Shows

{{< pagelist shows >}}

## Bijlagen

{{< pagelist bijlagen >}}
