---
title: "smartmetals-0021065"
date: 2019-12-03T14:50:09+01:00
draft: false
merk: smartmetals
model: 0021065
leverancier:
attachments:
- title: smartmetals-0021065_gegevensblad.pdf
  src: https://files.museum.naturalis.nl/s/oGGNKG8grDeSqoE
---

![smartmetals-0021065](./0021065.png)
Wandbeugel: 600 - 900 mm

## Eigenschappen

* Vermogen: 25kg
* Afmetingen:

  * Hoogte: 600 - 900 mm
  * Breedte:
  * Diepte:

* Gewicht: 7.70 kg

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
