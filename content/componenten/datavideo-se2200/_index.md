---
title: "datavideo-se2200"
date: 2019-12-03T14:50:03+01:00
draft: false
merk: Datavideo
model: SE-2200
interactives:
- studiolab
leverancier:
attachments:
- title: datavideo-se2200_handleiding.pdf
  src: https://files.museum.naturalis.nl/s/a3WfDeZ7LZxDiRC
---

![SE-2200-image](./SE-2200-image.png "SE-2200-image")
![SE-2200-image-rear](./SE-2200-image-rear.jpg "SE-2200-image-rear")
Video mixer

## Eigenschappen

* Afmetingen 19" mixer:

  * Hoogte: 88 mm excl 5 mm pootjes
  * Breedte: 432.4 mm / 482 mm
  * Diepte: 302 mm

* Afmetingen paneel:

  * Hoogte: 83 mm
  * Breedte: 430.98 mm
  * Diepte: 320 mm

## Technische specificaties

* Video inputs: 6 video inputs (6SDI or 4SDI+2HDMI )
* Video outpus: 6x SDI Outputs assignable
  * AUX 1-4, PGM, PVW, PGM Clean &multi-view
  * 2x HDMI assignable: PGM or multi-view
* HD Format Support:
  * 1080i50
  * 1080i59.94
  * 1080i60
  * 720p50
  * 720p59.94
  * 720p


## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.


## Interactives

  Dit component wordt gebruik in de interactives:

  {{< pagelist interactives >}}


## Bijlagen

{{< pagelist bijlagen >}}
