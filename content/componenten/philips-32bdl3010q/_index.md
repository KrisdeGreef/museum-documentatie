---
title: "philips-32bdl3010q"
date:
draft: false
merk: philips
model: 32bdl3010q
leverancier: ATA-tech
attachments:
- title: Techinsche Specificaties
  src: https://files.museum.naturalis.nl/s/XLrdZ2KZjPGqtJ7
---

![32BDL3010Q](./32BDL3010Q.png)
Eenvoudig in te stellen 18/7-display.

## Eigenschappen

* Video-in:
  * 2 x HDMI
  * 1 x DVI-I(DVI-D & VGA)
* Bediening:
  * RS232
  * RJ-45
  * IR
* Schermdiagonaal: 80 cm / 31,5 inch
* Resolutie: 1920 x 1080p
* Beeldverhouding: 16:9
* Vermogen: 50 Watt
* Kijkhoek: 178 graden x 178 graden
* Afmetingen:

  * Hoogte: 425,4 mm
  * Breedte:  726,5 mm
  * Diepte: 63,6 mm

* Gewicht: 5,2 kg

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
