---
title: "elcer-ehsa4150"
date:
draft: false
merk: Elcer
model: eHSA4-150
leverancier:
shows:
- showrexperience
attachments:
- title: Ecler_eHSA4-150_Data_Sheet (pdf)
  src: https://files.museum.naturalis.nl/s/JwfXpypFXeg3diX
- title: rexperience_hardware_avh_versterker_handleiding_ecler_ehsa_2-4-150 (pdf)
  src: rexperience_hardware_avh_versterker_handleiding_ecler_ehsa_2-4-150
---

![ehsa4150](./ehsa4150.png)
High Impedance Multichannel Amplifier

## Eigenschappen

* Afmetingen: 440x44x341mm
* Gewicht: 10,5 kg
* Power: 4 x 150 W RMS output (70/ 100V line level)

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Show

Dit component wordt in de volgende shows gebruikt:
{{< pagelist shows >}}

## Bijlagen

{{< pagelist bijlagen >}}
