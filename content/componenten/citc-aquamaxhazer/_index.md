---
title: "citc-aquamaxhazer"
date:
draft: false
merk: CITC
model: Aquamax Hazer
leverancier: Dimatec
attachments:
- title: CITC_AquaMaxMP_Flyer2019Final (pdf)
  src: https://files.museum.naturalis.nl/s/dpgM7ffjsNAZCkY
---

![aquamaxhazer1](./aquamaxhazer1.png)
![aquamaxhazer2](./aquamaxhazer2.png)
Rook machine, DMX aangestuurd.

## Eigenschappen

* Afmetingen: 53 cm x 33 cm x 30 cm
* Gewicht: 16 kg

* Controls: 2 x DMX & Remote Cycle Timer
* DMX: 2 channel, 0-100% Volume Control; 2-speed fan (speed via remote and DMX)
* Power: 1200 W

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
