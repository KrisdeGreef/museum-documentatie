---
title: "jvc-gyhm2000"
date:
draft: false
merk: JVC
model: GY-HM2000
leverancier:
attachments:
- title: jvc-gyhm2000 Handleiding (pdf)
  src: https://files.museum.naturalis.nl/s/QiSHawXsZE5pZHc
- title: JVC Camcorder API (pdf)
  src: https://files.museum.naturalis.nl/s/B6JiJMLymkHQ7ps
- title: Supported uSB adapters (pdf)
  src: https://files.museum.naturalis.nl/s/gSt8wmL9M8dN6A6
---

![](./gyhm200_300.jpg)

4KCAM COMPACT HANDHELD CAMCORDER w/INTEGRATED 12X LENS

## Eigenschappen

### GENERAL SPECIFICATIONS

* Power: DC 12V (AC adapter), DC 7.4V (Battery)
* Power Consumption: Approx. Approx. 7.9W （with VF in 4K REC mode, default setting)
* Weight: Approx. 3.53 lb (1.6kg) (including battery)
* Dimensions:149(W) x 191(H) x 307(D)mm　　　　　　　　　　　　　　　
* Operation temperature: 0℃ to 40℃　(32°F to 104°F)
* Storage temperature: -20℃ to 50℃ ( 14°F to 122°F)
* Operating humidity: 30% to 80%
* Storage humidity: under 85%

### CAMERA

* Image Sensor:
  * 1/2.3-inch Back Side Illuminated CMOS
  * Total 12.4M pixels
* Synchronizing: Internal synchronization
* Stabilizer: Optical image stabilizer
* Shutter speed: 1/6～ 1/10000
* Lens:
  * F1.2 (wide) to F3.5 (tele)
  * f=4.67mm to 56.04mm
  * 35mm equivalent: 29.6mm to 355mm
* Filter diameter: 62mm
* Gain: 0, 3, 6, 9, 12, 15, 18,21,24 dB, Lolux(30,36 dB), AGC
* ND filter: none , 1/4, 1/16
* LCD display: 3.5-inch 920 k pixels, 16:9
* Viewfinder:0.24-inch 1.56 M pixel, 16:9

### VIDEO/AUDIO RECORDING

* Recording media: 2x SDHC/SDXC memory card (4K:UHS-1 U3, HD:50Mbps Class10, HD:35Mbps Class6, AVCHD/SD Class4)
* Video recording
  * Video codec: MPEG-4 AVC/H.264 (4K/HD/SD/Proxy) ,AVCHD(HD/SD)
  * File format : MOV(H.264), MTS(AVCHD)

* 4K(H.264)
  * NTSC setting: 3840 x 2160/29.97p, 23.98p (70,150 Mbps)
  * PAL setting: 3840 x 2160/25p (70,150Mbps)
  * PAL setting: 3840 x 2160/25p
* HD(H.264)
  * NTSC setting:
    * YUV422 mode:1920 x1080/59.94p.29.97p,23.98p(50Mbps),
    * XHQ mode: 1920x1080/59.94p,29.97p,23.98p(50Mbps)
    * UHQ mode: 1920x1080/59.94i,29.97p, 23.98p(35Mbps) 1280x720/59.94p(35Mbps)
  * PAL setting:
    * XHQ mode:1920 x 1080/50p(50Mbps)
    * XHQ mode:1920x1080/50p,50i,25p(50Mbps),1920x1080/50i,25p(35Mbps), 1280x720/50p(35Mbps)
* AVCHD
  * NTSC setting:
    * Progressive mode(Max 28Mbps):1920 x 1080/59.94p,
    * HQ mode(24Mbps):1920 x 1080/59.94i, SP mode(17Mbps):1920 x1080/59.94i
    * LP mode(9Mbps):1440 x 1080/59.94i (Web mode), EP mode(5Mbps):1440 x1080/59.94i (Web mode)
  * PAL setting:
    * Progressive mode(Max 28Mbps):1920 x 1080/50p,
    * HQ mode(24Mbps):1920 x 1080/50i, SP mode(17Mbps):1920 x1080/50i
    * LP mode(9Mbps):1440 x 1080/50i (Web mode), EP mode(5Mbps):1440 x1080/50i (Web mode)
* SD(MOV/AVCHD)
  * NTSC setting: 720 x 480/59.94i (8 Mpbs)
* Proxy(H.264)
  * NTSC setting: HQ mode(3Mbps):960 x 540/29.97p, 23.98p, LP mode(1.2Mbps):480 x 270/29.97p, 23.98p
  * PAL setting: HQ mode(3Mbps):960 x 540/25p, LP mode(1.2Mbps):480 x 270/25p


* Audio recording
  * LPCM 2ch, 48kHz/16-bit(4k/HD/SD MOV)), AC3 2ch(AVCHD), μlow 2ch(Proxy)

### LIVE VIDEO STREAMING

* Protocol: RTMP, MPEG2-TS/UDP, MPEG2-TS/TCP, RTSP/RTP, ZIXI
* Bitrate: 0.2 - 12 Mbps
* Resolution: 1920x1080, 1280x720, 720x480, 480x270

### INTERFACE

* Video output
  * AV output (φ3.5mm mini jack x1)
  * SDI output (BNC x1)
  * HDMI output x1
* Audio input: XLR x2 (MIC,+48V/LINE), φ3.5mm mini jack x1
* Audio output: AV output (φ3.5mm mini jack x1)
* Headphone: φ3.5mm mini jack x1
* Remote: φ2.5mm mini jack x1
* USB:
  * HOST x1(Network Connection) , DEVICEx1(Mass storage)
  * Supported devices: Verizon, AT&T 4G LTE modems, Wi-Fi and LAN adapters
* Network Function: Live Video Streaming, FTP, Remote Control

### INCLUDED ACCESSORIES

* Handle unit, Battery(SSL-JVC50) x1, AC Adapter x1

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
