---
title: cls-focushptdmx
date:
draft: false
merk: CLS
model: Focus HP T DMX
leverancier: CLS
attachments:
- title: CLS Focus HP T DMX manual (pdf)
  src: https://files.museum.naturalis.nl/s/xofrYXDDETkatEm
- title: CLS Focus HP T DMX (pdf)
  src: https://files.museum.naturalis.nl/s/cgb9HHZAjgfC6zz

---

![focushptdmx](./focushptdmx.png)
6 Watt, compact zoom fixture.
## Eigenschappen

* Dimensions (LxWxH): 86 mm x 36 mm x 143 mm
* Weight: 220 gr

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
