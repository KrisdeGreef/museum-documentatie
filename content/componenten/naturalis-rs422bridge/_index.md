---
title: "naturalis helmet-rs422-bridge"
date: 2019-07-25T13:06:06Z
merk: Sparkfun 
model: ESP32-thing
leverancier: Frank Verweij/Naturalis
experiences: 
 - schattenjacht
attachments:
- title: Bridge schema 
  src: BLE_RS422_SCH.pdf
- title: Bridge PCB
  src: BLE_RS422_PCB.pdf
- title: Power supply
  src: Mean_well_RS-25-5.pdf
- title: Helmet RS422 Bridge Gitlab repository
  src: https://gitlab.com/naturalis/mii/schattenjacht-rs422-bridge
---

De BLE-RS422-Bridge is een apparaat dat in de schatkamer de Bluetooth Low
Energy signalen van de daar gebruikte schattenjacht helmpjes opvangt en via een
RS422 verbinding naar de Pharos lichtcomputer in de SER stuurt.

## Algemeen

Er is een printplaat gemaakt waarop de Sparkfun ESP32 Thing microcontoller gemonteerd wordt. 

Op het processorbord is een led geplaatst die elke seconde flitst. Hieraan kan
men zien dat de software loopt. 

Op deze printplaat is een led geplaatst die flitst wanneer een boodschap via RS422 verstuurd wordt.

![alt_text](image2.jpg "image_tooltip")

Het belangrijkste deel van de hardware is het [Sparkfun ESP32 Thing](https://learn.sparkfun.com/tutorials/esp32-thing-hookup-guide) processorbord. Er is een [printplaat](https://drive.google.com/file/d/1ltIi-dySeP_sJjVv1t4SgqRsiyOyMj_2/view?usp=sharing) gemaakt waar dit processorbord op gemonteerd kan worden. 

Het schema van deze print is [hier](https://drive.google.com/file/d/1x4rCtGhhHyr8a5AbGcdQL6MAMStW6vvd/view?usp=sharing) te vinden.

### Deelschema’s

De aansluitingen op het processorboard.

![aansluitingen](image7.png "aansluitingen")

IO17 wordt als seriële uitgang gebruikt. Dit signaal wordt via een optocoupler
verbonden met de ingang van een MAX 483 chip verbonden. Deze chip zet het
signaal om in RS422.

De instelling van het seriële signaal is: 9600, 8 data bits , geen parity en 1 stopbit



### RS422 interface


![rs422 converter](image3.png "rs422 converter")


De MAX483CPA chip die het RS422 signaal genereert is via een optocoupler
(SFH6106-3) verbonden met de TX lijn van de microprocessor. De chip wordt van
spanning voorzien door onderstaande DC-DC converter-schakeling. Door deze
configuratie is de processor galvanisch gescheiden van de RS422 uitgang. 

### DC-DC converter

![DC-DC converter](image1.png "DC-DC converter")


De 5 volt spanning op pin 1 en 2 van de Traco Power TMR3-0511 DC-DC converter
is galvanisch gescheiden van de 5 volt spanning die op de uitgang pin 6 en 7
staat.

Hierdoor wordt de processor beschermd tegen storingen van buitenaf. (belangrijk
bij lange leidingen)


### Monitor led

![monitor led](image6.png "monitor led")

Deze schakeling laat led D1 branden als er data over de seriële poort verstuurd
wordt. Hiermee kan de werking van de BLE-RS422 Bridge gecontroleerd worden.


### Voeding

![voeding](image4.png "voeding")


De print is samen met een [5 volt voeding](https://drive.google.com/file/d/1ETYqB4hpKjwTR9xLCYPoh5AlnBadkqLU/view?usp=sharing) in een [behuizing](https://nl.farnell.com/hammond/1554h2gycl/small-enclosure-small-pc-grey/dp/2988671?CMP=i-bf9f-00001000) gebouwd.


![voeding print](image5.jpg "voeding print")


Het RS422 signaal wordt via een 3 polige DIN connector naar buiten gevoerd.

De aansluitingen van het signaal zijn:

* Pin 1: T-
* Pin 2: Gnd
* Pin 3: T+

De net entree is geschikt voor en standaard Euro snoer en heeft een zekering. 

(1,6A traag)

## Software

### BLE-RS422 bridge

De BLE-RS422-Bridge is een apparaat dat in de schatkamer de Bluetooth Low
Energy-signalen van de daar gebruikte schattenjacht helmpjes opvangt en een
RS422 verbinding doorstuurt naar de Pharos lichtcomputer 

### BLE

De communicatie tussen de helmen en de hub verloopt via een BLE protocol.

[Informatie over BLE](https://www.oreilly.com/library/view/getting-started-with/9781491900550/ch01.html) is ruimschoots te vinden op internet.

In BLE terminologie krijgen BLE apparaten rollen toebedeeld. In het geval van
het helmen systeem gebruiken we de rollen “Broadcaster” en “Observer”.

> A device in the **Broadcaster** role does nothing more than **transmitting** data to its surroundings. It does so by constantly advertising, and usually has useful data in the advertising packet, data that is meant for everyone to see. Such a device does not require a receiver, as its only role is to broadcast to others, so it never accepts connections. The **Observer** is the opposite of the Broadcaster: it passively **listens** to BLE devices in its area and processes the data from the advertising packets it receives. It does not need a transmitter, as it sends nothing and is never meant to enter a connection.

De helmen zijn “Broadcasters”. Ze zenden advertisement packets uit en ze zijn niet connecteerbaar.

De ontvanger is een “Observer”. Deze scant  de BLE radiofrequenties af voor advertisement packets.

Over de formatering en syntax van de advertisement packets voor deze toepassing [hier](https://docs.google.com/document/d/1L0iY1KwRpBaLZ0ajCHbuVczqWuKo2LDx3xcPTv1cbyE/edit?usp=sharing) meer.

De keuze voor deze manier van communiceren is gemaakt vanwege de
energiezuinigheid. De helmen werken op een accu dus hoe minder stroomverbruik
hoe beter.

### Software

De software loopt op een Sparkfun ESP32 processor board..

De software voert elke seconde een BLE scan uit. In deze scan worden alle
advertisement packets die op dat moment uitgezonden worden verzameld. De
software selecteert de packets die afkomstig zijn van schattenjachthelmen en
filtert de “gevonden object” boodschappen eruit.

Indien een object gevonden is wordt dit als commando naar de Pharos gestuurd.

De commando’s zijn records die bestaan uit de tekens “OBJ” + twee digits.
Voorbeeld: “OBJ09” als de spot bij object 9 moet gaan branden.


### Watchdog

De ESP32 heeft een aantal timers. Timer0 is zo geconfigureerd dat deze om de
100 milliseconde een interrupt geeft. Deze timer vormt de tijdbasis voor de
scheduler.

Timer1 is geconfigureerd om een interrupt te genereren na 5 seconden. Deze
timer wordt gebruikt als watchdog. In de loop() functie wordt timer1 steeds
gereset. Als de software ergens blijft hangen zal er na 5 seconde een reset
plaatsvinden.


## Technische specificaties

Te programmeren met behulp van [platformio.org](https://platformio.org/).

## Known issues

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
