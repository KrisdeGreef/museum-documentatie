---
title: "kramer-tp580t"
date:
draft: false
merk: Kramer
model: TP-580 T
leverancier:
shows:
- showrexperience
attachments:
- title: PT-580T Datasheet (pdf)
  src: https://files.museum.naturalis.nl/s/N6q4fS9JDtWXy3q
- title: rexperience_hardware_handleiding_kramer_extender_pt-580t_tp-580t_tp-580r (pdf)
  src: https://files.museum.naturalis.nl/s/ikKiwQ9G4yHYoef
---

![tp580t](./tp580t.png)
PT580T is a compact high performance, longreach HDBaseT transmitter for 4K60Hz (4:2:0) HDMI signalover twisted pair.

## Eigenschappen

* Afmetingen: 6.22cm x 5.18cm x 2.44cm

* Gewicht: 0,1 kg

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.


## Shows

Dit scherm wordt gebruik in de shows:

{{< pagelist shows >}}

## Bijlagen

{{< pagelist bijlagen >}}
