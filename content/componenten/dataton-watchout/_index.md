---
title: "dataton-watchout"
date: 2019-12-03T14:50:03+01:00
draft: false
merk: Dataton
model: WATCHOUT
leverancier:
interactives:
- cirkelprojectie
- projectiewaterplaats
attachments:
- title: dataton-watchout_handleiding.pdf
  src: https://files.museum.naturalis.nl/s/tgtbSYECrnD9cF9
---

![watchout](./watchout_schema.png)

Dataton Watchout is de software waarmee Dataton Watchpax players worden
geconfigureerd.

## Technische specificaties

De versie van de Watchout software die je gebruikt om content up te daten moet
overeenkomen met de versie die op de Watchpax players draait. De in het museum
gebruikte versie is 6.3.1. Deze versie is [te downloaden van de content
server](https://files.museum.naturalis.nl/s/zkmXQfEaH87me5p) en staat eveneens
geïnstalleerd op de gemeenschappelijk laptop van het Technisch Team.

## Known issues

* Na het updaten van content op de Watchpax players vanuit Watchout moet
  Watchout worden afgesloten en de Watchpax players opnieuw worden opgestart om
  er voor te zorgen dat de players luisteren naar de showcontroller. Volg de
  stappen in [de handleiding voor het updaten van content]({{< relref
  "handleidingen/updaten-watchpax-players" >}}).

## Interactives

  Dit component wordt gebruik in de interactives:

  {{< pagelist interactives >}}

## Bijlagen

{{< pagelist bijlagen >}}
