---
title: "theaterlab-switchdac62a"
date:
draft: false
merk: Theater technisch Lab
model: SwitchDAC6-2A
leverancier:
attachments:
- title: rexperience_licht_handleiding_switchdac6-2a (pdf)
  src: https://files.museum.naturalis.nl/s/gAGPKBCPZ8JpnXL
---

![](./.png)

<!-- Voeg een algemene omschrijving van het component toe -->

## Eigenschappen


## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
