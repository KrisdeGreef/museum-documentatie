---
title: "allenheath-dt168"
date:
draft: false
merk: allenheath
model: dt168
leverancier: ata-tech
attachments:
- title: getting started
  src: https://files.museum.naturalis.nl/s/isjtZEq78a6XqmH
- title: technical Datasheet
  src: https://files.museum.naturalis.nl/s/TgtxbmZc2bW9twr
---

![dt168](./dt168.jpg)
16 XLR Input / 8 XLR Output – 96kHz Dante Expander

## Eigenschappen

* Vermogen: 40W max
* Afmetingen:

  * Hoogte: 185mm
  * Breedte: 410mm
  * Diepte: 192mm

* Gewicht: 4,6kg

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.


## Bijlagen

{{< pagelist bijlagen >}}
