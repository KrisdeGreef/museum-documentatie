---
title: "soliddrive-sd1g"
date:
draft: false
merk: soliddrive
model: sd1g
leverancier:
shows:
- showrexperience
attachments:
- title: datasheet
  src: https://files.museum.naturalis.nl/s/cbMaHpaA8K6qoBR
- title: installation guide
  src: https://files.museum.naturalis.nl/s/gGraFmN43AJAtar
---

![SD1g](./SD1g.jpg)
SolidDrive SD-1 Glass Surface Mount Actuator in Chrome.
The patented technology at the heart of SolidDrive ® transforms any solid surface (such as drywall, glass, wood, or fiberglass) into a sound source.

## Eigenschappen

* Impedatie: 8 Ohm

* Afmetingen:
  * Hoogte: 51 mm
  * Diameter: 89 mm

* Gewicht: 0.54 kg

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Shows

{{< pagelist shows >}}


## Bijlagen

{{< pagelist bijlagen >}}
