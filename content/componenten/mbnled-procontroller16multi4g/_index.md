---
title: "mbnled-procontroller16multi4g"
date:
draft: false
merk: MBNLED
model: Pro Controller 16 Multi 4G
leverancier: de Cirkel
attachments:
- title: MBNLED Pro Controller 16 Multi 4G manual (pdf)
  src: https://files.museum.naturalis.nl/s/pKTnXMDoTB23PcM

---

![procontroller](./procontroller.png)
16 channels 12-24V led driver

## Eigenschappen

* Afmetingen:
  * Diepte: 80 mm
  * Hoogte: 23 mm
  * Breedte: 240 mm
* Gewicht: 300 gr


## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
