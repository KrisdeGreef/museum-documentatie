---
title: "hager-svn311"
date: 2019-12-03T14:50:04+01:00
draft: false
merk: Hager
model: SVN311
leverancier:
attachments:
- title: hager-svn311_gegevensblad.pdf
  src: https://files.museum.naturalis.nl/s/zH2dweMasp3X9aR
---

![SVN311](./SVN311.jpg "SVN311")
Pulsdrukker 1 maak, 16 A, 230 VAC

## Eigenschappen

* Vermogen:
* Afmetingen:

  * Hoogte: 83 mm
  * Breedte: 17,5 mm
  * Diepte: 66 mm

* Gewicht:

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
