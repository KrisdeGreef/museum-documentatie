---
title: "soraa-mr16gu10"
date:
draft: false
merk: Soraa
model: MR16 GU10
leverancier: Q-Cat
attachments:
- title: Soraa MR16 GU10 (pdf)
  src: https://files.museum.naturalis.nl/s/zDWDaAPditpwRxK
---

![mr16](./mr16.png)


## Eigenschappen


## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
