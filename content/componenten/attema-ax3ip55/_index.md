---
title: "attema-ax3ip55"
date: 2019-12-03T14:50:02+01:00
draft: false
merk: Attema
model: ax3ip55
leverancier:
attachments:
- title: attema-ax3ip55_gegevensblad.pdf
  src: https://files.museum.naturalis.nl/s/RX4YH93NGH9cgBk
---

![ax3ip55](./ip55.png "ax3ip55")
Doos voor montage op de wand/plafond

## Eigenschappen

* Afmetingen:

  * Hoogte: 75mm
  * Breedte: 75mm
  * Diepte: 37mm

* Voorzien van 7 invoeringen rondom en 3 op de bodem
* Invoeren zijn geschikt voor kabel van Ø 2-14 mm


## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
