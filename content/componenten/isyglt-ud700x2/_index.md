---
title: "isyglt-ud700x2"
date:
draft: false
merk: ISYGLT
model: UD-700-X2
leverancier: Light Technology
attachments:
- title: ISYGLT UD-700-X2 (pdf)
  src: https://files.museum.naturalis.nl/s/BJkr8A55YK5774M

---

![ud700x2](./ud700x2.png)
Universal dimmer

## Eigenschappen

* Gewicht: 300 gr


## Technische specificaties

The universal dimmer is suited for usage with high voltage lamps, magnetic transformers, electric transformers, ESL and LED retrofit lamps. There are 2 separate dimmer channels available, which are resistant to up to 700W respectively.

Inputs/Outputs
* dimmer outputs 700W
* 2 inputs 0-10V or 1-10V for “emergency operation“ or “standalone operation“

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
