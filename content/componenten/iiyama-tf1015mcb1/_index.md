---
title: "iiyama-tf1015mcb1"
date: 2019-12-03T14:50:04+01:00
draft: false
merk: iiyama
model: ProLite tf1015mcb1
interactives:
  - knaagdierontvangsthal
  - camperhawaii
  - waternesten
leverancier:
attachments:
- title: Specificaties
  src: https://files.museum.naturalis.nl/s/9zTmjsm7TmAjX3A
---

![tf1015mcb1](./tf1015mcb1.jpg "tf1015mcb1")
Open Frame PCAP 10 point touch screen voorzien van een schuimrubberen afdichting vooreen naadloze (kiosk) integratie

## Eigenschappen

* Video-in:
  * 1 x Displayport
  * 1 x HDMI
  * 1 x VGA
* Bediening: touch
* Schermdiagonaal: 10.1", 25.7cm
* Resolutie: 1280:800
* Beeldverhouding: 16:10
* Vermogen: 15 W
* Kijkhoek: horizontal/vertical: 170°/170°, right/left: 85°/85°, up/down: 85°/85°
* Afmetingen: 260.5 x 183.5 x 39.5mm
* Gewicht: 1,5 KG

## Technische specificaties

* Stroomvoorziening:  DC 12V

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Interactives

Dit component wordt gebruik in de interactives:

{{< pagelist interactives >}}

## Bijlagen

{{< pagelist bijlagen >}}
