---
title: "iguzzini-p319"
date:
draft: false
merk: iGuzzini
model: P319
leverancier: de Cirkel
attachments:
- title: iGuzzini P319 (pdf)
  src: https://files.museum.naturalis.nl/s/HbkwmWCtBX2qqib
- title: iGuzzini P319 manual (pdf)
  src: https://files.museum.naturalis.nl/s/mcNHCdtAc8G7F5f
---

![p319](./p319.png)

## Eigenschappen

* Afmetingen:
  * diameter: 67 mm
  * Hoogte: 77 mm
* Gewicht: 13 gr


## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
