---
title: cls-linacliphp
date:
draft: false
merk: CLS
model: Lina Clip HP
leverancier: CLS
attachments:
- title: CLS Lina Clip HP (pdf)
  src: https://files.museum.naturalis.nl/s/FP85bZR8gnFxFdL
- title: CLS Lina Clip HP manual (pdf)
  src: https://files.museum.naturalis.nl/s/mrkZQpqo8nxPjmm

---

![lina-clip-hp](./lina-clip-hp.jpg)

## Eigenschappen



## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
