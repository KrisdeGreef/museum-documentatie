---
title: "neutrik-np2x"
date: 2019-12-03T14:50:07+01:00
draft: false
merk: Neutrix
model: pn2x
leverancier:
attachments:
- title: neutrik-np2x_technischetekening.pdf
  src: https://files.museum.naturalis.nl/s/doJ7Bb5obFrSKRB
---

![np2x](np2x.png)
6.35mm mono jackplug

## Eigenschappen


## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
