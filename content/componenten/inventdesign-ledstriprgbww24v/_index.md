---
title: "inventdesign-ledstriprgbww24v"
date:
draft: false
merk: Invent Design
model: LED strip RGBWW 24V
leverancier:
attachments:
- title: Invent Design LED Strip RGBWW 24V (pdf)
  src: https://files.museum.naturalis.nl/s/k95ZHB942rkCKkY

---

![ledstriprgbww24v](./ledstriprgbww24v.png)
LED Strip RGBWW 5m 24V

## Eigenschappen

* Afmetingen:
  * Lengte: 5000 mm
  * Hoogte: 2 mm
  * Breedte: 10 mm
* Gewicht: 34 gr


## Technische specificaties

* Power consumption: 21,6 W/m
* Input Voltage: 24V DC
* Current max: 0,9A/m
* Powersupply: External

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
