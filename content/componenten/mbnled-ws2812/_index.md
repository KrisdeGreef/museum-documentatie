---
title: "mbnled-ws2812"
date:
draft: false
merk: MBN LED
model: WS2812
leverancier:
attachments:
- title: rexperience_licht_handleiding_mbnled_pixelcontroller
  src: https://files.museum.naturalis.nl/s/6ispMzRwTecrnE6
---

![ws2812](./ws2812.png)

PIXEL CONTROLLER

## Eigenschappen


## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
