---
title: "datalogic-joyatouch3slotcradle"
date:
draft: false
merk: datalogic
model: joya touch 3 slot cradle
leverancier:
attachments:
- title: Installation Guide
  src: https://files.museum.naturalis.nl/s/6x9zSgtMDQnkgDJ
---

![joyatouch3slotcradle](./joyatouch3slotcradle.png)
For Wall Mounting, requires power supply, line cord and wall bracket.
3-Slot Cradle-Wall Mount.

## Eigenschappen

* Afmetingen: 313 x 120 x 110 mm

* Gewicht: 1280 g

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
