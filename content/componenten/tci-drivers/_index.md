---
title: "tci-drivers"
date:
draft: false
merk: TCI
model: Drivers
leverancier: Diversen
attachments:
---

[![md20](./md20.png)](https://files.museum.naturalis.nl/s/fjBejjgLzrRabXE)
TCI DC Mini Jolly MD BI

[![minijolly](./minijolly.png)](https://files.museum.naturalis.nl/s/egWE9JK89wgnDeS)
TCI DC Mini Jolly

[![hvbilevel](./hvbilevel.png)](https://files.museum.naturalis.nl/s/QncNwNfQXXp5iWx)
TCI DC Maxi Jolly HV Bilevel

[![us](./us.png)](https://files.museum.naturalis.nl/s/HxxP3ogKCCM9Tfw)
TCI DC Jolly US

## Eigenschappen


## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
