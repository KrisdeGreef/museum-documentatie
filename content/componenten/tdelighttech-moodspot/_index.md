---
title: "tdelighttech-moodspot"
date:
draft: false
merk: TDE Lighttech
model: Moodspot RGBW
leverancier: TDE Lighttech
attachments:
- title: TDE Moodspot RGBW (pdf)
  src: https://files.museum.naturalis.nl/s/JWa9pcTFZKa4TYW
---

![moodspot](./moodspot.png)


## Eigenschappen

* Afmetingen:

  * Hoogte: 134 mm
  * Diameter: 148 mm

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
