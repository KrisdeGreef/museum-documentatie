---
title: "proliad-accentspot"
date:
draft: false
merk: Proliad
model: Accentspot
leverancier: Proliad
attachments:
- title: Proliad Accentspot manual (pdf)
  src: https://files.museum.naturalis.nl/s/Z2eGwJwEDDL9DHF
- title: Proliad Accentspot (pdf)
  src: https://files.museum.naturalis.nl/s/7HJXar9WcbWCS46
---

![accentspot](./accentspot.png)

## Eigenschappen

* Afmetingen:
  * Diameter: 60 mm
  * Hoogte: 230 mm


## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
