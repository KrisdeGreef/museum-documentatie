---
title: "volfoni-smartcrystalcinemaneo"
date:
draft: false
merk: Volfoli
model: Smart Crystal Cinema Neo
leverancier: ATAtech
decors:
- capsule
interactives:
- projectiejungle
attachments:
- title: rexperience_hardware_avh_beamer_handleiding_polarisationmodulator1 (pdf)
  src: https://files.museum.naturalis.nl/s/L7TMTGzmeFt7QdP
- title: rexperience_hardware_avh_beamer_handleiding_polarisationmodulator2 (pdf)
  src: https://files.museum.naturalis.nl/s/aKwPm5iss5FmmFM
- title: rexperience_hardware_avh_beamer_specsheet_polarisationmodulator (pdf)
  src: https://files.museum.naturalis.nl/s/7YyrF3gMJjxA5sz

---

![smartcrystal](./smartcrystal.png)


## Eigenschappen

* Type lamp: Pi-Cell
* Projectie techniek: D-Cinema 3xDLP
* Max lamp power: 7 kW / 32 000 lumens

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Decors

{{< pagelist decors >}}

## Interactives

{{< pagelist interactives >}}

## Bijlagen

{{< pagelist bijlagen >}}
