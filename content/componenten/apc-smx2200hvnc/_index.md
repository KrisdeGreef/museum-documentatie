---
title: "apc-smx2200hvnc"
date:
draft: false
merk: APC
model: Smart-UPS X 2200VA Short Depth Tower/Rack Convertible LCD 200-240V with Network Card
leverancier:
shows:
  - showrexperience
attachments:
- title: Technical Specifications (pdf)
  src: https://files.museum.naturalis.nl/s/biazCJ3AnRN4N2d
- title: Installation Guide Smart-UPS (pdf)
  src: https://files.museum.naturalis.nl/s/HL8dzRK7z82R9GG
- title: Brochure Smart-UPS (pdf)
  src: https://files.museum.naturalis.nl/s/QYETFrBRTmSmNne
- title: Operation Manual (pdf)
  src: https://files.museum.naturalis.nl/s/WSbyQ94bwX7zmY4
---

![smart-front](./smart-front.png)
![smart-rear](./smart-rear.png)
Smart-UPS X 2200VA Short Depth Tower/Rack Convertible LCD 200-240V with Network Card

## Eigenschappen

* Afmetingen:

  * Hoogte: 432 mm
  * Breedte: 178 mm
  * Diepte: 483 mm

* Gewicht: 38,56 kg

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Show

Dit component wordt in de volgende shows gebruikt:
{{< pagelist shows >}}

## Bijlagen

{{< pagelist bijlagen >}}
