---
title: "jbl-control28"
date:
draft: false
merk: JBL
model: Control 28
leverancier:
interactives:
- studiolab
attachments:
- title: Control_28_Spec_Sheet (pdf)
  src: https://files.museum.naturalis.nl/s/rec5Fkp8y4CJsAi
---

![Control_28](./Control_28.jpg)
High Output Indoor/Outdoor Background/Foreground Speaker

## Eigenschappen

* Afmetingen: 380 x 280 x 220 mm

* Gewicht: 5,5 kg

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Interactives

Dit component wordt gebruik in de interactives:

{{< pagelist interactives >}}

## Bijlagen

{{< pagelist bijlagen >}}
