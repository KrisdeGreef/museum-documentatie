---
title: "qsc-cx254"
date: 2019-12-03T14:50:08+01:00
draft: false
merk: QSC
model: cx254
leverancier:
interactives:
- studiolab
attachments:
- title: qsc-cx254_gegevensblad.pdf
  src: https://files.museum.naturalis.nl/s/nZ4KYT8GQkqCk4a
---

![cx254](./cx254.png)
![cx254](./cx254_rear.png)
4-Channel Low-Z Power Amplifier

## Eigenschappen

* Vermogen:
* Afmetingen:

  * Hoogte: 89 mm
  * Breedte: 483 mm
  * Diepte: 356 mm

* Gewicht: 9,5kg

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Interactives

{{< pagelist interactives >}}

## Bijlagen

{{< pagelist bijlagen >}}
