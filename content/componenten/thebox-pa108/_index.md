---
title: thebox-pa108
date:
draft: true
merk: the box
model: PA180
leverancier:
- thomann
attachments:
- title: PA108 specs (pdf)
  src: https://files.museum.naturalis.nl/s/Fm63fP3e6Wos8zF

---

![pa108](./pa108.png)
![pa108 rear](./pa108_rear.jpg)
Passieve luidsprekers

## Eigenschappen

* Vermogen: 100 W
* Afmetingen: 277 x 257 x 400 mm
* Gewicht: 6,5 kg
* Impedancie: 8 Ohm

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
