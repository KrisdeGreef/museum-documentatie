---
title: "startech-cdp2hd"
date: 2019-12-03T14:50:10+01:00
draft: false
merk: startech
model: cdp2hd
leverancier:
attachments:
- title: startech-cdp2hd_gegevensblad.pdf
  src: https://files.museum.naturalis.nl/s/LBa6BTr7NyMabDw
---

![cdp2hd](./cdp2hd.png)
USB-C naar HDMI adapter met 4K 30Hz - zwart

## Eigenschappen

* Vermogen:
* Afmetingen:

  * Hoogte: 9 mm
  * Breedte: 23 mm
  * Diepte (lengte incl kabel): 147 mm

* Gewicht: 22 g

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
