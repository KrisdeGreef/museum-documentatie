---
title: cls-jadecftw
date:
draft: false
merk: CLS
model: Jade Colour Flow & Turnable White
leverancier: CLS
attachments:
- title: CLS Jade CF TW manual (pdf)
  src: https://files.museum.naturalis.nl/s/x3knwyD2Cb89YPo
- title: CLS Jade CF TW (pdf)
  src: https://files.museum.naturalis.nl/s/JejysfAArojzcca

---

![jade-colour-flow](./jade-colour-flow-800.jpg)
Unique new lighting concepts are made possible by the use of innovative new LED modules.

## Eigenschappen

* Dimensions (LxWxH): 110 mm x 75 mm x 157,7 mm
  * Diameter: 100 mm
* Weight: 780 gr

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
