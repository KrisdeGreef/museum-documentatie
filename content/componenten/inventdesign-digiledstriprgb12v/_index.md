---
title: "inventdesign-digiledstriprgb12v"
date:
draft: false
merk: Invent Design
model: DiGi LEd Strip RGB 12v
leverancier:
attachments:
- title: Invent Design DiGi LED Strip RGB 12v (pdf)
  src: https://files.museum.naturalis.nl/s/JKbbDeHZNqNpfdA

---

![digiledstriprgb12v](./digiledstriprgb12v.png)
DiGi LED Strip 5m RGB 38mm 12V

## Eigenschappen

* Afmetingen:
  * Lengte: 5000 mm
  * Hoogte: 2 mm
  * Breedte: 12 mm
* Gewicht: 160 gr


## Technische specificaties

* Input Voltage: 12V DC
* Power consumption: 6.5 W/m
* Power Supply: External (not included)

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
