---
title: "wyrestorm-cabhaoc15"
date: 2019-12-03T14:50:11+01:00
draft: false
merk: wyrestorm
model: cabhaoc15
leverancier:
attachments:
- title: wyrestorm-cabhaoc15_gegevensblad.pdf
  src: https://files.museum.naturalis.nl/s/87zkBQzi9Ctj8gH
---

![cabhaoc15](./cabhaoc15.png)

## Eigenschappen

* Vermogen:
* Afmetingen:

  * Lengte: 15m

* Gewicht:

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
