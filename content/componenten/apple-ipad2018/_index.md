---
title: "apple-ipad2018"
date:
draft: false
merk: Apple
model: iPad 2018
leverancier:
attachments:
- title: Technische specificaties
  src: https://files.museum.naturalis.nl/s/9j3qwxbCnM8Kncm
---

{{% notice warning %}}
Aandachtspunt:
Dit is ondertussen een verouderd model. Specs hieronder zijn w.s. niet meer te koop.
{{% /notice %}}

![ipad2018](./ipad2018.png)




## Eigenschappen

* ID: iPad7,6
* Naam: iPad 6th Generation
* Merk: Apple
* Model: A1954


## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
