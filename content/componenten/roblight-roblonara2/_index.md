---
title: "roblight-roblonara2"
date:
draft: false
merk: Roblight
model: Roblon Ara 2
leverancier: Q-Cat
attachments:
- title: Roblon Ara 2 (pdf)
  src: https://files.museum.naturalis.nl/s/B8TN6HXRXoaCp2X
- title: Roblon Ara 2 manual (pdf)
  src: https://files.museum.naturalis.nl/s/z8FGQrMof5Wb7Rs
---

![ara2](./ara2.png)

## Eigenschappen

* Afmetingen:
  * Diameter: 50 mm
  * Hoogte: 44 mm



## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
