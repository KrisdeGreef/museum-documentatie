---
title: "crestron-audext100"
date: 2019-12-03T14:50:02+01:00
draft: false
merk: Crestron
model: AUD-EXT 100
leverancier:
interactives:
- cirkelprojectie
- dinovoetstappen
- filmdubois
shows:
- showijstijd
- showleven
attachments:
- title: crestron-audext100_gegevensblad.pdf
  src: https://files.museum.naturalis.nl/s/J5dsybKQ9ARE8zr
---

![AUD-EXT 100](./audext100.jpg "AUD-EXT 100")
Audio over CAT5 Extenders voor ongebalanceerd stereo signaal

## Eigenschappen

* Vermogen:
* Afmetingen:

  * Hoogte: 43 mm
  * Breedte: 131 mm
  * Diepte: 97 mm

* Gewicht:

## Technische specificaties

* Audio IN: 1 pair RCA female (tulp) Unbalanced stereo line-level audio input
    * Input Impedance: 10k Ohms
    * Maximum Output Level: 2 Vrms

* Audio OUT: 1 pair RCA female (tulp) Unbalanced stereo line-level audio output
    * Output Impedance: 100 Ohms
    * Maximum Output Level: 2 Vrms

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Interactives

Dit component wordt gebruik in de interactives:

{{< pagelist interactives >}}

## Show

Dit component wordt in de volgende shows gebruikt:
{{< pagelist shows >}}

## Bijlagen

{{< pagelist bijlagen >}}
