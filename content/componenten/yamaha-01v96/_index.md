---
title: "yamaha-01v96"
date: 2019-12-03T14:50:11+01:00
draft: false
merk: yamaha
model: 01v96
leverancier:
attachments:
- title: yamaha-01v96_handleiding.pdf
  src: https://files.museum.naturalis.nl/s/nakCM4ZAMAjr3Xr
---

![01v96](./01v96.jpg)
![01V96_Rear](./01V96_Rear.jpg)
Digital Mixing Console

## Eigenschappen


* Vermogen: 70W
* Afmetingen:

  * Hoogte: 150mm
  * Breedte: 430mm
  * Diepte: 540mm

* Gewicht: 12.5 kg

## Technische specificaties


* Total # Of Inputs Up to 40 total line inputs:
  * 16 balanced 1/4" TRS (standard)
  * 16 digital with 16-I/O Serie
  * Mini-YGDAI cards (8 digital/analog with 96-kHz Series, Standard Series or third party models)

* Mic/Line Inputs:
  * 16 balanced 1/4" TRS line inputs: 12 Mono, 16 Stereo Paired
  * 12 balanced XLR microphone inputs

* Main/Monitor Out:
  * L/R balanced 1/4 TRS outputs ("monitor")
  * L/R balanced XLR outputs ("stereo outputs")

* AUX Sends:
  * 4 1/4" Phone omni outputs can be used for routing signal to external processors. The 6 internal AUX sends are automatically routed to the 2 internal DSP effects processors.

* AUX Returns:
    * Returning signal from external processors is permitted via any input channel (1-16). The optional I/O card inputs can be used as external AUX returns as well. Internal AUX Returns are also pre-routed and are accessible via fader control.

* Inserts
  * 1/4" TRS Phone Inserts: Mono Channels 1 through 12

etc....

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
