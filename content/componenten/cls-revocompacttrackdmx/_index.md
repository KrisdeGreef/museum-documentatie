---
title: cls-revocompacttrackdmx
date:
draft: false
merk: CLS
model: Revo Compact Track DMX
leverancier: CLS
attachments:
- title: CLS Revo Compact Track DMX manual (pdf)
  src: https://files.museum.naturalis.nl/s/rT4LaLFLQcL6brC
---

![revo-compact-t-dmx](./revo-compact-t-dmx.jpg)

## Eigenschappen

* Dimensions (LxWxH): 175 mm x 78 mm x 78 mm
* Weight: 1,3 kg

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
