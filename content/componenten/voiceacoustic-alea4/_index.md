---
title: "voiceacoustic-alea4"
date:
draft: false
merk: Voice Acoustic
model: Alea 4
leverancier:
shows:
- showrexperience
attachments:
- title: rexperience_hardware_avh_speakers_handleiding_voiceacoustic_alea4 (pdf)
  src: https://files.museum.naturalis.nl/s/CziyJJrmAZmdR8s
- title: data-sheet-voice-acoustic-alea-4 (pdf)
  src: data-sheet-voice-acoustic-alea-4
---

![alea4](./alea4.png)
Ultra compact mid-high loudspeaker

## Eigenschappen

* Afmetingen: 134 (h) x 134 (w) x 150 mm (d)

* Gewicht: 1,4 kg
* Power handling 16 Ohms: 30 W AES / 60 W program / 120 W peak

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Shows

{{< pagelist shows >}}


## Bijlagen

{{< pagelist bijlagen >}}
