---
title: "prolight-minieclipse"
date:
draft: false
merk: Prolight
model: Mini Eclipse
leverancier: Lumen Solutions
attachments:
- title: Prolights Mini Eclipse (pdf)
  src: https://files.museum.naturalis.nl/s/ZJNKGJgNtgTCq3N
- title: Prolights Mini Eclipse manual (pdf)
  src: https://files.museum.naturalis.nl/s/zjwWPGX4r6ST8NQ

---

![minieclipse](./minieclipse.png)

## Eigenschappen

* Gewicht: 1700 gr


## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
