---
title: "kramer-tp573"
date: 2019-12-03T14:50:05+01:00
draft: false
merk: kramer
model: tp573
leverancier:
interactive:
- studiolab
attachments:
- title: kramer-tp573_handleiding.pdf
  src: https://files.museum.naturalis.nl/s/jy8GZbmEX4z6ypH
- title: TP-573 Datasheet.pdf
  src: https://files.museum.naturalis.nl/s/NWi4WTRJ86S9r5W
---

![T573](./TP573.png "T573")
HDMI/RS-232/IR Line Transmitter

## Eigenschappen

* Inputs:
  * 1 HDMI connector
  * 1 bidirectional IR port on a 3.5mm mini jack
  * 1 bidirectional RS-232 port on a 9-pin  D-sub connector
* Outputs:
    * 1 CAT 5 OUT on an RJ-45 connector
* Bandbreedte: Supports up to 1.65Gbps bandwidth per graphic channel
* Voeding: 12V DC, 510mA
* Afmetingen: 12.1cm x 7.18cm x 2.42cm
* Gewicht: 0,3 kg

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Interactives

Dit component wordt gebruik in de interactives:

{{< pagelist interactives >}}

## Bijlagen

{{< pagelist bijlagen >}}
