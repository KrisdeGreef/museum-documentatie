---
title: chauvet-ovatione260ww
date:
draft: false
merk: Chauvet
model: Ovation E-260WW
leverancier: Lichtpunt
attachments:
- title: Chauvet Ovation E-260WW DMX (pdf)
  src: https://files.museum.naturalis.nl/s/HHxNXZWGskmncjd
- title: Chauvet Ovation E-260WW manual (pdf)
  src: https://files.museum.naturalis.nl/s/wCxfmXojittfoGj
- title: Chauvet Ovation E-260WW Quick (pdf)
  src: https://files.museum.naturalis.nl/s/LEDMjKeZP6E7798

---

![ovatione260ww](./ovatione260ww.png)
The Ovation  E-260WWdelivers  an  excellent  white  light  with  a  warm  color  temperature  and  a  beautiful  flat  field.  It  also  features  standard  beam  shaping  shutters,  a  gobo/effect  slot  and  lens  barrels  that  are  interchangeable  with  other  popular  ERS  fixtures.  Selectable  dimming  curves ensure smooth fading cues that intermix well with the output of conventional theatre ellipsoidals.

## Eigenschappen

* Dimensions (LxWxH): 495 mm x 285 mm x 487 mm
* Weight: 7.1 kg

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
