---
title: "elt-lv2"
date:
draft: false
merk: ELT
model: LV-C2
leverancier: Q-Cat
attachments:
- title: ELT LV 75-24-C2 (pdf)
  src: https://files.museum.naturalis.nl/s/9jr6LFADiibd3ZK

---

![lv-c2](./lv-c2.png)
Constant voltage control gears up to 75W. IP20

## Eigenschappen

* Afmetingen: diverse maten


## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
