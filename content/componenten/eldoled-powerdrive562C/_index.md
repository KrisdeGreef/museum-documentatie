---
title: "eldoled-powerdrive562C"
date:
draft: false
merk: eldoLED
model: POWERdrive 562C
leverancier:
attachments:
- title: EldoLED POWERdrive 562C 3ch (pdf)
  src: https://files.museum.naturalis.nl/s/zwozPKFFo34k3BT

---



## Eigenschappen

* Afmetingen:
  * 152.5x76x30.1 mm
* Gewicht: 372 gr


## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

Deze lijkt erg op de POWERdrive 562A en hij wordt ook niet gemoend op de website.
Misschien klopt het niet helemaal.

## Bijlagen

{{< pagelist bijlagen >}}
