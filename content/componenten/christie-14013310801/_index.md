---
title: "christie-14013310801"
date: 2019-12-03T14:50:02+01:00
draft: false
merk: Christie
model: Lens UST 0.36
leverancier:
attachments:
- title: christie-14013310801_gegevensblad.pdf
  src: https://files.museum.naturalis.nl/s/C2gJ2YWZE7znL2x
- title: christie-14013310801_handleiding.pdf
  src: https://files.museum.naturalis.nl/s/eNMsAPPTEQYyaZf
---

![Lens_UST_0](./Lens_UST_0.36.jpeg "Lens_UST_0")
Christie Lens UST 0.36

## Eigenschappen

* This lens has a throw ratio of 0.361 (when used with a 0.67" WUXGA 1DLP projector)
* This lens has a throw ratio of 0.361 (when used with a 0.65" 1080p 1DLP projector)
* Focal length of 9.49-9.55F
* Number: 2.00 (TBD)
* Zoom Ratio: No zoom
* Net weight (kg): 3.0
* Lens configuration: 23 group, 23 elements
* Screen size: 120"~350" (tolerance +/- 3%)

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
