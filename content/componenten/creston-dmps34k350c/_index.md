---
title: "creston-dmps34k350c"
date:
draft: false
merk: creston
model: dmps3 4k 350 c
leverancier:
attachments:
- title: ui guide (presentation)
  src: https://files.museum.naturalis.nl/s/dywEGg34NHa2xBW
- title: technical specifications
  src: https://files.museum.naturalis.nl/s/tc9AMfaSeKadR62
- title: supplemental guide
  src: https://files.museum.naturalis.nl/s/qSgH2PrSGwcMTX5
- title: design guide
  src: https://files.museum.naturalis.nl/s/7ERYtBw3TJKLdWc
---

![dmps34k350c](./dmps34k350c.png)
![dmps34k350c_rear](./dmps34k350c_rear.png)
4K DigitalMedia™ Presentation Systems

## Eigenschappen

* Vermogen: 93 Watts typical, 72 Watts idle
* Afmetingen:

  * Hoogte: 133 mm, without feet
  * Breedte: 439 mm, 483 mm with rack ears
  * Diepte: 400 mm

* Gewicht: 7,1 kg

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
