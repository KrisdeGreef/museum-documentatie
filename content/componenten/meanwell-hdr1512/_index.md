---
title: "meanwell-hdr1512"
date: 2019-12-03T14:50:06+01:00
draft: false
merk: meanwell
model: hdr1512
leverancier:
attachments:
- title: meanwell-hdr1512_gegevensblad.pdf
  src: https://files.museum.naturalis.nl/s/CNm2G3Kprf97mbd
---

![hdr1512](./hdr1512.png "hdr1512")
15W  Ultra Slim Step Shape DIN Rail

## Eigenschappen

* Vermogen: 15W
* Output: 12V / 1,25A
* Afmetingen:

  * Hoogte: 90mm
  * Breedte: 17,5mm
  * Diepte: 54,5mm

* Gewicht: 78g

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
