---
title: "jbl-control24c"
date:
draft: false
merk: JBL
model: Control 24 C
leverancier:
shows:
- showrexperience
attachments:
- title: Control 24C and 24CT Spec Sheet Review (pdf)
  src: https://files.museum.naturalis.nl/s/6o3pNoDGg4ATRYE
- title: Technical Application Guide (pdf)
  src: https://files.museum.naturalis.nl/s/3woE3QsmRKRpCDH
---

![24c](./24c.jpg)

Background / Foreground Ceiling Loudspeakers

## Eigenschappen

* Afmetingen:

  * Hoogte: 200 mm
  * Diameter: 195 mm
  * Diepte: 184 mm

* Gewicht: 2.7 kg
* Impedantie: 16 ohms

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Shows

Dit scherm wordt gebruik in de shows:

{{< pagelist shows >}}

## Bijlagen

{{< pagelist bijlagen >}}
