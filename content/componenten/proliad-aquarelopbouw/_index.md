---
title: "proliad-aquarelopbouw"
date:
draft: false
merk: Proliad
model: Aquarel Opbouw
leverancier: PRoliad
attachments:
- title: Proliad Aquarel Opbouw manual (pdf)
  src: https://files.museum.naturalis.nl/s/B7w8TDsKFKTLeBJ
- title: Proliad Aquarel Opbouw (pdf)
  src: https://files.museum.naturalis.nl/s/Qzdq6GnCZP9qKSY
---

![aquarelopbouw](./aquarelopbouw.png)

## Eigenschappen

* Afmetingen:
  * Diameter: 59 mm
  * Hoogte: 181 mm



## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
