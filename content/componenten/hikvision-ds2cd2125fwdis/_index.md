---
title: "hikvision-ds2cd2125fwdis"
date: 2019-12-03T14:50:04+01:00
draft: false
merk: hikvision
model: ds1272zj110
leverancier:
decor:
- vertrekhal
- capsule
- jungle
- wormhole
attachments:
- title: gegevensblad
  src: https://files.museum.naturalis.nl/s/tkMDGpALHjYTjsN
---

![DS-2CD21X5FWD](./DS-2CD21X5FWD.png)
2 MP Powered-by-DarkFighter Fixed Dome Network Camera
## Eigenschappen

* Vermogen:
* Afmetingen:

  * Hoogte: 82.4 mm  
  * Diameter: (raduis) 111 mm

* Gewicht: 500 g

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Decor

dit component wordt gebruikt in het decor:

{{< pagelist decor >}}


## Bijlagen

{{< pagelist bijlagen >}}
