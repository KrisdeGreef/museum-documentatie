---
title: "microsonic-mic130dtc"
date:
draft: false
merk: Microsonic
model: mic+130/D/TC
leverancier: sensor partners
decors:
- ontsmettingszone
attachments:
- title: microsonic_mic+130_D_TC (pdf)
  src: https://files.museum.naturalis.nl/s/yHMjaGxKo7eyLcT
---

![sensor](./micplus130.jpg)
naderingsschakelaar/reflexsensor

## Eigenschappen

* bedrijfsdetectiewijdte: 200 - 2.000 mm
* bouwvorm: cilindrisch M30
* bedrijfsmodus:
    * naderingsschakelaar/reflexsensor
    * reflectiebarrière
    * venstermodus
* hoogtepunten:
    * tonen
    * UL Listed



## Technische specificaties
![](https://files.museum.naturalis.nl/s/iG85dgkCBCC9GbD/preview)
![](https://files.museum.naturalis.nl/s/rg4CLH6mkBQ6ixd/preview)
![](https://files.museum.naturalis.nl/s/dWqQ82QMBRwyg3q/preview)

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Decors

Dit scherm wordt gebruik in de decors:

{{< pagelist decors >}}


## Bijlagen

{{< pagelist bijlagen >}}
