---
title: "micropython op-de-esp32"
date: 2019-06-13T10:00:04+02:00
draft: false
weight: 32
---

Naast de mogelijkheid om de ESP32 te programmeren met de arduino omgeving
bestaat er ook nog de mogelijkheid om deze apparaatjes dingen te laten
doen via Python. Micropython, een subset van python3 speciaal voor
embedded devices.

## Micropython installeren

Om meer te weten te komen over micropython kan je terecht op 
[docs.micropython.org](http://docs.micropython.org/en/latest/). De
uitleg voor ESP32 is genoeg om snel aan de slag te gaan. Maar
de stappen zijn kort samengevat deze:

1. Installeren van esptool: `sudo pip install esptool`
2. Esp32 bordje inpluggen via usb en bepalen via welk device (meestal `/dev/ttyUSB0`) hij te benaderen is
3. Download de [meest recente versie van de firmware](https://micropython.org/download#esp32)
4. Flash geheugen legen: `esptool --port /dev/ttyUSB0 erase_flash` (deze stap werkt niet met de olimex esp32 bordjes)
5. Firmware schrijven: 

```
    esptool --chip esp32 --baud 115200 --port /dev/ttyUSB0 write_flash \
    -z 0x1000 ~/Downloads/esp32-20190529-v1.11.bin
```

Hierna heb je esp32 bordje met een recente stabiele versie van micropython.

## Commandline

Om de esp te kunnen programmeren kan je gebruik maken van de serie&euml;le
verbinding. Het makkelijkste gaat dit via picocom (`sudo apt install picocom`).

```
picocom /dev/ttyUSB0 -b115200
```

Na connectie zie je vaak niks gebeuren. Als je op CTRL-C drukt stop je 
de boot.by en verschijnt er een python prompt `>>>`. Met het commando
`help()` krijg je tips over wat je allemaal kan doen.

## WebREPL

Zorg ervoor dat je bordje via ethernet is aangesloten op het netwerk
en hij contact kan maken met internet. Tik op de python prompt het commando:

```
import webrepl_setup
```

Volg de instructies. Zet een goed moeilijk te raden wachtwoord.

Hierna zou je via het ip adres toegang kunnen krijgen tot de webrepl. In
ieder geval na reset, door op het reset knopje te klikken of

```
import machine
machine.reset()
```

Hierna zie je de debug informatie. Bijvoorbeeld:

```
WebREPL daemon started on ws://192.168.192.31:8266
```

De webRepl is te bedienen via een websocket terminal in je browser.
[micropython.org/webrepl/](http://micropython.org/webrepl/). Ipnummer
invoeren en je kan aan de slag.

Een andere mogelijkheid is gebruik maken van het cli script en de html
pagina [github.com/micropython/webrepl](https://github.com/micropython/webrepl).

## boot.py

Via de web interface of de cli kan je bestanden up- en downloaden in het
flash geheugen van de esp32. De belangrijkste is boot.py, die wordt
gestart wanneer het bordje opstart. Een boot.py script is bijvoorbeeld.

```
import esp
esp.osdebug(None)

import webrepl
import picoweb
from machine import Pin
import time
import _thread

webrepl.start()

def toggle(p):
    p.value(not p.value())
    return p.value()

app = picoweb.WebApp(__name__)
@app.route("/")
def get_index(req, resp):
    ledpin = Pin(5,Pin.OUT)
    answer = {"LED": toggle(ledpin)}
    yield from picoweb.jsonify(resp, answer)

app.run(debug=1, host='0.0.0.0', port=80)

```

Dit script start een picoweb applicatie die luistert op poort 80
naar een GET query op '/' en die zet en led aan of uit die
aangesloten is op pin 5.

<video width="320" height="240" autoplay loop>
  <source src="esp32blink.mp4" type="video/mp4">
</video>

## Tips 

De mogelijkheden met micropython zijn enorm. Er zijn vele micropython
modules die gelijk te gebruiken zijn. Bovendien staan op github vele
voorbeelden met [micropython en esp32](https://github.com/mcauser/awesome-micropython).

En vergeet niet de [library met micropython modules](https://pypi.org/search/?q=&o=&c=Programming+Language+%3A%3A+Python+%3A%3A+Implementation+%3A%3A+MicroPython).
Modules die nog niet zijn geinstalleerd kan je installeren met
upip.

```
import upip
upip.install('datetime')
```

Let op! Het geheugen van de esp32 is maar beperkt tot 4Mb, alleen installeren
wat je echt gebruikt en anders verwijderen met `os.remove`.

```
import os
os.listdir('lib')
os.remove('lib/datetime.py')
```



