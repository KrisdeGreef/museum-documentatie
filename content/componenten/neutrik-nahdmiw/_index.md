---
title: "neutrik-nahdmiw"
draft: false
merk: Neutrik
model: Nahdmiw
leverancier: Ata Tech
interactives:
 - readingroom
 - studiolab
attachments:
- title: neutrik-nahdmiw_technischetekening.pdf
  src: https://files.museum.naturalis.nl/s/i9nxHFaWzazzDRb
---

![Schema](schema.png)

HDMI doorsteek plug.

## Interactives

{{< pagelist interactives >}}


## Bijlagen

{{< pagelist bijlagen >}}
