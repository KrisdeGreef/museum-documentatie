---
title: cls-focustrackmango
date:
draft: false
merk: CLS
model: Focus Track Magno
leverancier: CLS
attachments:
- title: CLS Focus Track Magno manual (pdf)
  src: https://files.museum.naturalis.nl/s/sL9BEYPwL4wGsFi
- title: CLS Focus Track Magno (pdf)
  src: https://files.museum.naturalis.nl/s/frbYkoWfZnHYPNk

---

![focus-t-230v](./focus-t-230v.png)
6 Watt, compact zoom fixture. 230 V.

## Eigenschappen

* Dimensions (LxWxH): 107 mm x 35 mm x 192 mm
* Weight: 275 gr

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
