---
title: "eldoled-powerdrive1060A"
date:
draft: false
merk: eldoLED
model: POWERdrive 1060A
leverancier:
attachments:
- title: EldoLED POWERdrive 1060A (pdf)
  src: https://files.museum.naturalis.nl/s/QLxBbkNKAMXnS3M

---

![powerdrive1060A](./powerdrive1060A.png)
100W  DMX/RDM/DALI Full-Colour (RGBW) Dimmable LED Driver

## Eigenschappen

* Afmetingen:
  * Diepte: 388 mm
  * Hoogte: 30 mm
  * Breedte: 42 mm
* Gewicht: 681,5 gr


## Technische specificaties

POWERdrive 1060/A delivers 100W and is DALI and DMX compatible. The end caps function as wire restraints and make the driver perfectly suited for independent installations. POWERdrive 1060/A has four separately controllable LED outputs, which can be used for a wide range of full-colour (RGBW), dynamic LED lighting applications.



You can configure the driver over its intuitive, 3-button user interface with display: it lets you configure the number of channels, DMX or DALI settings for networked mode and show, colour and dim values for standalone operation.

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
