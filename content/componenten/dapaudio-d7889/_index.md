---
title: "dapaudio-d7889"
date: 2019-12-03T14:50:02+01:00
draft: false
merk: DAP Audio
model: D-7889
leverancier:
attachments:

---

![dapaudio-d7889_afbeelding](./dapaudio-d7889_afbeelding.png "dapaudio-d7889_afbeelding")
![dap19inch](./dap19inch.jpg "dap19inch")
DAP-Audio 19" 3U switch box

## Eigenschappen

* Hoogte: 3U
* Afmetingen: 484 x 135 x 65 mm (LxBxH)
* Gewicht: 1,81 kg

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
