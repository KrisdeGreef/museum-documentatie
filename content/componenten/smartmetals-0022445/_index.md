---
title: "smartmetals-0022445"
date: 2019-12-03T14:50:09+01:00
draft: false
merk: smartmetals
model: 0022445
leverancier:
attachments:
- title: smartmetals-0022445_gegevensblad.pdf
  src: https://files.museum.naturalis.nl/s/Yo8yC4biwYPdGWM
---

![smartmetals-0022445](./0022445.png)
L2: vaste lengte 280 mm

## Eigenschappen

* Vermogen: 25kg
* Afmetingen:

  * Hoogte: 280 mm
  * Breedte:
  * Diepte:

* Gewicht:1,8 kg

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
