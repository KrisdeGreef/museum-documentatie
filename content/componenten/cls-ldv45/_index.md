---
title: cls-ldv45
date:
draft: false
merk: CLS
model: LDV-45
leverancier: CLS
attachments:
- title: CLS LDV-45 manual (pdf)
  src: https://files.museum.naturalis.nl/s/78rfoqdCjQZjR7F

---

![ldv45](./ldv45.png)
4 channel for 12-24V VDC LED fixtures. DIN mountable.

## Eigenschappen

* Dimensions (LxWxH): 125 mm x 25 mm x 110 mmm
* Weight: 140 gr

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
