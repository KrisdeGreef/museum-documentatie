---
title: "vesa-4270"
draft: false
merk: Vesa
model: 4270
leverancier: Ata Tech
interactives:
- vroegemensportaal
- introjapanstheater
- readingroom
- cabinijsland
- greenporn
- spermarace
attachments:
- title: vesa-4270_gegevensblad.pdf
  src: https://files.museum.naturalis.nl/s/S8jSpfgNYQd7mbC
---

![beugel](screenshot.png)

Universele ophangbeugel voor televisie.

## Eigenschappen

* Maximaal gewicht: 60 kg
* Afmetingen:

  * Hoogte: Maximaal 400 mm
  * Breedte: Maximaal 600 mm 
  * Diepte: 29 mm (afstand tot de muur)

* Gewicht: 2.00 kg

## Interactives

{{< pagelist interactives >}}

## Bijlagen

{{< pagelist bijlagen >}}
