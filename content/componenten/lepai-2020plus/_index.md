---
title: "lepai-2020plus"
date: 2019-12-03T14:50:06+01:00
draft: false
merk: Lepai
model: 2020 plus
leverancier:
interactives:
- camperhawaii
- datingquiz
- flipperkast
- greenporn
- handscanner
- knaagdierenontvangsthal
- saxystories
- vogelskeletten
- vogelsvoeren
- waternesten
attachments:
- title: lepai-2020plus_handleiding.pdf
  src: https://files.museum.naturalis.nl/s/w8w2iaSoo4kJqmo
---

![LP2020A](./LP2020A.png "LP2020A")
Hi-Fi 2 Ch Output Power Amplifiers Speakers Car stereo Digital AMP

## Eigenschappen

* Vermogen: Output Power 2 x 20W 4ohm, 2 x 12W 8ohm
* Input: RCA X1 & 3.5mm earphone jack x 1
* Afmetingen:

  * Hoogte: 40mm
  * Breedte: 140mm
  * Diepte: 120mm




## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Interactives

Dit component wordt gebruik in de interactives:

{{< pagelist interactives >}}


## Bijlagen

{{< pagelist bijlagen >}}
