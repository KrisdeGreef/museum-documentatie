---
title: "jbsystems-4646mdmxtranceiver"
date:
draft: false
merk: JB Systems
model: 4646 M-DMX Receiver
leverancier: Lumen Solutions
attachments:
- title: JB Systems 4646 M-DMX Transciever (pdf)
  src: https://files.museum.naturalis.nl/s/b5T9rEpzEpzkQRy

---

![mdmxtranceiver](./mdmxtranceiver.png)
Draadloos M-DMX Tranceiver

## Eigenschappen

* Afmetingen: 103 x 76 x 36 mm (excl. antenne van 86 mm)
* Gewicht: 25 gr


## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
