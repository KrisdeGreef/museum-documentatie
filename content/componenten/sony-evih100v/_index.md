---
title: "sony-evih100v"
date: 2019-12-03T14:50:09+01:00
draft: false
merk: Sony
model: evih100v
leverancier:
interactives:
- studiolab
attachments:
- title: sony-evih100v_handleiding.pdf
  src: https://files.museum.naturalis.nl/s/FgHGqgfzASFFeKw
- title: EVI-H100V_specs.pdf
  src: https://files.museum.naturalis.nl/s/aPips8fdL2iBfpR
---

![evih100v](./evih100v.jpeg)
![evih100v_rear](./evih100v_rear.jpg)
Full HD PTZ camera (pan/tilt/zoom)

## Eigenschappen

* Vermogen: 11 W
* Afmetingen:

  * Hoogte: 164 mm
  * Breedte: 145 mm
  * Diepte:164 mm

* Gewicht: 1550 g

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Interactives

{{< pagelist interactives >}}


## Bijlagen

{{< pagelist bijlagen >}}
