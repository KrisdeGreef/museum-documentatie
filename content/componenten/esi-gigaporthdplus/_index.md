---
title: "esi-gigaporthdplus"
date: 2019-12-03T14:50:03+01:00
draft: false
merk: ESI
model: Gigaport HD+
leverancier:
interactives:
- cirkelprojectie
- dinovoetstappen
shows:
- showijstijd
attachments:
- title: esi-gigaporthdplus_gegevensblad.pdf
  src: https://files.museum.naturalis.nl/s/KNjeQssJrKFcarS
---

![esi-gigaporthdplus](./esi-gigaporthdplus.png "esi-gigaporthdplus")
High Quality 24-bit 8-out USB Audio Interface

## Eigenschappen

* Vermogen:
* Afmetingen:

  * Hoogte: 20 mm
  * Breedte: 70 mm
  * Diepte: 120 mm

* Gewicht:

## Technische specificaties

* 24-bit / 96kHz D/A converter
* 8 independent output channels with RCA connectors
* different playback modes: 44.1kHz with 16-bit and 8 channels, 44.1kHz with 24-bit and 6 channels, 48kHz with 24-bit and 6 channels, 96kHz with 24-bit and 2 channels
* 2 independent stereo headphone outputs: 1st output sends out mixed signal of all playback channels, 2nd output sends out different signal from playback channel 1/2 - perfect for monitoring and to pre-listen to signals
* compatible with Windows XP and Windows Vista/7/8/8.1/10 supporting ASIO 2.0, WDM, MME (using ESI's EWDM driver technology)
* compatible with Mac OS X and supported by the native CoreAudio USB audio support from Apple, no special driver installation required


## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Interactives

Dit component wordt gebruik in de interactives:

{{< pagelist interactives >}}

## Show

Dit component wordt in de volgende shows gebruikt:
{{< pagelist shows >}}


## Bijlagen

{{< pagelist bijlagen >}}
