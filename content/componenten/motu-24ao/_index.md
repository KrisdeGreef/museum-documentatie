---
title: "motu-24ao"
date: 2019-12-03T14:50:06+01:00
draft: false
merk: MOTU
model: 24ao
leverancier:
shows:
- showrexperience
attachments:
- title: motu-24ao_handleiding.pdf
  src: https://files.museum.naturalis.nl/s/wF4x5CNqjyQ2cnf
---

![24AO](./24AO.png "24AO")
MOTU 24 AO USB 2.0 Audio Interface met 72 kanalen

## Eigenschappen

* Vermogen: 100/240V /  0.5A max
* Afmetingen: 48.3 x 17.75 x 4.5 cm
* Gewicht: 1.99 kg

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Shows

Dit scherm wordt gebruik in de shows:

{{< pagelist shows >}}

## Bijlagen

{{< pagelist bijlagen >}}
