---
title: "creston-tsw760bs"
date:
draft: false
merk: creston
model: tsw760bs
leverancier:
attachments:
- title: quickstart
  src: https://files.museum.naturalis.nl/s/qgqCwJzs8qMn7fA
- title: supplemental guide
  src: https://files.museum.naturalis.nl/s/bibmFJJg4C2yCgK
- title: technical specifications
  src: https://files.museum.naturalis.nl/s/XA3TddE8LNjZoYa
- title: IP Guidelines for the IT Professional
  src: https://files.museum.naturalis.nl/s/6KK3HjGirbfDQPg
---

![tsw760bs](./tsw760bs.png)
7" Touch Screen, Black Smooth

## Eigenschappen

* Afmetingen:

  * Hoogte: 122 mm
  * Breedte: 194 mm
  * Diepte: 39 mm

* Gewicht: 400 g

## Technische specificaties

* Display Type: TFT active matrix color LCD
* Size: 17 inch (257mm) diagonal
* Aspect Ratio: 17:10 WSVGA
* Resolution: 1024x600 pixels
* Brightness: 350 nits (cd/m2)
* Contrast: 1100:1
* Color Depth: 24bit, 16.7M colors
* Illumination: Edgelit LED with auto brightness control
* ViewingAngle±80°horizontal,±80°vertical
* Touch Screen: Projected capacitive, 5-point multitouch capable

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
