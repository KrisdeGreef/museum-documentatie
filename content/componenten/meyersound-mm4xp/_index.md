---
title: "meyersound-mm4xp"
date:
draft: false
merk: meyersound
model: mm4xp
leverancier:
shows:
- showrexperience
attachments:
- title: Technische specificaties
  src: https://files.museum.naturalis.nl/s/cRTJZzzg8Xq5c7F
- title: OPERATING INSTRUCTIONS
  src: https://files.museum.naturalis.nl/s/on4yHrQTd8EqDPw
---

![mm4xp](./mm4xp.png)
Miniature Loudspeaker

## Eigenschappen

* Afmetingen:

  * Hoogte: 103 mm
  * Breedte: 103 mm
  * Diepte: 145 mm (203 mm incl. stekker)

* Gewicht: 1,9 kg

## Technische specificaties

MM-4XP loudspeaker systems require an ![MPS-488](https://docs.museum.naturalis.nl/latest/componenten/meyersound-mps488hp) external power supply.

* Impedantie: 10 K Ohm
* Type connector: MM-4XP connector

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Shows

Dit scherm wordt gebruik in de shows:

{{< pagelist shows >}}


## Bijlagen

{{< pagelist bijlagen >}}
