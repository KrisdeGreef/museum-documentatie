---
title: "lg-43se3kbb"
date:
draft: false
merk: LG
model: 43SE3KB-B
leverancier:
interactives:
- onschatbaar
attachments:
- title: User Guide (pdf)
  src: https://files.museum.naturalis.nl/s/Ax8Pw2wxmXw8DGF
- title: Owners Manual (pdf)
  src: https://files.museum.naturalis.nl/s/b6x9jdEYKBmQqw4
- title: Standard Essentials (pdf)
  src: https://files.museum.naturalis.nl/s/BSESbio7XpC4JLt
---

![43SE3KB-B](./43SE3KB-B.jpg)
43 inch class (42.5 inch diagonal) Edge-Lit LED IPS Digital Signage Display

## Eigenschappen


* Video-in:
  * 1 x DisplayPort
  * 2 x HDMI
  * 1 x DVI-D
* Bediening:
  * RS-232c x1 (Get/Set Command)
  * RJ45 (LAN) x1
  * IR 1x
* Schermdiagonaal: 43" (42.5" measured diagonally)
* Resolutie: 1920 x 1080 (FHD)
* Beeldverhouding: 16:9
* Vermogen: 60W
* Kijkhoek:
* Afmetingen: 43.4”x25.1”x2.1”
* Gewicht: 31.52lbs


## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Interactives

Dit component wordt gebruik in de interactives:

{{< pagelist interactives >}}


## Bijlagen

{{< pagelist bijlagen >}}
