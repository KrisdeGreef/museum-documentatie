---
title: "wahlberg-winch50"
date:
draft: false
merk: Wahlberg
model: Winch 50
leverancier:
interactives:
- aardbevingplafond
attachments:
- title: Wahlberg Winch 50 User Manual
  src: https://files.museum.naturalis.nl/s/ApbjfMAaffmZqKx
---

![winch50](./winch50.png)


## Eigenschappen

* Afmetingen Winch 25 245:

  * Hoogte: 298 mm
  * Breedte: 345 mm
  * Diepte: 445 mm

* Gewicht: 29.1 kg

* Afmetingen Winch 50 246:

  * Hoogte: 298 mm
  * Breedte: 797 mm
  * Diepte: 445 mm

* Gewicht: 45 kg

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## interactives

{{< pagelist interactives >}}

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
