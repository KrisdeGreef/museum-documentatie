---
title: "dapaudio-pr82"
date: 2019-12-03T14:50:02+01:00
draft: false
merk: DAP Audio
model: PR-82
leverancier:
interactives:
- cirkelprojectie
- camarasaurus
- dinovoetstappen
- edmontorasurus
- filmdubois
- plateosaurus
- stegosaurus
- trex
shows:
- showleven
- showijstijd
attachments:
- title: dapaudio-pr82_gegevensblad.pdf
  src: https://files.museum.naturalis.nl/s/NDYogo4xSM4X4QF
---

![PR-82](./PR-82.jpg "PR-82")
Passieve PA luidspreker

## Eigenschappen

* RMS vermogen: 85 W
* Impedantie: 16 Ohm
* Type connector: speaker cable connector
* Actief of passief: Passief
* Afmetingen: 265 x 270 x 365 mm (LxWxH)
* Gewicht: 6,6 KG

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Interactives

Dit component wordt gebruik in de interactives:

{{< pagelist interactives >}}

## Show

Dit component wordt in de volgende shows gebruikt:
{{< pagelist shows >}}


## Bijlagen

{{< pagelist bijlagen >}}
