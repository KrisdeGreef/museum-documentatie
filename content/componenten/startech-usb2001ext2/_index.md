---
title: "startech-usb2001ext2"
date:
draft: false
merk: Startech
model: USB2001ext2
leverancier:
attachments:
- title: usb2001ext2_datasheet-nl (pdf)
  src: https://files.museum.naturalis.nl/s/wtbi9JNcK5CZdJz
- title: usb200xext2 manual (pdf)
  src: https://files.museum.naturalis.nl/s/2cSNsaQwJH6cX5W
---

![usb2001ext2](./usb2001ext2-b.png)
![usb2001ext2](./usb2001ext2-c.png)

1-poort USB 2.0 via Cat5 / Cat6 Ethernet Verlenger - tot 100 m

## Eigenschappen

* Afmetingen:

  * Hoogte: 100 mm
  * Breedte: 76 mm
  * Diepte: 26 mm

* Gewicht: 315 g

* Poorten: 1
* Bekabeling: Cat 5e of beter
* Max afstand: 100 m

* Connectors
  * lokale eenheid: USB B (4pin) & RJ45
  * externe eenheid: USB Type-A (4pin) USB 2.0 & RJ45

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
