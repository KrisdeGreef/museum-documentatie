---
title: "dbox-gen11actuator"
date:
draft: false
merk: D-Box
model: Gen II Actuator
leverancier:
attachments:
- title: rexperience_technischeinstallaties_dboxstoelen_actuatorspecssheet (pdf)
  src: https://files.museum.naturalis.nl/s/2mRq3tSQKnnB5gE
---

![actuator](./actuator.png)


## Eigenschappen

* Afmetingen:

  * Hoogte:  mm
  * Breedte:  mm
  * Diepte:  mm

* Gewicht:  kg

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
