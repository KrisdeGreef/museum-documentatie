---
title: "qsc-adc4tbk"
date: 2019-12-03T14:50:08+01:00
draft: false
merk: QSC
model: adc4tbk
leverancier:
interactives:
- letsdance
- orgel
- spermarace
shows:
- showleven
attachments:
- title: qsc-adc4tbk_gegevensblad.pdf
  src: https://files.museum.naturalis.nl/s/ok8r54oNNw447Te
---

![adc4tbk](./adc4tbk.png)
The QSC AcousticDesign™ AD-C4T is a 4.5" two-way ceiling loudspeaker ideally suited for a wide variety of foreground and background sound reinforcement applications which utilize 70/100V or 16Ω bypass configurations.

## Eigenschappen

* Vermogen:
* Afmetingen:

  * Hoogte: 176 mm
  * Diameter: 230 mm


* Gewicht: 2,9kg

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Interactives

{{< pagelist interactives >}}


## Shows

Dit scherm wordt gebruik in de shows:

{{< pagelist shows >}}



## Bijlagen

{{< pagelist bijlagen >}}
