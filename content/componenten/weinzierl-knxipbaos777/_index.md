---
title: "weinzierl-knxipbaos777"
date:
draft: false
merk: weinzierl
model: KNX IP BAOS 777
leverancier:
attachments:
- title: Weinzierl-777-KNX-IP-BAOS-5193-Datasheet-EN (pdf)
  src: https://files.museum.naturalis.nl/s/MkFBpTJJyBcaxeW
- title: Weinzierl-777-KNX-IP-BAOS-5193-Manual-EN (pdf)
  src: https://files.museum.naturalis.nl/s/6QRdcMEXCZgzak8
- title: Weinzierl-KNX-over-IP-EN (pdf)
  src: https://files.museum.naturalis.nl/s/XqTwXz4rbpricXH
- title: The Word of BAOS (pdf)
  src: https://files.museum.naturalis.nl/s/fyFbaYZ7jJsAaAN
---

![Weinzierl777KNXIPBAOSxsmall](./Weinzierl777KNXIPBAOSxsmall.jpg)

The KNX IP BAOS 777 is a powerful universal device for KNX installation: Thanks to the integrated web server, the device enables the visualization and control of buildings, rooms and functions in a standard web browser on a PC or mobile devices such as smartphones and tablets.

## Eigenschappen

* Afmetingen:

  * Hoogte:
  * Breedte:
  * Diepte:

* Gewicht:

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
