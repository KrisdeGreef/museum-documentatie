---
title: "segula-sgseries"
date:
draft: false
merk: Segula
model: SG series
leverancier: Light Technology
attachments:

---

[![SG-50293](./SG-50293.png)](https://files.museum.naturalis.nl/s/XXFsp9dn3HFbnLc)
Segula SG-50293
[![SG-50298](./SG-50298.png)](https://files.museum.naturalis.nl/s/zPedCYBAJmbskF2)
Segula SG-50298
[![SG-50325](./SG-50325.png)](https://files.museum.naturalis.nl/s/JgNFkWfn9t9AXbp)
Segula SG-50325
[![SG-50340](./SG-50340.png)](https://files.museum.naturalis.nl/s/piHDFLXckM4SQj6)
Segula SG-50340 (SG-50342)
[![SG-50503](./SG-50503.png)](https://files.museum.naturalis.nl/s/rXnEDNiR3dXqKFE)
Segula SG-50503
[![SG-50525](./SG-50525.png)](https://files.museum.naturalis.nl/s/J5siBeEjXLGPtEC)
Segula SG-50525
[![SG-50526](./SG-50526.png)](https://files.museum.naturalis.nl/s/Y4NeoPAqmR3GNdt)
Segula SG-50526
[![SG-50598](./SG-50598.png)](https://files.museum.naturalis.nl/s/2e8WNWpx5amYTwm)
Segula SG-50598

## Eigenschappen



## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
