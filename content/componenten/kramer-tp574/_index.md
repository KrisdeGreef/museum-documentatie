---
title: "kramer-tp574"
date: 2019-12-03T14:50:05+01:00
draft: false
merk: kramer
model: tp574
leverancier:
interactives:
- studiolab
attachments:
- title: kramer-tp573_handleiding.pdf
  src: https://files.museum.naturalis.nl/s/jy8GZbmEX4z6ypH
- title: TP-574 Datasheet.pdf
  src: https://files.museum.naturalis.nl/s/8mjbM8M4TGRE37a
---

![tp-574](./tp-574.png "tp-574")
HDMI/RS-232/IR Line Receiver

## Eigenschappen

* Inputs:
    * 1 CAT 5 OUT on an RJ-45 connector
* Outputs:
  * 1 HDMI connector
  * 1 bidirectional IR port on a 3.5mm mini jack
  * 1 bidirectional RS-232 port on a 9-pin  D-sub connector
* Bandbreedte: Supports up to 1.65Gbps bandwidth per graphic channel
* Voeding: 12V DC, 510mA __from TP-573__
* Afmetingen: 12.1cm x 7.18cm x 2.42cm
* Gewicht: 0,3 kg

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Interactives

Dit component wordt gebruik in de interactives:

{{< pagelist interactives >}}

## Bijlagen

{{< pagelist bijlagen >}}
