---
title: cls-ldc41
date:
draft: false
merk: CLS
model: LDC 41
leverancier: CLS
attachments:
- title: CLS LDC-41 manual (pdf)
  src: https://files.museum.naturalis.nl/s/NRQ8Jo2XcS8dSjT

---

![ldc41](./ldc41.png)
4 channel constant current DMX LED driver. DIN moutable

## Eigenschappen

* Dimensions (LxWxH): 125 mm x 25 mm x 110 mmm
* Weight: 210 gr

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Bijlagen

{{< pagelist bijlagen >}}
