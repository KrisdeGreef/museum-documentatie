---
title: "visualproductions-cuecore2"
date: 2019-12-03T14:50:10+01:00
draft: false
merk: Visual Productions
model: cuecore2
leverancier: ATA Tech
shows:
- showdood
- showijstijd
- showleven
- showverleiding
- showvroegemens
interactive:
- orgel
attachments:
- title: visualproductions-cuecore2_gegevensblad.pdf
  src: https://files.museum.naturalis.nl/s/XX3PHFa8HtzEPo4
---

![cuecore2](./cuecore2.png)
multi-protocol architectural lighting controller

## Eigenschappen

* Vermogen:
* Afmetingen:

  * Hoogte: 41.5 mm
  * Breedte: 170 mm
  * Diepte: 100 mm

* Gewicht:

## Technische specificaties

<!-- Voeg hier de belangrijkste technische specificaties toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Interactives

Dit component wordt gebruik in de interactives:

{{< pagelist interactives >}}

## Shows

Dit scherm wordt gebruik in de shows:

{{< pagelist shows >}}

## Bijlagen

{{< pagelist bijlagen >}}
