---
title: "Sloten"
date: 2019-05-23T14:33:41+02:00
draft: false
weight: 30
---

Standaard voor Sloten ten behoeve van het afsluiten van onderhoudsluiken en
kastdeuren.

Uitganspunten:

* Onderhoudsluiken van de interactives (weinig gebruik):
  Veergrendel of draaispanvergrendeling met inbussleutel (imbus,
  binnenzeskantstiftsleutel, hex key, allen key, allen wrench) maat 8mm (5/16
  hex), tenzij de deur te zwaar is, dan een bakslot met vierkantsleutel
  (bakslotsleutel, robertson).

* Deuren/luiken voor dagelijks of zeer regelmatig gebruik:
  Bakslot met vierkantsleutel (bakslotsleutel, robertson).

* Geen cilindersloten, vitrines zijn hierop een uitzondering.

* Niet dichtschroeven.

* Het slot moet makkelijk te vervangen zijn.

Meer info:

Voorbeelden baksloten: https://www.hinscha.com/nl/producten/sluitingen/baksloten/

Voorbeeld vierkantsleutel: https://www.trailerandtools.nl/Bakslot-sleutel,-Vierkante-sleutel,-Bakslotsleutel,-Sleutel,-Bakslot-sleutel-vierkant

Voorbeeld inbus slot: https://www.southco.com/en-us/57/57-10-801-10

Voorbeeld inbus slot: http://www.essentracomponents.com/nl-nl/toegangshardware/sloten-en-vergrendelingen/draai-span-vergrendelingen/draai-span-vergrendelingen-met-sleutel-of-tool

Voorbeeld inbussleutel: https://nl.wikipedia.org/wiki/Inbussleutel
