---
title: "Software"
date: 2018-10-12T09:57:18+02:00
draft: true
---

De eisen en wensen die ten aanzien van software worden omschreven hebben als
doel de bezoekerservaring op een duurzame wijze te kunnen garanderen.

Zoals hier nader
[toegelicht](/systemen/ansible/showcontrol/#achtergrond) streeft Naturalis
daarom naar een technische inrichting van het museum waarbij automatisering en
reproduceerbaarheid op basis van open source technologie centraal staat. Zo is
gekozen voor het aansturen en configureren van alle systemen op basis van
[Ansible](https://www.ansible.com/).

Deze achtergrond is in belangrijke mate bepalend voor de eisen die worden
gesteld aan software voor interactives in het museum.

## Deployment

Voor het kunnen installeren van de software gelden de volgende eisen:

* De software draait op een recente versie van Linux. Van deze eis kan
  uitsluitend worden afgeweken na toestemming van Naturalis.
* De software is (bij voorkeur) open source en is (eveneens bij voorkeur)
  gebaseerd op open source libraries. Van deze eis kan uitsluitend worden
  afgeweken na toestemming van Naturalis.
* Het versiebeheer van de broncode van de applicatie gebeurt in een open source
  versiebeheer applicatie (bij voorkeur [Git](https://git-scm.com/)).
* Naturalis krijgt de beschikking over code repository en de broncode van de
  software.
* De installatieprocedure voor de software is goed gedocumenteerd.
* De installatie van de software en alle relevantie afhankelijkheden kan
  geautomatiseerd op basis van een (idealiter bij de software meegeleverd)
  Ansible playbook / role worden uitgevoerd.
* De software maakt waar relevant en mogelijk gebruik van hardware acceleratie.
* De minimale hardware vereisten die nodig zijn voor een goede werking van de
  software zijn gedocumenteerd of worden in samenspraak met Naturalis bepaalt.
* De software is gebaseerd op componenten die actief worden ontwikkelt
  en door een levensvatbare community dan wel organisatie worden onderhouden.

## Configuratie

Configuratie vatten we - in navolging van [twelve factor
app](https://12factor.net/config) - op als datgene wat varieert tussen
deployments (productie, test, staging) van een applicatie. Gedacht kan worden
aan credentials en parameters die de werking van de applicatie beïnvloeden.

De configuratie van software in het museum dient te voldoen aan de volgende
voorwaarden:

* Er is een strikte scheiding tussen code en configuratie. Dit betekent dat er
  geen bronbestanden van de software aangepast hoeven te worden om verschillende
  deployments mogelijk te maken.
* De software kent *sane defaults*. Dit betekent dat uitsluitend voor de
  specifieke toepassing relevante configuratieparameters hoeven te worden
  ingesteld voor een goede werking van de applicatie.
* Configuratie van de applicatie is mogelijk op basis van goed gedocumenteerde
  environment variabelen, command line argumenten en/of configuratiebestanden.

## Monitoring

Om de werking van de software te kunnen monitoren dient de software logging te
ondersteunen en heeft de software idealiter een manier om metrics over het
gedrag van de applicatie beschikbaar te stellen.

Wat betreft logging hanteert Naturalis eveneens de [twelve
factor app](https://12factor.net/logs) definitie:

> "Logs are the stream of aggregated, time-ordered events collected from the
> output streams of all running processes and backing services. Logs in their raw
> form are typically a text format with one event per line (though backtraces from
> exceptions may span multiple lines). Logs have no fixed beginning or end, but
> flow continuously as long as the app is operating."

Op basis hiervan dient de software bij voorkeur:

* Gegevens over het gebruik en het gedrag van de software op een
  gestandaardiseerde manier beschikbaar te stellen.
* Dit te doen op een wijze dat er normaal gesproken voor elk event één regel
  wordt gelogd.
* Zich idealiter niet te bemoeien met de wijze waarop de logs worden opgeslagen
  of verwerkt.

Voor de monitoring van de juiste werking en het gebruik van de applicatie dient
de software bovendien bij voorkeur:

* Via [Prometheus
  instrumentation](https://prometheus.io/docs/instrumenting/clientlibs/)
  metrics over het gedrag van de applicatie te publiceren.
* Of, als dit niet mogelijk is, een API of een vergelijkbaar mechanisme te hebben
  om, vanaf de host machine metrics uit te vragen zodat deze door middel van een
  [Prometheus exporter](https://prometheus.io/docs/instrumenting/exporters/)
  alsnog gepubliceerd kunnen worden.

## Beschikbaarheid en betrouwbaarheid

Voor een optimale bezoekerservaring is het belangrijk dat software robuust is en
zo goed mogelijk omgaat met de omstandigheden in een museum;

* Software moet kwalitatief in orde zijn en gemaakt zijn om zonder crashen de
  hele dag te kunnen draaien.
* De software is uitgebreid getest op oneigenlijk gebruik: gebruikers moeten
  niet door middel van bijv. toetscombinaties de applicatie kunnen crashen.
* De software is zo geschreven dat ongeacht op welk moment de applicatie (of de
  computer) herstart wordt de applicatie weer normaal functioneert.
* De software blijft normaal functioneren wanneer er bij het opstarten of
  gedurende het gebruik van de software een werkende netwerkverbinding
  ontbreekt. Van deze eis mag uitsluitend worden afgeweken wanneer dit op basis
  van de functionele eisen onontkombaar is. In dat geval dient de impact op de
  bezoeker van het ontbreken van een netwerkverbinding zoveel mogelijk te worden
  beperkt.

## Frameworks

* Er wordt gebruik gemaakt van moderne en open (web)technologie. Het gebruik van
  frameworks waarvan duidelijk is dat deze niet toekomstbestendig zijn, zoals
  Flash en Silverlight, is niet toegestaan.
* Zeker bij toepassingen met relatief beperkte grafische vereisten wordt bij
  voorkeur gebruik gemaakt van web based technologieën zoals HTML5 en React.js
