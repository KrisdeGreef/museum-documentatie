---
title: "Multimedia bestanden"
date: 2018-11-09T16:28:46+01:00
draft: true
---

*Concept*

Voor het aanleveren van multimedia-bestanden hanteert Naturalis de volgende
standaarden:

* Bestandsformaat / container formaat (in volgorede van voorkeur):

  1. [Matroska](https://en.wikipedia.org/wiki/Digital_container_format)
  1. [MP4 / MPEG-4 Part 14](https://en.wikipedia.org/wiki/MPEG-4_Part_14)

* Video codec:[H.264 / MPEG-4
  AVC](https://en.wikipedia.org/wiki/H.264/MPEG-4_AVC)
* Audio codec:

  1. [FLAC](https://en.wikipedia.org/wiki/FLAC)
  1. [AAC](https://en.wikipedia.org/wiki/Advanced_Audio_Coding)
  1. [MP3](https://en.wikipedia.org/wiki/MP3)

* Video framerate: minimaal 25 fps, maximaal 60 fps
* Video resolutie: conform specificaties, over het algemeen een keuze uit:

  1. 1920 x 1200 pixels
     ([WUXGA](https://en.wikipedia.org/wiki/Graphics_display_resolution#1920_%C3%97_1200_(WUXGA)))
  1. 1920 x 1080 pixels
     ([FHD](https://en.wikipedia.org/wiki/Graphics_display_resolution#1920_%C3%97_1080_(FHD)))

* Video scanning: [progressive
  scan](https://en.wikipedia.org/wiki/Progressive_scan)

Het is mogelijk om bij het afspelen van geluid bij een video losstaande
audiobestanden bij te leveren. Het voordeel daarbij is dat bij een nieuwe
rendering uitsluitend het video-bestand hoeft te worden geupdated. Bovendien kan
met een lossless formaat (FLAC) worden gewerkt onafhankelijk van ondersteuning
van het container formaat en/of de gebruikte video rendering software.

Het nadeel van het apart aanleveren van video- en audiocontent is dat het qua
configuratie en contentbeheer (zijn dit de goede versies van zowel audio- als
video?) complexer is.
