---
title: "deaarde"
draft: false
status:
unid: 9cb2ce58-f6d5-4055-9344-cc1f3e879097
shows:
- showaarde
interactives:
- cabinijsland
- camperhawaii
- camperjapan
- demogeode
- demosteensmelten
- ertsenelektro
- fotospotbrazilie
- fotospothottub
- fotospotjapan
- fotospottektoniek
- lava
- schattenjacht
exhibits:
- picknicktafeljapanhawaii
- picknicktafelijsland
- schatkameraarde
decors:
- introaarde
- mijningang
- panorama
- motorbrazilie
- wegwijzeraarde
faciliteiten:
- infopuntaarde
- demogeodekraker
- demosteensmelten
attachments:
- title: kabelschema av (png)
  src: https://files.museum.naturalis.nl/s/jReKwPNAXkFKd5s
- title: kabelschema av (pdf)
  src: https://files.museum.naturalis.nl/s/9L6PS5iYAgjgyBn
- title: kabelschema av (vwx)
  src: https://files.museum.naturalis.nl/s/XHtZNYsZxR5Hjom
- title: vlekkenplan av (png)
  src: https://files.museum.naturalis.nl/s/zaTSGSRMdoFkyrW
- title: vlekkenplan av (pdf)
  src: https://files.museum.naturalis.nl/s/DbYZ9TTMzXFA2GK
- title: vlekkenplan av (vwx)
  src: https://files.museum.naturalis.nl/s/EBX6BPmcFbJonoB
- title: definitief ontwerp (pdf)
  src: https://files.museum.naturalis.nl/s/5jFCkb7yD5SdBwo
- title: plattegrond definitiefontwerp (png)
  src: https://files.museum.naturalis.nl/s/ARnfWdsY45o9QEZ
- title: plattegrond definitiefontwerp (pdf)
  src: https://files.museum.naturalis.nl/s/4zSsBG86LQyBi5e
- title: deaarde_plattegrond_revisie2020 (png)
  src: https://files.museum.naturalis.nl/s/gJkcfknibLFrfZD
- title: deaarde_lichtplan_bouwtekening (pdf)
  src: https://files.museum.naturalis.nl/s/M4P4GQG3e3HmPSE

- title: NAT LX AsBuilt 06 de Aarde 20200617 (pdf)
  src: https://files.museum.naturalis.nl/s/2ennrjMKXdRgF4S
- title: NAT LX AsBuilt 06 de Aarde overzicht op Data-aansluiting (pdf)
  src: https://files.museum.naturalis.nl/s/WytdCdYXyFCDqdH
- title: NAT LX AsBuilt 06 de Aarde overzicht op DMX nummer (pdf)
  src: https://files.museum.naturalis.nl/s/qwjXEHBiSWaDSCq
- title: NAT LX AsBuilt 06 de Aarde overzicht op Groepnummer (pdf)
  src: https://files.museum.naturalis.nl/s/Z5PJD3C3L7eofzw
- title: NAT LX AsBuilt 06 de Aarde overzicht op Posities (pdf)
  src: https://files.museum.naturalis.nl/s/598ztNMwPo9MWxi
- title: NAT LX AsBuilt 06 de Aarde overzicht op Unit nummer (pdf)
  src: https://files.museum.naturalis.nl/s/Rje7aTfpG4mzwL6
- title: NAT LX AsBuilt 06 de Aarde (xls)
  src: https://files.museum.naturalis.nl/s/E68EfcKbLBD2kSf
- title: NAT LX AsBuilt 06 de Aarde (lw6)
  src: https://files.museum.naturalis.nl/s/8NGCpoGFWEECjM7
- title: NAT LX AsBuilt 06 de Aarde (vwx)
  src: https://files.museum.naturalis.nl/s/iBJY35r3arF9YTR
- title: NAT LX AsBuilt 06 de Aarde (xml)
  src: https://files.museum.naturalis.nl/s/G8cWzdqBc9YADTe
---

<!-- Voeg een overzichtsafbeelding toe -->

[![deaarde_plattegrond_definitiefontwerp](https://files.museum.naturalis.nl/s/ARnfWdsY45o9QEZ/preview)](https://files.museum.naturalis.nl/s/4zSsBG86LQyBi5e)

## Functionele omschrijving

De zaal De aarde is een ode aan de enorme kracht van onze planeet: scheppend en
gevend, maar ook vernietigend. Hier reis je langs plekken waar die kracht
zichtbaar en zelfs ook voelbaar is.

Kijk voor meer info op [naturalis.nl](https://www.naturalis.nl/museum/museumzalen/aarde)
of raadpleeg het [definitief ontwerp](https://files.museum.naturalis.nl/s/5jFCkb7yD5SdBwo).

## Technische omschrijving

[![kabelschema](https://files.museum.naturalis.nl/s/jReKwPNAXkFKd5s/preview)](https://files.museum.naturalis.nl/s/9L6PS5iYAgjgyBn)

Het VLAN van deaarde heeft het subnet 10.133.0.0/16, de showcontroller (Pharos) heeft het IP adres 10.133.1.1.

## Onderdelen

[![deaarde_vlekkenplan_av](https://files.museum.naturalis.nl/s/zaTSGSRMdoFkyrW/preview)](https://files.museum.naturalis.nl/s/DbYZ9TTMzXFA2GK)

Een functionele en technische beschrijving van de specifieke onderdelen vind je
op de onderstaande pagina's.

### Show

Meer informatie over de show in deze tentoonstelling vind je hier:

{{< pagelist shows >}}

### Interactives

In de tentoonstelling staan de volgende interactives:

{{< pagelist interactives >}}

### Exhibits

In de tentoonstelling staan de volgende exhibits:

{{< pagelist exhibits >}}

### Decors

De volgende decors maken onderdeel uit van de tentoonstelling:

{{< pagelist decors >}}

### Faciliteiten

De volgende faciliteiten maken onderdeel uit van de tentoonstelling:

{{< pagelist faciliteiten >}}

## Handleidingen en procedures

[Onderhoud algemeen](https://files.museum.naturalis.nl/s/kqHSjr5wtf7odzx)

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues met betrekking tot de gehele
tentoonstelling bekend.

## Afspraken en verantwoordelijkheden

<!-- Voeg hier een overzicht van verantwoordelijken en afspraken omtrent de
exhibit toe -->

### Contact

<!-- Interne contactpersonen -->
* Collectie:
* Ontwerp: [Caroline Breunesse](mailto:caroline.breunesse@naturalis.nl)
* Techniek: [Technisch Team](docs.museum.naturalis.nl)
* Aanspreekpunt TOP: [Sjan Janssen](mailto:sjan.janssen@naturalis.nl)

### SLA

#### Garantie
Indien en geen documentatie te vinden is over garantiebepalingen, dan geldt de algemene overeengekomen regeling. Hieronder nog een keer de toelichting van Erik Jan Elderman op die regeling uit de overeenkomst.
Deze regeling geldt zowel voor de tentoonstellingsbouwers als voor Atatech:

#### Artikel 7               Garantie
* 7.1      	Opdrachtnemer blijft aansprakelijk voor herstel door middel van reparatie of vervanging van het geleverde of onderdelen daarvan die binnen 12 maanden na datum van aflevering
gebreken vertonen. Opdrachtnemer is niet tot herstel gehouden indien hij aantoont dat het
gebrek niet is te wijten aan fouten van Opdrachtnemer of door haar geleverde materialen.

* 7.2      	De in dit artikel geregelde garantie van Opdrachtnemer houdt in dat het geleverde van
deugdelijk materiaal en met goed vakmanschap is vervaardigd en de montagewerkzaamheden
vakkundig zijn uitgevoerd. Mochten zich binnen de garantietermijn materiaal-, fabricage- of ontwerpfouten voordoen in het geleverde, dan zal Opdrachtnemer zonder berekening van kosten zorgdragen voor reparatie daarvan. Naast de feitelijke herstelkosten zullen ook eventuele aan dit herstel verbonden kosten als kosten gemoeid met montage en demontage van apparatuur; transportkosten van apparatuur, alsmede reis- en verblijfskosten van personeel van Opdrachtnemer; extra kosten in verband met spoedeisend herstel van storingen, die als garantieverplichting moeten worden verholpen; aantoonbare technische gevolgschade, welke optreedt door een voor garantie in aanmerking komend defect onderdeel, door Opdrachtnemer worden vergoed.

* 7.3      	Opdrachtnemer staat ervoor in dat het door hem geleverde blijft functioneren conform het Bestek. Mocht binnen een periode van 3 jaar na datum van ondertekening van het overname-protocol, zoals bepaald in artikel 6, aan de hand van systematisch optreden van identieke storingen, blijken dat het opgeleverde of delen daarvan toch niet blijken te voldoen aan het Bestek, zal Opdrachtnemer alsnog zorgdragen voor aanpassing van het geleverde. Opdrachtnemer zal in dat geval het in het kader van deze overeenkomst geleverde door middel van een (ontwerp)-aanpassing herstellen, zonder dat hier voor Naturalis kosten aan zijn verbonden.

Opleverdata verschillen per zaal en per leverancier, maar de meeste zalen/de meeste AVH zijn opgeleverd in juni/juli/augustus 2019. Let op: als binnen die garantietermijn een item vervangen wordt, gaat de garantie van 12 maanden voor dat onderdeel opnieuw in. Oplevering van vervangen onderdelen moet dus goed worden geadministreerd.

### Bouwer

<!-- Naam bouwer interactives (met link naar Topdesk)-->

* Decor: [Bruns](https://www.bruns.nl/)
* Showcontrol: Rutger van Dijk ([SemMika](https://www.semmika.nl/))
* Licht: [Tim Blom](mailto:blomartt@live.nl)
* AV Content (camperjapan, camperjapan): [Studio Kluut](https://aukeflorian.nl/)
* AV Content (cabinijsland): [Studio Louter](http://www.studiolouter.nl/)
* Hardware: [AtaTECH](https://www.ata-tech.nl/)
* Techniek (ertsenelektro, schattenjacht): [Frank Vermeij](mailto:frank@vermeij.nu)
* Bouw Japanse Shrine: [Shofukan (i.s.m. Kees Ouwens)](https://shofukan.nl/)
* Staalwerk onder Japanse Shrine: [Staalwerk Huizen](https://www.staalwerkhuizen.nl/)
* demosteensmelten & ertsenelectro: [Nelissen Decorbouw](mailto:info@nelissendecorbouw.nl)
* lasapparaat demosteensmelten: [Messer Eutectic Castolin benelux n.v.](http://www.castolin.nl)
* Renovatie VW-bus & plaatsing motor: [Martin Mulder](mailto:diesmartin@hetnet.nl)
* Wegdek VW-bus: [Houthoff Zoodesign](http://www.houthoffzoodesign.com)
* Tentoonstellingsinrichting: [De Omslag](http://www.de-omslag.nl/)
* Contactpersoon voor print meerval (door M4four): [Ad Borrenbergs Projectinrichting](https://adborrenbergs.nl/)
* mounting collectie schatkamer: [Artcare](https://artcare.nl/)
* Voiceover opnames schattenjacht: [Bob Kommer Studio's](http://www.bobkommer.com/)
* Preparaat IJslands paard: [Inside Out Animals](https://www.insideoutanimals.com/)
* Gefreesde grenslijn in vloertegels: [Studio America](http://www.america.nl/)

### Ontwerper

<!-- Naam ontwerper interactives (met link naar Topdesk)-->
* Ruimtelijk ontwerp: [Pieter Aartsen](mailto:Pieter.Aartsen@naturalis.nl)
* Techniek (ertsenelektro, schattenjacht): [Frank Vermeij](mailto:frank@vermeij.nu)
* Decor: [Bruns](https://www.bruns.nl/)
* Licht: [Joost de Beij](http://www.licht-joostdebeij.nl/)
* Projecleiding: [Marianne Fokkens](mailto:marianne.fokkens@naturalis.nl)

### Aardbevingexperience


## Bijlagen

{{< pagelist bijlagen >}}
