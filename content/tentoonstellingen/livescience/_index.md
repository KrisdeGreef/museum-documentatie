---
title: "livescience"
draft: false
status:
unid: c4c38367-2419-46a5-bd80-8b4e6d091376
shows:
- showlivescience
interactives:
- digibieb
- expeditietent
- naturalisdigitaal
- readingroom
- studiolab
- tekentafellivescience
exhibits:
- actualiteitenvitrine
- collectieuitdekast
- favorietenvitrine
- tekenkamer
- topstukkenvitrine
- walvissenlivescience
decors:
- boomlivescience
- stellagelivescience
faciliteiten:
- dinolab
- vergaderplekbalkon
- werkstraatlivescience
- pantrymeubel
attachments:
- title: definitief ontwerp (pdf)
  url: https://files.museum.naturalis.nl/s/H42YeWT5qCb3kdA

- title: plattegrond definitief ontwerp (pdf)
  url: https://files.museum.naturalis.nl/s/Med8BA5HxESf7pG
- title: plattegrond definitief ontwerp (png)
  url: https://files.museum.naturalis.nl/s/4qXfDzJ94oKTHdd

- title: beschrijving definitief ontwerp (pdf)
  url: https://files.museum.naturalis.nl/s/fatSf6PfWyBxE4r

- title: vlekkenplan av (pdf)
  url: https://files.museum.naturalis.nl/s/3xPdYxHYgFNdRct
- title: vlekkenplan av (vwx)
  url: https://files.museum.naturalis.nl/s/4dGPX8gCePDiaDd
- title: vlekkenplan av (png)  
  url: https://files.museum.naturalis.nl/s/MWnsj75HR9XeJw9

- title: service manual BRUNS (pdf)
  url: https://files.museum.naturalis.nl/s/Sggyk78Em5aW6YP

- title: akoestische adviezen PEUTZ (pdf)
  url: https://files.museum.naturalis.nl/s/p6WpApqGRbeNRjt

- title: waterpunten en afzuiging (pdf)
  url: https://files.museum.naturalis.nl/s/Tjs5tjfk2wd42HP

- title: kabelschema (pdf)
  url: https://files.museum.naturalis.nl/s/Q9FJJBWw3indr45
- title: Kabelschema (vwx)
  url: https://files.museum.naturalis.nl/s/wHZ8TNfxYDcQ6Xp
- title: kabelschema (png)  
  url: https://files.museum.naturalis.nl/s/PkMLtrscgA7XdEM

- title: verf (directory)
  url: https://files.museum.naturalis.nl/s/cPkb4nr523TF393
- title: staal (directory)
  url: https://files.museum.naturalis.nl/s/e9AS9EiHSXnPZ8n
- title: componenten (directory)
  url: https://files.museum.naturalis.nl/s/D8sQiZDQf9XAfar
- title: lijm (directory)
  url: https://files.museum.naturalis.nl/s/S4EeASL24oBtbBE
- title: kit (directory)
  url: https://files.museum.naturalis.nl/s/2m4cfikSCgLNgAM
- title: hout (directory)
  url: https://files.museum.naturalis.nl/s/2m4cfikSCgLNgAM
- title: sloten (directory)
  url: https://files.museum.naturalis.nl/s/KN9HYb3LsJFTG9p
- title: bevestiging (directory)
  url: https://files.museum.naturalis.nl/s/Qo87c95cK3XB68m
- title: 3D weergaven (directory)
  url: https://files.museum.naturalis.nl/s/GjcXHQZifBCz2rD
- title: livescience_waterpunten en afzuiging_plattegrond (pdf)
  url: https://files.museum.naturalis.nl/s/JtSdK5a9QDimbLK

- title: NAT LX AsBuilt 01 Live Science 20200529 (pdf)
  url: https://files.museum.naturalis.nl/s/4fHZZsGdtdc9cWH
- title: NAT LX AsBuilt 01 Live Science overzicht op Data-aansluiting (pdf)
  url: https://files.museum.naturalis.nl/s/pCZrPJWT75zPrbP
- title: NAT LX AsBuilt 01 Live Science overzicht op DMX nummer (pdf)
  url: https://files.museum.naturalis.nl/s/bRYfkS2RogEN9wY
- title: NAT LX AsBuilt 01 Live Science overzicht op Groepnummer (pdf)
  url: https://files.museum.naturalis.nl/s/z5PXaQJ2jbsW3EF
- title: NAT LX AsBuilt 01 Live Science overzicht op Posities (pdf)
  url: https://files.museum.naturalis.nl/s/DwqQox8YYy4yWsE
- title: NAT LX AsBuilt 01 Live Science overzicht op Unit nummer (pdf)
  url: https://files.museum.naturalis.nl/s/Sn8RtsRRbmXY5LP
- title: NAT LX AsBuilt 01 Live Science (xls)
  url: https://files.museum.naturalis.nl/s/WDgQ6ZB4bwo5J3x
- title: NAT LX AsBuilt 01 Live Science (lw6)
  url: https://files.museum.naturalis.nl/s/FbHdKMD9gnPZefJ
- title: NAT LX AsBuilt 01 Live Science (vwx)
  url: https://files.museum.naturalis.nl/s/pPZeTexFxcjddJ3
- title: NAT LX AsBuilt 01 Live Science (xml)
  url: https://files.museum.naturalis.nl/s/8RKs4642RcCm6cj

---


<!-- Voeg een overzichtsafbeelding toe -->

[![plattegrond](https://files.museum.naturalis.nl/s/4qXfDzJ94oKTHdd/preview)](https://files.museum.naturalis.nl/s/Med8BA5HxESf7pG)

## Functionele omschrijving

LiveScience is geen tentoonstelling in de strikte zin van het woord. In plaats
van installaties en exhibits draait het hier om de mensen die dagelijks aan
onze collectiewerken en onderzoek doen. Centraal staat de ontmoeting tussen de
bezoekers en onzemedewerkers. Dat heeft gevolgen voor het ontwerp, omdat
werkplekken voor de schermen wordengehaald en uiteraard de objecten en
materialen die daarvoor nodig zijn. De inrichting moet faciliterendzijn aan de
gesprekken en activiteiten die de kern van LiveScience vormen.

LiveScience richt zich op families: (groot)ouders met kinderen (voor de meeste
onderdelen 8 jaar en ouder), maar ook op amateuronderzoekers,
Naturalismedewerkers en op collega’s van de Naturalis experts,intern of extern.
Het is een plek om grootschalige presentaties mee te maken, maar biedt
ookgelegenheid tot meer intieme ontmoetingen tussen personen of kleine groepjes
en om activiteiten tedoen. LiveScience is bedoeld om te vertragen, tot de kern
van Naturalis te komen en om te ontdekkenhoeveel kennis hier wordt gemaakt aan
de hand van onze unieke collectie. Gezien de gratis toegang is de
laagdrempeligheid groot, iedereen die zich wil laten verrassen over de natuur
is van harte welkom!

Kijk voor meer info op [naturalis.nl](https://www.naturalis.nl/museum/museumzalen/livescience)

Klik [hier](https://files.museum.naturalis.nl/s/fatSf6PfWyBxE4r) voor een functionele omschrijving.

Klik [hier](https://files.museum.naturalis.nl/s/H42YeWT5qCb3kdA) voor het definitieve ontwerp.

## Technische omschrijving

[![kabelschema](https://files.museum.naturalis.nl/s/PkMLtrscgA7XdEM/preview)](https://files.museum.naturalis.nl/s/eQL49WfztLWfLZW)

Het VLAN van livescience heeft het subnet 10.128.0.0/16, de showcontroller (Pharos) heeft het IP adres 10.128.1.1.

## Onderdelen

[![vlekkenplan av](https://files.museum.naturalis.nl/s/MWnsj75HR9XeJw9/preview)](https://files.museum.naturalis.nl/s/rjEys8dYZZBBdtT)

Een functionele en technische beschrijving van de specifieke onderdelen vind je
op de onderstaande pagina's.

### Show

Meer informatie over de show in deze tentoonstelling vind je hier:

{{< pagelist shows >}}

### Interactives

In de tentoonstelling staan de volgende interactives:

{{< pagelist interactives >}}

### Exhibits

In de tentoonstelling staan de volgende exhibits:

{{< pagelist exhibits >}}

### Faciliteiten

In de tentoonstelling staan de volgende faciliteiten:

{{< pagelist faciliteiten >}}


### Decors

De volgende decors maken onderdeel uit van de tentoonstelling:

{{< pagelist decors >}}

## Handleidingen en procedures

[Service Manual opgesteld door BRUNS](https://files.museum.naturalis.nl/s/Sggyk78Em5aW6YP)

## Known issues

Op dit moment zijn er geen known issues met betrekking tot de gehele
tentoonstelling bekend.

## Afspraken en verantwoordelijkheden

### Contact

* [Pieter Aartsen](mailto:pieter.aartsen@naturalis.nl)
* [Manon Laterveer - de Beer](mailto:Manon.Laterveer-debeer@naturalis.nl)
* [Technisch Team](mailto:support@naturalis.nl)
* Project: [Sjan Janssen](mailto:Sjan.Janssen@naturalis.nl)

### SLA

#### Garantie
Indien en geen documentatie te vinden is over garantiebepalingen, dan geldt de algemene overeengekomen regeling. Hieronder nog een keer de toelichting van Erik Jan Elderman op die regeling uit de overeenkomst.
Deze regeling geldt zowel voor de tentoonstellingsbouwers als voor Atatech:

#### Artikel 7               Garantie
* 7.1      	Opdrachtnemer blijft aansprakelijk voor herstel door middel van reparatie of vervanging van het geleverde of onderdelen daarvan die binnen 12 maanden na datum van aflevering
gebreken vertonen. Opdrachtnemer is niet tot herstel gehouden indien hij aantoont dat het
gebrek niet is te wijten aan fouten van Opdrachtnemer of door haar geleverde materialen.

* 7.2      	De in dit artikel geregelde garantie van Opdrachtnemer houdt in dat het geleverde van
deugdelijk materiaal en met goed vakmanschap is vervaardigd en de montagewerkzaamheden
vakkundig zijn uitgevoerd. Mochten zich binnen de garantietermijn materiaal-, fabricage- of ontwerpfouten voordoen in het geleverde, dan zal Opdrachtnemer zonder berekening van kosten zorgdragen voor reparatie daarvan. Naast de feitelijke herstelkosten zullen ook eventuele aan dit herstel verbonden kosten als kosten gemoeid met montage en demontage van apparatuur; transportkosten van apparatuur, alsmede reis- en verblijfskosten van personeel van Opdrachtnemer; extra kosten in verband met spoedeisend herstel van storingen, die als garantieverplichting moeten worden verholpen; aantoonbare technische gevolgschade, welke optreedt door een voor garantie in aanmerking komend defect onderdeel, door Opdrachtnemer worden vergoed.

* 7.3      	Opdrachtnemer staat ervoor in dat het door hem geleverde blijft functioneren conform het Bestek. Mocht binnen een periode van 3 jaar na datum van ondertekening van het overname-protocol, zoals bepaald in artikel 6, aan de hand van systematisch optreden van identieke storingen, blijken dat het opgeleverde of delen daarvan toch niet blijken te voldoen aan het Bestek, zal Opdrachtnemer alsnog zorgdragen voor aanpassing van het geleverde. Opdrachtnemer zal in dat geval het in het kader van deze overeenkomst geleverde door middel van een (ontwerp)-aanpassing herstellen, zonder dat hier voor Naturalis kosten aan zijn verbonden.

Opleverdata verschillen per zaal en per leverancier, maar de meeste zalen/de meeste AVH zijn opgeleverd in juni/juli/augustus 2019. Let op: als binnen die garantietermijn een item vervangen wordt, gaat de garantie van 12 maanden voor dat onderdeel opnieuw in. Oplevering van vervangen onderdelen moet dus goed worden geadministreerd.

### Bouwer

* Decor: [Bruns](https://bruns.nl)
* Techniek: [Ata Tech](https://ata-tech.nl)
* Licht: [Tim Blom](mailto:blomartt@live.nl)
* Showcontrol: Rutger van Dijk ([SemMika](https://www.semmika.nl/))
* Software: [YiPP](https://yipp.nl)
* Software/filmpjes: [IQ Media](https://iqmedia.nl)
* Filmpjes: [Studio Kluut](mailto:studiokluut@gmail.com)
* Plaatsen objecten: [De Omslag](http://www.de-omslag.nl/)
* Montage Walvisskeletten: [Inside Out Animals](http://www.insideoutanimals.com/)
* Regiemeubel: [Denting Custom Cases](http://www.denting.nl)
* Special Prints: [Studio America](http://www.america.nl/)
* Stellingkasten: [Bruynzeel](https://bruynzeel-storage.nl/)
* Tribune: [Houtwerk](https://houtwerk.nl/)
* Lift werkstraat: [Mobiel BV](http://www.mobiel-autoaanpassingen.nl/)
* Installatietechniek: [Rijndorp](https://www.rijndorp.com/)

### Ontwerper

* Inhoud: [Pieter Aartsen](mailto:pieter.aartsen@naturalis.nl)
* Decor: [Bruns](https://bruns.nl)
* Showcontrol: Rutger van Dijk ([SemMika](https://www.semmika.nl/))
* Techniek: [Ata Tech](https://ata-tech.nl)
* Licht: [Tim Blom](mailto:blomartt@live.nl)
* Lichtontwerp: [Rapenburg Plaza](https://rapenburgplaza.nl/)
* Software: [YiPP](https://yipp.nl)
* Software/filmpjes: [IQ Media](https://iqmedia.nl)

## Bijlagen

{{< pagelist bijlagen >}}
