---
title: "atrium"
date:
draft: false
status:
unid:
shows:
interactives:
exhibits:
decors:
faciliteiten:
- servicebaliekassa1
- servicebaliekassa2
- servicebaliekassa3
- servicebaliekassa4
- ticketzuil1
- ticketzuil2
- ticketzuil3
- ticketzuil4
- toegangscontroleatrium
- winkelkassa
attachments:
- title: asbuilt zaal (pdf)
  src: https://files.museum.naturalis.nl/s/QiZ3tCNNig4QKAS
- title: Plattegrondatrium_locatieticketzuilen
  src: https://files.museum.naturalis.nl/s/p7DHrcW9nKzWgJS
---

{{% pagequality %}}
* Funcionele omschrijving ontbreekt
* Techinsche omschrijving ontbreekt
* Handleidingen en procedures ontbreken
* Afspraken en verantwoordelijkheden ontbreken
* SLAs ontbreken
* Contact nog niet compleet
{{% /pagequality %}}

<!-- Voeg een overzichtsafbeelding toe -->


## Functionele omschrijving

<!-- Voeg hier een functionele omschrijving van de tentoonstelling toe -->



## Technische omschrijving

Het atrium heeft geen eigen subnet. De signage hebben hun eigen VLAN: subnet 10.142.0.0/16.
De bedoeling is dat het VLAN 142 het atrium VLAN wordt, signage komt dan te vervallen.

## Onderdelen

Een functionele en technische beschrijving van de specifieke onderdelen vind je
op de onderstaande pagina's.

### Faciliteiten

Meer informatie over de faciliteiten in deze tentoonstelling vind je hier:

{{< pagelist faciliteiten >}}

### Show

Meer informatie over de show in deze tentoonstelling vind je hier:

{{< pagelist shows >}}

### Interactives

In de tentoonstelling staan de volgende interactives:

{{< pagelist interactives >}}

### Exhibits

In de tentoonstelling staan de volgende exhibits:

{{< pagelist exhibits >}}

### Decors

De volgende decors maken onderdeel uit van de tentoonstelling:

{{< pagelist decors >}}

## Handleidingen en procedures

<!-- Voeg hier verwijzingen naar gebruikers- en beheerdershandleidingen en
procedures toe -->



## Known issues

<!-- Voeg hier known issues toe -->

Het Wifi bereik van de scanners hebben vaak issues.

## Afspraken en verantwoordelijkheden

<!-- Voeg hier een overzicht van verantwoordelijken en afspraken omtrent de
exhibit toe -->

### Contact

<!-- Interne contactpersonen -->



### SLA

#### Garantie
Indien en geen documentatie te vinden is over garantiebepalingen, dan geldt de algemene overeengekomen regeling. Hieronder nog een keer de toelichting van Erik Jan Elderman op die regeling uit de overeenkomst.
Deze regeling geldt zowel voor de tentoonstellingsbouwers als voor Atatech:

#### Artikel 7               Garantie
* 7.1      	Opdrachtnemer blijft aansprakelijk voor herstel door middel van reparatie of vervanging van het geleverde of onderdelen daarvan die binnen 12 maanden na datum van aflevering
gebreken vertonen. Opdrachtnemer is niet tot herstel gehouden indien hij aantoont dat het
gebrek niet is te wijten aan fouten van Opdrachtnemer of door haar geleverde materialen.

* 7.2      	De in dit artikel geregelde garantie van Opdrachtnemer houdt in dat het geleverde van
deugdelijk materiaal en met goed vakmanschap is vervaardigd en de montagewerkzaamheden
vakkundig zijn uitgevoerd. Mochten zich binnen de garantietermijn materiaal-, fabricage- of ontwerpfouten voordoen in het geleverde, dan zal Opdrachtnemer zonder berekening van kosten zorgdragen voor reparatie daarvan. Naast de feitelijke herstelkosten zullen ook eventuele aan dit herstel verbonden kosten als kosten gemoeid met montage en demontage van apparatuur; transportkosten van apparatuur, alsmede reis- en verblijfskosten van personeel van Opdrachtnemer; extra kosten in verband met spoedeisend herstel van storingen, die als garantieverplichting moeten worden verholpen; aantoonbare technische gevolgschade, welke optreedt door een voor garantie in aanmerking komend defect onderdeel, door Opdrachtnemer worden vergoed.

* 7.3      	Opdrachtnemer staat ervoor in dat het door hem geleverde blijft functioneren conform het Bestek. Mocht binnen een periode van 3 jaar na datum van ondertekening van het overname-protocol, zoals bepaald in artikel 6, aan de hand van systematisch optreden van identieke storingen, blijken dat het opgeleverde of delen daarvan toch niet blijken te voldoen aan het Bestek, zal Opdrachtnemer alsnog zorgdragen voor aanpassing van het geleverde. Opdrachtnemer zal in dat geval het in het kader van deze overeenkomst geleverde door middel van een (ontwerp)-aanpassing herstellen, zonder dat hier voor Naturalis kosten aan zijn verbonden.

Opleverdata verschillen per zaal en per leverancier, maar de meeste zalen/de meeste AVH zijn opgeleverd in juni/juli/augustus 2019. Let op: als binnen die garantietermijn een item vervangen wordt, gaat de garantie van 12 maanden voor dat onderdeel opnieuw in. Oplevering van vervangen onderdelen moet dus goed worden geadministreerd.

### Bouwer


### Ontwerper



## Bijlagen

{{< pagelist bijlagen >}}
