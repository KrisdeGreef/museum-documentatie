---
title: "auditorium"
date:
draft: false
componenten:
- allenheath-sq5
- allenheath-dt168
- creston-dmps34k350c
- creston-tsw1060bs
- creston-tsw760bs
- apple-ipad2018
- christie-dwu1075gs
- audinate-avio0x1
- xilica-neutrinoa1608
- sennheiser-ew300g4
- denon-dn500bdmkii
- barco-cse200
- lacoustics-108p
attachments:
- title: Dante overview (jpg)
  src: https://files.museum.naturalis.nl/s/datZeX7TR4w2CiG
- title: Kabelschema auditorium (pdf)
  url: https://files.museum.naturalis.nl/s/AQDHWxHzzdTZcJA
- title: Auditorium opleverdocument (pdf)
  url: https://files.museum.naturalis.nl/s/QpaRnXw8CQxKWWm
- title: Auditorium opleverdocument (docx)
  url: https://files.museum.naturalis.nl/s/K8Dq2jetnziMpKA
- title: Gebruikersinstructies auditorium (pdf)
  url: https://files.museum.naturalis.nl/s/oa6gyL8ebqyjqS8
---

<!-- Voeg een overzichtsafbeelding toe -->

## Functionele omschrijving

<!-- Voeg hier een functionele omschrijving van de tentoonstelling toe -->

## Technische omschrijving

<!-- Voeg hier een blokschema van de hele tentoonstelling toe -->

<!-- Voeg hier een technische omschrijving toe -->

## Onderdelen

Een functionele en technische beschrijving van de specifieke onderdelen vind je
op de onderstaande pagina's.


### Componenten

De volgende componenten maken onderdeel uit van de tentoonstelling:

{{< pagelist componenten >}}

## Handleidingen en procedures

* [Handleiding Opstarten Auditorium](https://docs.google.com/document/d/1CK0CzsR6Cd17Lzn9IDIDM0ynu3H7yNWQrJPjPkfReP8)

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues met betrekking tot de gehele
tentoonstelling bekend.

## Afspraken en verantwoordelijkheden

<!-- Voeg hier een overzicht van verantwoordelijken en afspraken omtrent de
exhibit toe -->

### Contact

<!-- Interne contactpersonen -->

### SLA

<!-- SLAs (met link naar Topdesk) -->

### Bouwer

<!-- Naam bouwer interactives (met link naar Topdesk)-->

### Ontwerper

<!-- Naam ontwerper interactives (met link naar Topdesk)-->

## Bijlagen

{{< pagelist bijlagen >}}
