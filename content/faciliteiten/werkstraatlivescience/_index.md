---
title: werkstraatlivescience
draft: false
status:
componenten:
tentoonstellingen:
- livescience
attachments:
- title: Werkstraat ontwerp (png)
  url: https://files.museum.naturalis.nl/s/Sjn5dsRZdseKsX5
- title: Werkstraat werkplek (png)
  url: https://files.museum.naturalis.nl/s/7NtboEmddsxEN6S
- title: Werkstraat ID sheet (pdf)
  url: https://files.museum.naturalis.nl/s/5f53J7CEWsTTsBX
---

![ontwerp](https://files.museum.naturalis.nl/s/Sjn5dsRZdseKsX5/preview)

![werkplek](https://files.museum.naturalis.nl/s/7NtboEmddsxEN6S/preview)

## Functionele omschrijving

Plek waar bezoekers kunnen kijken naar Naturalis-medewerkers terwijl zij aan het werk 
zijn en hen vragen kunnen stellen. In LiveScience kan je echte onderzoekers en 
collectie-medewerkers van Naturalis ontmoeten en kom je erachter wat ze doen en waarom.

## Technische omschrijving

Deze faciliteit bestaat uit de volgende componenten:

- Werkplekken (# 12) voor onderzoekers, collectiemedewerkers,
  gastmedewerkers,amateurs, studenten en piketmedewerkers.
- (Afsluitbare) kasten voor opbergen van collectie en materialen voor de
  volgende werkdag.
- Wasbak met warm en koud water voor wassen van handen of spoelen van
  materiaal.


## Known issues

Op dit moment zijn er geen known issues bekend.

## Afspraken en verantwoordelijkheden

<!-- Voeg hier een overzicht van verantwoordelijken en afspraken omtrent de
faciliteit toe -->

### Contact

* [Pieter Aartsen](mailto:pieter.aartsen@naturalis.nl)
* [Technisch Team](mailto:support@naturalis.nl)

### Bouwer

* Decor: [Bruns](https://bruns.nl)
* Techniek: [Ata Tech](https://ata-tech.nl)

### Ontwerper

* Inhoud: [Pieter Aartsen](mailto:pieter.aartsen@naturalis.nl)
* Decor: [Bruns](https://bruns.nl)

## Bijlagen

{{< pagelist bijlagen >}}
