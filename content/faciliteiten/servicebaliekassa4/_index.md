---
title: servicebaliekassa4
draft: false
status:
componenten:
tentoonstellingen:
- atrium
attachments:
- title: 
  url: 
---



## Functionele omschrijving

Zie voor de omschrijvingen, leveranciers en alle afspraken: [servicebaliekassa1](https://docs.museum.naturalis.nl/latest/faciliteiten/servicebaliekassa1/)

## Bijlagen

{{< pagelist bijlagen >}}
