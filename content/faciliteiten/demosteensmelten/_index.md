---
title: demosteensmelten
date:
draft: false
status:
unid: fcdf37cb-ae08-42ef-b86a-d7d56c5d8949
componenten:
- castolin-ohm
- ecler-dam614
- labgruppen-ipd1200
- sennheiser-ew300g3
tentoonstellingen:
- deaarde
experiences:
attachments:
- title: Handleiding Waterstofbrander (docx)
  src: https://files.museum.naturalis.nl/s/Tk26LgfigMynyaG
- title: PVM lassen stenen smelten (docx)
  url: https://files.museum.naturalis.nl/s/bsQXkjbEttPkCqt
- title:  Instructie steen smelten-_ Vervolg SJ (docx)
  url: https://files.museum.naturalis.nl/s/wcm4AwEFBjjkb96
- title: Aceton opslag en  bijvullen in lab (docx)
  url: https://files.museum.naturalis.nl/s/F5Pn4X5ybwZTmMK
- title: Bemensen demo stenen smelten_ (docx)
  url: https://files.museum.naturalis.nl/s/ZxgsCHANeXGz2ep
- title: stenen smelten -_ gevaarlijke emissie_ (docx)
  url: https://files.museum.naturalis.nl/s/y3WwRD3R8eCSrY3
- title: deaarde_interactives_demosteensmelten_instructie_documentatie (pdf)
  src: https://files.museum.naturalis.nl/s/jtF4qFL68LiHbfz
- title: Demonstratie Hawaii
  url: https://files.museum.naturalis.nl/s/ZrnzdJ5z9mfrzZm
- title: Kabelschema Spraakversterking (pdf)
  url: https://files.museum.naturalis.nl/s/xWpPkFLr6PHMAaM
---

[![ontwerp](https://files.museum.naturalis.nl/s/LW4TqmEw9G8Eozd/preview)](https://files.museum.naturalis.nl/s/LW4TqmEw9G8Eozd)


## Functionele omschrijving

Tijdens de vijf minuten durende demonstratie 'Lava Live' in zaal De aarde maakt het publiek mee hoe een stukje basaltsteen live wordt gesmolten tot lava. Het smelten gebeurt door een hete vlam (2500-2800 0C) op de steen te richten. Het gesmolten gesteente stolt vervolgens vlug tot een bolletje obsidiaan.

Voor educatieve begeleiders is dit de meest uitdagende demonstratie in de zaal. Uiteraard is de demonstratie omgeven door strenge veiligheidsmaatregelen. Zo is het smeltapparaat afgeschermd van het publiek en dragen bezoekers zonnebrillen klasse 3. Ook staan ze op veilige afstand. Omdat dit een activiteit is die geconcentreerde aandacht vergt van de demonstrateur, wordt hij uitgevoerd door twee personen. Dan kan de een alle aandacht richten op de techniek van het smelten, terwijl de ander de interactie aangaat met het publiek.

## Technische omschrijving

<!-- Voeg hier een blokschema toe -->

<!-- Voeg hier een technische omschrijving toe -->

Deze faciliteit bestaat uit de volgende componenten:

{{< pagelist componenten >}}

## Handleidingen en procedures

* [Opstart procedure OHM 2.4](https://files.museum.naturalis.nl/s/GX6xs6aqCYTxGn2)
* [Handleiding Waterstofbrander](https://files.museum.naturalis.nl/s/Tk26LgfigMynyaG)
* [PVM lassen stenen smelten](https://files.museum.naturalis.nl/s/bsQXkjbEttPkCqt)
* [Instructie steen smelten-_ Vervolg SJ](https://files.museum.naturalis.nl/s/wcm4AwEFBjjkb96)
* [Aceton opslag en  bijvullen in lab](https://files.museum.naturalis.nl/s/F5Pn4X5ybwZTmMK)


## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Afspraken en verantwoordelijkheden

* [Bemensen demo stenen smelten_](https://files.museum.naturalis.nl/s/ZxgsCHANeXGz2ep)
* [stenen smelten -_ gevaarlijke emissie_](https://files.museum.naturalis.nl/s/y3WwRD3R8eCSrY3)

### Contact

<!-- Interne contactpersonen -->

### SLA

<!-- SLAs (met link naar Topdesk) -->

### Bouwer

<!-- Naam bouwer interactives (met link naar Topdesk)-->

### Ontwerper

<!-- Naam ontwerper interactives (met link naar Topdesk)-->

## Bijlagen

{{< pagelist bijlagen >}}
