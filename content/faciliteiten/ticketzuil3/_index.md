---
title: ticketzuil3
draft: false
status:
componenten:
tentoonstellingen:
- atrium
attachments:
- title: Plattegrondatrium_locatieticketzuilen
  url: https://files.museum.naturalis.nl/s/p7DHrcW9nKzWgJS
---



## Functionele omschrijving

In het atrium staan 4 ticketzuilen waarmee bezoekers zelf tickets kunnen kopen . Dit is 1 van die ticketzuilen.

Zie hier het beheerdocument in Google Drive (moet nog overgenomen worden op deze pagina):
[Beheerdocument Ticketzuil (Vending machine)](https://docs.google.com/document/d/1qGiHEppLy1iNhH6zGyHifznTtm-uOc--tjizMSKE-a4/edit?pli=1#)

[![Plattegrondatrium_locatieticketzuilen](https://files.museum.naturalis.nl/s/p7DHrcW9nKzWgJS/preview)](https://files.museum.naturalis.nl/s/p7DHrcW9nKzWgJS)

## Technische omschrijving

[![Applicatielandschap Ticketing Recreatex - Focus Ticketzuil](https://files.museum.naturalis.nl/s/Bp6ey5JKLoz6J5i/preview)](https://files.museum.naturalis.nl/s/Bp6ey5JKLoz6J5i)

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

### Contact

+ Projectleider: Denise de Haan
+ Applicatiebeheerder: Sander Bal, Marijn Prins of Bart den Dulk

### Bouwer



### Ontwerper



## Bijlagen

{{< pagelist bijlagen >}}
