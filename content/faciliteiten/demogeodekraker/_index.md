---
title: demogeodekraker
date:
draft: false
unid: a0a6e57f-db64-499e-9bd5-c653b1288c13
status:
componenten:
tentoonstellingen:
- deaarde
experiences:
attachments:
- title: extra collectie demo geode kraken (docx)
  src: https://files.museum.naturalis.nl/s/gLmkWY9MGF4NWRy
- title: Demonstratie Brazilië
  src: https://files.museum.naturalis.nl/s/Dn4BftH4tFFM76X
---

<!-- Voeg een overzichtsafbeelding toe -->

## Functionele omschrijving

<!-- Voeg hier een functionele omschrijving van de faciliteit toe -->

## Technische omschrijving

<!-- Voeg hier een blokschema toe -->

<!-- Voeg hier een technische omschrijving toe -->

Deze faciliteit bestaat uit de volgende componenten:

{{< pagelist componenten >}}

## Handleidingen en procedures

* [extra collectie demo geode kraken](https://files.museum.naturalis.nl/s/gLmkWY9MGF4NWRy)

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Afspraken en verantwoordelijkheden

<!-- Voeg hier een overzicht van verantwoordelijken en afspraken omtrent de
faciliteit toe -->

### Contact

<!-- Interne contactpersonen -->

### SLA

<!-- SLAs (met link naar Topdesk) -->

### Bouwer

<!-- Naam bouwer interactives (met link naar Topdesk)-->

### Ontwerper

<!-- Naam ontwerper interactives (met link naar Topdesk)-->

## Bijlagen

{{< pagelist bijlagen >}}
