---
title: dinolab
draft: false
status:
componenten:
tentoonstellingen:
- livescience
attachments:
- title: Dinolab ID sheet (pdf)
  url: https://files.museum.naturalis.nl/s/7tdse3H5FoZstLF
- title: livescience_faciliteiten_werkstraatlivescience_bouwtekening (pdf)
  url: livescience_faciliteiten_werkstraatlivescience_bouwtekening
---

![Dinolab](https://files.museum.naturalis.nl/s/z3wzP2NtXw25HCL/preview)

## Functionele omschrijving

Plek waar Naturalis-medewerkers dino-collecties prepareren.

Naturalis is niet alleen een museum met geweldige tentoonstellingen en
spannende evenementen. Het museum is vooral een gerenommeerd wetenschappelijk
instituut op het gebied van biodiversiteitsonderzoek, met een collectie waaraan
dagelijks door allerlei interessante mensen wordt gewerkt.

## Technische omschrijving

Deze faciliteit bestaat uit de volgende componenten:

- Geluids- en stofdichte cabine waarin medewerkers fossielen kunnen prepareren (*viezedingenherriehok*).
- Podium met werkplek buiten de cabine, waar bezoekers vragen kunnen stellen aanNaturalis-medewerkers.
- Faciliteiten voor het prepareren van fossielen.
- Showcase met uitgeprepareerde fossielen (eindproducten van het werk dat je ziet).
- 3D printer (buiten cabine).

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

### Contact

* [Pieter Aartsen](mailto:pieter.aartsen@naturalis.nl)
* [Technisch Team](mailto:support@naturalis.nl)

### Bouwer

* Decor: [Bruns](https://bruns.nl)
* Techniek: [Ata Tech](https://ata-tech.nl)

### Ontwerper

* Inhoud: [Pieter Aartsen](mailto:pieter.aartsen@naturalis.nl)
* Decor: [Bruns](https://bruns.nl)

## Bijlagen

{{< pagelist bijlagen >}}
