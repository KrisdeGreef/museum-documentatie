---
title: "japanstheater"
date: 2019-07-24T13:49:25Z
draft: true
status:
unid: dd181a28-f604-45bf-9bd8-d15c1e5e4d4d
shows:
- showjapanstheater
interactives:
- aardbevingplafond
- bewakingjapanstheater
- introjapanstheater
- mythischesteen
- theaterpoppen
exhibits:
decors:
- decorjapanstheater
faciliteiten:
attachments:


- title: Manual bewegende vloer (directory)
  src: https://files.museum.naturalis.nl/s/3YQwgR2ZALgbt9Z
- title: Werktekeningen Bruns (directory)
  src: https://files.museum.naturalis.nl/s/XpCE6ddnebK5Kra
- title: Elektroducumentatie japanstheater (folder)
  src: https://files.museum.naturalis.nl/s/zy9iLy5b4K8AsTK
- title: Vlekkenplan japanstheater (pdf)
  src: https://files.museum.naturalis.nl/s/Sik4zBzLcizkLEq
- title: Totaalkabelschema japanstheater (pdf)
  src: https://files.museum.naturalis.nl/s/S3pHzM7XR3iQimA
- title: Luidsprekerplan japanstheater (pdf)
  src: https://files.museum.naturalis.nl/s/KZR5diCcHQ84BsW
- title: japanstheater_stroom_datapunten_bouwtekening (pdf)
  src: https://files.museum.naturalis.nl/s/jzZCgZPbpDosRN2
- title: japanstheater_bouwtekenningA (pdf)
  src: https://files.museum.naturalis.nl/s/Kirg3etLxRirnor
- title: japanstheater_bouwtekeningB (pdf)
  src: https://files.museum.naturalis.nl/s/FsCALEKB3WpJrgB
- title: japanstheater_verhoogdevloeruitgang_bouwtekening (pdf)
  src: https://files.museum.naturalis.nl/s/kkbnaxnqemSoLT9
- title: japanstheater_vloerbelastingen_documentatie (pdf)
  src: https://files.museum.naturalis.nl/s/zj6BcajZG3BpmNy
- title: japanstheater_DGMRrapport_bouwfysische aspecten_documentatie (pdf)
  src: https://files.museum.naturalis.nl/s/ZpfCKJ6E8nRy3PQ
- title: japanstheater_keuringsrapportTÜV_documentatie (pdf)
  src: https://files.museum.naturalis.nl/s/D7beGNRHptoa5Nj
- title: japanstheater_vloeraardbeving_bouwtekening (pdf)
  src: https://files.museum.naturalis.nl/s/YZ2Az5fiZxJor7Y


- title: deaarde_decors_bamboeafzetpalen-1A_bouwtekening (pdf)
  src: https://files.museum.naturalis.nl/s/oZcDdwZPeHQqSfz
- title: deaarde_decors_bamboeafzetpalen_1C_bouwtekening (pdf)
  src: https://files.museum.naturalis.nl/s/F4FBXPjB6PcbBPX
- title: Service contract Bruns (pdf)
  src: https://files.museum.naturalis.nl/f/81337

- title: NAT LX AsBuilt 06 Japans Theater 20200525 (pdf)
  url: https://files.museum.naturalis.nl/s/ySjSqcrK7XTjD3y
- title: NAT LX AsBuilt 06 Japans Theater overzicht op Data aansluiting (pdf)
  url: https://files.museum.naturalis.nl/s/TNQKKP4oKanH8H3
- title: NAT LX AsBuilt 06 Japans Theater overzicht op DMX nummer (pdf)
  url: https://files.museum.naturalis.nl/s/QKwNtc9QToQ2iRH
- title: NAT LX AsBuilt 06 Japans Theater overzicht op Groepnummer (pdf)
  url: https://files.museum.naturalis.nl/s/xP9NA8ZPacjbnzT
- title: NAT LX AsBuilt 06 Japans Theater overzicht op Posities (pdf)
  url: https://files.museum.naturalis.nl/s/DECrMp5mG3KGcmD
- title: NAT LX AsBuilt 06 Japans Theater overzicht op Unit nummer (pdf)
  url: https://files.museum.naturalis.nl/s/2m8X9Zic3iJ34iL
- title: NAT LX AsBuilt 06 Japans Theater (xls)
  url: https://files.museum.naturalis.nl/s/WcMgsFSq9iP9Q4z
- title: NAT LX AsBuilt 06 Japans Theater (lw6)
  url: https://files.museum.naturalis.nl/s/oyX2QJgQ4i5TSs3
- title: NAT LX AsBuilt 06 Japans Theater (vwx)
  url: https://files.museum.naturalis.nl/s/yr7YQF2YrCcJJQZ
- title: NAT LX AsBuilt 06 Japans Theater (xml)
  url: https://files.museum.naturalis.nl/s/ZW7Kg3PiXLcZfPm


---

<!-- Voeg een overzichtsafbeelding toe -->

## Functionele omschrijving

<!-- Voeg hier een functionele omschrijving van de experience toe -->

## Technische omschrijving

<!-- Voeg hier een blokschema van de hele experience toe -->

Het VLAN van het japanstheater heeft het subnet 10.140.0.0/16, de showcontroller (Pharos) heeft het IP adres 10.140.1.1.

## Onderdelen

Een functionele en technische beschrijving van de specifieke onderdelen vind je
op de onderstaande pagina's.

### Show

Meer informatie over de show in deze experience vind je hier:

{{< pagelist shows >}}

### Interactives

In de experience staan de volgende interactives:

{{< pagelist interactives >}}

### Exhibits

In de experience staan de volgende exhibits:

{{< pagelist exhibits >}}

### Decors

De volgende decors maken onderdeel uit van de experience:

{{< pagelist decors >}}

## Handleidingen en procedures

* [Handleiding poppentheater](https://files.museum.naturalis.nl/s/CHGykQKyaqKATYp)
* [Handleiding bewegende vloer](https://files.museum.naturalis.nl/s/zEDTXWEfxyHdddp)
* [Onderhoud algemeen](https://files.museum.naturalis.nl/s/5HpfdEk3fkEEMQA)

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues met betrekking tot de gehele
experience bekend.

## Afspraken en verantwoordelijkheden

<!-- Voeg hier een overzicht van verantwoordelijken en afspraken omtrent de
exhibit toe -->

### Contact

- Project: [Sjan Janssen](mailto:Sjan.Janssen@naturalis.nl)
- Technisch: [Technisch Team](mailto:support@naturalis.nl)

### SLA

  * [Service contract Bruns](https://files.museum.naturalis.nl/f/81337#pdfviewer)

### Bouwer

* Decor: [Bruns](https://www.bruns.nl/)
* Showcontrol: Rutger van Dijk ([SemMika](https://www.semmika.nl/))
* Voiceover en soundscape: [Bob Kommer Studio's](http://www.bobkommer.com/)
* Aanpassing bankjes: [De Omslag](http://www.de-omslag.nl/)

### Ontwerper

* ruimtelijk ontwerp: [Studio Daniel Ament](http://danielament.com)

## Bijlagen

{{< pagelist bijlagen >}}
