---
title: "Vervang NUC"
date: 2019-08-19T13:47:43+02:00
draft: false
status:
attachments:
---

Wanneer een NUC problemen geeft en vervangen moet worden ga je als volgt te
werk:

## Voorbereiden nieuwe NUC

Wanneer een NUC nieuw is geleverd moeten er eerst een aantal standaard
instellingen in de BIOS worden toegepast en administratieve handelingen in
TOPdesk worden gepleegd.

### BIOS Settings

1. Start de NUC op in de BIOS (F2)
2. Controleer de volgende instellingen (te vinden onder Advanced)
  * Devices > Onboard Devices > WLAN en Bluetooth uit
  * Cooling > Fan Control Mode > Balanced (of anders wanneer de NUC in een afgesloten ruimte zit)
  * Devices > PCI > M.2 Slot moet aan staan
  * Power > Balanced Enabled (of anders wanneer het een NUC is met een zware taak zoals een Unity game)
  * Power > After Power Failure > Last state
  * Boot > UEFI Boot Priority >  'PXE IP4' bovenaan
  * Boot > Boot Configuration > 'Boot Network device last' uit
  * Boot > Boot Configuration > 'Failsafe Watchdog' uit
  * Boot > Boot Configuration > 'Display F12 for Network Boot' aan
  * Boot > Secure Boot > 'Secure Boot' uit
3. Sla de BIOS instellingen op door op F10 en Yes te drukken
4. Zet de NUC weer uit

## Registreren in TOPdesk

### Voorbereidingen

Voor het uitvoeren van alle onderstaande handelingen heb je een aantal tools
nodig.

* TOPdesk account
* AWX account
* Gitlab account met schrijfrechten op `ansible-darwinweg` repository.
* git
* [topdesk_to_awx script](https://gitlab.com/naturalis/mii/housekeeping/-/tree/master/ )

### TOPdesk

1. Zoek in TOPdesk met Module 'Assets overview' en op basis van de naam en
   van de te vervangen NUC de bijbehorende assetkaart.
1. Zoek eveneens de assetkaart van de nieuwe NUC, ditmaal op basis van het
   serienummer.
1. Neem de velden outlet, IP adres, aansluitpunt, outlet en beoogde naam over
   van de huidige NUC naar de nieuwe NUC en verwijder deze bij de huidige NUC.
1. Neem de relationship van de huidige NUC over op de nieuwe NUC en verwijder
   deze bij de huidige NUC door te klikken op 'Unlink'.
1. Vervang de naam bij de huidige NUC (bijv. van `waternesten-cmp-1` naar
   `nn-cmp-151`). Je kunt het oorspronkelijk `nn` nummer van de asset vinden
   door in History te filteren op alleen 'Card modifications'.

### Ansible inventory

1. Open de `master` branch van de `ansible-darwinweg` repository.

{{% notice warning %}}
Onderstaande werkt niet meer
{{% /notice %}}
1. Open AWX en voer de template 'Asset informatie' uit met de naam van de NUC.
1. Neem de waarden van de velden `mac_address` en `unid` over in het host_var
   bestand van de NUC, bijvoorbeeld:
   `inventories/production/host_vars/waternesten-cmp-1.yml`
1. Commit deze code:
   `git commit -m 'Replace NUC' \
   inventories/production/host_vars/waternesten-cmp-1.yml`
1. Sync de code:

   ```
   git pull --rebase
   git push
   git checkout master
   git pull --rebase
   git merge develop
   git push
   ```

### Aansluiten computer

Haal de huidige NUC weg en sluit de nieuwe, vervangende NUC aan op de fysieke
plek van de huidige NUC. Controleer voor de zekerheid ook of de outlet of
switchpoort waarop de NUC wordt aangesloten overeenkomt met de informatie in de
Ansible inventory.

Zorg er voor dat de nieuwe NUC wordt voorzien van het label met de naam van de
computer (bijv. `waternesten-cmp-1`) en verwijder dit label van de oude NUC.

## AWX

Volg de stappen zoals uitgelegd in [de handleiding (re)deployment
interactive]({{< ref "/handleidingen/redeployment" >}}).

## Bijlagen

{{< pagelist bijlagen >}}
