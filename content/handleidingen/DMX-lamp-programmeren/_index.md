---
title: "DMX lamp programmeren"
date: 2019-07-08T09:37:18+02:00
draft: false
status:
attachments:
---

## Inleiding
Deze handleiding legt uit hoe je een DMX lamp programmeert. Het is goed om de
DMX manual van de lamp erbij te houden om er zeker van te zijn of je de juiste
channels gebruikt. Als voorbeeld programmeren we de CLS Sapphire.

## Benodigdheden

![](./dmx_programeer.png)

* DMX tool
* DMX magneet
* DMX spanning

## Lamp aansluiten

![](./kanaal_1.png)

* Sluit de lamp aan en zet de fase op fase 1.

## DMX tool

![](./dmx_tool.png)

Volg de handleiding van de lamp, in ons geval de [CLS Sapphire](https://files.museum.naturalis.nl/s/DAQxRoe7AKTMnwB).

* Selecteer het juiste kanaal
  * Voor kanaal 40 zet je channel 1 op 40 en channel 2 op 0
  * Voor kanaal 328 zet je channel 1 op 0 en channel 2 op (328-255=73) 73
* Ga naar channel 3 en zet de waarde op 1 (last DMX value)
* Ga naar channel 4 en zet de waarde op 2 (Soft dimm = on)
* Let op channel 5 bij een RGB(W) lamp (liever Tim vragen voor RGB(W) lamp!)

## DMX magneet

Pak nu de magneet en hou hem bij de programmeer plek van de lamp, bij de Sapphire is
dat bij de SN# stikker onderop. De lamp knippert 3 keer heel snel, dan is hij geprogrammeerd.

## Test

Zet de DMX tool op je geprogrammeerde kanaal en test door de waarde op 50% en 100% te zetten.
De lamp zou daarom moeten reageren.

## DMX sticker

Vergeet niet het DMX kanaal op te schrijven op de lamp-stikker.
