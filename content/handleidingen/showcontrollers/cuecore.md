---
title: "Visual Solutions Cuecore2"
draft: false
shows:
 - showdood
 - showijstijd
 - showleven
 - showverleiding
 - showvroegemens
componenten:
 - visualproductions-cuecore2
attachments:
 - title: Handleiding Cuecore2 (pdf)
   url: https://files.museum.naturalis.nl/s/mCdfPHGbFtRtyPd
---

Dit is een handleiding die gaat over de configuratie en bediening van de
[Cuecore2]({{< relref "componenten/visualproductions-cuecore2" >}})
showcontroller.

## Shows

Dit type showcontroller wordt gebruikt in de volgende shows:

{{< pagelist shows >}}

Belangrijke punten die te maken hebben met deze shows:

* Rutger heeft in juni 2020 alle CueCore2 showcontrollers de firmware geupdate naar versie 1.36.
* De configuratie bestanden per show-controller zijn te vinden op de content server.

## Componenten

Deze handleiding heeft betrekking op de volgende componenten:

{{< pagelist componenten >}}

De Cuecore2 ondersteunt maximaal 1024 DMX kanalen. De voordelen ten opzichte
van de Pharos LPC showcontrollers zijn:

* Goedkoper in aanschaf
* Programmering door middel van webbased interface
* Mogelijkheid voor het opnemen en afspelen van shows
* Bij de bouw van het museum minder technische problemen

Het nadeel is dat shows lang niet altijd makkelijk kunnen worden aangepast,
zeker wanneer ze 'live' zijn opgenomen.

Daar staat tegenover dat elk willekeurig programma of licht controller die DMX
uitstuurt kan worden gebruikt voor opnames van een show op de Cuecore. Zoals
hieronder staat uitgelegd is voor deze toepassing met name e:cue gebruikt.

Het beheren en configureren van de cuecore kan met
[diverse soorten software](https://www.visualproductions.nl/downloads/)
die op allerlei platformen draaien. Het actieve programma kan als XML file
worden gedownload met VManager.

Dit type showcontroller krijgt stroom via Power over Ethernet.

De LEDjes op de showcontroller geeft weer welke netwerk status het kastje
heeft:

* Rood geeft aan dat DHCP actief is.
* Wit geeft aan dat een statisch IP adres is geconfigureerd.

## Algemeen

Met uitzondering van Leven, wordt poort A in alle zalen gebruikt voor input en
poort B voor output. Bij Leven worden twee poorten voor de output gebruikt.

De DMX aanstuurbare _fixtures_ in de zalen kunnen op drie manieren worden
ingesteld als geen DMX signaal wordt ontvangen:

1. Laatste stand
1. Geen weergave
1. Emergency stand

Indeling van triggers is ongeveer hetzelfde als op de Pharos showcontrollers.

## Cirkelprojectie

In zaal leven en dedood draaien projecties via [Watchpax players]({{< relref
"componenten/dataton-watchpax20" >}}) waarop de software [Watchout]({{< relref
"componenten/dataton-watchout" >}}). Ook deze worden aangestuurd via de
Cuecore2. De showcontroller stuurt een Artnet bericht om een en ander te
starten.

## Webinterface

Via de webinterface kan je onder Track -> Open Console individuele aansturen
via de commandline. Met commando `1 @ 58` zet je bijvoorbeeld kanaal 1 op
waarde 58.

## Zaal specifieke instellingen

 * Leven - is ingesteld via een opname, e:cue nodig voor
 * De Dood - is ingesteld via een opname
 * De Vroege Mens - is via de webinterface gedaan, is ook via de webinterface
   aan te passen
 * De IJstijd - golf over landschap is opgenomen, rest via
   webinterface aan te passen
 * De Dood - merendeel via opname, rest via webinterface
