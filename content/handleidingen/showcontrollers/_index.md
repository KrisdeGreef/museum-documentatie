---
title: "Beheren Showcontrollers"
date: 2019-07-08T09:37:18+02:00
draft: false
status:
attachments:
---

Dit zijn de handleidingen die gaan over de showcontrollers. In het museum maken we gebruik van twee
merken showcontrollers voor de diverse zalen. De cuecore2 van visual solutions en drie
verschillende typen van het merk pharos. Deze kennen ieder hun eigen manier van configureren.

* [Pharos](https://docs.museum.naturalis.nl/latest/handleidingen/showcontrollers/pharos/)
* [Visual Solutions Cuecore2](https://docs.museum.naturalis.nl/latest/handleidingen/showcontrollers/cuecore/)
