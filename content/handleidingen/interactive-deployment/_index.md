---
title: "Interactive deployment"
date: 2019-07-08T09:37:18+02:00
draft: false
status:
attachments:
---

Dit document beschrijft alle stappen die nodig zijn om een nieuwe
interactive, mediaspeler, kiosk of andere applicatie die draait op een
Intel NUC te voorzien van een besturingssysteem, software, content en de juiste
instellingen.

In de meeste gevallen gaan we ervanuit dat het gaat om Intel NUC's die
nog helemaal _kaal_ zijn of vanaf niets opnieuw weer moeten worden
geïnstalleerd.

## Voorbereidingen

### Ansible

Voordat je aan de slag kan moet je op zijn minst op je werkomgeving de
beschikking hebben over Ansible en de [`ansible-darwinweg`
repository](https://gitlab.com/naturalis/darwinweg/ansible-darwinweg)
met playbooks en inventories.

```
pip3 install ansible
git clone git@gitlab.com:naturalis/darwinweg/ansible-darwinweg.git
```

Na het clonen van deze repo kan je alle requirements van deze repo
installeren met behulp van ansible-galaxy:

```
ansible-galaxy install -r roles/requirements.yml --force --ignore-errors --roles-path ./roles/
```

Dit commando installeert alle rollen die nodig zijn om ieder mogelijke machine
binnen het netwerk van het museum te configureren. De configuratie verloopt via
de inventories, playbooks, hostvars en groupvars in deze repo.

Zowel Ansible als AWX communiceren met de interactives via MAAS. Het is
belangrijk om op zijn minst ssh toegang te hebben tot de MAAS productie
omgeving en via ssh bij een interactive (bijvoorbeeld via ubuntu@).

### AWX

De bovenstaande ansible-galaxy setup is ook de basis van de AWX installatie.
AWX draait op [awx.museum.naturalis.nl](https://awx.museum.naturalis.nl/#/login).

De Job Templates (playbooks) die nu zijn geconfigureerd zijn:

* Deploy interactives
* Housekeeping
* Start interactive

### BIOS instellingen

Voor de installatie van NUC's maken we gebruik van MAAS. MAAS zorgt voor de
installatie van Ubuntu op de NUC's. De installatie van Ubuntu door MAAS gebeurt
op basis van PXE boot (opstarten van het besturingssysteem vanaf het netwerk).
Voor het aanzetten van NUC's maken we gebruik van Wake-on-LAN (WoL).

Om een nieuwe NUC te configureren moet de BIOS worden geconfigureerd zodat PXE boot
als eerste in de 'Boot Order' staat en WoL aan staat.

De eerste stap bij nieuwe NUC's is daarom de juiste BIOS instellingen laden. Je
doet dit door:

* Een USB stick te formateren met FAT32
* Dit [BIOS-profiel](https://files.museum.naturalis.nl/s/sact4ak5wPC4mwT) te
  downloaden en op de USB-stick te zetten
* De USB-stick in de NUC te steken en bij het opstarten op F2 te drukken.
* In het BIOS-menu te klikken op het profiel-icoon, te kiezen voor import
  profile, te navigeren naar het profiel op de USB-stick en deze te selecteren.
* Na het laden van het profiel af te sluiten met F10. Kies daarbij voor het
  opslaan van de instellingen.

### Switchpoort configureren

Het netwerk in het museum is verdeeld in verschillende segmenten (VLAN's). Het
is daarom belangrijk dat de switchpoort waarop een NUC is aangesloten is
voorzien van de juiste VLAN configuratie.

Naast de NUC's worden ook de switches (die draaien op Cumulus Linux)
geconfigureerd met Ansible.

Op dit moment is het uitsluitend mogelijk om de netwerkpoorten te configureren
vanaf een zogeheten out of band server. Op korte termijn zal het ook mogelijk
zijn om de poorten te configureren vanuit AWX.

### Content

Veel van de interactives bevatten content van derde partijen. Het is belangrijk
van te voren te controleren of de content al beschikbaar is op de [content
server](https://files.museum.naturalis.nl) en er voor te zorgen dat
het 'Download' account op deze server leesrechten heeft op de content.

## Configuratie en deployment

Op het moment dat de NUC een poort en netwerk locatie heeft gekregen, via
PXE boot en Wake-on-LAN te besturen is en de content beschikbaar is, kan de
complete deployment worden uitgevoerd.

### Sheets to topdesk

Serienummers, poortnummers en mac adressen worden door de leverancier als het
goed is netjes bijgehouden in de [assets
sheet](https://docs.google.com/spreadsheets/d/1Z3CmsDAxJTDSwA6Jz11BLskBGZxJ3ZzifC_T5nXaxec/edit#gid=0).
Deze moeten eens in de zoveel tijd worden gelijk getrokken met het housekeeping
script
[sheets_to_topdesk](https://gitlab.com/naturalis/mii/housekeeping#google-sheets-to-topdesk).
Dit kan gewoon in [AWX](https://awx.museum.naturalis.nl).

### Hostvars maken

Vervolgens moet je de hostvars van de te configureren interactive toevoegen aan
de inventory. Dit kan je doen via de housekeeping scripts. Het makkelijkste is
de [housekeeping scripts](https://gitlab.com/naturalis/mii/housekeeping)
_clonen_ en vervolgens het [topdesk_to_awx
commando](https://gitlab.com/naturalis/mii/housekeeping#topdesk-to-awx)
draaien waarmee je een hostvar config kan maken:

```
topdesk_to_awx -o -s flipper -p ./inventories/production
```

Dit commando levert een hostvars configuratie bestand zoals:

```
# host_vars/flipperkast-cmp-1.yml
---
ansible_host: 10.130.1.61
network_interfaces:
- ip_address: 10.130.1.61
  mac_address: 94:c6:91:a8:23:8e
  outlet: N1.D31
  switch: netdw2-leaf-n1d
  switch_port: 31
type: Computer
unid: c4aa901e-c9bf-4d25-bd47-8aefe84265f1
```

Als je het bovenstaande commando zonder `-p` aanroept krijg je de configuratie
via `stdout` te zien.

Het is belangrijk om deze configuratie nog te verifiëren en uit te breiden
want de specifieke variabelen en rol van de machine wordt niet automatische
uit Topdesk gehaald. Het is een goed idee om een voorbeeld van reeds
geïnstalleerde interactives te volgen. Het is ook belangrijk om de
`group_vars` te controleren waar vaak groep- of inhoudspecifieke variabelen
gedefinieerd staan.

### Groups

Een belangrijk onderdeel van de configuratie is het indelen van de host in
een _group_ binnen de inventory. In `./inventories/production/hosts` staan
de diverse hosts verdeeld in groepen. Zo kan je een host een specifiek
karaktereigenschap geven. Bijvoorbeeld de host `kijkersgelderland-cmp-3`
behoort tot de groepen:

* interactives
* games
* kijkersgelderland
* deijstijd
* ijstijdkijkers

Al deze groepen kennen gemeenschappelijke variabelen die ook weer opnieuw
kunnen worden gezet via onderliggende `group_vars` en `host_vars`. Volg hierbij
zoveel mogelijk de bestaande setup of de suggesties in de documentatie
van de darwinweg ansible configuratie.

### Ansible playbook

[Interactive](https://gitlab.com/naturalis/mii/ansible-interactive) vormt
de basis voor de specifiekere rollen:

* [ansible-unity3d](https://gitlab.com/naturalis/mii/ansible-unity3d)
  De rol voor de installatie van games en andere unity applicaties.
* [ansible-mpv](https://gitlab.com/naturalis/mii/ansible-mpv)
  De rol voor de installatie van video- en geluidspelers
* [ansible-kiosk](https://gitlab.com/naturalis/mii/ansible-kiosk)
  De rol voor de installatie van kiosk installaties.

Met deze drie rollen kunnen ook installaties worden toegevoegd met Arduino
hardware of UDP interfaces. Dit is allemaal te doen via variabelen.

Als de voorbereidende configuratie klaar is moeten de nieuwe
configuratiebestanden toegevoegd, gecommit en gepushed naar de [git
repository](https://gitlab.com/naturalis/darwinweg/ansible-darwinweg). Zorg er
daarbij voor dat je wijzigingen ook in de `master` branch van de repository
staan.

Zodra dat het geval is kun je de complete deployment uitvoeren via
[AWX](https://awx.museum.naturalis.nl/). Volg de stappen zoals uitgelegd in [de
handleiding (re)deployment interactive]({{< ref "/handleidingen/redeployment"
>}}).
