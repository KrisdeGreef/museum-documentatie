---
title: "Lichtplan volgens Rutger"
date: 2019-09-18T16:16:12+02:00
draft: false
status:
attachments:
---

Dit is een beknopte samenvatting van een wandeling met Rutger en Ashwin door
het museum op woensdagmiddag 11 september 2019 hierbij heeft Rutger een aantal
belangrijke zaken uitgelegd over het lichtplan waarop gelet moet worden
tijdens het gebruik van het museum.

## Configuratie

De CueCore2 controllers zijn alleen te uploaden via wifi vmanager. Dat betekent
dat voor aanpassingen aan deze machines de wifi in de zaal moeten worden
aangezet. Het gaat om:

 * orgel-ctl-1
 * showdood-ctl-1
 * showijstijd-ctl-1
 * showleven-ctl-1
 * showverleiding-ctl-1
 * showvroegemens-ctl-1

## Nissen

Het licht van de nissen hoort standaard uit, overal.

Maar de switch van 'overigen' moet *juist* aan in zalen:

 * 2 - leven
 * 6 - aarde
 - 8 - de dood

In De Dood moeten alle lampen het in principe doen. Een deel zit alleen in de
lichtgroep van de nissen.


## De Verleiding:

 - taartlicht in het midden van de zaal moet lopen, dan is de goede lichtstand, zo niet, dan moet de showcontroller een 'start show' krijgen
 - geluidorgel staat helemaal los van de rest, ook het licht, die doet het dus altijd ook als de rest van het museum uit is

## De Dood

  - ledstrip veranderd heel langzaam van kleur, dit gebeurt los van de showcontroller
  - nissen moeten aan voor de rest van de lampjes (of omprikken met hoogwerker)

## Atrium

 - plafondlampen zijn ook met pharos te bedienen
 - kleur is nu alleen zacht- en hardwit

Dit kan worden aangepast door alle lampen te vervangen voor meerkleurige modellen waardoor het lichtplan van
het Atrium flink kan worden veranderd.

## IJstijd:

 - de wolken moeten "lopen"
 - er komt nog licht in de vitrines (waarschijnlijk ledstrips)

## De vroege mens:

 - klein beetje beweging in de projectie, schijnwerpers die elkaar opvolgen
 - alle lampen altijd aan

## Dinotijd:

 - lichtsturing stuurt de show
 - een deel van de filmpjes wordt door de pharos geloopt
 - lichtsturing doen voetstappen
 - voetstaplampjes draadloos dmx (er zijn reserven)

## De Aarde:

 - ze willen een mooie stand
 - ook hier moeten de nissen aan
 - schattenjacht helmpjes lichten stenen op
 - schaduw zou wegmoeten bij de 'afgrond' om effect te verbeteren
