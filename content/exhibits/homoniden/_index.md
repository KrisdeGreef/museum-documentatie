---
title: "homoniden"
draft: false
status:
unid: f084039a-4773-46fd-b997-5437fc3f3003
componenten:
tentoonstellingen:
- devroegemens
attachments:
- title: asbuilt
  url: https://files.museum.naturalis.nl/s/AymP6frsYeDynYp
- title: definitiefontwerp
  url: https://files.museum.naturalis.nl/s/Co7j8Zj8WZK9zCK
- title: onderhoudsvoorschrift
  url: https://files.museum.naturalis.nl/s/ipAgq2TDcc2mPig
---

![devroegemens_homoniden_definitiefontwerp](https://files.museum.naturalis.nl/s/QnjL9TrjordgfET/preview)

## Functionele omschrijving

<!-- Voeg hier een functionele omschrijving van de exhibit toe -->

Een compacte presentatie van afgietsels van andere "key"-fossils die ons beeld
van de menselijke evolutie bepalen.

## Technische omschrijving

<!-- Voeg hier een blokschema toe -->

<!-- Voeg hier een technische omschrijving toe -->

Deze interactive bestaat uit de volgende componenten:

{{< pagelist componenten >}}

## Handleidingen en procedures

<!-- Voeg hier verwijzingen naar gebruikers- en beheerdershandleidingen en
procedures toe -->

* [onderhoudsvoorschrift](https://files.museum.naturalis.nl/s/ipAgq2TDcc2mPig)

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Afspraken en verantwoordelijkheden

<!-- Voeg hier een overzicht van verantwoordelijken en afspraken omtrent de
exhibit toe -->

### Contact

[Matthijs Vegter](mailto:matthijs.vegter@naturalis.nl)

### SLA

<!-- SLAs (met link naar Topdesk) -->

### Bouwer

* [Hypsos](https://hypsos.com/soesterberg/)

### Ontwerper

* Collectie:
* Ontwerp: [Designwolf](http://designwolf.nl/contact/)

## Bijlagen

{{< pagelist bijlagen >}}
