---
title: "gestiktevis"
date: 2019-07-24T13:48:35Z
draft: true
status:
unid: c6f94662-8f62-4f05-a4ae-2a4ec06389c2
componenten:
tentoonstellingen:
- dedood
attachments:
- title: dood_exhibit_vitrine_gestikte_vis_bouwtekening (pdf)
  src: https://files.museum.naturalis.nl/s/2FgzM6a8cWwDdgm
---

![dedood_gestiktevis_bouwtekening](https://files.museum.naturalis.nl/s/oZqJHmysgMqG44a/preview)

## Functionele omschrijving

<!-- Voeg hier een functionele omschrijving van de exhibit toe -->

## Technische omschrijving

<!-- Voeg hier een blokschema toe -->

<!-- Voeg hier een technische omschrijving toe -->

Deze interactive bestaat uit de volgende componenten:

{{< pagelist componenten >}}

## Handleidingen en procedures

<!-- Voeg hier verwijzingen naar gebruikers- en beheerdershandleidingen en
procedures toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Afspraken en verantwoordelijkheden

<!-- Voeg hier een overzicht van verantwoordelijken en afspraken omtrent de
exhibit toe -->

### Contact

<!-- Interne contactpersonen -->

### SLA

<!-- SLAs (met link naar Topdesk) -->

### Bouwer

* Decor: [Kloosterboer](http://www.kloosterboer-decor.nl/)

### Ontwerper

* Decor: [Kloosterboer](http://www.kloosterboer-decor.nl/)
* Inhoud:

## Bijlagen

{{< pagelist bijlagen >}}
