---
title: "walvissenlivescience"
draft: false
status:
unid: 8bcf435b-b6aa-43e7-bb3f-12d1f3660a6a
tentoonstellingen:
- livescience
attachments:
- title: Positie Walvissen in Livescience
  url: https://files.museum.naturalis.nl/s/Er6E2eSaNJ3QB8z
- title: Schematisch ontwerp walvis hangen
  url: https://files.museum.naturalis.nl/s/aAKfbDpXeMmaeY2
- title: 3D weergave ophangen walvissen
  url: https://files.museum.naturalis.nl/s/QxGwMBTNwjYaWgj
- title: livescience_exhibits_walvissenlifescience_gridflamcorail_bouwtekening (pdf)
  url: https://files.museum.naturalis.nl/s/gNbqoeacxkpQgsJ
---

![schema](https://files.museum.naturalis.nl/s/aAKfbDpXeMmaeY2/preview)

## Functionele omschrijving

In de zaal live science hangen diverse zee zoogdieren aan de metalen
draagbalken aan het plafond. Het gaat om skeletten van:

* Tuimelaar
* Witsnuitdolfijn
* Zeeleeuw
* Noordelijke butskop
* Orca
* Tursio sp.

## Bijlagen

{{< pagelist bijlagen >}}
