---
title: "etalageopvallen"
date: 2019-07-24T13:48:34Z
draft: true
status:
unid: 395af7c8-2532-49f4-b06e-5382caf1f832
componenten:
tentoonstellingen:
- deverleiding
attachments:
- title: verleiding_exhibit_etalageopvallen_kijkertjes_bouwtekening (png)
  src: https://files.museum.naturalis.nl/s/DTgy4m84YAeY3EE
- title: etalageopvallen_technischetekening (pdf)
  src: https://files.museum.naturalis.nl/s/PKe3c8xBsT5EAqn
- title: verleiding_exhibit_etalageopvallen_bouwtekening (png)
  src: https://files.museum.naturalis.nl/s/8DYK8m97JFmA7RQ
---

![opvallen](./opvallen.png)

## Functionele omschrijving

<!-- Voeg hier een functionele omschrijving van de exhibit toe -->

## Technische omschrijving

<!-- Voeg hier een blokschema toe -->

<!-- Voeg hier een technische omschrijving toe -->

Deze interactive bestaat uit de volgende componenten:

{{< pagelist componenten >}}

## Handleidingen en procedures

<!-- Voeg hier verwijzingen naar gebruikers- en beheerdershandleidingen en
procedures toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Afspraken en verantwoordelijkheden

<!-- Voeg hier een overzicht van verantwoordelijken en afspraken omtrent de
exhibit toe -->

### Contact

<!-- Interne contactpersonen -->

### SLA

<!-- SLAs (met link naar Topdesk) -->

### Bouwer

<!-- Naam bouwer interactives (met link naar Topdesk)-->

### Ontwerper

<!-- Naam ontwerper interactives (met link naar Topdesk)-->

## Bijlagen

{{< pagelist bijlagen >}}
