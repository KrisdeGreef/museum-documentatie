---
title: "nestenvitrine"
date: 2019-07-24T16:05:07Z
draft: true
status:
unid: 0aafec06-62a6-4a16-babc-3a535deafdf3
componenten:
tentoonstellingen:
- deverleiding
attachments:
- title: nestenkast
  scr: https://files.museum.naturalis.nl/s/gg8oCeKb2xxoGA9
- title: samenstelling spiegel boven nestkastje
  src: https://files.museum.naturalis.nl/s/do6KnTq7sY6dtWq
- title: spiegel ooievaarsnest
  url: https://files.museum.naturalis.nl/s/8SZWTLMkp2eWY69
- title: spiegel met scharnier
  url: https://files.museum.naturalis.nl/s/FDL7EDWsDwmCnsX

---

![nestenkast](./nestkast.png)

## Functionele omschrijving

<!-- Voeg hier een functionele omschrijving van de exhibit toe -->

## Technische omschrijving

<!-- Voeg hier een blokschema toe -->

<!-- Voeg hier een technische omschrijving toe -->

Deze interactive bestaat uit de volgende componenten:

{{< pagelist componenten >}}

## Handleidingen en procedures

<!-- Voeg hier verwijzingen naar gebruikers- en beheerdershandleidingen en
procedures toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Afspraken en verantwoordelijkheden

<!-- Voeg hier een overzicht van verantwoordelijken en afspraken omtrent de
exhibit toe -->

### Contact

<!-- Interne contactpersonen -->

### SLA

<!-- SLAs (met link naar Topdesk) -->

### Bouwer

* Decor: [Bruns](https://bruns.nl)

### Ontwerper

<!-- Naam ontwerper interactives (met link naar Topdesk)-->

## Bijlagen

{{< pagelist bijlagen >}}
