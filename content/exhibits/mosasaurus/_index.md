---
title: "mosasaurus"
date: 2019-07-24T16:05:06Z
draft: true
status:
unid: 86b4c96d-c9db-4154-803d-082e40117148
componenten:
tentoonstellingen:
- dinotijd
attachments:
- title: 4125_ILD_GA_006B
  src: https://files.museum.naturalis.nl/s/L433tL8K9GkLndk
- title: dino_exhibit_mosasaurus_bouwtekening (pdf)
  url: https://files.museum.naturalis.nl/s/gmgBKPHqtdxnZjo
---

![mosasaurus](./4125_ILD_GA_006B.png)

## Functionele omschrijving

<!-- Voeg hier een functionele omschrijving van de exhibit toe -->

## Technische omschrijving

<!-- Voeg hier een blokschema toe -->

<!-- Voeg hier een technische omschrijving toe -->

Deze interactive bestaat uit de volgende componenten:

{{< pagelist componenten >}}

## Handleidingen en procedures

<!-- Voeg hier verwijzingen naar gebruikers- en beheerdershandleidingen en
procedures toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Afspraken en verantwoordelijkheden

<!-- Voeg hier een overzicht van verantwoordelijken en afspraken omtrent de
exhibit toe -->

### Contact

<!-- Interne contactpersonen -->

### SLA

<!-- SLAs (met link naar Topdesk) -->

### Bouwer

* Decor: [Kloosterboer](http://www.kloosterboer-decor.nl/)


### Ontwerper

<!-- Naam ontwerper interactives (met link naar Topdesk)-->

## Bijlagen

{{< pagelist bijlagen >}}
