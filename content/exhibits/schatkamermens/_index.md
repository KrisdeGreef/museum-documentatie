---
title: "schatkamermens"
draft: false
status:
unid: 6a8d06b2-0ee4-4872-a37d-6f87c20a5d94
componenten:
tentoonstellingen:
- devroegemens
attachments:
- title: asbuilt (pdf)
  src: https://files.museum.naturalis.nl/s/rxQDKY4jqmP5C2f
- title: definitiefontwerp (pdf)
  url: https://files.museum.naturalis.nl/s/AJMCeioefExis9a
- title: schoonmaakvoorschrift duboisvitrine (pdf)
  src: https://files.museum.naturalis.nl/s/NL5yceXCsyDARtY
- title: schoonmaakvoorschrift schatkamer (pdf)
  url: https://files.museum.naturalis.nl/s/RaaXKLbNyAtepMp
- title: vloerdetail (pdf)
  url: https://files.museum.naturalis.nl/s/9RtebRjqQtXxwjL
- title: devroegemens_rapport_brandvertraging_materiaal_projectiescherm (pdf)
  src: https://files.museum.naturalis.nl/s/Hrxr4tFFmEC3to4
---

![devroegemens_schatkamermens_definitiefontwerp](https://files.museum.naturalis.nl/s/PENrEYbNXoo43GM/preview)

## Functionele omschrijving

<!-- Voeg hier een functionele omschrijving van de exhibit toe -->

Een schatkamer met alleen de vier topstukken van Dubois: schedelkapje, kies,
dijbeen en schelp; gepresenteerd met compacte uitleg en link (pepper’s ghost)
naar silhouet Homo erectus, om botten in context te plaatsen.

## Technische omschrijving

Deze exhibit bevat geen technische onderdelen.

## Handleidingen en procedures

<!-- Voeg hier verwijzingen naar gebruikers- en beheerdershandleidingen en
procedures toe -->

- [schoonmaakvoorschrift schatkamer](https://files.museum.naturalis.nl/s/RaaXKLbNyAtepMp)
- [schoonmaakvoorschrift duboisvitrine](https://files.museum.naturalis.nl/s/NL5yceXCsyDARtY)

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Afspraken en verantwoordelijkheden

<!-- Voeg hier een overzicht van verantwoordelijken en afspraken omtrent de
exhibit toe -->

### Contact

<!-- Interne contactpersonen -->

- Projectleider: [Matthijs Vegter](mailto:matthijs.vegter@naturalis.nl)

### SLA

<!-- SLAs (met link naar Topdesk) -->

### Bouwer

<!-- Naam bouwer interactives (met link naar Topdesk)-->

 - [Hypsos](https://hypsos.com/soesterberg/)

### Ontwerper

 - [Designwolf](http://designwolf.nl/contact/)

## Bijlagen

{{< pagelist bijlagen >}}
