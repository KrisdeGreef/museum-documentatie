---
title: "eieren"
date: 2019-07-24T13:48:33Z
draft: true
status:
unid: 57b4e428-8d38-4b46-ac80-a3f490a086ae
componenten:
tentoonstellingen:
- deverleiding
attachments:
- title: 17326.4.8.2_2.001A_REV01 Meubel Eieren 20181217.pdf
  src: https://files.museum.naturalis.nl/s/xKkoKmbKamKQwsS
---

![eieren](./eieren.png)

## Functionele omschrijving

<!-- Voeg hier een functionele omschrijving van de exhibit toe -->

## Technische omschrijving

<!-- Voeg hier een blokschema toe -->

<!-- Voeg hier een technische omschrijving toe -->

Deze interactive bestaat uit de volgende componenten:

{{< pagelist componenten >}}

## Handleidingen en procedures

<!-- Voeg hier verwijzingen naar gebruikers- en beheerdershandleidingen en
procedures toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Afspraken en verantwoordelijkheden

<!-- Voeg hier een overzicht van verantwoordelijken en afspraken omtrent de
exhibit toe -->

### Contact

<!-- Interne contactpersonen -->

### SLA

<!-- SLAs (met link naar Topdesk) -->

### Bouwer

 * Decor: [Bruns](https://bruns.nl)

### Ontwerper

 * Decor: [Bruns](https://bruns.nl)
 * Inhoud:

## Bijlagen

{{< pagelist bijlagen >}}
