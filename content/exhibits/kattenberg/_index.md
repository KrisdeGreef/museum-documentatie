---
title: "kattenberg"
date: 2019-07-24T13:48:37Z
draft: true
status:
unid: e5de124a-f52d-4f7d-888f-3655a2780ea7
componenten:
tentoonstellingen:
- leven
attachments:
- title: leven_exhibits_katenberg_ventilatie_bouwtekening (pdf)
  src: https://files.museum.naturalis.nl/s/NnntM3LRZGiXC3o
- title: leven_exhibits_kattenberg_L6_bouwtekening (pdf)
  url: https://files.museum.naturalis.nl/s/4Yy62SXf9t84mCd
---

![kattenberg](./kattenberg.png)

## Functionele omschrijving

<!-- Voeg hier een functionele omschrijving van de exhibit toe -->

## Technische omschrijving

<!-- Voeg hier een blokschema toe -->

<!-- Voeg hier een technische omschrijving toe -->

Deze interactive bestaat uit de volgende componenten:

{{< pagelist componenten >}}

## Handleidingen en procedures

<!-- Voeg hier verwijzingen naar gebruikers- en beheerdershandleidingen en
procedures toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Afspraken en verantwoordelijkheden

<!-- Voeg hier een overzicht van verantwoordelijken en afspraken omtrent de
exhibit toe -->

### Contact

<!-- Interne contactpersonen -->

### SLA

<!-- SLAs (met link naar Topdesk) -->

### Bouwer

<!-- Naam bouwer interactives (met link naar Topdesk)-->

### Ontwerper

<!-- Naam ontwerper interactives (met link naar Topdesk)-->

## Bijlagen

{{< pagelist bijlagen >}}
