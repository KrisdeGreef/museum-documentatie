---
title: "neushoornvitrine"
date: 2019-07-24T16:05:07Z
draft: true
status:
unid: 94e0362c-7ae0-4e3c-9c0d-a5967a477c3d
componenten:
tentoonstellingen:
- deijstijd
attachments:
- title: deijstijd_vitrinetypeC_bouwtekening_neushoorn_en_steppenwisenten_onderkaken (pdf)
  src: https://files.museum.naturalis.nl/s/miSrYdqzPCHJXo5
---

![deijstijd_vitrinetypeC_bouwtekening_neushoorn_en_steppenwisenten_onderkaken](https://files.museum.naturalis.nl/s/QbZ8bJkcj5aMj7T/preview)

## Functionele omschrijving

<!-- Voeg hier een functionele omschrijving van de exhibit toe -->


## Handleidingen en procedures

<!-- Voeg hier verwijzingen naar gebruikers- en beheerdershandleidingen en
procedures toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Afspraken en verantwoordelijkheden

<!-- Voeg hier een overzicht van verantwoordelijken en afspraken omtrent de
exhibit toe -->

### Contact

<!-- Interne contactpersonen -->

### SLA

<!-- SLAs (met link naar Topdesk) -->

### Bouwer

<!-- Naam bouwer interactives (met link naar Topdesk)-->

### Ontwerper

<!-- Naam ontwerper interactives (met link naar Topdesk)-->

## Bijlagen

{{< pagelist bijlagen >}}
