---
title: "reuzenhert"
date: 2019-07-24T16:05:09Z
draft: true
status:
unid: 39f84adb-4e6d-4421-846b-f819e3b3bbdf
componenten:
tentoonstellingen:
- deijstijd
attachments:
- title: deijstijd_podiumReuzenhert_bouwtekening (pdf)
  src: https://files.museum.naturalis.nl/s/3gTWE7F8PfsRa97
---

![deijstijd_podiumReuzenhert_bouwtekening](https://files.museum.naturalis.nl/s/j5okasSRWTdxE9c/preview)

## Functionele omschrijving

<!-- Voeg hier een functionele omschrijving van de exhibit toe -->



## Handleidingen en procedures

<!-- Voeg hier verwijzingen naar gebruikers- en beheerdershandleidingen en
procedures toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Afspraken en verantwoordelijkheden

<!-- Voeg hier een overzicht van verantwoordelijken en afspraken omtrent de
exhibit toe -->

### Contact

<!-- Interne contactpersonen -->

### SLA

<!-- SLAs (met link naar Topdesk) -->

### Bouwer

<!-- Naam bouwer interactives (met link naar Topdesk)-->

### Ontwerper

<!-- Naam ontwerper interactives (met link naar Topdesk)-->

## Bijlagen

{{< pagelist bijlagen >}}
