---
title: "mammoet"
date: 2019-07-24T13:48:37Z
draft: true
status:
unid: 846df42c-9cbd-4aac-b5a7-5bc6c6a4b21f
componenten:
tentoonstellingen:
- deijstijd
attachments:
- title: deijstijd_podiummammoet_bouwtekening (pdf)
  src: https://files.museum.naturalis.nl/s/3xYtfqC7yjfQngS
---

![deijstijd_podiummammoet_bouwtekening](https://files.museum.naturalis.nl/s/tNRFXQdMADNJAxz/preview)

## Functionele omschrijving

<!-- Voeg hier een functionele omschrijving van de exhibit toe -->



## Handleidingen en procedures

<!-- Voeg hier verwijzingen naar gebruikers- en beheerdershandleidingen en
procedures toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Afspraken en verantwoordelijkheden

<!-- Voeg hier een overzicht van verantwoordelijken en afspraken omtrent de
exhibit toe -->

### Contact

<!-- Interne contactpersonen -->

### SLA

<!-- SLAs (met link naar Topdesk) -->

### Bouwer

<!-- Naam bouwer interactives (met link naar Topdesk)-->

### Ontwerper

<!-- Naam ontwerper interactives (met link naar Topdesk)-->

## Bijlagen

{{< pagelist bijlagen >}}
