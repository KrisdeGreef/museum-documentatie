---
title: "vogeldal"
date: 2019-07-24T16:05:11Z
draft: true
status:
unid: 02a015ae-0070-45e0-ac24-49629f3708c7
componenten:
tentoonstellingen:
- leven
attachments:
- title: leven_exhibits_vogeldal_vlinderinstallatie_materiaalspecificatie_collectie (pdf)
  src: https://files.museum.naturalis.nl/s/gNjTM8gfXLxiPJa
---

<!-- Voeg een overzichtsafbeelding toe -->

## Functionele omschrijving

<!-- Voeg hier een functionele omschrijving van de exhibit toe -->

## Technische omschrijving

<!-- Voeg hier een blokschema toe -->

<!-- Voeg hier een technische omschrijving toe -->

Deze interactive bestaat uit de volgende componenten:

{{< pagelist componenten >}}

## Handleidingen en procedures

* [vogeldal vlinderinstallatie materiaalspecificatie collectie](https://files.museum.naturalis.nl/s/gNjTM8gfXLxiPJa)

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Afspraken en verantwoordelijkheden

<!-- Voeg hier een overzicht van verantwoordelijken en afspraken omtrent de
exhibit toe -->

### Contact

<!-- Interne contactpersonen -->

### SLA

<!-- SLAs (met link naar Topdesk) -->

### Bouwer

<!-- Naam bouwer interactives (met link naar Topdesk)-->

### Ontwerper

<!-- Naam ontwerper interactives (met link naar Topdesk)-->

## Bijlagen

{{< pagelist bijlagen >}}
