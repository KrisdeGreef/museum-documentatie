---
title: "schatkameraarde"
date: 2019-07-24T16:05:09Z
draft: true
status:
unid: 4cd0f6cf-e39b-4efb-b0a3-265c7d4377be
componenten:
tentoonstellingen:
- deaarde
attachments:
- title: schatkamer plattegrond
  src: https://files.museum.naturalis.nl/s/HsTwmADJQDbsep5
- title: doorsnede schatkamer
  url: https://files.museum.naturalis.nl/s/F5orW5aXNxbpgG7
- title: collectiedrager
  url: https://files.museum.naturalis.nl/s/2QyrMaCaeTSy7Az
- title: grafische leuning
  url: https://files.museum.naturalis.nl/s/zMYHL5z2Q575abE
- title: schattenjacht
  url: https://files.museum.naturalis.nl/s/abegZHwYFPKxLqR
- title: schatkamer vitrine 1 (Bruns)
  src: https://files.museum.naturalis.nl/s/3NzWs3ZEB2YiYqH
- title: schatkamer vitrine 2 (Bruns)
  url: https://files.museum.naturalis.nl/s/BjSCi2DCSwpyc2F
- title: schatkamer vitrine / akoestische wand (Bruns)
  url: https://files.museum.naturalis.nl/s/tzgCzgXoac9gr3N
- title: schatkamer vitrine / verhoogde vloer met ramp (Bruns)
  url: https://files.museum.naturalis.nl/s/BrqtW7nGc2yaYKD
- title: schatkamer collectie drager (Bruns)
  url: https://files.museum.naturalis.nl/s/YzjS6sk3zFqTsaW
- title: schatkamer ID sheets (directory met docx documenten)
  url: https://files.museum.naturalis.nl/s/6ya3w4FHCG99PHj
- title: deaarde_exhibits_schatkameraarde_hekje_bouwtekening (png)
  url: https://files.museum.naturalis.nl/s/e4ejN5NKMgfy3Dz

---

![schatkamer plattegrond](https://files.museum.naturalis.nl/s/HsTwmADJQDbsep5/preview)

## Functionele omschrijving

<!-- Voeg hier een functionele omschrijving van de exhibit toe -->

## Technische omschrijving

<!-- Voeg hier een blokschema toe -->

<!-- Voeg hier een technische omschrijving toe -->

Deze interactive bestaat uit de volgende componenten:

{{< pagelist componenten >}}

## Handleidingen en procedures

- [glas schoonmaak advies](https://files.museum.naturalis.nl/s/qTKCwwN9Pf5p6db)

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Afspraken en verantwoordelijkheden

<!-- Voeg hier een overzicht van verantwoordelijken en afspraken omtrent de
exhibit toe -->

### Contact

<!-- Interne contactpersonen -->

### SLA

<!-- SLAs (met link naar Topdesk) -->

### Bouwer

* Decor: [Bruns](https://bruns.nl)

### Ontwerper

<!-- Naam ontwerper interactives (met link naar Topdesk)-->

## Bijlagen

{{< pagelist bijlagen >}}
