---
title: "fallusfestival"
date: 2019-07-24T13:48:34Z
draft: true
status:
unid: ff4dd4be-16e1-4cf8-b2f8-9d4c8222667f
componenten:
tentoonstellingen:
- deverleiding
attachments:
- title: 17326.4.5.1_1.001A REV01 Stellingkast Fallus festival 20190221.pdf
  src: https://files.museum.naturalis.nl/s/4BKXA24rtKmoemg
---

![deverleiding_fallusfestival_bouwtekening](https://files.museum.naturalis.nl/s/j9L3LiqDHXpxP4d/preview)

## Functionele omschrijving

<!-- Voeg hier een functionele omschrijving van de exhibit toe -->

## Technische omschrijving

<!-- Voeg hier een blokschema toe -->

<!-- Voeg hier een technische omschrijving toe -->

Deze interactive bestaat uit de volgende componenten:

{{< pagelist componenten >}}

## Handleidingen en procedures

<!-- Voeg hier verwijzingen naar gebruikers- en beheerdershandleidingen en
procedures toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Afspraken en verantwoordelijkheden

<!-- Voeg hier een overzicht van verantwoordelijken en afspraken omtrent de
exhibit toe -->

### Contact

<!-- Interne contactpersonen -->

### SLA

<!-- SLAs (met link naar Topdesk) -->

### Bouwer

* Decor: [Bruns](https://bruns.nl)

### Ontwerper

* Decor: [Bruns](https://bruns.nl)
* Inhoud:

## Bijlagen

{{< pagelist bijlagen >}}
