---
title: "tekenkamer"
date: 2019-07-24T16:03:35Z
draft: false
status:
tentoonstellingen:
- livescience
attachments:
- title: Tekenkamer ID sheet
  src: https://files.museum.naturalis.nl/s/3cTiC3omYwT6zLp
- title: Tekenkamer wand
  src: https://files.museum.naturalis.nl/s/68LRMFZjXAf6gKm
- title: livescience_exhibits_tekenkamer_bouwtekening (pdf)
  url: https://files.museum.naturalis.nl/s/eTeb7xFqAWCTQo3
---

![tekenkamer](./tekenkamer.png)

## Functionele omschrijving

Kijk mee terwijl wetenschappelijke illustratoren van Naturalis aan het werk zijn, bewonder hun
prachtige tekeningen (vroeger en nu) van levensechte planten en dieren of probeer het zelf.

Wetenschappelijke illustraties van planten en dieren spelen een onmisbare rol in het onderzoek
van Naturalis. Ze laten precies die details zien die nodig zijn en zijn veel nauwkeuriger dan een
foto.

## Technische omschrijving

* Mobiele Tekentafel als werkplek voor wetenschappelijk illustrator.
* Tafel met tekenmateriaal voor bezoekers.
* Wand met originele wetenschappelijke illustraties, gemaakt door (huidige en
vroegere)wetenschappelijk illustratoren van Naturalis, geordend op techniek
(aquarel, pen en inkt,potlood, computer) en tentoongesteld volgens ICN
richtlijnen.
* De illustraties zijn ingelijst en beveiligd tegen diefstal.
* Door regelmatige wisseling van de illustraties wordt schade als gevolg van licht voorkomenen kan worden ingespeeld op actuele projecten, mogelijk gekoppeld aan een tijdelijkeopstelling in de _Actualiteitenvitrine_.
* Presentatie van tekeningen, gemaakt door bezoekers.

### Contact

* [Pieter Aartsen](mailto:pieter.aartsen@naturalis.nl)
* [Manon Laterveer - de Beer](mailto:Manon.Laterveer-debeer@naturalis.nl)

### Bouwer

* [Bruns](https://bruns.nl)

## Tentoonstelling

{{< pagelist tentoonstellingen >}}

## Bijlagen

{{< pagelist bijlagen >}}
