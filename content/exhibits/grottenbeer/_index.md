---
title: "grottenbeer"
date: 2019-07-24T13:48:35Z
draft: true
status:
unid: c01aa567-5203-4165-878f-452bd7caa177
componenten:
tentoonstellingen:
- deijstijd
attachments:
- title: deijstijd_podiumgrottenbeer_bouwtekening (pdf)
  src: https://files.museum.naturalis.nl/s/5FKxXecpfJikDJQ
---

![deijstijd_podiumgrottenbeer_bouwtekening](https://files.museum.naturalis.nl/s/XRLLLgTWDMpqBGH/preview)

## Functionele omschrijving

<!-- Voeg hier een functionele omschrijving van de exhibit toe -->


## Handleidingen en procedures

<!-- Voeg hier verwijzingen naar gebruikers- en beheerdershandleidingen en
procedures toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Afspraken en verantwoordelijkheden

<!-- Voeg hier een overzicht van verantwoordelijken en afspraken omtrent de
exhibit toe -->

### Contact

<!-- Interne contactpersonen -->

### SLA

<!-- SLAs (met link naar Topdesk) -->

### Bouwer

<!-- Naam bouwer interactives (met link naar Topdesk)-->

### Ontwerper

<!-- Naam ontwerper interactives (met link naar Topdesk)-->

## Bijlagen

{{< pagelist bijlagen >}}
