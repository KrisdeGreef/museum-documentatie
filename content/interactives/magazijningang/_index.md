---
title: "magazijningang"
date: 2019-07-24T16:03:29Z
draft: true
status:
unid: a4d9a8f6-a246-49b4-8dfe-d6c202078b01
componenten:
tentoonstellingen:
- deverleiding
attachments:
- title: magazijningang_bouwtekening_a.pdf
  src: https://files.museum.naturalis.nl/s/P8nFCg3yG3zaQfa
- title: magazijningang_bouwtekening_b.pdf
  src: https://files.museum.naturalis.nl/s/iaK2mtgXfLMWmf6
- title: magazijningang_bouwtekening_c.pdf
  src: https://files.museum.naturalis.nl/s/kCqDTi82ScqDtDM
---

![magazijningang](./magazijningang.png)

## Functionele omschrijving

<!-- Voeg hier een functionele omschrijving van de interactive toe -->

<!-- Voeg indien beschikbaar een tijd-volgorde-toestands diagram toe -->

## Technische omschrijving

<!-- Voeg hier een blokschema toe -->

<!-- Voeg hier een technische omschrijving toe -->

Deze interactive bestaat uit de volgende componenten:

{{< pagelist componenten >}}

## Configuratie

<!-- Voeg hier een overzicht van handmatig ingestelde parameters toe -->

<!-- Voeg hier een verwijzing naar de configuratie in Ansible toe -->

## Handleidingen en procedures

<!-- Voeg hier verwijzingen naar gebruikers- en beheerdershandleidingen en
procedures toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Afspraken en verantwoordelijkheden

<!-- Voeg hier een overzicht van verantwoordelijken en afspraken omtrent de
interactive toe -->

### Contact

<!-- Interne contactpersonen -->

### SLA

<!-- SLAs (met link naar Topdesk) -->

### Bouwer

<!-- Naam bouwer interactives (met link naar Topdesk)-->

### Ontwerper

<!-- Naam ontwerper interactives (met link naar Topdesk)-->

## Bijlagen

{{< pagelist bijlagen >}}
