---
title: "stoelentijdcapsule"
date: 2019-07-24T16:03:35Z
draft: true
status:
unid: 4e32b854-1b32-4eb5-a048-182cf2381052
componenten:
- dbox-gen11actuator
tentoonstellingen:
experiences:
- rexperience
attachments:
- title: rexperience_brandveiligheid_stoelbekleding_dboxstoelen (pdf)
  src: https://files.museum.naturalis.nl/s/TNcsPWPAtPwwT2A
- title: rexperience_e_installatie_netwerk_dmx_audio_dbox_aansluitschema (pdf)
  url: https://files.museum.naturalis.nl/s/8MYsGoDo3WdEKRs
- title: rexperience_e_installatie_netwerk_dmx_audio_dbox_aansluitschema(1) (pdf)
  url: https://files.museum.naturalis.nl/s/436f6pkj7kJHn2k
- title: rexperience_technischeinstallaties_dboxsteoelen_sterktetoets (pdf)
  url: https://files.museum.naturalis.nl/s/X3bYa4LabwBMjak
- title: rexperience_technischeinstallaties_dboxstoelen_handleiding_actuator_replacement_rear_right (pdf)
  url: https://files.museum.naturalis.nl/s/JLsBxAziiKmynCW
- title: rexperience_technischeinstallaties_dboxstoelen_handleiding_actuator_replacement_rear_left (pdf)
  url: https://files.museum.naturalis.nl/s/8gpTi6Xr2JLstXB
- title: rexperience_technischeinstallaties_dboxstoelen_handleiding_kca_replacement (pdf)
  url: https://files.museum.naturalis.nl/s/ktqqpHRG8njdJYg
- title: rexperience_technischeinstallaties_dboxstoelen_handleidingrubberonderhoud (pdf)
  url: https://files.museum.naturalis.nl/s/tat6Hi3fkzbsDiX
- title: rexperience_technischeinstallaties_dboxstoelen_materiaal_certificaat_frame (pdf)
  url: https://files.museum.naturalis.nl/s/BAqBzCGRJ7AqBeb
- title: rexperience_technischeinstallaties_dboxstoelen_maximumforcesinduced (pdf)
  url: https://files.museum.naturalis.nl/s/JitWsw3HrXPSncB
- title: rexperience_technischeinstallaties_dboxstoelen_principemontage_bouwtekening (pdf)
  url: https://files.museum.naturalis.nl/s/KijiackpzJAeHyw
- title: rexperience_technischeinstallaties_dboxstoelen_seat_acceleration (pdf)
  url: https://files.museum.naturalis.nl/s/fMo97yA3LCgL62w
- title: rexperience_technischeinstallaties_dboxstoelen_staaldboxframes (pdf)
  url: https://files.museum.naturalis.nl/s/Xw4s2AxErpFTs9L
- title: rexperience_capsule_dboxstoelen_maten_bouwtekening (pdf)
  url: https://files.museum.naturalis.nl/s/czbsSzDGEZzska5
- title: rexperience_capsule_dboxstoelen_montageplaat_bouwtekening (pdf)
  url: https://files.museum.naturalis.nl/s/rbLzHAQ8cRWzAm4
- title: rexperience_capsule_dboxstoelen_schaal_bouwtekening (pdf)
  url: https://files.museum.naturalis.nl/s/ti9YGjjArsFS5Sg
- title: rexperience_e_installatie_netwerk_dmx_audio_focurightsettingdbox (pdf)
  url: https://files.museum.naturalis.nl/s/FrGoobAMPFYrkSL
---

![dbox](./dbox.png)

## Functionele omschrijving

<!-- Voeg hier een functionele omschrijving van de interactive toe -->

<!-- Voeg indien beschikbaar een tijd-volgorde-toestands diagram toe -->

## Technische omschrijving

<!-- Voeg hier een blokschema toe -->

<!-- Voeg hier een technische omschrijving toe -->

Deze interactive bestaat uit de volgende componenten:

{{< pagelist componenten >}}

## Configuratie

<!-- Voeg hier een overzicht van handmatig ingestelde parameters toe -->

<!-- Voeg hier een verwijzing naar de configuratie in Ansible toe -->

## Handleidingen en procedures

* [rexperience_technischeinstallaties_dboxstoelen_handleiding_actuator_replacement_rear_right](https://files.museum.naturalis.nl/s/JLsBxAziiKmynCW)
* [rexperience_technischeinstallaties_dboxstoelen_handleiding_actuator_replacement_rear_left](https://files.museum.naturalis.nl/s/8gpTi6Xr2JLstXB)
* [rexperience_technischeinstallaties_dboxstoelen_handleiding_kca_replacement](https://files.museum.naturalis.nl/s/ktqqpHRG8njdJYg)
* [rexperience_technischeinstallaties_dboxstoelen_handleidingrubberonderhoud](https://files.museum.naturalis.nl/s/tat6Hi3fkzbsDiX)

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Afspraken en verantwoordelijkheden

* [rexperience_brandveiligheid_stoelbekleding_dboxstoelen](https://files.museum.naturalis.nl/s/TNcsPWPAtPwwT2A)

### Contact

<!-- Interne contactpersonen -->

### SLA

<!-- SLAs (met link naar Topdesk) -->

### Bouwer

<!-- Naam bouwer interactives (met link naar Topdesk)-->

### Ontwerper

<!-- Naam ontwerper interactives (met link naar Topdesk)-->

## Bijlagen

{{< pagelist bijlagen >}}
