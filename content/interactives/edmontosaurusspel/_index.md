---
title: "edmontosaurusspel"
date: 2019-07-24T13:47:29Z
draft: true
status:
unid: d633a81e-55af-4987-aa8b-df0236412463
componenten:
tentoonstellingen:
- dinotijd
attachments:
- title: dino_interactives_edmontosaurusspel_draaischijf1_bouwtekening (pdf)
  src: https://files.museum.naturalis.nl/s/B5LFrWLqE6KY6rF
- title: dino_interactives_edmontosaurusspel_draaischijf2_bouwtekening (pdf)
  url: https://files.museum.naturalis.nl/s/jtgJXxz4rXjdajA
- title: dino_interactives_edmontosaurusspel_draaischijf_details (pdf)
  src: https://files.museum.naturalis.nl/s/FzrzBwF5cRJPLQT
---

![edmontosaurusspel](./edmontosaurusspel.png)

## Functionele omschrijving

<!-- Voeg hier een functionele omschrijving van de interactive toe -->

<!-- Voeg indien beschikbaar een tijd-volgorde-toestands diagram toe -->

## Technische omschrijving

<!-- Voeg hier een blokschema toe -->

<!-- Voeg hier een technische omschrijving toe -->

Deze interactive bestaat uit de volgende componenten:

{{< pagelist componenten >}}

## Configuratie

<!-- Voeg hier een overzicht van handmatig ingestelde parameters toe -->

<!-- Voeg hier een verwijzing naar de configuratie in Ansible toe -->

## Handleidingen en procedures

<!-- Voeg hier verwijzingen naar gebruikers- en beheerdershandleidingen en
procedures toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Afspraken en verantwoordelijkheden

<!-- Voeg hier een overzicht van verantwoordelijken en afspraken omtrent de
interactive toe -->

### Contact

<!-- Interne contactpersonen -->

### SLA

<!-- SLAs (met link naar Topdesk) -->

### Bouwer

<!-- Naam bouwer interactives (met link naar Topdesk)-->

### Ontwerper

<!-- Naam ontwerper interactives (met link naar Topdesk)-->

## Bijlagen

{{< pagelist bijlagen >}}
