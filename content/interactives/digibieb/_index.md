---
title: "digibieb"
draft: false
status:
unid: 44d8ba2b-65c4-48d9-9d63-7cd0bcbd6733
componenten:
- iiyama-tf2234mcb5x
- intel-nuc8i7beh
- nedis-cctvcf10bk5
tentoonstellingen:
- livescience
attachments:
- title: Ontwerp
  url: https://files.museum.naturalis.nl/s/sDRdkGZMw4WLzFP
- title: Kabelschema (pdf)
  url: https://files.museum.naturalis.nl/s/AkJfq4CEHLxdM6x
- title: Kabelschema (vwx)
  url: https://files.museum.naturalis.nl/s/msX7Ca58e3sSxa9
- title: Content lijst (excel)
  url: https://files.museum.naturalis.nl/s/AjkG39dCkEjpDcE
- title: definitiefontwerp (pdf)
  url: https://files.museum.naturalis.nl/s/JKsrjeLrRSAadpr
- title: werktekening (pdf)
  url: https://files.museum.naturalis.nl/s/9ejt9kH3ZD5gGNB

---

[![Digibieb](https://files.museum.naturalis.nl/s/sDRdkGZMw4WLzFP/preview)](https://files.museum.naturalis.nl/s/sDRdkGZMw4WLzFP)

## Functionele omschrijving

Digibieb bestaat uit twee luxe stoelen met daaraan een groot
interactief beeldscherm. Dit beeldscherm is te bedienen als een
groot touchscreen vergelijkbaar met een iPad. De gebruiker kan
hier op zijn gemak enkele bijzondere oude naslagwerken doorbladeren.

## Technische omschrijving

Technisch bestaat deze interactive uit een beeldscherm en een
Intel Nuc waarop een versie van linux draait. Deze machine is
zo ingericht dat daarop een Unity 3D applicatie draait waarmee
de naslagwerken worden getoond.

{{<mermaid>}}
graph TD;
    A[Bezoeker] --> |bedient| B
    B[Touchscreen] --> |interface| E[Unity applicatie]
    C[Intel Nuc] --> |toon beeld| B[TouchScreen]
    C[Intel Nuc] --> |draait| D[Linux]
    D[Linux] --> |draait| E[Unity applicatie]
{{</mermaid>}}

Deze interactive bestaat uit de volgende componenten:

{{< pagelist componenten >}}

De computers waarop deze interactive draait zijn:

 * digibieb-cmp-1
 * digibieb-cmp-2

De installaties zijn exact hetzelfde.

## Configuratie

De configuratie van de Intel Nuc wordt verzorgd met behulp van ansible
configuratiemanagement. De configuratie is terug te vinden in de
[ansible configuratie van het museum](https://gitlab.com/naturalis/darwinweg/ansible-darwinweg).

De applicatie werkt precies zoals andere interactive installaties en maakt
gebruik van de
[interactive playbook](https://gitlab.com/naturalis/mii/ansible-interactive).
De content komt van de content server en staat in
[content map van livescience](https://files.museum.naturalis.nl/s/DAi58MgdBXb27xc).


## Handleidingen en procedures

Voor het beheren en bedienen van dit soort interactives. Gebruik
de algemene handleiding
[Technisch museum beheer](https://docs.museum.naturalis.nl/latest/handleidingen/museum-beheer/).

## Known issues

Op dit moment zijn er geen known issues bekend.

Maar digibieb heeft in oktober 2019 wel technische problemen gehad:

 - temperatuur liep te hoog op in de computerkast
 - teveel geheugengebruik
 - vastlopende bediening

Deze lijken met de meest recente update van de software leverancier te zijn
opgelost. Mochten deze problemen opnieuw naar voren komen dan moet opnieuw
contact worden opgenomen met de software leverancier.

## Afspraken en verantwoordelijkheden

### Contact

 * [Technisch Team](mailto:support@naturalis.nl)
 * [Pieter Aartsen](mailto:pieter.aartsen@naturalis.nl)

### Bouwer

 * Software: [Yipp.nl](https://yipp.nl/)
 * Techniek: [Ata Tech](https://ata-tech.nl)
 * Decor: [Bruns](https://bruns.nl)

### Ontwerper

 * Inhoud: [Pieter Aartsen](mailto:pieter.aartsen@naturalis.nl)

## Bijlagen

{{< pagelist bijlagen >}}
