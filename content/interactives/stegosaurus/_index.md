---
title: "stegosaurus"
date: 2019-07-24T16:03:34Z
draft: true
status:
unid: 02b1357f-d2d1-4051-9234-9476d1683032
componenten:
- canon-xeedwux6600z
- dapaudio-pr82
- ecler-eca120
- intel-nuc8i3beh
- smartmetals-0022070
- kramer-tp590txr
- kramer-tp590rxr
tentoonstellingen:
- dinotijd
attachments:
- title: stegosaurus_bouwtekening.pdf
  src: https://files.museum.naturalis.nl/s/EfKSXKCbD4G2YdN
- title: stegosaurus_definitiefontwerp.pdf
  src: https://files.museum.naturalis.nl/s/7jptnHfAzgT4yMw
- title: stegosaurus_kabelschema.pdf
  src: https://files.museum.naturalis.nl/s/gGJaqHwCAtioWzN
- title: stegosaurus_kabelschema.vwx
  src: https://files.museum.naturalis.nl/s/4DyT5Q8iAyyNEDw
- title: stegosaurus_projectieopstelling.pdf
  src: https://files.museum.naturalis.nl/s/BP4wepS4jYYnncy
- title: stegosaurus_projectieopstelling.vwx
  src: https://files.museum.naturalis.nl/s/6b9R8HWigAF2Cf2
- title: stegosaurus_projectorophanging.pdf
  src: https://files.museum.naturalis.nl/s/SbMeTjHWtNFbjHD
- title: stegosaurus_projectorophanging.vwx
  src: https://files.museum.naturalis.nl/s/8jEzdtcPwmCAWAd
---

![stegosaurus](./stegosaurus.png)

## Functionele omschrijving

<!-- Voeg hier een functionele omschrijving van de interactive toe -->

<!-- Voeg indien beschikbaar een tijd-volgorde-toestands diagram toe -->

## Technische omschrijving

<!-- Voeg hier een blokschema toe -->

<!-- Voeg hier een technische omschrijving toe -->

Deze interactive bestaat uit de volgende componenten:

{{< pagelist componenten >}}

## Configuratie

<!-- Voeg hier een overzicht van handmatig ingestelde parameters toe -->

<!-- Voeg hier een verwijzing naar de configuratie in Ansible toe -->

## Handleidingen en procedures

<!-- Voeg hier verwijzingen naar gebruikers- en beheerdershandleidingen en
procedures toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Afspraken en verantwoordelijkheden

<!-- Voeg hier een overzicht van verantwoordelijken en afspraken omtrent de
interactive toe -->

### Contact

<!-- Interne contactpersonen -->

### SLA

<!-- SLAs (met link naar Topdesk) -->

### Bouwer

<!-- Naam bouwer interactives (met link naar Topdesk)-->

### Ontwerper

<!-- Naam ontwerper interactives (met link naar Topdesk)-->

## Bijlagen

{{< pagelist bijlagen >}}
