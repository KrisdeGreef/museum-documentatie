---
title: "waternesten"
date: 2019-07-24T16:03:37Z
draft: true
status:
unid: 2e16ca5d-2af3-458b-bf3d-2c3ba1a690a7
componenten:
- intel-nuc8i3beh
- iiyama-tf1015mcb1
- iiyama-tf2234mcb5x
- lepai-2020plus
- vistaton-frs8
tentoonstellingen:
- deverleiding
attachments:
- title: waternesten_bouwtekening.pdf
  src: https://files.museum.naturalis.nl/s/3PqQkgDEtfpMten
- title: waternesten_definitiefontwerp.pdf
  src: https://files.museum.naturalis.nl/s/xDekmkBfX8jfxMS
- title: waternesten_kabelschema.pdf
  src: https://files.museum.naturalis.nl/s/Q4cEFrLHNK7ynBL
- title: waternesten_kabelschema.vwx
  src: https://files.museum.naturalis.nl/s/jxRjEKiSED9x6ir
---

![waternesten](./waternesten.png)

## Functionele omschrijving

<!-- Voeg hier een functionele omschrijving van de interactive toe -->

<!-- Voeg indien beschikbaar een tijd-volgorde-toestands diagram toe -->

## Technische omschrijving

<!-- Voeg hier een blokschema toe -->

<!-- Voeg hier een technische omschrijving toe -->

Deze interactive bestaat uit de volgende componenten:

{{< pagelist componenten >}}

## Configuratie

<!-- Voeg hier een overzicht van handmatig ingestelde parameters toe -->

<!-- Voeg hier een verwijzing naar de configuratie in Ansible toe -->

## Handleidingen en procedures

<!-- Voeg hier verwijzingen naar gebruikers- en beheerdershandleidingen en
procedures toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Afspraken en verantwoordelijkheden

<!-- Voeg hier een overzicht van verantwoordelijken en afspraken omtrent de
interactive toe -->

### Contact

<!-- Interne contactpersonen -->

### SLA

<!-- SLAs (met link naar Topdesk) -->

### Bouwer

<!-- Naam bouwer interactives (met link naar Topdesk)-->

### Ontwerper

<!-- Naam ontwerper interactives (met link naar Topdesk)-->

## Bijlagen

{{< pagelist bijlagen >}}
