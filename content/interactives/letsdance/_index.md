---
title: "letsdance"
date: 2019-07-24T16:03:29Z
draft: true
status:
unid: 7741605d-fb92-45a1-afec-ce86dfb5c94f
componenten:
- christie-dhd400s
- ecler-eca120
- intel-nuc8i7beh
- qsc-adc4tbk
tentoonstellingen:
- deverleiding
attachments:
- title: letsdance_kabelschema.pdf
  src: https://files.museum.naturalis.nl/s/SHfYWzbzfH3jHkg
- title: letsdance_kabelschema.vwx
  src: https://files.museum.naturalis.nl/s/ik6XktS6aQkLK6f
- title: letsdance_projectieopstelling.pdf
  src: https://files.museum.naturalis.nl/s/oJMtMLAME9ZgCoW
- title: letsdance_projectieopstelling.vwx
  src: https://files.museum.naturalis.nl/s/JNaiip5tcrkoHPK
- title: letsdance_projectorophanging.pdf
  src: https://files.museum.naturalis.nl/s/D9Ldy8EGi6fbaE8
- title: letsdance_projectorophanging.vwx
  src: https://files.museum.naturalis.nl/s/gnd6CaeiDfRXxQL
- title: verleiding_interactive_letsdance_montagebeugelbeamer_bouwtekening (png)
  src: https://files.museum.naturalis.nl/s/4oogoRge4yKnQKZ
- title: verleiding_interactives_letsdance_informatie_webcam (docx)
  src: https://files.museum.naturalis.nl/s/kkfd3mmY84W2sD2
- title: deverleiding_letsdance_bouwtekening (pdf)
  src: https://files.museum.naturalis.nl/s/qNryrMqT3sHx8to
---

![letsdance](./letsdance.png)

## Functionele omschrijving

<!-- Voeg hier een functionele omschrijving van de interactive toe -->

<!-- Voeg indien beschikbaar een tijd-volgorde-toestands diagram toe -->

## Technische omschrijving

<!-- Voeg hier een blokschema toe -->

<!-- Voeg hier een technische omschrijving toe -->

Deze interactive bestaat uit de volgende componenten:

{{< pagelist componenten >}}

## Configuratie

<!-- Voeg hier een overzicht van handmatig ingestelde parameters toe -->

<!-- Voeg hier een verwijzing naar de configuratie in Ansible toe -->

## Handleidingen en procedures

<!-- Voeg hier verwijzingen naar gebruikers- en beheerdershandleidingen en
procedures toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Afspraken en verantwoordelijkheden

<!-- Voeg hier een overzicht van verantwoordelijken en afspraken omtrent de
interactive toe -->

### Contact

<!-- Interne contactpersonen -->

### SLA

<!-- SLAs (met link naar Topdesk) -->

### Bouwer

<!-- Naam bouwer interactives (met link naar Topdesk)-->

### Ontwerper

<!-- Naam ontwerper interactives (met link naar Topdesk)-->

## Bijlagen

{{< pagelist bijlagen >}}
