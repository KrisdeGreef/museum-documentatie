---
title: "flipperkast"
date: 2019-07-24T13:47:30Z
draft: true
status:
unid: bafc8951-f867-4da5-a1a6-1eafa63dc19e
componenten:
- arduino-leonardo
- intel-nuc8i7beh
- lepai-2020plus
- meanwell-gsm60b12p1j
- philips-bdl4330ql00
- vistaton-frs8
tentoonstellingen:
- deverleiding
attachments:
- title: flipperkast_kabelschema.pdf
  src: https://files.museum.naturalis.nl/s/Qte3EjPBj8p23Lk
- title: flipperkast_kabelschema.vwx
  src: https://files.museum.naturalis.nl/s/DiriTKQXqGB6YXQ
- title: flipperkast_bouwtekening.pdf
  src: https://files.museum.naturalis.nl/s/xz4jAXW4kpg5nEj
---

![flipperkast](./flipperkast.png)

## Functionele omschrijving

<!-- Voeg hier een functionele omschrijving van de interactive toe -->

<!-- Voeg indien beschikbaar een tijd-volgorde-toestands diagram toe -->

## Technische omschrijving

<!-- Voeg hier een blokschema toe -->

<!-- Voeg hier een technische omschrijving toe -->

Deze interactive bestaat uit de volgende componenten:

{{< pagelist componenten >}}

## Configuratie

<!-- Voeg hier een overzicht van handmatig ingestelde parameters toe -->

<!-- Voeg hier een verwijzing naar de configuratie in Ansible toe -->

## Handleidingen en procedures

<!-- Voeg hier verwijzingen naar gebruikers- en beheerdershandleidingen en
procedures toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Afspraken en verantwoordelijkheden

<!-- Voeg hier een overzicht van verantwoordelijken en afspraken omtrent de
interactive toe -->

### Contact

<!-- Interne contactpersonen -->

### SLA

<!-- SLAs (met link naar Topdesk) -->

### Bouwer

<!-- Naam bouwer interactives (met link naar Topdesk)-->

### Ontwerper

<!-- Naam ontwerper interactives (met link naar Topdesk)-->

## Bijlagen

{{< pagelist bijlagen >}}
