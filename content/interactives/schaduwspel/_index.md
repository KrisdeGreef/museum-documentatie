---
title: "schaduwspel"
date: 2019-07-24T16:03:33Z
draft: true
status:
unid: fa524b76-2d59-4809-9005-afb0d788c624
componenten:
tentoonstellingen:
- devroegemens
attachments:
- title: schaduwspel_asbuilt_bank.pdf
  src: https://files.museum.naturalis.nl/s/PYBeLD9bgqLpKAD
- title: schaduwspel_definitiefontwerp_banklamp.pdf
  url: https://files.museum.naturalis.nl/s/rNTR4aePDAAt66p
- title: schaduwspel_definitiefontwerp_model.pdf
  src: https://files.museum.naturalis.nl/s/F39fWsLQ8KXK8sS
- title: schaduwspel_schoonmaakvoorschrift_bank.pdf
  url: https://files.museum.naturalis.nl/s/GRorYF8JXs3be4F
- title: schaduwspel_schoonmaakvoorschrift_frame.pdf
  src: https://files.museum.naturalis.nl/s/q9LnmmeJtpX6Mf9
- title: schaduwspel_schoonmaakvoorschrift_peesdoek.pdf
  url: https://files.museum.naturalis.nl/s/tqYQM4mkbYM3p3j
---

![Schaduwspel](https://files.museum.naturalis.nl/s/i2f7obSbPqFpqQC/preview)

## Functionele omschrijving

<!-- Voeg hier een functionele omschrijving van de interactive toe -->

Een realistisch beeld van Homo erectus. Centraal geplaatst. Licht. Daaromheen spelen met schaduwen, je bent zelf deel van de stamboom.

<!-- Voeg indien beschikbaar een tijd-volgorde-toestands diagram toe -->

## Technische omschrijving

<!-- Voeg hier een blokschema toe -->

<!-- Voeg hier een technische omschrijving toe -->

Deze interactive bestaat uit de volgende componenten:

{{< pagelist componenten >}}

## Configuratie

<!-- Voeg hier een overzicht van handmatig ingestelde parameters toe -->

<!-- Voeg hier een verwijzing naar de configuratie in Ansible toe -->

## Handleidingen en procedures

<!-- Voeg hier verwijzingen naar gebruikers- en beheerdershandleidingen en
procedures toe -->

* [schoonmaakvoorschrift bank](https://files.museum.naturalis.nl/s/GRorYF8JXs3be4F)
* [schoonmaakvoorschrift frame](https://files.museum.naturalis.nl/s/q9LnmmeJtpX6Mf9)
* [schoonmaakvoorschrift peesdoek](https://files.museum.naturalis.nl/s/tqYQM4mkbYM3p3j)

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Afspraken en verantwoordelijkheden

<!-- Voeg hier een overzicht van verantwoordelijken en afspraken omtrent de
interactive toe -->

### Contact

<!-- Interne contactpersonen -->

* [Matthijs Vegter](mailto:matthijs.vegter@naturalis.nl)

### SLA

<!-- SLAs (met link naar Topdesk) -->

### Bouwer

<!-- Naam bouwer interactives (met link naar Topdesk)-->

* Zaal: [Hypsos](https://hypsos.com/soesterberg/)
* Beeld Lucy: [Kennis & Kennis Reconstructions](https://www.kenniskennis.com/site/Contact/)

### Ontwerper

* [Designwolf](http://designwolf.nl/contact/)

## Bijlagen

{{< pagelist bijlagen >}}
