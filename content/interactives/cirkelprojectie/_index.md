---
title: "cirkelprojectie"
date: 2019-07-24T13:47:28Z
draft: true
status:
unid: 19d5c54b-8efa-4288-bcfa-dd00fb5bdab4
componenten:
- dataton-watchpax20
- crestron-audext100
- canon-lens
- canon-xeedwux5800z
- dapaudio-pra82
- esi-gigaporthdplus
- qsc-kw181
- kramer-tp590txr
- kramer-tp590rxr
tentoonstellingen:
- dedood
attachments:
- title: cirkelprojectie_bouwtekening_bank.pdf
  src: https://files.museum.naturalis.nl/s/MGZT3MP4c86o9b9
- title: cirkelprojectie_kabelschema.pdf
  src: https://files.museum.naturalis.nl/s/2GWZyde9NLNEKTz
- title: cirkelprojectie_kabelschema.vwx
  src: https://files.museum.naturalis.nl/s/DracbJ9BiEbygGP
- title: cirkelprojectie_luidsprekerplan.pdf
  src: https://files.museum.naturalis.nl/s/NWbMXE4iS7LTp5P
- title: cirkelprojectie_luidsprekerplan.vwx
  src: https://files.museum.naturalis.nl/s/7YP27n7jRpd7j6A
- title: cirkelprojectie_projectieopstelling.pdf
  src: https://files.museum.naturalis.nl/s/F2KGtBQk2oC9opg
- title: cirkelprojectie_projectieopstelling.vwx
  src: https://files.museum.naturalis.nl/s/DrWRz3aeEgcPrYo
- title: cirkelprojectie_projectorophanging.vwx
  src: https://files.museum.naturalis.nl/s/5rq7NapG7KwCQoy
- title: cirkelprojectie_projectorophanging_a.pdf
  src: https://files.museum.naturalis.nl/s/mFQqoF7oBBcdWMf
- title: cirkelprojectie_projectorophanging_b.pdf
  src: https://files.museum.naturalis.nl/s/GS9ZHaYRca43XDG
---

![circelprojectie](./testbeeld.jpg)

## Functionele omschrijving

<!-- Voeg hier een functionele omschrijving van de interactive toe -->

<!-- Voeg indien beschikbaar een tijd-volgorde-toestands diagram toe -->

## Technische omschrijving

<!-- Voeg hier een blokschema toe -->

<!-- Voeg hier een technische omschrijving toe -->

Deze interactive bestaat uit de volgende componenten:

{{< pagelist componenten >}}

## Configuratie

<!-- Voeg hier een overzicht van handmatig ingestelde parameters toe -->

<!-- Voeg hier een verwijzing naar de configuratie in Ansible toe -->

## Handleidingen en procedures

Lees hier [de handleiding voor het updaten van content]({{< relref
"handleidingen/updaten-watchpax-players" >}}) op de Watchpax players.

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Afspraken en verantwoordelijkheden

<!-- Voeg hier een overzicht van verantwoordelijken en afspraken omtrent de
interactive toe -->

### Contact

<!-- Interne contactpersonen -->

### SLA

<!-- SLAs (met link naar Topdesk) -->

### Bouwer

<!-- Naam bouwer interactives (met link naar Topdesk)-->

### Ontwerper

<!-- Naam ontwerper interactives (met link naar Topdesk)-->

## Bijlagen

{{< pagelist bijlagen >}}
