---
title: "sexystories"
date: 2019-07-24T16:03:34Z
draft: true
status:
unid: c20aec65-4af8-4cfc-9632-5b09f8f043eb
componenten:
- intel-nuc8i3beh
- lepai-2020plus
- meanwell-gsm60b12p1j
- philips-bdl3230ql00
- vistaton-frs8
tentoonstellingen:
- deverleiding
attachments:
- title: sexystories_bouwtekening.pdf
  src: https://files.museum.naturalis.nl/s/zrxC7SeCr4Q8CBQ
- title: sexystories_kabelschema.pdf
  src: https://files.museum.naturalis.nl/s/4SyxMzyEsqmCTc7
- title: sexystories_kabelschema.vwx
  src: https://files.museum.naturalis.nl/s/PwW9efy5pHm3nRr
---

![sexystories](./sexystories.png)

## Functionele omschrijving

<!-- Voeg hier een functionele omschrijving van de interactive toe -->

<!-- Voeg indien beschikbaar een tijd-volgorde-toestands diagram toe -->

## Technische omschrijving

<!-- Voeg hier een blokschema toe -->

<!-- Voeg hier een technische omschrijving toe -->

Deze interactive bestaat uit de volgende componenten:

{{< pagelist componenten >}}

## Configuratie

<!-- Voeg hier een overzicht van handmatig ingestelde parameters toe -->

<!-- Voeg hier een verwijzing naar de configuratie in Ansible toe -->

## Handleidingen en procedures

<!-- Voeg hier verwijzingen naar gebruikers- en beheerdershandleidingen en
procedures toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Afspraken en verantwoordelijkheden

<!-- Voeg hier een overzicht van verantwoordelijken en afspraken omtrent de
interactive toe -->

### Contact

<!-- Interne contactpersonen -->

### SLA

<!-- SLAs (met link naar Topdesk) -->

### Bouwer

<!-- Naam bouwer interactives (met link naar Topdesk)-->

### Ontwerper

<!-- Naam ontwerper interactives (met link naar Topdesk)-->

## Bijlagen

{{< pagelist bijlagen >}}
