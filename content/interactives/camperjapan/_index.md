---
title: "camperjapan"
date: 2019-07-24T13:47:27Z
draft: true
status:
unid: ca5d8ba9-fd78-4de0-9f4c-4f94f577cc2e
componenten:
- dell-latitude3590
tentoonstellingen:
- deaarde
attachments:
- title: camperjapan_kabelschema.pdf
  src: https://files.museum.naturalis.nl/s/eKDBAmRPgJG3PkE
- title: camperjapan_kabelschema.vwx
  src: https://files.museum.naturalis.nl/s/iHXJxBeYTW5DQWM
---

<!-- Voeg een overzichtsafbeelding of blokschema toe -->

[![moodboard](https://files.museum.naturalis.nl/s/rmkGFJn853EjZC6/preview)](https://files.museum.naturalis.nl/s/rmkGFJn853EjZC6)

## Functionele omschrijving

<!-- Voeg hier een functionele omschrijving van de interactive toe -->

<!-- Voeg indien beschikbaar een tijd-volgorde-toestands diagram toe -->

## Technische omschrijving

<!-- Voeg hier een blokschema toe -->

<!-- Voeg hier een technische omschrijving toe -->

Deze interactive bestaat uit de volgende componenten:

{{< pagelist componenten >}}

## Configuratie

<!-- Voeg hier een overzicht van handmatig ingestelde parameters toe -->

<!-- Voeg hier een verwijzing naar de configuratie in Ansible toe -->

## Handleidingen en procedures

<!-- Voeg hier verwijzingen naar gebruikers- en beheerdershandleidingen en
procedures toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Afspraken en verantwoordelijkheden

<!-- Voeg hier een overzicht van verantwoordelijken en afspraken omtrent de
interactive toe -->

### Contact

<!-- Interne contactpersonen -->

### SLA

<!-- SLAs (met link naar Topdesk) -->

### Bouwer

<!-- Naam bouwer interactives (met link naar Topdesk)-->

### Ontwerper

<!-- Naam ontwerper interactives (met link naar Topdesk)-->

## Bijlagen

{{< pagelist bijlagen >}}
