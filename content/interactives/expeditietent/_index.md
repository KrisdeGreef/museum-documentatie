---
title: "expeditietent"
draft: false
status:
unid: 595c6786-7b78-44d3-9fa6-a430655bdc4f
componenten:
- iiyama-tf2234mcb5x
- iiyama-xu2595wsub1
- intel-nuc8i3beh
- spv-110202
tentoonstellingen:
- livescience
attachments:
- title: Kabelschema (pdf)
  src: https://files.museum.naturalis.nl/s/QWQARA7R3MCx3Pn
- title: Kabelschema (vwx)
  url: https://files.museum.naturalis.nl/s/76gEdPpa48rJirZ
- title: Ontwerp (png)
  url: https://files.museum.naturalis.nl/s/M48TS5XZeTWEeFz
---

[![Expeditietent](expeditie3d.png)](https://files.museum.naturalis.nl/s/QWQARA7R3MCx3Pn)

![Expeditietent](https://files.museum.naturalis.nl/s/M48TS5XZeTWEeFz/preview)

## Functionele omschrijving

Expeditietent is een onderdeel in livescience waar bezoekers een
indruk kunnen krijgen wat er komt kijken bij wetenschappelijk onderzoek
in het veld. Zowel de expedities uit het verleden worden hier getoond
als hoe het er tegenwoordig aan toe gaat.

De bezoeker kan gaan zitten op een van de expeditie dozen en de tijd
nemen om de films te bekijken.

## Technische omschrijving

Technisch bestaat deze interactive uit een drietal Intel Nucs die de hele
dag ieder via een eigen beeldscherm een korte film tonen. De bezoeker
kan deze applicaties niet bedienen en de technische setup is daardoor
relatief eenvoudig en vergelijkbaar met film vertoningen in andere
zalen. Hierbij wordt gebruik gemaakt van MPV.

{{<mermaid>}}
graph TD;
    A[Bezoeker] --> |bekijkt| B
    B[Scherm] --> |toon film| E[MPV mediaspeler]
    C[Intel Nuc] --> |toon beeld| B[Scherm]
    C[Intel Nuc] --> |draait| D[Linux]
    D[Linux] --> |draait| E[MPV mediaspeler]
{{</mermaid>}}

Deze interactive bestaat uit de volgende componenten:

{{< pagelist componenten >}}

## Configuratie

De configuratie van de Intel Nuc wordt verzorgd met behulp van ansible
configuratiemanagement. De configuratie is terug te vinden in de
[inventory van het museum](https://gitlab.com/naturalis/darwinweg/ansible-darwinweg).

De applicatie werkt precies zoals andere interactive installaties en maakt
gebruik van de 
[interactive playbook](https://gitlab.com/naturalis/mii/ansible-interactive).
De content komt van de content server en staat in 
[content map van livescience](https://files.museum.naturalis.nl/s/CGJcyGXTKzHHe7E).

## Tentoonstelling

Deze interactive hoort bij tentoonstelling:

{{< pagelist tentoonstellingen >}}


## Handleidingen en procedures

Voor het beheren en bedienen van dit soort interactives. Gebruik 
de algemene handleiding 
[Technisch museum beheer](https://docs.museum.naturalis.nl/latest/handleidingen/museum-beheer/).

## Known issues

Op dit moment zijn er geen known issues bekend.

## Afspraken en verantwoordelijkheden

### Contact

* [Pieter Aartsen](mailto:pieter.aartsen@naturalis.nl)
* [Technisch Team](mailto:support@naturalis.nl)

### Bouwer

 * Decor: [Bruns](https://www.bruns.nl)
 * Technisch: [Ata Tech](https://ata-tech.nl)

### Ontwerper

 * Inhoud: [Pieter Aartsen](mailto:pieter.aartsen@naturalis.nl)

## Bijlagen

{{< pagelist bijlagen >}}
