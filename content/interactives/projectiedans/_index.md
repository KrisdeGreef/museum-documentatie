---
title: "projectiedans"
date: 2019-07-24T16:03:31Z
draft: true
status:
unid: 028de6cb-1618-4b9d-98c1-4870e028aba6
componenten:
- christie-dhd400s
- intel-nuc8i3beh
tentoonstellingen:
- deverleiding
attachments:
- title: projectiedans_kabelschema.pdf
  src: https://files.museum.naturalis.nl/s/7wqw2A4FWQtab7C
- title: projectiedans_kabelschema.vwx
  src: https://files.museum.naturalis.nl/s/HJEF7ZJWLTdJ4qo
- title: projectiedans_projectorophanging.pdf
  src: https://files.museum.naturalis.nl/s/kqQMcMgMtTH5bHX
- title: projectiedans_projectorophanging.vwx
  src: https://files.museum.naturalis.nl/s/SkyHR3Y8Y2k3WsS
- title: deverleiding_projectiedans_bouwtekening (pdf)
  src: https://files.museum.naturalis.nl/s/pzGM3xSD4xdbCSp
---

![projectiedans](./projectiedans.png)

## Functionele omschrijving

<!-- Voeg hier een functionele omschrijving van de interactive toe -->

<!-- Voeg indien beschikbaar een tijd-volgorde-toestands diagram toe -->

## Technische omschrijving

<!-- Voeg hier een blokschema toe -->

<!-- Voeg hier een technische omschrijving toe -->

Deze interactive bestaat uit de volgende componenten:

{{< pagelist componenten >}}

## Configuratie

<!-- Voeg hier een overzicht van handmatig ingestelde parameters toe -->

<!-- Voeg hier een verwijzing naar de configuratie in Ansible toe -->

## Handleidingen en procedures

<!-- Voeg hier verwijzingen naar gebruikers- en beheerdershandleidingen en
procedures toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Afspraken en verantwoordelijkheden

<!-- Voeg hier een overzicht van verantwoordelijken en afspraken omtrent de
interactive toe -->

### Contact

<!-- Interne contactpersonen -->

### SLA

<!-- SLAs (met link naar Topdesk) -->

### Bouwer

<!-- Naam bouwer interactives (met link naar Topdesk)-->

### Ontwerper

<!-- Naam ontwerper interactives (met link naar Topdesk)-->

## Bijlagen

{{< pagelist bijlagen >}}
