---
title: "studiolab"
draft: false
status:
unid: 2fba4a32-bb1b-4ca9-8002-9bbe4aaeeb67
componenten:
- yamaha-01v96
- qsc-cx254
- sennheiser-ew300g3
- focusrite-6i6
- neutrik-nahdmiw
- neutrik-nbb75dfi
- pharos-lpc1
- ldsystems-sat62
- kramer-tp573
- kramer-tp574
- pharos-tpsbb
- aten-ue350a
- kramer-vm2h2
- kramer-vs211ha
- lg-98uh5eb
- iiyama-tf2234mcb5x
- skaarhoj-ptzpro
- canon-crn300
- birddog-flex4kin
- birddog-flex4kout
- magewell-proconvertnditohdmi4k
- magewell-proconverthdmitondi4k
- dell-precision3650tower
- intel-nuc8i3beh

tentoonstellingen:
- livescience
attachments:
- title: StudioLab Pharos config files
  url: https://files.museum.naturalis.nl/s/8gXBnk2rrEFKJWq
- title: Studiolab Kabel schema (pdf)
  url: https://files.museum.naturalis.nl/s/fFCK5HxbXskTEyi
- title: Studiolab Kabel schema (vwx)
  url: https://files.museum.naturalis.nl/s/fxYw5q86b6nqo3Y
- title: Studiolab Camera Control schema (pdf)
  url: https://files.museum.naturalis.nl/s/6qMwAp3bfTxnB5E
- title: Studiolab Camera Control schema (vwx)
  url: https://files.museum.naturalis.nl/s/4qgCJF7SfBd6qz7
- title: Studiolab idsheet
  url: https://files.museum.naturalis.nl/s/Qe6ATfs5reHSoWs
- title: livescience_interactives_ studiolab_tribune_bouwtekening (pdf)
  url: https://files.museum.naturalis.nl/s/HBWJZHtLX7P6wBz
- title: livescience_interactives_studiolab_regieunit_bouwtekening (pdf)
  url: https://files.museum.naturalis.nl/s/4JBQCSFFjLAqcbR
- title: livescience_interactives_studiolab_programmabord_bouwtekening (pdf)
  url: https://files.museum.naturalis.nl/s/cD3FtEgaxfxpoEg
---



## Functionele omschrijving

Veelzijdige live presentaties in een theater-achtige setting door
Naturalis-onderzoekers en andere medewerkers (collectie, bibliotheek, educatie,
enz.) die vertellen over hun werk en laten zien wat zedoen. Mogelijkheden tot
vragen stellen en echte dingen te zien

Naturalis is niet alleen een museum met geweldige tentoonstellingen en
spannende evenementen. Het museum is vooral een gerenommeerd wetenschappelijk
instituut op het gebied van biodiversiteitsonderzoek, met een collectie waaraan
dagelijks door allerlei interessante mensen wordt gewerkt.

## Technische omschrijving

 * Programmabord met aankondigingen over activiteiten van de dag.
 * Tribune met 50 comfortabele zitplaatsen
 * uit te breiden met 50 zitplaatsen, 150 staanplaatsen.
 * Een hele tribune met comfortabele zitplaatsen
 * Groot scherm voor presentatie (slideshow, film, livestream verbinding e.d.).
 * Presentatieplek met geluid en aansluitingsmogelijkheden voor een laptop.
 * Faciliteiten om live prepareersessies te doen of andere demonstraties

Deze interactive bestaat uit de volgende componenten:

{{< pagelist componenten >}}

## StudioLab 2.0

![studiolab 2.0 blokschema](https://files.museum.naturalis.nl/s/BHSaaHtKjXcmBfJ/preview)

## Afspraken en verantwoordelijkheden

<!-- Voeg hier een overzicht van verantwoordelijken en afspraken omtrent de
interactive toe -->

### Contact

* [Daphne Suijker](mailto:daphne.suijker@naturalis.nl)
* [Pieter Aartsen](mailto:pieter.aartsen@naturalis.nl)
* [Manon Laterveer - de Beer](mailto:Manon.Laterveer-debeer@naturalis.nl)

### SLA

<!-- SLAs (met link naar Topdesk) -->

### Bouwer

* [Bruns](https://bruns.nl)
* [Ata Tech](https://ata-tech.nl)


### Ontwerper

* [Pieter Aartsen](mailto:pieter.aartsen@naturalis.nl)


## Bijlagen

{{< pagelist bijlagen >}}
