---
title: "greenporn"
date: 2019-07-24T13:47:32Z
draft: true
status:
unid: bd37d75c-2f93-4e59-956e-02ee7780ab67
componenten:
- iiyama-tf3238mscb1ag
- intel-nuc8i3beh
- lepai-2020plus
- meanwell-gsm60b12p1j
- vesa-4270
- vistaton-frs8
tentoonstellingen:
- deverleiding
attachments:
- title: greenporn_bouwtekening_vitrinestolp.pdf
  src: https://files.museum.naturalis.nl/s/XLroz8fn4zi4mrb
- title: greenporn_kabelschema.pdf
  src: https://files.museum.naturalis.nl/s/zcXxmpGsneZRFA4
- title: greenporn_kabelschema.vwx
  src: https://files.museum.naturalis.nl/s/qJNeCBjccpxSQi7
- title: greenporn_definitiefontwerp.pdf
  src: https://files.museum.naturalis.nl/s/QqjwmRgMakA48BW
---

![greenporn](./greenporn.png)

## Functionele omschrijving

<!-- Voeg hier een functionele omschrijving van de interactive toe -->

<!-- Voeg indien beschikbaar een tijd-volgorde-toestands diagram toe -->

## Technische omschrijving

<!-- Voeg hier een blokschema toe -->

<!-- Voeg hier een technische omschrijving toe -->

Deze interactive bestaat uit de volgende componenten:

{{< pagelist componenten >}}

## Configuratie

<!-- Voeg hier een overzicht van handmatig ingestelde parameters toe -->

<!-- Voeg hier een verwijzing naar de configuratie in Ansible toe -->

## Handleidingen en procedures

<!-- Voeg hier verwijzingen naar gebruikers- en beheerdershandleidingen en
procedures toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Afspraken en verantwoordelijkheden

<!-- Voeg hier een overzicht van verantwoordelijken en afspraken omtrent de
interactive toe -->

### Contact

<!-- Interne contactpersonen -->

### SLA

<!-- SLAs (met link naar Topdesk) -->

### Bouwer

<!-- Naam bouwer interactives (met link naar Topdesk)-->

### Ontwerper

<!-- Naam ontwerper interactives (met link naar Topdesk)-->

## Bijlagen

{{< pagelist bijlagen >}}
