---
title: "projectiediepzee"
date: 2019-07-24T16:03:32Z
draft: true
status:
unid: 73a30083-382a-43ff-83d2-7798b9e0ef3f
componenten:
- canon-lens
- canon-xeedwux5800z
- intel-nuc8i3beh
- kramer-tp590txr
- kramer-tp590rxr
- smartmetals-0022440
tentoonstellingen:
- leven
attachments:
- title: projectiediepzee_kabelschema.pdf
  src: https://files.museum.naturalis.nl/s/E84EFY74M4i6Wxx
- title: projectiediepzee_kabelschema.vwx
  src: https://files.museum.naturalis.nl/s/aYZ9AZf8YPXeSfs
- title: projectiediepzee_projectieopstelling.pdf
  src: https://files.museum.naturalis.nl/s/oPzwCnZjjszLEnj
- title: projectiediepzee_projectieopstelling.vwx
  src: https://files.museum.naturalis.nl/s/qp3ZRcWDafsCNSs
- title: projectiediepzee_projectorophanging.pdf
  src: https://files.museum.naturalis.nl/s/Y8FKpsQkDmDGjJf
- title: projectiediepzee_projectorophanging.vwx
  src: https://files.museum.naturalis.nl/s/FwLgXRJ5Ybiz9xA
---

![diepzee](./diepzee.png)

## Functionele omschrijving

<!-- Voeg hier een functionele omschrijving van de interactive toe -->

<!-- Voeg indien beschikbaar een tijd-volgorde-toestands diagram toe -->

## Technische omschrijving

<!-- Voeg hier een blokschema toe -->

<!-- Voeg hier een technische omschrijving toe -->

Deze interactive bestaat uit de volgende componenten:

{{< pagelist componenten >}}

## Configuratie

<!-- Voeg hier een overzicht van handmatig ingestelde parameters toe -->

<!-- Voeg hier een verwijzing naar de configuratie in Ansible toe -->

## Handleidingen en procedures

<!-- Voeg hier verwijzingen naar gebruikers- en beheerdershandleidingen en
procedures toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Afspraken en verantwoordelijkheden

<!-- Voeg hier een overzicht van verantwoordelijken en afspraken omtrent de
interactive toe -->

### Contact

<!-- Interne contactpersonen -->

### SLA

<!-- SLAs (met link naar Topdesk) -->

### Bouwer

<!-- Naam bouwer interactives (met link naar Topdesk)-->

### Ontwerper

<!-- Naam ontwerper interactives (met link naar Topdesk)-->

## Bijlagen

{{< pagelist bijlagen >}}
