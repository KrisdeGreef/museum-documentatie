---
title: "timelapse"
date: 2019-07-24T16:03:35Z
draft: true
status:
unid: c0cb3635-9017-4263-9ec2-f7107d961fa2
componenten:
- arduino-leonardo
- intel-nuc8i7beh
- omnimount-100wc
- optoma-ml750st
tentoonstellingen:
- dedood
attachments:
- title: timelapse_bouwtekening_projectie.pdf
  src: https://files.museum.naturalis.nl/s/ZKGRKnddwZpCQnA
- title: timelapse_bouwtekening_verticaledoorsnede.pdf
  src: https://files.museum.naturalis.nl/s/ztRWgXmXijAKAp9
- title: timelapse_elektroschema.pdf
  src: https://files.museum.naturalis.nl/s/x27xpgELCZka9qr
- title: timelapse_kabelschema.pdf
  src: https://files.museum.naturalis.nl/s/5PK6wtE9AWeRDmd
- title: timelapse_kabelschema.vwx
  src: https://files.museum.naturalis.nl/s/5aAtA5RkbLapXk6
- title: timelapse_projectieopstelling.pdf
  src: https://files.museum.naturalis.nl/s/XdsY2sfGbK6QiKp
- title: timelapse_projectieopstelling.vwx
  src: https://files.museum.naturalis.nl/s/Nzk5iR3bARkQqwq
- title: timelapse_projectorophanging.pdf
  src: https://files.museum.naturalis.nl/s/rfZtwrRoLgdGBqR
---

![timelaps](https://files.museum.naturalis.nl/s/YBefsqjR9e5BYyr/preview)

## Functionele omschrijving

<!-- Voeg hier een functionele omschrijving van de interactive toe -->

<!-- Voeg indien beschikbaar een tijd-volgorde-toestands diagram toe -->

## Technische omschrijving

<!-- Voeg hier een blokschema toe -->

<!-- Voeg hier een technische omschrijving toe -->

Deze interactive bestaat uit de volgende componenten:

{{< pagelist componenten >}}

## Configuratie

<!-- Voeg hier een overzicht van handmatig ingestelde parameters toe -->

<!-- Voeg hier een verwijzing naar de configuratie in Ansible toe -->

## Handleidingen en procedures

<!-- Voeg hier verwijzingen naar gebruikers- en beheerdershandleidingen en
procedures toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Afspraken en verantwoordelijkheden

<!-- Voeg hier een overzicht van verantwoordelijken en afspraken omtrent de
interactive toe -->

### Contact

<!-- Interne contactpersonen -->

### SLA

<!-- SLAs (met link naar Topdesk) -->

### Bouwer

<!-- Naam bouwer interactives (met link naar Topdesk)-->

### Ontwerper

<!-- Naam ontwerper interactives (met link naar Topdesk)-->

## Bijlagen

{{< pagelist bijlagen >}}
