---
title: "introjapanstheater"
date: 2019-07-24T13:47:33Z
draft: true
status:
unid: e38c8963-813d-4255-b08a-1783295045c1
componenten:
- ecler-eca120
- intel-nuc8i3beh
- philips-bdl3230ql00
- qsc-adc4tbk
- startech-cdp2hd
- vesa-4270
- wyrestorm-cabhaoc15
tentoonstellingen:
experiences:
- japanstheater
attachments:
- title: introjapanstheater_kabelschema.pdf
  src: https://files.museum.naturalis.nl/s/qMqLdA4iZ4aWFYD
- title: introjapanstheater_kabelschema.vwx
  src: https://files.museum.naturalis.nl/s/b4jKEn6qxGkdCS3
---

<!-- Voeg een overzichtsafbeelding of blokschema toe -->

## Functionele omschrijving

<!-- Voeg hier een functionele omschrijving van de interactive toe -->

<!-- Voeg indien beschikbaar een tijd-volgorde-toestands diagram toe -->

## Technische omschrijving

<!-- Voeg hier een blokschema toe -->

<!-- Voeg hier een technische omschrijving toe -->

Deze interactive bestaat uit de volgende componenten:

{{< pagelist componenten >}}

## Configuratie

<!-- Voeg hier een overzicht van handmatig ingestelde parameters toe -->

<!-- Voeg hier een verwijzing naar de configuratie in Ansible toe -->

## Handleidingen en procedures

<!-- Voeg hier verwijzingen naar gebruikers- en beheerdershandleidingen en
procedures toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Afspraken en verantwoordelijkheden

<!-- Voeg hier een overzicht van verantwoordelijken en afspraken omtrent de
interactive toe -->

### Contact

<!-- Interne contactpersonen -->

### SLA

<!-- SLAs (met link naar Topdesk) -->

### Bouwer

<!-- Naam bouwer interactives (met link naar Topdesk)-->

### Ontwerper

<!-- Naam ontwerper interactives (met link naar Topdesk)-->

## Bijlagen

{{< pagelist bijlagen >}}
