---
title: "aardbevingplafond"
date: 2019-07-24T13:47:25Z
draft: true
status:
unid: 85bd523a-4cad-4849-8181-28e0d58e4236
componenten:
- wahlberg-winch25
- wahlberg-winch50
tentoonstellingen:
experiences:
- japanstheater
attachments:
- title: japanstheater_plafond_detail1 (png)
  src: https://files.museum.naturalis.nl/s/LNKwWrKCerS8jyd
- title: japanstheater_plafond_detail2 (png)
  url: https://files.museum.naturalis.nl/s/qqonW3i36ALdGWW
---

<!-- Voeg een overzichtsafbeelding of blokschema toe -->

## Functionele omschrijving

<!-- Voeg hier een functionele omschrijving van de interactive toe -->

<!-- Voeg indien beschikbaar een tijd-volgorde-toestands diagram toe -->

## Technische omschrijving

<!-- Voeg hier een blokschema toe -->

<!-- Voeg hier een technische omschrijving toe -->

Deze interactive bestaat uit de volgende componenten:

{{< pagelist componenten >}}

## Configuratie

<!-- Voeg hier een overzicht van handmatig ingestelde parameters toe -->

<!-- Voeg hier een verwijzing naar de configuratie in Ansible toe -->

## Handleidingen en procedures

<!-- Voeg hier verwijzingen naar gebruikers- en beheerdershandleidingen en
procedures toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Afspraken en verantwoordelijkheden

<!-- Voeg hier een overzicht van verantwoordelijken en afspraken omtrent de
interactive toe -->

### Contact

<!-- Interne contactpersonen -->

### SLA

<!-- SLAs (met link naar Topdesk) -->

### Bouwer

<!-- Naam bouwer interactives (met link naar Topdesk)-->

### Ontwerper

<!-- Naam ontwerper interactives (met link naar Topdesk)-->

## Bijlagen

{{< pagelist bijlagen >}}
