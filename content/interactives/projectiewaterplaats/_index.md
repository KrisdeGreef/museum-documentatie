---
title: "projectiewaterplaats"
date: 2019-07-24T16:03:33Z
draft: true
status:
unid: 894c2501-d8b5-42cb-9a97-8c0ed7ac26f6
componenten:
- canon-lens
- canon-xeedwux5800z
- kramer-tp590txr
- kramer-tp590rxr
- smartmetals-0021561
- dataton-watchout
- dataton-watchpax20
tentoonstellingen:
- leven
attachments:
- title: projectiewaterplaats_kabelschema.vwx
  src: https://files.museum.naturalis.nl/s/eg8rEeM5Q8jptWn
- title: projectiewaterplaats_projectieopstelling.pdf
  src: https://files.museum.naturalis.nl/s/b7n2TKBqiqoQggn
- title: projectiewaterplaats_projectieopstelling.vwx
  src: https://files.museum.naturalis.nl/s/g8qCxAfMkgwccDx
- title: projectiewaterplaats_projectorophanging.vwx
  src: https://files.museum.naturalis.nl/s/EEc3L9AybmM3wHd
- tile: leven_totaal_waterplaats_bouwtekening (pdf)
  src: https://files.museum.naturalis.nl/s/EknGJadNYL4rbn8
- title: leven_interactives_projectiewaterplaats_schoonmakenprojectiedoek_instructie (pdf)
  src: https://files.museum.naturalis.nl/s/rQTZSafYJ5KxsZP
- title: leven_interactives_projectiewaterplaats_tekeningvervangenprojectiedoek_instructie (pdf)
  src: https://files.museum.naturalis.nl/s/QNjxxr4yotGqLta
- title: leven_interactives_projectiewaterplaats_wisselenprojectiedoek_instructie.docx (pdf)
  src: https://files.museum.naturalis.nl/s/baZpTEqsPyi7tpr
---

![waterplaats](./waterplaats.png)

## Functionele omschrijving

<!-- Voeg hier een functionele omschrijving van de interactive toe -->

<!-- Voeg indien beschikbaar een tijd-volgorde-toestands diagram toe -->

## Technische omschrijving

<!-- Voeg hier een blokschema toe -->

<!-- Voeg hier een technische omschrijving toe -->

Deze interactive bestaat uit de volgende componenten:

{{< pagelist componenten >}}

## Configuratie

<!-- Voeg hier een overzicht van handmatig ingestelde parameters toe -->

<!-- Voeg hier een verwijzing naar de configuratie in Ansible toe -->

## Handleidingen en procedures

* [projectiewaterplaats schoonmakenprojectiedoek instructie](https://files.museum.naturalis.nl/s/rQTZSafYJ5KxsZP)
* [projectiewaterplaats tekeningvervangenprojectiedoek instructie](https://files.museum.naturalis.nl/s/QNjxxr4yotGqLta)
* [projectiewaterplaats wisselenprojectiedoek instructie](https://files.museum.naturalis.nl/s/baZpTEqsPyi7tpr)


Lees hier [de handleiding voor het updaten van content]({{< relref
"handleidingen/updaten-watchpax-players" >}}) op de Watchpax players.

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Afspraken en verantwoordelijkheden

<!-- Voeg hier een overzicht van verantwoordelijken en afspraken omtrent de
interactive toe -->

### Contact

<!-- Interne contactpersonen -->

### SLA

<!-- SLAs (met link naar Topdesk) -->

### Bouwer

<!-- Naam bouwer interactives (met link naar Topdesk)-->

### Ontwerper

<!-- Naam ontwerper interactives (met link naar Topdesk)-->

## Bijlagen

{{< pagelist bijlagen >}}
