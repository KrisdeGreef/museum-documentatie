---
title: "cabinijsland"
date: 2019-07-24T13:47:27Z
draft: true
status:
unid: 570d8125-17e5-420e-bd51-0cc14d749940
componenten:
- ecler-eca120
- intel-nuc8i3beh
- iiyama-tf5538uhscb1ag
- qsc-acs4tbk
- vesa-4270
tentoonstellingen:
- deaarde
attachments:
- title: cabinijsland_bouwtekening.pdf
  src: https://files.museum.naturalis.nl/s/6MCD4eQKXj6rieF
- title: cabinijsland_kabelschema.pdf
  src: https://files.museum.naturalis.nl/s/ZE2Gd2PHCJKxS5c
- title: cabinijsland_kabelschema.vwx
  src: https://files.museum.naturalis.nl/s/i7DLp5bx77xdbEq
---

<!-- Voeg een overzichtsafbeelding of blokschema toe -->

[![plattegrond](https://files.museum.naturalis.nl/s/YECgtCHB4NbbXKy/preview)](https://files.museum.naturalis.nl/s/YECgtCHB4NbbXKy)


## Functionele omschrijving

<!-- Voeg hier een functionele omschrijving van de interactive toe -->

<!-- Voeg indien beschikbaar een tijd-volgorde-toestands diagram toe -->

## Technische omschrijving

<!-- Voeg hier een blokschema toe -->

<!-- Voeg hier een technische omschrijving toe -->

Deze interactive bestaat uit de volgende componenten:

{{< pagelist componenten >}}

## Configuratie

<!-- Voeg hier een overzicht van handmatig ingestelde parameters toe -->

<!-- Voeg hier een verwijzing naar de configuratie in Ansible toe -->

## Handleidingen en procedures

<!-- Voeg hier verwijzingen naar gebruikers- en beheerdershandleidingen en
procedures toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Afspraken en verantwoordelijkheden

<!-- Voeg hier een overzicht van verantwoordelijken en afspraken omtrent de
interactive toe -->

### Contact

<!-- Interne contactpersonen -->

### SLA

<!-- SLAs (met link naar Topdesk) -->

### Bouwer

<!-- Naam bouwer interactives (met link naar Topdesk)-->

### Ontwerper

<!-- Naam ontwerper interactives (met link naar Topdesk)-->

## Bijlagen

{{< pagelist bijlagen >}}
