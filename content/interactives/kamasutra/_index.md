---
title: "kamasutra"
date: 2019-07-24T13:47:33Z
draft: true
status:
unid: fad5ede9-19c9-4a7a-929f-600eec96e01e
componenten:
tentoonstellingen:
- deverleiding
attachments:
- title: kamasutra_bouwtekening.pdf
  src: https://files.museum.naturalis.nl/s/nwjbWJiYSY3K89J
- title: kamasutra_definitiefontwerp.pdf
  src: https://files.museum.naturalis.nl/s/rbc78kBPAGq9tY7
---

![kamasutra](./kamasutra.png)

## Functionele omschrijving

<!-- Voeg hier een functionele omschrijving van de interactive toe -->

<!-- Voeg indien beschikbaar een tijd-volgorde-toestands diagram toe -->

## Technische omschrijving

<!-- Voeg hier een blokschema toe -->

<!-- Voeg hier een technische omschrijving toe -->

Deze interactive bestaat uit de volgende componenten:

{{< pagelist componenten >}}

## Configuratie

<!-- Voeg hier een overzicht van handmatig ingestelde parameters toe -->

<!-- Voeg hier een verwijzing naar de configuratie in Ansible toe -->

## Handleidingen en procedures

<!-- Voeg hier verwijzingen naar gebruikers- en beheerdershandleidingen en
procedures toe -->

## Known issues

<!-- Voeg hier known issues toe -->

Op dit moment zijn er geen known issues bekend.

## Afspraken en verantwoordelijkheden

<!-- Voeg hier een overzicht van verantwoordelijken en afspraken omtrent de
interactive toe -->

### Contact

<!-- Interne contactpersonen -->

### SLA

<!-- SLAs (met link naar Topdesk) -->

### Bouwer

<!-- Naam bouwer interactives (met link naar Topdesk)-->

### Ontwerper

<!-- Naam ontwerper interactives (met link naar Topdesk)-->

## Bijlagen

{{< pagelist bijlagen >}}
